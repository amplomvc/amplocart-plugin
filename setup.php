<?php

/**
 * The AmploCart Plugin
 *
 * Version: 0.8.1
 * Name: amplocart
 * Title: AmploCart
 * Description: A fully functional Cart for AmploMVC
 * Author: Daniel Newman
 * Date: 3/15/2013
 * Link: http://www.amplocart.com
 *
 */
class Plugin_Amplocart_Setup extends Plugin_Setup
{
	private $links = array(
		'cart'                               => array(
			'display_name' => 'Cart',
			'sort_order'   => 3,
			'children'     => array(
				'cart_attributes'   => array(
					'display_name' => 'Attributes',
					'path'         => 'admin/cart/attribute',
				),
				'cart_options'      => array(
					'display_name' => 'Options',
					'path'         => 'admin/cart/option',
				),
				'cart_categories'   => array(
					'display_name' => 'Categories',
					'path'         => 'admin/category',
				),
				'cart_products'     => array(
					'display_name' => 'Products',
					'path'         => 'admin/product',
				),
				'cart_manufacturer' => array(
					'display_name' => 'Manufacturers',
					'path'         => 'admin/cart/manufacturer',
				),
				'cart_downloads'    => array(
					'display_name' => 'Downloads',
					'path'         => 'admin/cart/download',
				),
				'cart_reviews'      => array(
					'display_name' => 'Reviews',
					'path'         => 'admin/cart/review',
				),

				'cart_extensions'                         => array(
					'display_name' => 'Extensions',
					'sort_order'   => 10,
					'children'     => array(
						'cart_extensions_order_totals'  => array(
							'display_name' => 'Order Totals',
							'path'         => 'admin/extension/total',
						),
						'cart_extensions_shipping'      => array(
							'display_name' => 'Shipping',
							'path'         => 'admin/extension/shipping',
						),
					),
				),
			),
		),

		'sales'                              => array(
			'display_name' => 'Sales',
			'sort_order'   => 4,
			'children'     => array(
				'sales_coupons'       => array(
					'display_name' => 'Coupons',
					'path'         => 'admin/sale/coupon',
				),
				'sales_customers'     => array(
					'display_name' => 'Customers',
					'children'     => array(
						'sales_customers_customers'       => array(
							'display_name' => 'Customers',
							'path'         => 'admin/sale/customer',
						),
						'sales_customers_customer_groups' => array(
							'display_name' => 'Customer Groups',
							'path'         => 'admin/sale/customer_group',
						),
						'sales_customers_ip_blacklist'    => array(
							'display_name' => 'IP Blacklist',
							'path'         => 'admin/sale/customer_blacklist',
						),
					),
				),
				'sales_orders'        => array(
					'display_name' => 'Orders',
					'path'         => 'admin/sale/order',
				),
				'sales_gift_vouchers' => array(
					'display_name' => 'Gift Vouchers',
					'children'     => array(
						'sales_gift_vouchers_voucher_themes' => array(
							'display_name' => 'Voucher Themes',
							'path'         => 'admin/sale/voucher_theme',
						),
						'sales_gift_vouchers_gift_vouchers'  => array(
							'display_name' => 'Gift Vouchers',
							'path'         => 'admin/sale/voucher',
						),
					),
				),
				'sales_returns'       => array(
					'display_name' => 'Returns',
					'path'         => 'admin/sale/return',
				),
			),
		),

		'system_settings_policies'           => array(
			'parent'       => 'system_settings',
			'display_name' => 'Policies',
			'children'     => array(
				'system_settings_policies_shipping_policies' => array(
					'display_name' => 'Shipping Policies',
					'path'         => 'admin/settings/shipping_policy',
				),
				'system_settings_policies_return_policies'   => array(
					'display_name' => 'Return Policies',
					'path'         => 'admin/settings/return_policy',
				),
			),
		),

		'system_localisation_taxes'          => array(
			'parent'       => 'system_localisation',
			'display_name' => 'Taxes',
			'children'     => array(
				'system_localisation_taxes_tax_classes' => array(
					'display_name' => 'Tax Classes',
					'path'         => 'admin/localisation/tax_class',
				),
				'system_localisation_taxes_tax_rates'   => array(
					'display_name' => 'Tax Rates',
					'path'         => 'admin/localisation/tax_rate',
				),
			),
		),
		'system_localisation_stock_statuses' => array(
			'parent'       => 'system_localisation',
			'display_name' => 'Stock Statuses',
			'path'         => 'admin/localisation/stock_status',
		),
	);

	public function install()
	{
		$this->Model_Navigation->saveGroupLinks('admin', $this->links);

		$this->route->registerHook('amplocart', 'amplocart_routing_hook');

		//Install the AmploCart DB
		require_once(DIR_PLUGIN . 'amplocart/install_db.php');
	}

	public function upgrade($from_version)
	{
		if (version_compare($from_version, '0.8', '<=')) {
			$this->db->addColumn('option_value', 'label', "VARCHAR(256) NOT NULL AFTER `option_id`");
		}

		if (version_compare($from_version, '0.8.1', '<=')) {
			if ($this->db->hasTable('product_to_category')) {
				$this->db->query("ALTER TABLE " . DB_PREFIX . "product_to_category RENAME TO " . DB_PREFIX . "product_category");
			}

			$this->db->changeColumn('product_category', 'product_id', 'product_id', "INT(11) UNSIGNED NOT NULL");
			$this->db->changeColumn('product_category', 'category_id', 'category_id', "INT(11) UNSIGNED NOT NULL");
			$this->db->dropTable('category_to_layout');

			$this->db->addColumn('category', 'layout_id', "INT UNSIGNED NOT NULL AFTER `category_id`");
		}
	}

	public function uninstall($keep_data = false)
	{
		$this->Model_Navigation->removeGroupLinks('admin', $this->links);

		$this->route->unregisterHook('amplocart');

		if (!$keep_data) {
			require_once(DIR_PLUGIN . 'amplocart/uninstall_db.php');
		}
	}
}
