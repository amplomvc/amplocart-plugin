<?php
$this->db->dropTable('attribute');

$this->db->dropTable('attribute_group');

$this->db->dropTable('category');

$this->db->dropTable('category_to_layout');

$this->db->dropTable('category_to_store');

$this->db->dropTable('coupon');

$this->db->dropTable('coupon_category');

$this->db->dropTable('coupon_customer');

$this->db->dropTable('coupon_history');

$this->db->dropTable('coupon_product');

$this->db->dropTable('customer_reward');

$this->db->dropTable('manufacturer');

$this->db->dropTable('manufacturer_description');

$this->db->dropTable('manufacturer_to_store');

$this->db->dropTable('option');

$this->db->dropTable('option_value');

$this->db->dropTable('order');

$this->db->dropTable('order_download');

$this->db->dropTable('order_fraud');

$this->db->dropTable('order_history');

$this->db->dropTable('order_option');

$this->db->dropTable('order_product');

$this->db->dropTable('order_product_meta');

$this->db->dropTable('order_total');

$this->db->dropTable('product');

$this->db->dropTable('product_attribute');

$this->db->dropTable('product_discount');

$this->db->dropTable('product_image');

$this->db->dropTable('product_option');

$this->db->dropTable('product_option_value');

$this->db->dropTable('product_option_value_restriction');

$this->db->dropTable('product_related');

$this->db->dropTable('product_reward');

$this->db->dropTable('product_special');

$this->db->dropTable('product_tag');

$this->db->dropTable('product_template');

$this->db->dropTable('product_category');

$this->db->dropTable('product_to_download');

$this->db->dropTable('product_views');

$this->db->dropTable('return');

$this->db->dropTable('return_history');

$this->db->dropTable('review');

$this->db->dropTable('shipping');

$this->db->dropTable('shipping_history');

$this->db->dropTable('stock_status');

$this->db->dropTable('tax_class');

$this->db->dropTable('tax_rate');

$this->db->dropTable('tax_rate_to_customer_group');

$this->db->dropTable('tax_rule');

$this->db->dropTable('transaction');

$this->db->dropTable('transaction_history');

$this->db->dropTable('voucher');

$this->db->dropTable('voucher_history');
