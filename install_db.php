<?php
$this->db->createTable('attribute', <<<SQL
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_id`)
SQL
);

$this->db->createTable('attribute_group', <<<SQL
  `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE latin1_general_ci NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_group_id`)
SQL
);

$this->db->createTable('category', <<<SQL
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` text COLLATE latin1_general_ci,
  `meta_description` text COLLATE latin1_general_ci,
  `meta_keywords` text COLLATE latin1_general_ci,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `__image_sort__image` float DEFAULT NULL,
  PRIMARY KEY (`category_id`)
SQL
);

$this->db->createTable('category_to_layout', <<<SQL
  `category_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`)
SQL
);

$this->db->createTable('coupon', <<<SQL
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `code` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `auto_apply` tinyint(3) unsigned NOT NULL,
  `type` char(1) COLLATE latin1_general_ci NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) COLLATE latin1_general_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_geozone` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupon_id`)
SQL
);

$this->db->createTable('coupon_category', <<<SQL
  `coupon_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`coupon_id`,`category_id`)
SQL
);

$this->db->createTable('coupon_customer', <<<SQL
  `coupon_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`coupon_id`,`customer_id`)
SQL
);

$this->db->createTable('coupon_history', <<<SQL
  `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`coupon_history_id`)
SQL
);

$this->db->createTable('coupon_product', <<<SQL
  `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`coupon_product_id`)
SQL
);

$this->db->createTable('customer_reward', <<<SQL
  `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE latin1_general_ci NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`customer_reward_id`)
SQL
);

$this->db->createTable('extension', <<<SQL
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `settings` text,
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `status` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`extension_id`)
SQL
);

$this->db->createTable('manufacturer', <<<SQL
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `image` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  `featured_product_id` int(11) DEFAULT NULL,
  `keyword` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `section_attr` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `vendor_id` varchar(45) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `date_active` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editable` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`manufacturer_id`)
SQL
);

$this->db->createTable('manufacturer_description', <<<SQL
  `manufacturer_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `description` text COLLATE latin1_general_ci NOT NULL,
  `shipping_return` text COLLATE latin1_general_ci,
  `teaser` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`manufacturer_id`,`language_id`)
SQL
);

$this->db->createTable('option', <<<SQL
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `display_name` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `type` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `group_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'single',
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_id`)
SQL
);

$this->db->createTable('option_value', <<<SQL
  `option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `value` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `display_value` varchar(256) COLLATE latin1_general_ci NOT NULL,
  `info` text COLLATE latin1_general_ci NOT NULL,
  `image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_value_id`)
SQL
);

$this->db->createTable('order', <<<SQL
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `transaction_id` int(10) unsigned NOT NULL,
  `shipping_id` int(10) unsigned NOT NULL,
  `comment` text COLLATE latin1_general_ci NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_code` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `currency_value` decimal(15,8) NOT NULL,
  `ip` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `forwarded_ip` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `user_agent` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `accept_language` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`order_id`)
SQL
);

$this->db->createTable('order_download', <<<SQL
  `order_download_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `filename` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `mask` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `remaining` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_download_id`)
SQL
);

$this->db->createTable('order_fraud', <<<SQL
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `countryMatch` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `countryCode` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `highRiskCountry` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `distance` int(11) NOT NULL,
  `ip_region` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `ip_city` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `ip_latitude` decimal(10,6) NOT NULL,
  `ip_longitude` decimal(10,6) NOT NULL,
  `ip_isp` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `ip_org` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `ip_asnum` int(11) NOT NULL,
  `ip_userType` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `ip_countryConf` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `ip_regionConf` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `ip_cityConf` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `ip_postalConf` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `ip_postalCode` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `ip_accuracyRadius` int(11) NOT NULL,
  `ip_netSpeedCell` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `ip_metroCode` int(3) NOT NULL,
  `ip_areaCode` int(3) NOT NULL,
  `ip_timeZone` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `ip_regionName` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `ip_domain` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `ip_countryName` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `ip_continentCode` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `ip_corporateProxy` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `anonymousProxy` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `proxyScore` int(3) NOT NULL,
  `isTransProxy` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `freeMail` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `carderEmail` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `highRiskUsername` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `highRiskPassword` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `binMatch` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `binCountry` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `binNameMatch` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `binName` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `binPhoneMatch` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `binPhone` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `custPhoneInBillingLoc` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `shipForward` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `cityPostalMatch` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `shipCityPostalMatch` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `score` decimal(10,5) NOT NULL,
  `explanation` text COLLATE latin1_general_ci NOT NULL,
  `riskScore` decimal(10,5) NOT NULL,
  `queriesRemaining` int(11) NOT NULL,
  `maxmindID` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `err` text COLLATE latin1_general_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`order_id`)
SQL
);

$this->db->createTable('order_history', <<<SQL
  `order_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(5) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text COLLATE latin1_general_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`order_history_id`)
SQL
);

$this->db->createTable('order_option', <<<SQL
  `order_option_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `order_product_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `product_option_id` int(11) unsigned NOT NULL,
  `product_option_value_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`order_option_id`)
SQL
);

$this->db->createTable('order_product', <<<SQL
  `order_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_class` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `cost` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL,
  `return_policy_id` int(10) unsigned NOT NULL DEFAULT '0',
  `shipping_policy_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_product_id`)
SQL
);

$this->db->createTable('order_product_meta', <<<SQL
  `order_product_meta_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `order_product_id` int(10) unsigned NOT NULL,
  `key` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `value` text COLLATE latin1_general_ci NOT NULL,
  `serialized` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`order_product_meta_id`)
SQL
);

$this->db->createTable('order_total', <<<SQL
  `order_total_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `method_id` varchar(64) COLLATE latin1_general_ci NOT NULL,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`order_total_id`)
SQL
);

$this->db->createTable('product', <<<SQL
  `product_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_class` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `layout_id` int(11) unsigned NOT NULL,
  `model` varchar(64) COLLATE latin1_general_ci NOT NULL,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` text COLLATE latin1_general_ci NOT NULL,
  `meta_description` varchar(511) COLLATE latin1_general_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `teaser` text COLLATE latin1_general_ci NOT NULL,
  `information` text COLLATE latin1_general_ci NOT NULL,
  `shipping_policy_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sku` varchar(64) COLLATE latin1_general_ci NOT NULL,
  `upc` varchar(12) COLLATE latin1_general_ci NOT NULL,
  `location` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` datetime NOT NULL,
  `date_expires` datetime DEFAULT '0000-00-00 00:00:00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_unit` VARCHAR(5) NOT NULL DEFAULT '',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_unit` VARCHAR(5) NOT NULL DEFAULT '',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `cost` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `return_policy_id` int(10) unsigned NOT NULL DEFAULT '0',
  `editable` int(10) unsigned NOT NULL DEFAULT '1',
  `__image_sort__image` float DEFAULT NULL,
  PRIMARY KEY (`product_id`,`date_available`)
SQL
);

$this->db->createTable('product_attribute', <<<SQL
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `text` text COLLATE latin1_general_ci NOT NULL,
  `image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `sort_order` int(10) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`attribute_id`)
SQL
);

$this->db->createTable('product_discount', <<<SQL
  `product_discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`product_discount_id`)
SQL
);

$this->db->createTable('product_image', <<<SQL
  `product_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_image_id`)
SQL
);

$this->db->createTable('product_option', <<<SQL
  `product_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `display_name` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `type` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `group_type` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `required` tinyint(1) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_option_id`)
SQL
);

$this->db->createTable('product_option_value', <<<SQL
  `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `value` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `display_value` varchar(256) COLLATE latin1_general_ci NOT NULL,
  `info` text COLLATE latin1_general_ci NOT NULL,
  `image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `cost` decimal(15,4) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `points` int(8) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_option_value_id`)
SQL
);

$this->db->createTable('product_option_value_restriction', <<<SQL
  `product_id` int(10) unsigned NOT NULL,
  `product_option_value_id` int(10) unsigned NOT NULL,
  `restrict_product_option_value_id` int(10) unsigned NOT NULL,
  `quantity` int(10) NOT NULL,
  PRIMARY KEY (`product_id`,`product_option_value_id`,`restrict_product_option_value_id`)
SQL
);

$this->db->createTable('product_related', <<<SQL
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`related_id`)
SQL
);

$this->db->createTable('product_reward', <<<SQL
  `product_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_reward_id`)
SQL
);

$this->db->createTable('product_special', <<<SQL
  `product_special_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `flashsale_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_special_id`)
SQL
);

$this->db->createTable('product_tag', <<<SQL
  `tag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`tag_id`,`product_id`)
SQL
);

$this->db->createTable('product_template', <<<SQL
  `product_id` int(10) unsigned NOT NULL,
  `template` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `theme` varchar(45) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  PRIMARY KEY (`product_id`,`theme`)
SQL
);

$this->db->createTable('product_category', <<<SQL
  `product_id` int(11) UNSIGNED NOT NULL,
  `category_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`)
SQL
);

$this->db->createTable('product_to_download', <<<SQL
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`download_id`)
SQL
);

$this->db->createTable('product_views', <<<SQL
  `product_view_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `session_id` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`product_view_id`)
SQL
);

$this->db->createTable('return', <<<SQL
  `return_id` int(11) NOT NULL AUTO_INCREMENT,
  `rma` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text COLLATE latin1_general_ci,
  `date_ordered` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`return_id`)
SQL
);

$this->db->createTable('return_history', <<<SQL
  `return_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text COLLATE latin1_general_ci NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`return_history_id`)
SQL
);

$this->db->createTable('review', <<<SQL
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `text` text COLLATE latin1_general_ci NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`review_id`)
SQL
);

$this->db->createTable('shipping', <<<SQL
  `shipping_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_code` varchar(127) NOT NULL,
  `shipping_key` varchar(255) NOT NULL,
  `tracking` varchar(45) NOT NULL,
  `address_id` int(10) unsigned NOT NULL,
  `status` varchar(45) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`shipping_id`)
SQL
);

$this->db->createTable('shipping_history', <<<SQL
  `shipping_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_id` int(10) unsigned NOT NULL,
  `type` varchar(45) NOT NULL,
  `comment` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`shipping_history_id`)
SQL
);

$this->db->dropTable('stock_status');

$this->db->createTable('stock_status', <<<SQL
  `stock_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`stock_status_id`,`language_id`)
SQL
);

$this->db->query("INSERT INTO `" . SITE_PREFIX . "stock_status` VALUES (5,1,'Out Of Stock'),(6,1,'2 - 3 Days'),(7,1,'In Stock'),(8,1,'Pre-Order')");

$this->db->dropTable('tax_class');

$this->db->createTable('tax_class', <<<SQL
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tax_class_id`)
SQL
);
$this->db->query("INSERT INTO `" . SITE_PREFIX . "tax_class` VALUES (9,'Taxable Goods','Taxed Stuff','2009-01-06 23:21:53','2011-09-23 14:07:50'),(10,'Downloadable Products','Downloadable','2011-09-21 22:19:39','2011-09-22 10:27:36'),(11,'CA Sales Tax','California Sales Tax For Shipping to California','2012-08-30 15:29:39','0000-00-00 00:00:00'),(12,'Standard','Ashley Ashoff Store Sales Tax','2013-02-11 16:04:46','0000-00-00 00:00:00')");

$this->db->dropTable('tax_rate');

$this->db->createTable('tax_rate', <<<SQL
  `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) COLLATE latin1_general_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tax_rate_id`)
SQL
);
$this->db->query("INSERT INTO `" . SITE_PREFIX . "tax_rate` VALUES (86,3,'VAT (17.5%)','17.5000','P','2011-03-09 21:17:10','2011-09-22 22:24:29'),(87,3,'Eco Tax (-2.00)','2.0000','F','2011-09-21 21:49:23','2011-09-23 00:40:19'),(88,6,'CA Tax','7.2500','P','2012-08-30 15:26:33','2012-08-31 19:17:38')");

$this->db->dropTable('tax_rate_to_customer_group');

$this->db->createTable('tax_rate_to_customer_group', <<<SQL
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  PRIMARY KEY (`tax_rate_id`,`customer_group_id`)
SQL
);

$this->db->query("INSERT INTO `" . SITE_PREFIX . "tax_rate_to_customer_group` VALUES (86,6),(86,8),(87,6),(87,8),(88,6),(88,8)");

$this->db->dropTable('tax_rule');

$this->db->createTable('tax_rule', <<<SQL
  `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`tax_rule_id`)
SQL
);
$this->db->query("INSERT INTO `" . SITE_PREFIX . "tax_rule` VALUES (120,10,87,'store',0),(121,10,86,'payment',1),(127,9,87,'shipping',2),(128,9,86,'shipping',1),(129,11,88,'shipping',1)");

$this->db->createTable('transaction', <<<SQL
  `transaction_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `payment_code` varchar(127) NOT NULL,
  `payment_key` varchar(255) NOT NULL,
  `address_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `retries` int(10) unsigned NOT NULL,
  `status` varchar(45) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`transaction_id`)
SQL
);

$this->db->createTable('transaction_history', <<<SQL
  `transaction_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) unsigned NOT NULL,
  `type` varchar(45) NOT NULL,
  `comment` text NOT NULL,
  `status` varchar(45) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`transaction_history_id`)
SQL
);

$this->db->createTable('voucher', <<<SQL
  `voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `code` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `title` varchar(64) COLLATE latin1_general_ci NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `template` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `data` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`voucher_id`)
SQL
);

$this->db->createTable('voucher_history', <<<SQL
  `voucher_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `voucher_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `message` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`voucher_history_id`)
SQL
);
