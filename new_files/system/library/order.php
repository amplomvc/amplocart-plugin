<?php

class Order Extends Library
{
	const
		STATUS_PENDING = 0,
		STATUS_COMPLETE = 1,
		STATUS_RECEIVED = 2,
		STATUS_RETURNED = 3,
		STATUS_FRAUD = 4,
		STATUS_BLACKLIST = 5;

	static $statuses = array(
		self::STATUS_PENDING   => 'Pending',
		self::STATUS_COMPLETE  => 'Complete',
		self::STATUS_RECEIVED  => 'Received',
		self::STATUS_RETURNED  => 'Returned',
		self::STATUS_FRAUD     => 'Fraud',
		self::STATUS_BLACKLIST => 'Blacklist',
	);

	const
		RETURN_PENDING = 1,
		RETURN_PROCESSING = 2,
		RETURN_COMPLETE = 3;

	static $return_statuses = array(
		self::RETURN_PENDING    => 'Pending',
		self::RETURN_PROCESSING => 'Processing',
		self::RETURN_COMPLETE   => 'Complete',
	);

	const
		RETURN_ACTION_DOA = 1,
		RETURN_ACTION_DESC = 2,
		RETURN_ACTION_OTHER = 3;

	static $return_actions = array(
		self::RETURN_REASON_DOA   => 'Dead on Arrival',
		self::RETURN_REASON_DESC  => 'Not as Described',
		self::RETURN_REASON_OTHER => 'Other',
	);

	const
		RETURN_REASON_DOA = 1,
		RETURN_REASON_DESC = 2,
		RETURN_REASON_OTHER = 3;

	static $return_reasons = array(
		self::RETURN_REASON_DOA   => 'Dead on Arrival',
		self::RETURN_REASON_DESC  => 'Not as Described',
		self::RETURN_REASON_OTHER => 'Other',
	);

	public function add()
	{
		if (!$this->verify()) {
			return false;
		}

		$order = array();

		//Customer Checkout
		if (is_logged()) {
			$order = $this->customer->info();
		} else {
			$order['customer_id']       = 0;
			$order['customer_group_id'] = option('config_customer_group_id');
			$order += $this->cart->loadGuestInfo();
		}

		//Order Information
		$order['store_id']       = option('store_id');
		$order['language_id']    = option('config_language_id');
		$order['currency_code']  = $this->currency->getCode();
		$order['currency_value'] = $this->currency->getValue();

		//Payment info
		$transaction = array(
			'description'  => _l('Order payment'),
			'amount'       => $this->cart->getTotal(),
			'payment_code' => $this->cart->getPaymentCode(),
			'payment_key'  => $this->cart->getPaymentKey(),
			'address_id'   => $this->cart->getPaymentAddressId(),
		);

		$order['transaction_id'] = $this->transaction->add('order', $transaction);

		//Shipping info
		if ($this->cart->hasShipping()) {
			$shipping = array(
				'shipping_code' => $this->cart->getShippingCode(),
				'shipping_key'  => $this->cart->getShippingKey(),
				'tracking'      => '',
				'address_id'    => $this->cart->getShippingAddressId(),
			);

			$order['shipping_id'] = $this->shipping->add('order', $shipping);
		}

		//Totals
		$order['total']  = $this->cart->getTotal();
		$order['totals'] = $this->cart->getTotals();

		//Products
		$cart_products = $this->cart->getProducts();

		foreach ($cart_products as &$cart_product) {
			$cart_product['tax'] = $this->tax->getTax($cart_product['total'], $cart_product['product']['tax_class_id']);
		}
		unset($product);

		$order['products'] = $cart_products;

		//Comments
		$order['comment'] = $this->cart->getComment();

		//Client Location / Browser Info
		$order['ip'] = $_SERVER['REMOTE_ADDR'];

		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$order['forwarded_ip'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$order['forwarded_ip'] = $_SERVER['HTTP_CLIENT_IP'];
		}

		if (isset($_SERVER['HTTP_USER_AGENT'])) {
			$order['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		}

		if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			$order['accept_language'] = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
		}

		//Call Total Extensions
		$total_extensions = $this->System_Extension_Total->getActive();

		foreach ($total_extensions as $extension) {
			if (method_exists($extension, 'addOrder')) {
				$extension->addOrder($order);
			}
		}

		$order_id = $this->Model_Sale_Order->save(null, $order);

		if (!$order_id) {
			$this->error = $this->Model_Sale_Order->getError();
			return false;
		}

		$this->session->set('order_id', $order_id);

		return $order_id;
	}

	public function setPaymentMethod($order_id, $payment_code, $payment_key = null)
	{
		$order = $this->get($order_id);

		$data = array(
			'payment_code' => $payment_code,
			'payment_key'  => $payment_key,
		);

		$result = $this->transaction->edit($order['transaction_id'], $data);

		if (!$result) {
			$this->error = $this->transaction->getError();
		}

		return $result;
	}

	public function hasOrder()
	{
		return $this->session->has('order_id');
	}

	public function getId()
	{
		return $this->session->get('order_id');
	}

	public function get($order_id = null)
	{
		if (!$order_id && !($order_id = $this->getId())) {
			return null;
		}

		return $this->Model_Sale_Order->getOrder($order_id);
	}

	public function getProducts($order_id)
	{
		return $this->Model_Sale_Order->getOrderProducts($order_id);
	}

	public function synchronizeOrders($customer_id, $email)
	{
		if (!$customer_id || !$email) {
			return;
		}

		$where = array(
			'customer_id' => 0,
			'email '      => $email,
		);

		$this->update('order', array('customer_id' => $customer_id), $where);
	}

	public function countOrdersWithStatus($order_status_id)
	{
		$filter = array(
			'order_status_ids' => array($order_status_id),
		);

		return $this->Model_Sale_Order->getTotalRecords($filter);
	}

	public function orderStatusInUse($order_status_id)
	{
		$filter = array(
			'order_status_ids' => array($order_status_id),
		);

		$order_total = $this->Model_Sale_Order->getTotalRecords($filter);

		if (!$order_total) {
			$order_total = $this->Model_Sale_Order->getTotalOrderHistories($filter);
		}

		return $order_total > 0;
	}

	public function productInConfirmedOrder($product_id)
	{
		$filter = array(
			'product_ids' => array($product_id),
			'confirmed'   => 1,
		);

		return $this->Model_Sale_Order->getTotalRecords($filter);
	}

	public function isEditable($order)
	{
		if (!is_array($order)) {
			$order = $this->get($order);
		}

		return strtotime($order['date_added']) > strtotime('-' . (int)option('config_order_edit') . ' day');
	}

	public function getOrderStatus($order_status_id)
	{
		return isset(self::$statuses[$order_status_id]) ? self::$statuses[$order_status_id] : null;
	}

	public function getReturnStatus($return_status_id)
	{
		return isset(self::$return_statuses[$return_status_id]) ? self::$return_statuses[$return_status_id] : null;
	}

	public function getReturnReason($return_reason_id)
	{
		return isset(self::$return_reasons[$return_reason_id]) ? self::$return_reasons[$return_reason_id] : null;
	}

	public function getReturnAction($return_action_id)
	{
		return isset(self::$return_actions[$return_action_id]) ? self::$return_actions[$return_action_id] : null;
	}

	public function getPaymentAddress($order_id)
	{
		$query = "SELECT * FROM {$this->t['address']} a" .
			" JOIN {$this->t['transaction']} t ON (t.address_id = a.address_id)" .
			" JOIN {$this->t['order']} o ON (o.transaction_id = t.transaction_id)" .
			" WHERE o.order_id = " . (int)$order_id . " LIMIT 1";

		return $this->queryRow($query);
	}

	public function getShippingAddress($order_id)
	{
		$query = "SELECT * FROM {$this->t['address']} a" .
			" JOIN {$this->t['shipping']} s ON (s.address_id = a.address_id)" .
			" JOIN {$this->t['order']} o ON (o.shipping_id = s.shipping_id)" .
			" WHERE o.order_id = " . (int)$order_id . " LIMIT 1";

		return $this->queryRow($query);
	}

	/**
	 * Update or Confirm (when order status is complete) an order
	 *
	 * @param $order_id - The ID of the order to update
	 * @param $order_status_id - The status to update the order to (use Order::$statuses for a list of valid statuses)
	 * @param $comment - A comment about the change in order status
	 * @param $notify - Notify the customer about the change in their order status
	 *
	 */

	public function updateOrder($order_id, $order_status_id, $comment = '', $notify = false)
	{
		$order = $this->get($order_id);

		//order does not exist or has already been processed
		if (!$order || $order['order_status_id'] === $order_status_id) {
			$this->error['status'] = _l("The status was unchanged.");
			return false;
		}

		$data = array(
			'order_status_id' => $order_status_id,
			'date_modified'   => $this->date->now(),
		);

		$this->update('order', $data, $order_id);

		$history_data = array(
			'order_status_id' => $order_status_id,
			'comment'         => $comment,
			'notify'          => $notify,
		);

		$this->addHistory($order_id, $history_data);

		if ($notify) {
			call('mail/order_update_notify', $comment, $order_status_id, $order);
		}

		return true;
	}

	public function confirm($order_id, $comment = '', $notify = true)
	{
		$order = $this->get($order_id);

		if (!$order) {
			$this->error['order_id'] = _l("Unable to locate order to confirm.");
			return false;
		}

		if (!empty($order['confirmed'])) {
			$this->error['confirmed'] = _l("The order has already been confirmed");
			return false;
		}

		// Fraud Detection
		if (option('config_fraud_detection') && $this->fraud->atRisk($order)) {
			$this->updateOrder($order_id, Order::STATUS_FRAUD, _l("Order has been flagged as potentially fraudulent"));
			return false;
		}

		// Blacklist
		if ($order['customer_id'] && $this->customer->isBlacklisted($order['customer_id'], array($order['ip']))) {
			$this->updateOrder($order_id, Order::STATUS_BLACKLIST, _l("Customer has been blacklisted, and is not allowed to order from this store"));
			return false;
		}

		//Confirm the Transaction. If the transaction failed to make the payment, do not confirm the order.
		if (!$this->transaction->confirm($order['transaction_id'])) {
			$this->error = $this->transaction->getError();
			return false;
		}

		$order['transaction'] = $this->transaction->get($order['transaction_id']);

		//Confirm Shipping
		if (!empty($order['shipping_id'])) {
			$this->shipping->confirm($order['shipping_id']);
			$order['shipping'] = $this->shipping->get($order['shipping_id']);
		}

		//Update Order Status
		$this->updateOrder($order_id, self::STATUS_COMPLETE, _l("Order has been completed."));

		//Confirm Order
		$this->query("UPDATE {$this->t['order']} SET confirmed = 1 WHERE order_id = $order_id");

		// Products
		$order_products = $this->queryRows("SELECT * FROM {$this->t['order_product']} WHERE order_id = $order_id");

		foreach ($order_products as &$product) {
			//subtract Quantity from this product
			$this->query("UPDATE {$this->t['product']} SET quantity = (quantity - " . (int)$product['quantity'] . ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");

			//Subtract Quantities from Product Option Values and Restrictions
			$product_option_values = $this->queryRows("SELECT pov.product_option_value_id, pov.option_value_id FROM {$this->t['order_option']} oo LEFT JOIN {$this->t['product_option_value']} pov ON (pov.product_option_value_id=oo.product_option_value_id) WHERE oo.order_id = '$order_id' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

			$pov_to_ov = array();

			foreach ($product_option_values as $option_value) {
				$pov_to_ov[$option_value['product_option_value_id']] = $option_value['option_value_id'];
			}

			$order_options = $this->Model_Sale_Order->getOrderProductOptions($order_id, $product['order_product_id']);

			foreach ($order_options as $option) {
				$this->query("UPDATE {$this->t['product_option_value']} SET quantity = (quantity - " . (int)$product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");

				$this->query(
					"UPDATE {$this->t['product_option_value_restriction']} SET quantity = (quantity - " . (int)$product['quantity'] . ")" .
					" WHERE product_option_value_id = '" . ($pov_to_ov[$option['product_option_value_id']]) . "' AND restrict_product_option_value_id IN (" . implode(',', $pov_to_ov) . ")"
				);
			}

			//Add Product Options to product data
			$product['options'] = $order_options;

			//We must invalidate the product for each order to keep the product quantities valid!
			clear_cache('product.' . $product['product_id']);
		}
		unset($product);

		$order['order_products'] = $order_products;

		// Downloads
		$order['order_downloads'] = $this->queryRows("SELECT * FROM {$this->t['order_download']} WHERE order_id = $order_id");

		// Order Totals
		$order['order_totals'] = $this->queryRows("SELECT * FROM `{$this->t['order_total']}` WHERE order_id = '$order_id' ORDER BY sort_order ASC");

		//Order Status
		$order['order_status'] = $this->order->getOrderStatus($order['order_status_id']);

		//Call Total Extensions
		$total_extensions = $this->System_Extension_Total->getActive();

		foreach ($total_extensions as $extension) {
			if (method_exists($extension, 'confirm')) {
				$extension->confirm($order);
			}
		}

		//Send Order Emails
		call('mail/order', $order);

		return true;
	}

	public function addHistory($order_id, $data)
	{
		$data['order_id']   = $order_id;
		$data['date_added'] = $this->date->now();

		$this->insert('order_history', $data);
	}

	public function verify()
	{
		if (!$this->cart->validate()) {
			$this->error = $this->cart->getError();
		} elseif (!is_logged() && !option('config_guest_checkout')) {
			//Guest checkout not allowed and customer not logged in
			$this->error['guest_checkout'] = "You must be logged in to complete the checkout process!";
		} else {
			//Validate Shipping Address & Method
			if ($this->cart->hasShipping()) {
				if (!$this->cart->hasShippingAddress()) {
					$this->error['shipping_address'] = _l('You must specify a Delivery Address!');
				} elseif (!$this->cart->hasShippingMethod()) {
					$this->error['shipping_method'] = _l('There was no Delivery Method specified!');
				}
			}

			//Validate Payment Address & Method
			if (!$this->cart->hasPaymentAddress()) {
				$this->error['payment_address'] = _l('You must specify a Billing Address!');
			} elseif (!$this->cart->hasPaymentMethod()) {
				$this->error['payment_method'] = _l('There was no Payment Method specified!');
			}
		}

		return empty($this->error);
	}

	public function clear()
	{
		$this->session->remove('order_id');
	}
}
