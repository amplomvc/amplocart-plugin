<?php

class Payment extends Library
{
	private $method;

	public function setMethod($method)
	{
		$method = 'App_Model_Payment_' . $method;

		$this->method = new $method();
	}

	public function createProfile($profile)
	{
		$this->setMethod($profile['method']);

		$profile_id = $this->method->createProfile(customer_info('customer_id'), $profile);

		if (!$profile_id) {
			$this->error = $this->method->getError();
			return false;
		}

		return $profile_id;
	}

	public function removeProfile($profile_id)
	{
		$profile = $this->getProfile($profile_id);

		if ($profile) {
			$this->setMethod($profile['method']);

			$result = $this->method->removeProfile(customer_info('customer_id'), $profile_id);

			if (!$result) {
				$this->error = $this->method->getError();
			}
		}

		return empty($this->error);
	}

	public function getProfile($profile_id)
	{
		$profiles = $this->getProfiles();
		return isset($profiles[$profile_id]) ? $profiles[$profile_id] : null;
	}

	public function getProfiles()
	{
		return (array)customer_meta('payment_profiles');
	}

	public function format($profile, $customer_id = null)
	{
		if (!is_array($profile)) {
			$profile = $customer_id ? $this->Model_Payment->getProfile($profile) : $this->getProfile($profile);
		}

		$name = '';
		$card = '';
		$address = '';

		if (!empty($profile['address']['name'])) {
			$name = $profile['address']['name'];
		}

		if (!empty($profile['last4'])) {
			$card = (!empty($profile['card_type']) ? $profile['card_type'] . ' -' : 'card') . ' ending in ' . $profile['last4'];
		}

		if (!empty($profile['address'])) {
			$profile['address']['name'] = '';

			$address = format('address', $profile['address']);
		}

		$format = ($name ? $name . '<BR>' : '') . ($card ? $card . '<Br>' : '') . $address;

		return $format;
	}
}
