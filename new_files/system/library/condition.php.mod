#<?php

//=====
class Condition extends Library
{
//.....
	static $conditions = array(
//-----
//>>>>> {php} {before}
		'cart_not_empty' => "The Cart has Products",
		'cart_empty'     => "The Cart is Empty",
//-----
//=====
	);
//-----
//=====
	public function is($condition)
	{
//.....
		switch ($condition) {
//-----
//>>>>> {php}
			case 'cart_not_empty':
				return !$this->cart->isEmpty();

			case 'cart_empty':
				return $this->cart->isEmpty();
//-----
//=====
		}
//.....
	}
//.....
}

//-----
