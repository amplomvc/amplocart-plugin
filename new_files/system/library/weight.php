<?php

class Weight extends Library
{
	static $units = array(
		'kg'  => array(
			'name'   => 'Kilogram',
			'plural' => 'Kilograms',
			'unit'   => 'kg',
			'value'  => 1,
		),
		'g'   => array(
			'name'   => 'Gram',
			'plural' => 'Grams',
			'unit'   => 'g',
			'value'  => 1000,
		),
		'mg'  => array(
			'name'   => 'Milligram',
			'plural' => 'Milligrams',
			'unit'   => 'mg',
			'value'  => 1000000,
		),
		'lbs' => array(
			'name'   => "Pound",
			'plural' => "Pounds",
			'unit'   => 'lbs',
			'value'  => 2.2046226218,
		),
		'oz'  => array(
			'name'   => 'Ounce',
			'plural' => 'Ounces',
			'unit'   => 'oz',
			'value'  => 35.27396195,
		),
	);

	private $default_unit = 'kg';

	public function __construct()
	{
		parent::__construct();

		$this->default_unit = option('config_weight_unit', 'kg');
	}

	public function getUnit($unit)
	{
		return isset(self::$units[$unit]) ? self::$units[$unit] : null;
	}

	public function getUnits()
	{
		return self::$units;
	}

	public function convert($value, $to, $from = null)
	{
		if (!$from) {
			$from = $this->default_unit;
		}

		if ($from === $to) {
			return $value;
		}

		if (!isset($this->units[$from]) || !isset($this->units[$to])) {
			return $value;
		}

		$from = $this->units[$from]['value'];
		$to   = $this->units[$to]['value'];

		return $value * ($to / $from);
	}

	public function format($value, $unit = null, $decimal_point = null, $thousand_point = null)
	{
		if (!$unit || !isset($this->units[$unit])) {
			$unit = $this->default_unit;
		}

		if (!$decimal_point) {
			$decimal_point = $this->language->info('decimal_point');
		}

		if (!$thousand_point) {
			$thousand_point = $this->language->info('thousand_point');
		}

		return number_format($value, 2, $decimal_point, $thousand_point) . $unit;
	}
}
