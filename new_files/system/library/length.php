<?php

class Length extends Library
{
	static $units = array(
		'm'  => array(
			'name'   => 'Meter',
			'plural' => 'Meters',
			'unit'   => 'm',
			'value'  => 1,
		),
		'nm' => array(
			'name'   => 'Nanometer',
			'plural' => 'Nanometers',
			'unit'   => 'nm',
			'value'  => 1000000000,
		),
		'mm' => array(
			'name'   => 'Millimeter',
			'plural' => 'Millimeters',
			'unit'   => 'mm',
			'value'  => 1000,
		),
		'cm' => array(
			'name'   => 'Centimeter',
			'plural' => 'Centimeters',
			'unit'   => 'cm',
			'value'  => 100,
		),
		'km' => array(
			'name'   => 'Kilometer',
			'plural' => 'Kilometers',
			'unit'   => 'km',
			'value'  => .001,
		),
		'in' => array(
			'name'   => 'Inch',
			'plural' => 'Inches',
			'unit'   => 'in',
			'value'  => 39.37007874,
		),
		'ft' => array(
			'name'   => 'Foot',
			'plural' => 'Feet',
			'unit'   => 'ft',
			'value'  => 3.280839895,
		),
		'yd' => array(
			'name'   => 'Yard',
			'plural' => 'Yards',
			'unit'   => 'yd',
			'value'  => 1.0936132983,
		),
		'mi' => array(
			'name'   => 'Mile',
			'plural' => 'Miles',
			'unit'   => 'mi',
			'value'  => 0.00062137119224,
		),
		'NM' => array(
			'name'   => 'Nautical Mile',
			'plural' => 'Nautical Miles',
			'unit'   => 'NM',
			'value'  => 0.00020712331461,
		),
	);

	public function __construct()
	{
		parent::__construct();

		$this->default_unit = option('config_length_unit', 'm');
	}

	public function getUnit($unit)
	{
		return isset(self::$units[$unit]) ? self::$units[$unit] : null;
	}

	public function getUnits()
	{
		return self::$units;
	}

	public function convert($value, $to, $from = null)
	{
		if (!$from) {
			$from = $this->default_unit;
		}

		if ($from === $to) {
			return $value;
		}

		if (!isset($this->units[$from]) || !isset($this->units[$to])) {
			return $value;
		}

		$from = $this->units[$from]['value'];
		$to   = $this->units[$to]['value'];

		return $value * ($to / $from);
	}

	public function format($value, $unit = null, $decimal_point = '.', $thousand_point = ',')
	{
		if (!$unit || !isset($this->units[$unit])) {
			$unit = $this->default_unit;
		}

		return number_format($value, 2, $decimal_point, $thousand_point) . $unit;
	}
}
