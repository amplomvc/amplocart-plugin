<?php

function amplocart_routing_hook(&$path, $segments)
{
	switch ($segments[0]) {
		case 'product':
			if (!empty($_GET['product_id'])) {
				$product_class = $this->Model_Product->getProductClass($_GET['product_id']);
				$path = preg_replace("#^product/product#", 'product/' . $product_class, $this->path);
			}
			break;
	}
}
