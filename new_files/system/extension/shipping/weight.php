<?php

class System_Extension_Shipping_Weight extends System_Extension_Shipping
{
	public function getQuotes($address)
	{
		if (!$this->validate($address)) {
			return array();
		}


		$total_weight = $this->cart->getWeight();
		$cost         = false;
		$label        = '';

		if (empty($this->settings['priceset'])) {
			return array();
		}

		foreach ($this->settings['priceset'] as $set) {
			switch ($set['range']) {
				case 'range':
					$is_valid = $total_weight >= $set['from'] && $total_weight <= $set['to'];
					break;
				case 'lt':
					$is_valid = $total_weight < $set['total'];
					break;
				case 'lte':
					$is_valid = $total_weight <= $set['total'];
					break;
				case 'gt':
					$is_valid = $total_weight > $set['total'];
					break;
				case 'gte':
					$is_valid = $total_weight >= $set['total'];
					break;
				case 'eq':
					$is_valid = $total_weight == $set['total'];
					break;
				default:
					$is_valid = false;
					break;
			}

			if ($is_valid) {
				if ($set['type'] == 'fixed') {
					$cost = $set['cost'];
				} else {
					$cost = ($set['cost'] / 100) * $total_weight;
				}

				$label = $set['label'];
				break;
			}
		}

		if (!empty($this->settings['zonerule'])) {
			$zonerules = $this->settings['zonerule'];
			$orig_cost = $cost;
			foreach ($zonerules as $rule) {
				if ($address['country_id'] != $rule['country_id'] || ($address['zone_id'] != $rule['zone_id'] && $rule['zone_id'] != 0)) {
					continue;
				}
				switch ($rule['mod']) {
					case 'add':
						if ($rule['type'] === 'fixed') {
							$cost += $rule['cost'];
						} else {
							$cost += $orig_cost * ($rule['cost'] / 100);
						}
						break;
					case 'subtract':
						if ($rule['type'] === 'fixed') {
							$cost -= $rule['cost'];
						} else {
							$cost -= $orig_cost * ($rule['cost'] / 100);
						}
						break;
					case 'fixed':
						if ($rule['type'] === 'fixed') {
							$cost = $rule['cost'];
						} else {
							$cost = $orig_cost * ($rule['cost'] / 100);
						}
						//We are done for fixed price (exit foreach loop)
						break 2;
					default:
						break;
				}
			}
		}

		$quote_data = array();

		if ($cost !== false) {
			$key = 'weight-' . $cost;

			$quote_data[$key] = array(
				'shipping_key' => $key,
				'title'        => $label,
				'cost'         => $cost,
			);
		}

		return $quote_data;
	}
}
