<?php

class System_Extension_Total_Voucher extends System_Extension_Total
{
	public function getTotal(&$total_data, &$total, &$taxes)
	{
		$vouchers = $this->cart->getVouchers();

		if ($vouchers) {
			foreach ($vouchers as $code => $voucher_id) {
				$voucher = $this->Model_Sale_Voucher->verifyVoucher($code);

				if ($voucher) {
					$amount = $total > $voucher['remaining'] ? $voucher['remaining'] : $total;

					$total_data['voucher-' . $voucher['voucher_id']] = array(
							'method_id' => $voucher['voucher_id'],
							'title'     => _l("Voucher (%s)", $voucher['code']),
							'amount'    => -$amount,
						) + $this->info();

					$total -= $amount;
				}
			}
		}
	}

	public function calculateProductTotal(&$product)
	{
		if (!empty($product['product']['product_class']) && $product['product']['product_class'] === 'voucher') {
			if (isset($product['meta']['amount'])) {
				$product['price'] = $product['meta']['amount'];
				$product['total'] = $product['price'] * $product['quantity'];
			}
		}
	}

	public function confirm(&$order)
	{
		// Redeem Gift Vouchers
		foreach ($order['order_totals'] as $order_total) {
			if ($order_total['code'] === 'voucher') {
				$this->Model_Sale_Voucher->redeem($order_total['method_id'], $order['order_id'], abs($order_total['amount']));
			}
		}

		//Activate Purchased Gift Vouchers
		$customer = $this->customer->info();

		foreach ($order['order_products'] as $order_product) {
			if ($order_product['product_class'] === 'voucher') {
				$name = $this->Model_Product->getProductName($order_product['product_id']);
				$data = $this->Model_Sale_Order->getOrderProductMeta($order['order_id'], $order_product['order_product_id']);

				if (empty($data['from_email'])) {
					$data['from_email'] = $customer ? $customer['email'] : $order['email'];
				}

				if (empty($data['from_name'])) {
					$data['from_name'] = $customer ? $customer['first_name'] . ' ' . $customer['last_name'] : $order['first_name'] . ' ' . $order['last_name'];
				}

				if (empty($data['to_email'])) {
					$data['to_email'] = $data['from_email'];
				}

				if (empty($data['to_name'])) {
					$data['to_name'] = $data['from_name'];
				}

				$voucher = array(
					'order_id' => $order['order_id'],
					'title'    => $name,
					'amount'   => $order_product['price'],
					'template' => !empty($data['template']) ? $data['template'] : '',
					'data'     => $data,
				);

				$count = 0;
				while ($count++ < $order_product['quantity']) {
					$this->Model_Sale_Voucher->save(0, $voucher);
				}
			}
		}
	}
}
