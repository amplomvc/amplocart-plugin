<?php

class System_Extension_Total_Total extends System_Extension_Total
{
	public function getTotal(&$total_data, &$total, &$taxes)
	{
		$total_data['total'] = array(
			'title'  => _l("Total"),
			'amount' => max(0, $total),
		) + $this->info();
	}
}
