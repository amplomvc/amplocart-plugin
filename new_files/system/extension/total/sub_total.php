<?php

class System_Extension_Total_SubTotal extends System_Extension_Total
{
	public function getTotal(&$total_data, &$total, &$taxes)
	{
		$sub_total = 0;

		foreach ($this->cart->getProducts() as $product) {
			$sub_total += $product['total'];
		}

		$total += $sub_total;

		$total_data['sub_total'] = array(
			'method_id' => 'sub_total',
			'title'     => _l("Sub Total"),
			'amount'    => $sub_total,
		) + $this->info();
	}
}
