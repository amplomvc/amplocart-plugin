<?php

class System_Extension_Total_Coupon extends System_Extension_Total
{
	public function getTotal(&$total_data, &$total, &$taxes)
	{
		$this->Model_Sale_Coupon->loadAutoCoupons();

		$coupons = $this->cart->getCoupons();

		if (!$coupons) {
			return;
		}

		foreach ($coupons as $code => $coupon_id) {
			$coupon = $this->Model_Sale_Coupon->verifyCoupon($code);

			if ($coupon) {
				$coupons[$code] = $coupon;
			} else {
				unset($coupons[$code]);
			}
		}

		foreach ($coupons as $coupon) {
			$sub_total = $this->cart->getSubTotal();

			if ($coupon['type'] === 'F') {
				$discount = min($coupon['discount'], $sub_total);
			} else {
				$discount = $sub_total * ($coupon['discount'] / 100);
			}

			if ($coupon['shipping'] && $this->cart->hasShippingMethod()) {
				$shipping_method = $this->cart->getShippingQuote();

				if (!empty($shipping_method['tax_class_id'])) {
					$tax_rates = $this->tax->getRates($shipping_method['cost'], $shipping_method['tax_class_id']);

					foreach ($tax_rates as $tax_rate) {
						if ($tax_rate['type'] == 'P') {
							$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
						}
					}
				}

				if ($this->cart->hasShippingAddress()) {
					$address = $this->cart->getShippingAddress();
				} else {
					$address = array(
						'zone_id'    => 0,
						'country_id' => 0,
					);
				}

				if ($this->address->inGeoZone($address, $coupon['shipping_geozone'])) {
					$discount += $shipping_method['cost'];
				}
			}

			$total_data['coupon-' . $coupon['code']] = array(
					'method_id' => $coupon['code'],
					'title'     => _l("Coupon (%s)", $coupon['code']),
					'amount'    => -$discount,
				) + $this->info();

			$total -= $discount;
		}

	}

	public function confirm($order)
	{
		foreach ($order['order_totals'] as $total) {
			if ($total['code'] === 'coupon') {
				if (!empty($total['method_id'])) {
					$coupon = $this->Model_Sale_Coupon->getCouponByCode($total['method_id']);

					if ($coupon) {
						$this->Model_Sale_Coupon->redeem($coupon['coupon_id'], $order['order_id'], $order['customer_id'], $total['amount']);
					}
				}
			}
		}
	}
}
