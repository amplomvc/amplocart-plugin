<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>

	<div class="box">
		<form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form">

			<div class="heading">
				<h1>
					<img src="<?= theme_url('image/customer.png'); ?>" alt=""/> {{Customer}}</h1>

				<div class="buttons">
					<button class="button">{{Save}}</button>
					<a href="<?= $cancel; ?>" class="button">{{Cancel}}</a>
				</div>
			</div>

			<div class="section">
				<div id="htabs" class="htabs">
					<a href="#tab-general">{{General}}</a>
					<? if ($customer_id) { ?>
						<a href="#tab-transaction">{{Transactions}}</a>
						<a href="#tab-reward">{{Reward Points}}</a>
					<? } ?>
					<a href="#tab-ip">{{IP Addresses}}</a>
				</div>

				<div id="tab-general">
					<div id="vtabs" class="vtabs">
						<a href="#tab-customer">{{General}}</a>
						<? $address_row = 1; ?>
						<? foreach ($addresses as $address) { ?>
							<a href="#tab-address-<?= $address_row; ?>"
								id="address-<?= $address_row; ?>"><?= _l("Address") . ' ' . $address_row; ?>&nbsp;
								<img
									src="<?= theme_url('image/add.png') ?>" alt="" onclick="$('#vtabs a:first').trigger('click'); $('#address-<?= $address_row; ?>').remove(); $('#tab-address-<?= $address_row; ?>').remove(); return false;"/>
							</a>
							<? $address_row++; ?>
						<? } ?>
						<span id="address-add">{{Add Address}}
							&nbsp;
							<img src="<?= theme_url('image/delete.png'); ?>" alt = "" onclick = "addAddress();" /></span ></div >
					<div id = "tab-customer" class="vtabs-content" >
						<table class="form" >
							<tr >
								<td class="required" > {{First Name:}}</td>
								<td><input type=" text
							" name="first_name" value="<?= $first_name; ?>"/>
							<? if (_l("First Name must be between 1 and 32 characters!")) { ?>
							<span class="error">{{First Name must be between 1 and 32 characters!}}</span>
							<? } ?></td>
							</tr>
							<tr>
								<td class="required"> {{Last Name:}}</td>
								<td><input type="text" name="last_name" value="<?= $last_name; ?>"/>
									<? if (_l("Last Name must be between 1 and 32 characters!")) { ?>
										<span class="error">{{Last Name must be between 1 and 32 characters!}}</span>
									<? } ?></td>
							</tr>
							<tr>
								<td class="required"> {{E-Mail:}}</td>
								<td><input type="text" name="email" value="<?= $email; ?>"/>
									<? if (_l("E-Mail Address does not appear to be valid!")) { ?>
										<span class="error">{{E-Mail Address does not appear to be valid!}}</span>
									<? } ?></td>
							</tr>
							<tr>
								<td class="required"> {{Telephone:}}</td>
								<td><input type="text" name="phone" value="<?= $phone; ?>"/>
									<? if (_l("Telephone must be between 3 and 32 characters!")) { ?>
										<span class="error">{{Telephone must be between 3 and 32 characters!}}</span>
									<? } ?></td>
							</tr>
							<tr>
								<td>{{Fax:}}</td>
								<td><input type="text" name="fax" value="<?= $fax; ?>"/></td>
							</tr>
							<tr>
								<td>{{Password:}}</td>
								<td><input type="password" autocomplete="off" name="password" value="<?= $password; ?>"/>
									<br/>
									<? if (_l("Password must be between 4 and 20 characters!")) { ?>
										<span class="error">{{Password must be between 4 and 20 characters!}}</span>
									<? } ?></td>
							</tr>
							<tr>
								<td>{{Confirm:}}</td>
								<td><input type="password" autocomplete="off" name="confirm" value="<?= $confirm; ?>"/>
									<? if (_l("Password and password confirmation do not match!")) { ?>
										<span class="error">{{Password and password confirmation do not match!}}</span>
									<? } ?></td>
							</tr>
							<tr>
								<td>{{Newsletter:}}</td>
								<td>
									<select name="newsletter">
										<? if ($newsletter) { ?>
											<option value="1" selected="selected">{{Enabled}}</option>
											<option value="0">{{Disabled}}</option>
										<? } else { ?>
											<option value="1">{{Enabled}}</option>
											<option value="0" selected="selected">{{Disabled}}</option>
										<? } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>{{Customer Group:}}</td>
								<td>
									<select name="customer_group_id">
										<? foreach ($customer_groups as $customer_group) { ?>
											<? if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
												<option value="<?= $customer_group['customer_group_id']; ?>"
													selected="selected"><?= $customer_group['name']; ?></option>
											<? } else { ?>
												<option value="<?= $customer_group['customer_group_id']; ?>"><?= $customer_group['name']; ?></option>
											<? } ?>
										<? } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>{{Status:}}</td>
								<td>
									<select name="status">
										<? if ($status) { ?>
											<option value="1" selected="selected">{{Enabled}}</option>
											<option value="0">{{Disabled}}</option>
										<? } else { ?>
											<option value="1">{{Enabled}}</option>
											<option value="0" selected="selected">{{Disabled}}</option>
										<? } ?>
									</select>
								</td>
							</tr>
							</table>
					</div>
					<? $address_row = 1; ?>
					<? foreach ($addresses as $address) { ?>
						<div id="tab-address-<?= $address_row; ?>" class="vtabs-content">
							<input type="hidden" name="address[<?= $address_row; ?>][address_id]" value="<?= $address['address_id']; ?>"/>
							<table class="form">
								<tr>
									<td class="required"> {{First Name:}}</td>
									<td>
										<input type="text" name="address[<?= $address_row; ?>][first_name]" value="<?= $address['first_name']; ?>"/>
										<? if (isset($error_address_first_name[$address_row])) { ?>
											<span class="error"><?= $error_address_first_name[$address_row]; ?></span>
										<? } ?></td>
								</tr>
								<tr>
									<td class="required"> {{Last Name:}}</td>
									<td>
										<input type="text" name="address[<?= $address_row; ?>][last_name]" value="<?= $address['last_name']; ?>"/>
										<? if (isset($error_address_last_name[$address_row])) { ?>
											<span class="error"><?= $error_address_last_name[$address_row]; ?></span>
										<? } ?></td>
								</tr>
								<tr>
									<td>{{Company:}}</td>
									<td>
										<input type="text" name="address[<?= $address_row; ?>][company]" value="<?= $address['company']; ?>"/>
									</td>
								</tr>
								<tr>
									<td class="required"> {{Address 1:}}</td>
									<td>
										<input type="text" name="address[<?= $address_row; ?>][address]" value="<?= $address['address']; ?>"/>
										<? if (isset($error_address_address[$address_row])) { ?>
											<span class="error"><?= $error_address_address[$address_row]; ?></span>
										<? } ?></td>
								</tr>
								<tr>
									<td>{{Address 2:}}</td>
									<td>
										<input type="text" name="address[<?= $address_row; ?>][address_2]" value="<?= $address['address_2']; ?>"/>
									</td>
								</tr>
								<tr>
									<td class="required"> {{City:}}</td>
									<td>
										<input type="text" name="address[<?= $address_row; ?>][city]" value="<?= $address['city']; ?>"/>
										<? if (isset($error_address_city[$address_row])) { ?>
											<span class="error"><?= $error_address_city[$address_row]; ?></span>
										<? } ?></td>
								</tr>
								<tr>
									<td class="required"> {{Postcode:}}</td>
									<td>
										<input type="text" name="address[<?= $address_row; ?>][postcode]" value="<?= $address['postcode']; ?>"/>
									</td>
								</tr>
								<tr>
									<td class="required"> {{Country:}}</td>
									<td>
										<?=
										build(array(
											'type' => 'select',
											'name'  => "address[$address_row][country_id]",
											'data'   => $countries,
											'select' => $address['country_id'],
											'value' => 'country_id',
											'label' => 'name',
										)); ?>
										<? if (isset($error_address_country[$address_row])) { ?>
											<span class="error"><?= $error_address_country[$address_row]; ?></span>
										<? } ?>
									</td>
								</tr>
								<tr>
									<td class="required"> {{Region / State:}}</td>
									<td>
										<select name="address[<?= $address_row; ?>][zone_id]" data-zone_id="<?= $address['zone_id']; ?>"
											class="zone_select"></select>
										<? if (isset($error_address_zone[$address_row])) { ?>
											<span class="error"><?= $error_address_zone[$address_row]; ?></span>
										<? } ?></td>
								</tr>
								<tr>
									<td>{{Default Address:}}</td>
									<td><? if (($address['address_id'] == $address_id) || !$addresses) { ?>
										<input type="radio" name="address[<?= $address_row; ?>][default]" value="<?= $address_row; ?>"
											checked="checked"/></td>
									<? } else { ?>
										<input type="radio" name="address[<?= $address_row; ?>][default]" value="<?= $address_row; ?>"/>
										</td>
									<? } ?>
								</tr>
							</table>
						</div>
						<? $address_row++; ?>
					<? } ?>
				</div>
				<? if ($customer_id) { ?>
					<div id="tab-transaction">
						<table class="form">
							<tr>
								<td>{{Description:}}</td>
								<td><input type="text" name="description" value=""/></td>
							</tr>
							<tr>
								<td>{{Amount:}}</td>
								<td><input type="text" name="amount" value=""/></td>
							</tr>
							<tr>
								<td colspan="2" style="text-align: right;">
									<a id="button-reward" class="button" onclick="addTransaction();">
										<span>{{Add Transaction}}</span>
									</a>
								</td>
							</tr>
						</table>
						<div id="transaction"></div>
					</div>
					<div id="tab-reward">
						<table class="form">
							<tr>
								<td>{{Description:}}</td>
								<td><input type="text" name="description" value=""/></td>
							</tr>
							<tr>
								<td><?= _l("Points:<br /><span class=\"help\">Use minus to remove points</span>"); ?></td>
								<td><input type="text" name="points" value=""/></td>
							</tr>
							<tr>
								<td colspan="2" style="text-align: right;">
									<a id="button-reward" class="button" onclick="addRewardPoints();">
										<span>{{Add Reward Points}}</span>
									</a>
								</td>
							</tr>
						</table>
						<div id="reward"></div>
					</div>
				<? } ?>
				<div id="tab-ip">
					<table class="list">
						<thead>
						<tr>
							<td class="left">{{IP}}</td>
							<td class="right">{{Total Accounts}}</td>
							<td class="left">{{Date Added}}</td>
							<td class="right">{{Action}}</td>
						</tr>
						</thead>
						<tbody>
						<? if ($ips) { ?>
							<? foreach ($ips as $ip) { ?>
								<tr>
									<td class="left">
										<a onclick="window.open('http://www.geoiptool.com/en/?IP=<?= $ip['ip']; ?>');"><?= $ip['ip']; ?></a>
									</td>
									<td class="right">
										<a onclick="window.open('<?= $ip['filter_ip']; ?>');"><?= $ip['total']; ?></a>
									</td>
									<td class="left"><?= $ip['date_added']; ?></td>
									<td class="right"><? if ($ip['blacklist']) { ?>
											<b>[</b>
											<a id="<?= str_replace('.', '-', $ip['ip']); ?>" onclick="removeBlacklist('<?= $ip['ip']; ?>');">{{Remove Blacklist}}</a>
											<b>]</b>
										<? } else { ?>
											<b>[</b>
											<a id="<?= str_replace('.', '-', $ip['ip']); ?>" onclick="addBlacklist('<?= $ip['ip']; ?>');">{{Add Blacklist}}</a>
											<b>]</b>
										<? } ?></td>
								</tr>
							<? } ?>
						<? } else { ?>
							<tr>
								<td class="center" colspan="3">{{No results!}}</td>
							</tr>
						<? } ?>
						</tbody>
					</table>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	var address_row = <?= $address_row; ?>;

	function addAddress() {
		html = '<div id="tab-address-' + address_row + '" class="vtabs-content" style="display: none;">';
		html += '	<input type="hidden" name="address[' + address_row + '][address_id]" value="" />';
		html += '	<table class="form">';
		html += '		<tr>';
		html += '		<td>{{First Name:}}</td>';
		html += '		<td><input type="text" name="address[' + address_row + '][first_name]" value="" /></td>';
		html += '		</tr>';
		html += '		<tr>';
		html += '			<td>{{Last Name:}}</td>';
		html += '			<td><input type="text" name="address[' + address_row + '][last_name]" value="" /></td>';
		html += '		</tr>';
		html += '		<tr>';
		html += '			<td>{{Company:}}</td>';
		html += '			<td><input type="text" name="address[' + address_row + '][company]" value="" /></td>';
		html += '		</tr>';
		html += '		<tr>';
		html += '			<td>{{Address 1:}}</td>';
		html += '			<td><input type="text" name="address[' + address_row + '][address]" value="" /></td>';
		html += '		</tr>';
		html += '		<tr>';
		html += '			<td>{{Address 2:}}</td>';
		html += '			<td><input type="text" name="address[' + address_row + '][address_2]" value="" /></td>';
		html += '		</tr>';
		html += '		<tr>';
		html += '			<td>{{City:}}</td>';
		html += '			<td><input type="text" name="address[' + address_row + '][city]" value="" /></td>';
		html += '		</tr>';
		html += '		<tr>';
		html += '			<td>{{Postcode:}}</td>';
		html += '			<td><input type="text" name="address[' + address_row + '][postcode]" value="" /></td>';
		html += '		</tr>';
		html += '			<td>{{Country:}}</td>';
		html += '			<td><select name="address[' + address_row + '][country_id]" class="country_select">';
		html += '				<option value="">{{ --- Please Select --- }}</option>';
		<? foreach ($countries as $country) { ?>
		html += '				<option value="<?= $country['country_id']; ?>"><?= addslashes($country['name']); ?></option>';
		<? } ?>
		html += '			</select></td>';
		html += '		</tr>';
		html += '		<tr>';
		html += '			<td>{{Region / State:}}</td>';
		html += '			<td><select name="address[' + address_row + '][zone_id]" class="zone_select"><option value="false">{{ --- None --- }}</option></select></td>';
		html += '		</tr>';
		html += '		<tr>';
		html += '			<td>{{Default Address:}}</td>';
		html += '			<td><input type="radio" name="address[' + address_row + '][default]" value="1" /></td>';
		html += '		</tr>';
		html += '	</table>';
		html += '</div>';

		$('#tab-general').append(html);

		$('#address-add').before('<a href="#tab-address-' + address_row + '" id="address-' + address_row + '">{{Address}} ' + address_row + '&nbsp;<img src="<?= theme_url('image/loading.gif'); ?>" alt="" onclick="$(\'#vtabs a:first\').trigger(\'click\'); $(\'#address-' + address_row + '\').remove(); $(\'#tab-address-' + address_row + '\').remove(); return false;" /></a>');

		$('.vtabs a').tabs();

		$('#address-' + address_row).trigger('click');

		address_row++;
	}
</script>

<script type="text/javascript">
	$('.table.form .zone_select').ac_zoneselect({listen: '.table.form .country_select'});

	$('#transaction .pagination a').live('click', function () {
		$('#transaction').load(this.href);

		return false;
	});

	$('#transaction').load("<?= $url_transaction; ?>");

	function addTransaction() {
		$.ajax({
			url:      "<?= $url_transaction; ?>",
			type:     'post',
			dataType: 'html',
			data:     'description=" + encodeURIComponent($("#tab-transaction input[name=\'description\']').val()
	)
		+'&amount=" + encodeURIComponent($("#tab-transaction input[name=\'amount\']'
	).
		val()
	),
		beforeSend: function () {
			$('.success, .warning').remove();
			$('#button-transaction').attr('disabled', true);
			$('#transaction').before('<div class="attention"><img src="<?= theme_url('image/loading.gif'); ?>" alt="" /> {{Please Wait!}}</div>');
		}
	,
		complete: function () {
			$('#button-transaction').attr('disabled', false);
			$('.attention').remove();
		}
	,
		success: function (html) {
			$('#transaction').html(html);

			$('#tab-transaction input[name=\'amount\']').val('');
			$('#tab-transaction input[name=\'description\']').val('');
		}
	}
	)
	;
	}
</script>
<script type="text/javascript">
	$('#reward .pagination a').live('click', function () {
		$('#reward').load(this.href);

		return false;
	});

	$('#reward').load("<?= $url_reward; ?>");

	function addRewardPoints() {
		$.ajax({
			url:      "<?= $url_reward; ?>",
			type:     'post',
			dataType: 'html',
			data:     'description=" + encodeURIComponent($("#tab-reward input[name=\'description\']').val()
	)
		+'&points=" + encodeURIComponent($("#tab-reward input[name=\'points\']'
	).
		val()
	),
		beforeSend: function () {
			$('.success, .warning').remove();
			$('#button-reward').attr('disabled', true);
			$('#reward').before('<div class="attention"><img src="<?= URL_theme_url('image/loading.gif'); ?>" alt="" /> {{Please Wait!}}</div>');
		}
	,
		complete: function () {
			$('#button-reward').attr('disabled', false);
			$('.attention').remove();
		}
	,
		success: function (html) {
			$('#reward').html(html);

			$('#tab-reward input[name=\'points\']').val('');
			$('#tab-reward input[name=\'description\']').val('');
		}
	}
	)
	;
	}

	function addBlacklist(ip) {
		$.ajax({
			url:        "<?= $url_blacklist; ?>",
			type:       'post',
				dataType
		:
			'json',
				data
		:
			'ip=" + encodeURIComponent(ip),
			beforeSend: function () {
				$(".success, .warning').remove();

				$('.box').before('<div class="attention"><img src="<?= URL_Ttheme_url('image/loading.gif'); ?>" alt="" /> Please wait!</div>');
			},
			complete:   function () {
				$('.attention').remove();
			},
			success:    function (json) {
				if (json['error']) {
					$('.box').before('<div class="message warning" style="display: none;">' + json['error'] + '</div>');

					$('.warning').fadeIn('slow');
				}

				if (json['success']) {
					$('.box').before('<div class="message success" style="display: none;">' + json['success'] + '</div>');

					$('.success').fadeIn('slow');

					$('#' + ip.replace(/\./g, '-')).replaceWith('<a id="' + ip.replace(/\./g, '-') + '" onclick="removeBlacklist(\'' + ip + '\');">{{Remove Blacklist}}</a>');
			}
		}
	}
	)
	;
	}

	function removeBlacklist(ip) {
		$.ajax({
			url:        "<?= $url_remove_blacklist; ?>",
			type:       'post',
			dataType:   'json',
			data: 'ip=" + encodeURIComponent(ip),
			beforeSend: function () {
				$(".success, .warning').remove();

				$('.box').before('<div class="attention"><img src="<?= theme_url('image/loading.gif'); ?>" alt="" /> Please wait!</div>');
			},
			complete:   function () {
				$('.attention').remove();
			},
			success:    function (json) {
				if (json['error']) {
					$('.box').before('<div class="message warning" style="display: none;">' + json['error'] + '</div>');

					$('.warning').fadeIn('slow');
				}

				if (json['success']) {
					$('.box').before('<div class="message success" style="display: none;">' + json['success'] + '</div>');

					$('.success').fadeIn('slow');

					$('#' + ip.replace(/\./g, '-')).replaceWith('<a id="' + ip.replace(/\./g, '-') + '" onclick="addBlacklist(\'' + ip + '\');">{{Add Blacklist}}</a>');
				}
			}
		});
	}
	;
</script>
<script type="text/javascript">
	$('.htabs a').tabs();
	$('.vtabs a').tabs();
</script>
<?= $is_ajax ? '' : call('admin/footer'); ?>
