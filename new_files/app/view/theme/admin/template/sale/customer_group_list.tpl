<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<? if ($error_warning) { ?>
		<div class="message warning"><?= $error_warning; ?></div>
	<? } ?>
	<? if ($success) { ?>
		<div class="message success"><?= $success; ?></div>
	<? } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/customer.png'); ?>" alt=""/> {{Customer Group}}</h1>

			<div class="buttons"><a onclick="location = '<?= $insert; ?>'" class="button">{{Insert}}</a><a onclick="$('form').submit();" class="button">{{Delete}}</a></div>
		</div>
		<div class="section">
			<form action="<?= $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="list">
					<thead>
						<tr>
							<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);"/>
							</td>
							<td class="left"><? if ($sort == 'name') { ?>
									<a href="<?= $sort_name; ?>" class="<?= strtolower($order); ?>">{{Customer Group Name}}</a>
								<? } else { ?>
									<a href="<?= $sort_name; ?>">{{Customer Group Name}}</a>
								<? } ?></td>
							<td class="right">{{Action}}</td>
						</tr>
					</thead>
					<tbody>
						<? if ($customer_groups) { ?>
							<? foreach ($customer_groups as $customer_group) { ?>
								<tr>
									<td style="text-align: center;"><? if ($customer_group['selected']) { ?>
											<input type="checkbox" name="batch[]" value="<?= $customer_group['customer_group_id']; ?>" checked="checked"/>
										<? } else { ?>
											<input type="checkbox" name="batch[]" value="<?= $customer_group['customer_group_id']; ?>"/>
										<? } ?></td>
									<td class="left"><?= $customer_group['name']; ?></td>
									<td class="right"><? foreach ($customer_group['action'] as $action) { ?>
											[ <a href="<?= $action['href']; ?>"><?= $action['text']; ?></a> ]
										<? } ?></td>
								</tr>
							<? } ?>
						<? } else { ?>
							<tr>
								<td class="center" colspan="3">{{There are no results to display.}}</td>
							</tr>
						<? } ?>
					</tbody>
				</table>
			</form>
			<div class="pagination"><?= $pagination; ?></div>
		</div>
	</div>
</div>
<?= $is_ajax ? '' : call('admin/footer'); ?>
