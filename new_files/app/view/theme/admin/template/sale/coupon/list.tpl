<?= $is_ajax ? '' : call('admin/header'); ?>
	<div class="section">
		<?= $is_ajax ? '' : breadcrumbs(); ?>
		<div class="box">
			<div class="heading">
				<h1><img src="<?= theme_url('image/setting.png'); ?>" alt=""/> {{Coupons}}</h1>

				<? if (user_can('modify', 'scopes')) { ?>
					<div class="buttons">
						<a href="<?= $insert; ?>" class="button">{{Insert}}</a>
					</div>
				<? } ?>
			</div>
			<div class="section">
				<?= block('widget/views', null, array('path' => 'admin/sale/coupon/listing', 'group' => 'coupons')); ?>
			</div>
		</div>
	</div>

<?= $is_ajax ? '' : call('admin/footer'); ?>
