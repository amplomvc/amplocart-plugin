<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>

	<form action="<?= $save; ?>" method="post" enctype="multipart/form-data" class="box">
		<div class="heading">
			<h1>
				<img src="<?= theme_url('image/customer.png'); ?>" alt=""/> {{Coupon}}</h1>

			<div class="buttons">
				<button>{{Save}}</button>
				<a href="<?= site_url('admin/sale/coupon'); ?>" class="button">{{Cancel}}</a>
			</div>
		</div>
		<div class="section">
			<div id="tabs" class="htabs">
				<a href="#tab-general">{{General}}</a>
			</div>

			<div id="tab-general">
				<table class="form">
					<tr>
						<td class="required"> {{Coupon Name:}}</td>
						<td><input name="name" value="<?= $name; ?>"/></td>
					</tr>
					<tr>
						<td class="required"> <?= _l("Code:<br /><span class=\"help\">The code the customer enters to get the discount</span>"); ?></td>
						<td><input type="text" name="code" value="<?= $code; ?>"/></td>
					</tr>
					<tr>
						<td>
							{{Apply Coupon Automatically}}
							<div class="help">{{Automatically apply this coupon for the customer when their cart meets the requirements}}</div>
						</td>
						<td>
							<?=
							build(array(
								'type' => 'radio',
								'name'  => 'auto_apply',
								'data'   => $data_yes_no,
								'select' => $auto_apply,
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("Type:<br /><span class=\"help\">Percentage or Fixed Amount</span>"); ?></td>
						<td><?=
							build(array(
								'type' => 'radio',
								'name'  => 'type',
								'data'   => $data_types,
								'select' => $type
							)); ?>
						</td>
					</tr>
					<tr>
						<td>{{Discount:}}</td>
						<td><input type="text" name="discount" value="<?= $discount; ?>"/></td>
					</tr>
					<tr>
						<td><?= _l("Total Amount:<br /><span class=\"help\">The total amount that must reached before the coupon is valid.</span>"); ?></td>
						<td><input type="text" name="total" value="<?= $total; ?>"/></td>
					</tr>
					<tr>
						<td><?= _l("Customer Login:<br /><span class=\"help\">Customer must be logged in to use the coupon.</span>"); ?></td>
						<td><?=
							build(array(
								'type' => 'radio',
								'name'  => 'logged',
								'data'   => $data_yes_no,
								'select' => $logged
							)); ?>
						</td>
					</tr>
					<tr>
						<td>{{Free Shipping:}}</td>
						<td>
							<div>
								<?=
								build(array(
									'type' => 'radio',
									'name'    => 'shipping',
									'data'     => $data_yes_no,
									'select'   => $shipping,
									'#onclick' => "if($(this).find(':checked').val() == '1')$('#coupon_ship_geozone').show(); else $('#coupon_ship_geozone').hide();",
								)); ?>
							</div>
							<div <?= (int)$shipping ? '' : "style=\"display:none\""; ?> id="coupon_ship_geozone">
								<?=
								build(array(
									'type' => 'select',
									'name'  => 'shipping_geozone',
									'data'   => $data_geo_zones,
									'select' => (int)$shipping_geozone,
									'value' => 'geo_zone_id',
									'label' => 'name',
								)); ?>
							</div>
						</td>
					</tr>
					<? /*<tr>
						<td>{{Customers:}}</td>
						<td>
							<? foreach ($coupon_customers as $cc) { ?>
								<div><?= $cc['last_name'] . ', ' . $cc['first_name'] . ' - ' . $cc['email']; ?></div>
								<input type="hidden" name="coupon_customers[]" value="<?= $cc['customer_id']; ?>"/>
							<? } ?>
						</td>
					</tr> */ ?>
					<tr>
						<td>
							{{Categories:}}
							<div class="help">{{Choose all products under selected category.}}</div>
						</td>
						<td>
							<?=
							build(array(
								'type' => 'multiselect',
								'name'  => 'categories',
								'data'   => $data_categories,
								'select' => $categories,
								'value' => 'category_id',
								'label' => 'name',
							)); ?>
						</td>
					</tr>
					<? /*<tr>
						<td><?= _l("Products:<br /><span class=\"help\">Choose specific products the coupon will apply to. Select no products to apply coupon to entire cart.</span>"); ?></td>
						<td><input type="text" name="product" value=""/></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<div id="coupon-product" class="scrollbox">
								<? foreach ($coupon_products as $coupon_product) { ?>
									<div id="coupon-product<?= $coupon_product['product_id']; ?>">
										<?= $coupon_product['name']; ?>
										<img src="<?= theme_url('image/delete.png'); ?>"/>
										<input type="hidden" name="coupon_products[]" value="<?= $coupon_product['product_id']; ?>"/>
									</div>
								<? } ?>
							</div>
						</td>
					</tr>*/ ?>
					<tr>
						<td>{{Date Start:}}</td>
						<td>
							<input type="text" name="date_start" class="datetimepicker" value="<?= format('date', $date_start); ?>" size="12" id="date-start"/>
						</td>
					</tr>
					<tr>
						<td>{{Date End:}}</td>
						<td>
							<input type="text" name="date_end" class="datetimepicker" value="<?= format('date', $date_end); ?>" size="12" id="date-end"/>
						</td>
					</tr>
					<tr>
						<td><?= _l("Uses Per Coupon:<br /><span class=\"help\">The maximum number of times the coupon can be used by any customer. Leave blank for unlimited</span>"); ?></td>
						<td><input type="text" name="uses_total" value="<?= $uses_total; ?>"/></td>
					</tr>
					<tr>
						<td><?= _l("Uses Per Customer:<br /><span class=\"help\">The maximum number of times the coupon can be used by a single customer. Leave blank for unlimited</span>"); ?></td>
						<td><input type="text" name="uses_customer" value="<?= $uses_customer; ?>"/></td>
					</tr>
					<tr>
						<td>{{Status:}}</td>
						<td><?=
							build(array(
								'type' => 'radio',
								'name'  => 'status',
								'data'   => $data_statuses,
								'select' => $status
							)); ?>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	$.ac_datepicker();

	$('#tabs a').tabs();
</script>

<?= $is_ajax ? '' : call('admin/footer'); ?>
