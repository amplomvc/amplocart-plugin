<?= $is_ajax ? '' : call('admin/header'); ?>
	<div class="section">
		<?= $is_ajax ? '' : breadcrumbs(); ?>
		<? if ($error_warning) { ?>
			<div class="message warning"><?= $error_warning; ?></div>
		<? } ?>
		<div class="box">
			<div class="heading">
				<h1><img src="<?= theme_url('image/customer.png'); ?>" alt=""/> {{Customer IP Blacklist}}</h1>

				<div class="buttons"><a onclick="$('#form').submit();" class="button">{{Save}}</a><a
						href="<?= $cancel; ?>" class="button">{{Cancel}}</a></div>
			</div>
			<div class="section">
				<form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form">
					<table class="form">
						<tr>
							<td class="required"> {{IP:}}</td>
							<td><input type="text" name="ip" value="<?= $ip; ?>"/>
								<? if (_l("IP must be between 1 and 15 characters!")) { ?>
									<span class="error">{{IP must be between 1 and 15 characters!}}</span>
								<? } ?></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
<?= $is_ajax ? '' : call('admin/footer'); ?>
