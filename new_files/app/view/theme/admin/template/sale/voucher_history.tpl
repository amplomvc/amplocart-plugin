<table class="list">
	<thead>
		<tr>
			<td class="right"><b>{{Order Id}}</b></td>
			<td class="left"><b>{{Customer}}</b></td>
			<td class="right"><b>{{Amount}}</b></td>
			<td class="left"><b>{{Date Added}}</b></td>
		</tr>
	</thead>
	<tbody>
		<? if ($histories) { ?>
			<? foreach ($histories as $history) { ?>
				<tr>
					<td class="right"><?= $history['order_id']; ?></td>
					<td class="left"><?= $history['customer']; ?></td>
					<td class="right"><?= $history['amount']; ?></td>
					<td class="left"><?= $history['date_added']; ?></td>
				</tr>
			<? } ?>
		<? } else { ?>
			<tr>
				<td class="center" colspan="4">{{There are no results to display.}}</td>
			</tr>
		<? } ?>
	</tbody>
</table>
<div class="pagination"><?= $pagination; ?></div>
