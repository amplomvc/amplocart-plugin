<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<? if (_l("Warning: Please check the form carefully for errors!")) { ?>
		<div class="message warning">{{Warning: Please check the form carefully for errors!}}</div>
	<? } ?>
	<? if ($success) { ?>
		<div class="message success"><?= $success; ?></div>
	<? } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/order.png'); ?>" alt=""/> {{Product Returns}}</h1>

			<div class="buttons"><a onclick="location = '<?= $insert; ?>'" class="button">{{Insert}}</a><a onclick="$('form').submit();" class="button">{{Delete}}</a></div>
		</div>
		<div class="section">
			<form action="<?= $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="list">
					<thead>
						<tr>
							<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);"/>
							</td>
							<td class="right"><? if ($sort == 'r.return_id') { ?>
									<a href="<?= $sort_return_id; ?>"
										class="<?= strtolower($order); ?>">{{Return ID}}</a>
								<? } else { ?>
									<a href="<?= $sort_return_id; ?>">{{Return ID}}</a>
								<? } ?></td>
							<td class="right"><? if ($sort == 'r.order_id') { ?>
									<a href="<?= $sort_order_id; ?>" class="<?= strtolower($order); ?>">{{Order ID}}</a>
								<? } else { ?>
									<a href="<?= $sort_order_id; ?>">{{Order ID}}</a>
								<? } ?></td>
							<td class="left"><? if ($sort == 'customer') { ?>
									<a href="<?= $sort_customer; ?>" class="<?= strtolower($order); ?>">{{Customer}}</a>
								<? } else { ?>
									<a href="<?= $sort_customer; ?>">{{Customer}}</a>
								<? } ?></td>
							<td class="left"><? if ($sort == 'r.product') { ?>
									<a href="<?= $sort_product; ?>" class="<?= strtolower($order); ?>">{{Product}}</a>
								<? } else { ?>
									<a href="<?= $sort_product; ?>">{{Product}}</a>
								<? } ?></td>
							<td class="left"><? if ($sort == 'r.model') { ?>
									<a href="<?= $sort_model; ?>" class="<?= strtolower($order); ?>">{{Model}}</a>
								<? } else { ?>
									<a href="<?= $sort_model; ?>">{{Model}}</a>
								<? } ?></td>
							<td class="left"><? if ($sort == 'status') { ?>
									<a href="<?= $sort_status; ?>" class="<?= strtolower($order); ?>">{{Status}}</a>
								<? } else { ?>
									<a href="<?= $sort_status; ?>">{{Status}}</a>
								<? } ?></td>
							<td class="left"><? if ($sort == 'r.date_added') { ?>
									<a href="<?= $sort_date_added; ?>"
										class="<?= strtolower($order); ?>">{{Date Added}}</a>
								<? } else { ?>
									<a href="<?= $sort_date_added; ?>">{{Date Added}}</a>
								<? } ?></td>
							<td class="left"><? if ($sort == 'r.date_modified') { ?>
									<a href="<?= $sort_date_modified; ?>"
										class="<?= strtolower($order); ?>">{{Date Modified}}</a>
								<? } else { ?>
									<a href="<?= $sort_date_modified; ?>">{{Date Modified}}</a>
								<? } ?></td>
							<td class="right">{{Action}}</td>
						</tr>
					</thead>
					<tbody>
						<tr class="filter">
							<td></td>
							<td align="right"><input type="text" name="filter_return_id" value="<?= $filter_return_id; ?>"
									size="4" style="text-align: right;"/></td>
							<td align="right"><input type="text" name="filter_order_id" value="<?= $filter_order_id; ?>" size="4"
									style="text-align: right;"/></td>
							<td><input type="text" name="filter_customer" value="<?= $filter_customer; ?>"/></td>
							<td><input type="text" name="filter_product" value="<?= $filter_product; ?>"/></td>
							<td><input type="text" name="filter_model" value="<?= $filter_model; ?>"/></td>
							<td>
								<?= build(array(
									'type' => 'select',
									'name'  => 'filter_return_status_id',
									'data'   => $data_return_statuses,
									'select' => $filter_return_status_id,
									'value' => false,
									'label' => 'title',
								)); ?>
							</td>
							<td><input type="text" name="filter_date_added" value="<?= $filter_date_added; ?>" size="12"
									class="datepicker"/></td>
							<td><input type="text" name="filter_date_modified" value="<?= $filter_date_modified; ?>" size="12"
									class="datepicker"/></td>
							<td align="right"><a onclick="filter();" class="button">{{Filter}}</a></td>
						</tr>
						<? if ($returns) { ?>
							<? foreach ($returns as $return) { ?>
								<tr>
									<td style="text-align: center;"><? if ($return['selected']) { ?>
											<input type="checkbox" name="batch[]" value="<?= $return['return_id']; ?>"
												checked="checked"/>
										<? } else { ?>
											<input type="checkbox" name="batch[]" value="<?= $return['return_id']; ?>"/>
										<? } ?></td>
									<td class="right"><?= $return['return_id']; ?></td>
									<td class="right"><?= $return['order_id']; ?></td>
									<td class="left"><?= $return['customer']; ?></td>
									<td class="left"><?= $return['product']; ?></td>
									<td class="left"><?= $return['model']; ?></td>
									<td class="left"><?= $return['status']; ?></td>
									<td class="left"><?= $return['date_added']; ?></td>
									<td class="left"><?= $return['date_modified']; ?></td>
									<td class="right"><? foreach ($return['action'] as $action) { ?>
											[ <a href="<?= $action['href']; ?>"><?= $action['text']; ?></a> ]
										<? } ?></td>
								</tr>
							<? } ?>
						<? } else { ?>
							<tr>
								<td class="center" colspan="10">{{There are no results to display.}}</td>
							</tr>
						<? } ?>
					</tbody>
				</table>
			</form>
			<div class="pagination"><?= $pagination; ?></div>
		</div>
	</div>
</div>
<script type="text/javascript">
		function filter() {
			url = "<?= URL_SITE . "admin/index.php?route=sale/return"; ?>";

			var filter_return_id = $('input[name=\'filter_return_id\']').attr('value');

			if (filter_return_id) {
				url += '&filter_return_id=" + encodeURIComponent(filter_return_id);
			}

			var filter_order_id = $("input[name=\'filter_order_id\']').attr('value');

			if (filter_order_id) {
				url += '&filter_order_id=" + encodeURIComponent(filter_order_id);
			}

			var filter_customer = $("input[name=\'filter_customer\']').attr('value');

			if (filter_customer) {
				url += '&filter_customer=" + encodeURIComponent(filter_customer);
			}

			var filter_product = $("input[name=\'filter_product\']').attr('value');

			if (filter_product) {
				url += '&filter_product=" + encodeURIComponent(filter_product);
			}

			var filter_model = $("input[name=\'filter_model\']').attr('value');

			if (filter_model) {
				url += '&filter_model=" + encodeURIComponent(filter_model);
			}

			var filter_return_status_id = $("select[name=\'filter_return_status_id\']').attr('value');

			if (filter_return_status_id != '*') {
				url += '&filter_return_status_id=" + encodeURIComponent(filter_return_status_id);
			}

			var filter_date_added = $("input[name=\'filter_date_added\']').attr('value');

			if (filter_date_added) {
				url += '&filter_date_added=" + encodeURIComponent(filter_date_added);
			}

			var filter_date_modified = $("input[name=\'filter_date_modified\']').attr('value');

			if (filter_date_modified) {
				url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
			}

			location = url;
		}
</script>
<script type="text/javascript">
		$.widget('custom.catcomplete', $.ui.autocomplete, {
			_renderMenu: function (ul, items) {
				var self = this, currentCategory = '';

				$.each(items, function (index, item) {
					if (item.category != currentCategory) {
						ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');

						currentCategory = item.category;
					}

					self._renderItem(ul, item);
				});
			}
		});

	$('input[name=\'filter_customer\']').catcomplete({
		delay: 0,
		source: function (request, response) {
			$.ajax({
				url: "<?= URL_SITE . "admin/index.php?route=sale/customer/autocomplete"; ?>" + '&filter_name=" + encodeURIComponent(request.term),
				dataType: "json',
				success: function (json) {
					response($.map(json, function (item) {
						return {
							category: item.customer_group,
							label: item.name,
							value: item.customer_id
						}
					}));
				}
			});
		},
		select: function (event, ui) {
			$('input[name=\'filter_customer\']').val(ui.item.label);

			return false;
		}
	});
</script>
<script type="text/javascript">
		$(document).ready(function () {
			$('.date').datepicker({dateFormat: 'yy-mm-dd'});
		});
</script>
<?= $is_ajax ? '' : call('admin/footer'); ?>
