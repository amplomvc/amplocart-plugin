<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/customer.png'); ?>" alt=""/> {{Product Returns}}</h1>

			<div class="buttons"><a href="<?= $cancel; ?>" class="button">{{Cancel}}</a></div>
		</div>
		<div class="section">
			<div class="vtabs"><a href="#tab-return"><?= $tab_return; ?></a><a
					href="#tab-product"><?= $tab_product; ?></a><a href="#tab-history"><?= $tab_return_history; ?></a>
			</div>
			<div id="tab-return" class="vtabs-content">
				<table class="form">
					<tr>
						<td>{{Return ID:}}</td>
						<td><?= $return_id; ?></td>
					</tr>
					<? if ($order) { ?>
						<tr>
							<td>{{Order ID:}}</td>
							<td><a href="<?= $order; ?>"><?= $order_id; ?></a></td>
						</tr>
					<? } else { ?>
						<tr>
							<td>{{Order ID:}}</td>
							<td><?= $order_id; ?></td>
						</tr>
					<? } ?>
					<tr>
						<td>{{Order Date:}}</td>
						<td><?= $date_ordered; ?></td>
					</tr>
					<? if ($customer) { ?>
						<tr>
							<td>{{Customer:}}</td>
							<td><a href="<?= $customer; ?>"><?= $first_name; ?> <?= $last_name; ?></a></td>
						</tr>
					<? } else { ?>
						<tr>
							<td>{{Customer:}}</td>
							<td><?= $first_name; ?> <?= $last_name; ?></td>
						</tr>
					<? } ?>
					<tr>
						<td>{{E-Mail:}}</td>
						<td><?= $email; ?></td>
					</tr>
					<tr>
						<td>{{Telephone:}}</td>
						<td><?= $phone; ?></td>
					</tr>
					<? if ($return_status) { ?>
						<tr>
							<td>{{Return Status:}}</td>
							<td id="return-status"><?= $return_status; ?></td>
						</tr>
					<? } ?>
					<tr>
						<td>{{Date Added:}}</td>
						<td><?= $date_added; ?></td>
					</tr>
					<tr>
						<td>{{Date Modified:}}</td>
						<td><?= $date_modified; ?></td>
					</tr>
				</table>
			</div>
			<div id="tab-product" class="vtabs-content">
				<table class="form">
					<tr>
						<td>{{Product:}}</td>
						<td><?= $subscription; ?></td>
					</tr>
					<tr>
						<td>{{Model:}}</td>
						<td><?= $model; ?></td>
					</tr>
					<tr>
						<td>{{Quantity:}}</td>
						<td><?= $quantity; ?></td>
					</tr>
					<tr>
						<td>{{Return Reason:}}</td>
						<td><?= $return_reason; ?></td>
					</tr>
					<tr>
						<td>{{Opened}}</td>
						<td><?= $opened; ?></td>
					</tr>
					<tr>
						<td>{{Return Action:}}</td>
						<td><select name="return_action_id">
								<option value="0"></option>
								<? foreach ($return_actions as $return_action) { ?>
									<? if ($return_action['return_action_id'] == $return_action_id) { ?>
										<option value="<?= $return_action['return_action_id']; ?>"
											selected="selected"><?= $return_action['name']; ?></option>
									<? } else { ?>
										<option value="<?= $return_action['return_action_id']; ?>"><?= $return_action['name']; ?></option>
									<? } ?>
								<? } ?>
							</select></td>
					</tr>
					<? if ($comment) { ?>
						<tr>
							<td>{{Comment:}}</td>
							<td><?= $comment; ?></td>
						</tr>
					<? } ?>
				</table>
			</div>
			<div id="tab-history" class="vtabs-content">
				<div id="history"></div>
				<table class="form">
					<tr>
						<td>{{Return Status:}}</td>
						<td>
						<td><?= build(array(
	'type' => 'select',
	'name'  => 'return_status_id',
	'data'   => $data_return_statuses,
	'select' => $return_status_id
)); ?></td>
					</tr>
					<tr>
						<td>{{Notify Customer:}}</td>
						<td><input type="checkbox" name="notify" value="1"/></td>
					</tr>
					<tr>
						<td>{{Comment:}}</td>
						<td><textarea name="comment" cols="40" rows="8" style="width: 99%"></textarea>

							<div style="margin-top: 10px; text-align: right;"><a onclick="history();" id="button-history"
									class="button">{{Add History}}</a>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
		$('select[name=\'return_action_id\']').bind('change', function () {
			$.ajax({
				url: "<?= URL_SITE . "admin/index.php?route=sale/return/action"; ?>" + '&return_id=<?= $return_id; ?>',
				type: 'post',
				dataType: 'json',
				data: 'return_action_id=" + this.value,
				beforeSend: function () {
					$(".success, .warning, .attention').remove();

					$('.box').before('<div class="attention"><img src="<?= theme_url('image/loading.gif') ?>" alt="" /> {{Please Wait!}}</div>');
				},
				success: function (json) {
					$('.success, .warning, .attention').remove();

					if (json['error']) {
						$('.box').before('<div class="message warning" style="display: none;">' + json['error'] + '</div>');

						$('.warning').fadeIn('slow');
					}

					if (json['success']) {
						$('.box').before('<div class="message success" style="display: none;">' + json['success'] + '</div>');

						$('.success').fadeIn('slow');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});

	$('#history .pagination a').live('click', function () {
		$('#history').load(this.href);

		return false;
	});

	$('#history').load("<?= URL_SITE . "admin/index.php?route=sale/return/history"; ?>" + '&return_id=<?= $return_id; ?>');

	function history() {
		$.ajax({
			url: "<?= URL_SITE . "admin/index.php?route=sale/return/history"; ?>" + '&return_id=<?= $return_id; ?>',
			type: 'post',
			dataType: 'html',
			data: 'return_status_id=" + encodeURIComponent($("select[name=\'return_status_id\']').val()
	)
		+'&notify=" + encodeURIComponent($("input[name=\'notify\']'
	).
		attr('checked') ? 1 : 0
	)
		+'&append=" + encodeURIComponent($("input[name=\'append\']'
	).
		attr('checked') ? 1 : 0
	)
		+'&comment=" + encodeURIComponent($("textarea[name=\'comment\']'
	).
		val()
	),
		beforeSend: function () {
			$('.success, .warning').remove();
			$('#button-history').attr('disabled', true);
			$('#history').prepend('<div class="attention"><img src="<?= theme_url('image/loading.gif'); ?>" alt="" /> {{Please Wait!}}</div>');
		}
	,
		complete: function () {
			$('#button-history').attr('disabled', false);
			$('.attention').remove();
		}
	,
		success: function (html) {
			$('#history').html(html);

			$('textarea[name=\'comment\']').val('');

			$('#return-status').html($('select[name=\'return_status_id\'] option:selected').text());
		}
	}
	)
	;
	}
</script>
<script type="text/javascript">
		$('.vtabs a').tabs();
</script>
<?= $is_ajax ? '' : call('admin/footer'); ?>
