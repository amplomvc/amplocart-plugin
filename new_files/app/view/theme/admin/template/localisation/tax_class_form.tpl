<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<? if ($error_warning) { ?>
		<div class="message warning"><?= $error_warning; ?></div>
	<? } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/tax.png'); ?>" alt=""/> {{Tax Class}}</h1>

			<div class="buttons"><a onclick="$('#form').submit();" class="button">{{Save}}</a><a
					href="<?= $cancel; ?>" class="button">{{Cancel}}</a></div>
		</div>
		<div class="section">
			<form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="form">
					<tr>
						<td class="required"> {{Tax Class Title:}}</td>
						<td><input type="text" name="title" value="<?= $title; ?>"/>
							<? if (_l("Tax Class Title must be between 3 and 32 characters!")) { ?>
								<span class="error">{{Tax Class Title must be between 3 and 32 characters!}}</span>
							<? } ?></td>
					</tr>
					<tr>
						<td class="required"> {{Description:}}</td>
						<td><input type="text" name="description" value="<?= $description; ?>"/>
							<? if (_l("Description must be between 3 and 255 characters!")) { ?>
								<br/>
								<span class="error">{{Description must be between 3 and 255 characters!}}</span>
							<? } ?></td>
					</tr>
				</table>
				<br/>
				<table id="tax-rule" class="list">
					<thead>
						<tr>
							<td class="left">{{Tax Rate:}}</td>
							<td class="left">{{Based On:}}</td>
							<td class="left">{{Priority:}}</td>
							<td></td>
						</tr>
					</thead>
					<? $tax_rule_row = 0; ?>
					<? foreach ($tax_rules as $tax_rule) { ?>
						<tbody id="tax-rule-row<?= $tax_rule_row; ?>">
							<tr>
								<td class="left"><select name="tax_rule[<?= $tax_rule_row; ?>][tax_rate_id]">
										<? foreach ($tax_rates as $tax_rate) { ?>
											<? if ($tax_rate['tax_rate_id'] == $tax_rule['tax_rate_id']) { ?>
												<option value="<?= $tax_rate['tax_rate_id']; ?>"
													selected="selected"><?= $tax_rate['name']; ?></option>
											<? } else { ?>
												<option value="<?= $tax_rate['tax_rate_id']; ?>"><?= $tax_rate['name']; ?></option>
											<? } ?>
										<? } ?>
									</select></td>
								<td class="left"><select name="tax_rule[<?= $tax_rule_row; ?>][based]">
										<? if ($tax_rule['based'] == 'shipping') { ?>
											<option value="shipping" selected="selected">{{Shipping Address}}</option>
										<? } else { ?>
											<option value="shipping">{{Shipping Address}}</option>
										<? } ?>
										<? if ($tax_rule['based'] == 'payment') { ?>
											<option value="payment" selected="selected">{{Payment Address}}</option>
										<? } else { ?>
											<option value="payment">{{Payment Address}}</option>
										<? } ?>
										<? if ($tax_rule['based'] == 'store') { ?>
											<option value="store" selected="selected">{{Store Address}}</option>
										<? } else { ?>
											<option value="store">{{Store Address}}</option>
										<? } ?>
									</select></td>
								<td class="left"><input type="text" name="tax_rule[<?= $tax_rule_row; ?>][priority]" value="<?= $tax_rule['priority']; ?>" size="1"/></td>
								<td class="left"><a onclick="$('#tax-rule-row<?= $tax_rule_row; ?>').remove();"
										class="button">{{Remove}}</a></td>
							</tr>
						</tbody>
						<? $tax_rule_row++; ?>
					<? } ?>
					<tfoot>
						<tr>
							<td colspan="3"></td>
							<td class="left"><a onclick="addRule();" class="button">{{Add Rule}}</a></td>
						</tr>
					</tfoot>
				</table>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	var tax_rule_row = <?= $tax_rule_row; ?>;

	function addRule() {
		html = '<tbody id="tax-rule-row' + tax_rule_row + '">';
		html += '	<tr>';
		html += '		<td class="left"><select name="tax_rule[' + tax_rule_row + '][tax_rate_id]">';
		<? foreach ($tax_rates as $tax_rate) { ?>
		html += '			<option value="<?= $tax_rate['tax_rate_id']; ?>"><?= addslashes($tax_rate['name']); ?></option>';
		<? } ?>
		html += '		</select></td>';
		html += '		<td class="left"><select name="tax_rule[' + tax_rule_row + '][based]">';
		html += '			<option value="shipping">{{Shipping Address}}</option>';
		html += '			<option value="payment">{{Payment Address}}</option>';
		html += '			<option value="store">{{Store Address}}</option>';
		html += '		</select></td>';
		html += '		<td class="left"><input type="text" name="tax_rule[' + tax_rule_row + '][priority]" value="" size="1" /></td>';
		html += '		<td class="left"><a onclick="$(\'#tax-rule-row' + tax_rule_row + '\').remove();" class="button">{{Remove}}</a></td>';
		html += '	</tr>';
		html += '</tbody>';

		$('#tax-rule > tfoot').before(html);

		tax_rule_row++;
	}
</script>
<?= $is_ajax ? '' : call('admin/footer'); ?>
