<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<? if ($error_warning) { ?>
		<div class="message warning"><?= $error_warning; ?></div>
	<? } ?>
	<? if ($success) { ?>
		<div class="message success"><?= $success; ?></div>
	<? } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/tax.png'); ?>" alt=""/> {{Tax Rates}}</h1>

			<div class="buttons"><a onclick="location = '<?= $insert; ?>'" class="button">{{Insert}}</a><a onclick="$('form').submit();" class="button">{{Delete}}</a></div>
		</div>
		<div class="section">
			<form action="<?= $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="list">
					<thead>
						<tr>
							<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);"/>
							</td>
							<td class="left"><? if ($sort == 'tr.name') { ?>
									<a href="<?= $sort_name; ?>" class="<?= strtolower($order); ?>">{{Tax Name}}</a>
								<? } else { ?>
									<a href="<?= $sort_name; ?>">{{Tax Name}}</a>
								<? } ?></td>
							<td class="right"><? if ($sort == 'tr.rate') { ?>
									<a href="<?= $sort_rate; ?>" class="<?= strtolower($order); ?>">{{Tax Rate}}</a>
								<? } else { ?>
									<a href="<?= $sort_rate; ?>">{{Tax Rate}}</a>
								<? } ?></td>
							<td class="left"><? if ($sort == 'tr.type') { ?>
									<a href="<?= $sort_type; ?>" class="<?= strtolower($order); ?>">{{Type}}</a>
								<? } else { ?>
									<a href="<?= $sort_type; ?>">{{Type}}</a>
								<? } ?></td>
							<td class="left"><? if ($sort == 'gz.name') { ?>
									<a href="<?= $sort_geo_zone; ?>"
										class="<?= strtolower($order); ?>">{{Geo Zone}}</a>
								<? } else { ?>
									<a href="<?= $sort_geo_zone; ?>">{{Geo Zone}}</a>
								<? } ?></td>
							<td class="left"><? if ($sort == 'tr.date_added') { ?>
									<a href="<?= $sort_date_added; ?>"
										class="<?= strtolower($order); ?>">{{Date Added}}</a>
								<? } else { ?>
									<a href="<?= $sort_date_added; ?>">{{Date Added}}</a>
								<? } ?></td>
							<td class="left"><? if ($sort == 'tr.date_modified') { ?>
									<a href="<?= $sort_date_modified; ?>"
										class="<?= strtolower($order); ?>">{{Date Modified}}</a>
								<? } else { ?>
									<a href="<?= $sort_date_modified; ?>">{{Date Modified}}</a>
								<? } ?></td>
							<td class="right">{{Action}}</td>
						</tr>
					</thead>
					<tbody>
						<? if ($tax_rates) { ?>
							<? foreach ($tax_rates as $tax_rate) { ?>
								<tr>
									<td style="text-align: center;"><? if ($tax_rate['selected']) { ?>
											<input type="checkbox" name="batch[]" value="<?= $tax_rate['tax_rate_id']; ?>"
												checked="checked"/>
										<? } else { ?>
											<input type="checkbox" name="batch[]" value="<?= $tax_rate['tax_rate_id']; ?>"/>
										<? } ?></td>
									<td class="left"><?= $tax_rate['name']; ?></td>
									<td class="right"><?= $tax_rate['rate']; ?></td>
									<td class="left"><?= $tax_rate['type']; ?></td>
									<td class="left"><?= $tax_rate['geo_zone']; ?></td>
									<td class="left"><?= $tax_rate['date_added']; ?></td>
									<td class="left"><?= $tax_rate['date_modified']; ?></td>
									<td class="right"><? foreach ($tax_rate['action'] as $action) { ?>
											[ <a href="<?= $action['href']; ?>"><?= $action['text']; ?></a> ]
										<? } ?></td>
								</tr>
							<? } ?>
						<? } else { ?>
							<tr>
								<td class="center" colspan="9">{{There are no results to display.}}</td>
							</tr>
						<? } ?>
					</tbody>
				</table>
			</form>
			<div class="pagination"><?= $pagination; ?></div>
		</div>
	</div>
</div>
<?= $is_ajax ? '' : call('admin/footer'); ?>
