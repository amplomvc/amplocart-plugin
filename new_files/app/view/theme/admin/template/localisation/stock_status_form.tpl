<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<? if ($error_warning) { ?>
		<div class="message warning"><?= $error_warning; ?></div>
	<? } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/stock-status.png'); ?>" alt=""/> {{Stock Status}}</h1>

			<div class="buttons"><a onclick="$('#form').submit();" class="button">{{Save}}</a><a
					href="<?= $cancel; ?>" class="button">{{Cancel}}</a></div>
		</div>
		<div class="section">
			<form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="form">
					<tr>
						<td class="required"> {{Stock Status Name:}}</td>
						<td><? foreach ($languages as $language) { ?>
								<input type="text" name="stock_status[<?= $language['language_id']; ?>][name]" value="<?= isset($stock_status[$language['language_id']]) ? $stock_status[$language['language_id']]['name'] : ''; ?>"/>
								<img src="<?= theme_url('image/flags/<?= $language[')image']; ?>'; ?>"
									title="<?= $language['name']; ?>"/><br/>
								<? if (isset(_l("Stock Status Name must be between 3 and 32 characters!")[$language['language_id']])) { ?>
									<span class="error"><?= _l("Stock Status Name must be between 3 and 32 characters!")[$language['language_id']]; ?></span><br/>
								<? } ?>
							<? } ?></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>
<?= $is_ajax ? '' : call('admin/footer'); ?>
