<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<form action="<?= site_url('admin/category/save', 'category_id=' . $category_id); ?>" method="post" enctype="multipart/form-data" class="box ctrl-save">
		<div class="heading">
			<h1><img src="<?= theme_url('image/category.png'); ?>" alt=""/> {{Category}}</h1>

			<div class="buttons">
				<button>{{Save}}</button>
				<a href="<?= site_url('admin/category'); ?>" class="button cancel">{{Cancel}}</a>
			</div>
		</div>
		<div class="section">
			<div id="tab-general">
				<table class="form">
					<tr>
						<td class="required"> {{Category Name:}}</td>
						<td><input type="text" name="name" size="60" value="<?= $name; ?>"/></td>
					</tr>
					<tr>
						<td>{{Meta Tag Keywords:}}</td>
						<td><textarea name="meta_keywords" rows="4" cols="60"><?= $meta_keywords; ?></textarea></td>
					</tr>
					<tr>
						<td>{{Meta Tag Description:}}</td>
						<td><textarea name="meta_description" rows="8" cols="60"><?= $meta_description; ?></textarea>
						</td>
					</tr>
					<tr>
						<td>{{Description:}}</td>
						<td><textarea class="ckedit" name="description"><?= $description; ?></textarea></td>
					</tr>
					<tr>
						<td>{{Parent Category:}}</td>
						<td>
							<?= build(array(
								'type'   => 'select',
								'name'   => 'parent_id',
								'data'   => $data_categories,
								'select' => (int)$parent_id,
								'value'  => 'category_id',
								'label'  => 'pathname',
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("SEO Url:<span class=\"help\">The Search Engine Optimized alias for this category.</span>"); ?></td>
						<td>
							<input type="text" onfocus="$(this).next().show_msg('error', '{{Warning! This may cause system instability! Please use the \\'Generate URL\\' button}}');" name="alias" value="<?= $alias; ?>"/>
							<a class="gen_url" onclick="generate_url($(this))">{{[Generate URL]}}</a>
						</td>
					</tr>
					<tr>
						<td>{{Image:}}</td>
						<td>
							<input type="text" class="imageinput" name="image" value="<?= $image; ?>"/>
						</td>
					</tr>
					<tr>
						<td>{{Sort Order:}}</td>
						<td><input type="text" name="sort_order" value="<?= $sort_order; ?>" size="1"/></td>
					</tr>
					<tr>
						<td>{{Layout ID:}}</td>
						<td><?= build(array(
								'type'   => 'select',
								'name'   => 'layout_id',
								'data'   => $data_layouts,
								'select' => $layout_id,
								'value'  => 'layout_id',
								'label'  => 'name',
							)); ?></td>
					</tr>
					<tr>
						<td>{{Status:}}</td>
						<td><?= build(array(
								'type'   => 'select',
								'name'   => 'status',
								'data'   => $data_statuses,
								'select' => $status
							)); ?></td>
					</tr>
				</table>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	function generate_url(context) {
		$.show_msg('clear');

		var name = $('input[name=name]').val();

		if (!name) {
			alert("Please make a name for this Category before generating the URL");
			return;
		}

		data = {category_id: <?= (int)$category_id; ?>, name: name};

		$(context).fade_post("<?= site_url('admin/cart/category/generate_url'); ?>", data, function (response) {
			$('input[name="alias"]').val(response);
		});
	}

	$('.imageinput').ac_imageinput();
</script>

<?= $is_ajax ? '' : call('admin/footer'); ?>
