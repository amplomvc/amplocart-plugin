<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/setting.png'); ?>" alt=""/> {{Categories}}</h1>

			<? if (user_can('w', 'admin/category/batch-action')) { ?>
				<div class="batch_actions">
					<?= block('widget/batch-action', null, $batch_action); ?>
				</div>
				<div class="buttons">
					<a href="<?= site_url('admin/category/form'); ?>" class="button">{{Add Category}}</a>
				</div>
			<? } ?>
		</div>
		<div class="section">
			<?= block('widget/views', null, array(
				'name'  => _l("Categories"),
				'group' => 'category_list',
				'path'  => 'admin/category/listing',
			)); ?>
		</div>
	</div>
</div>

<?= $is_ajax ? '' : call('admin/footer'); ?>
