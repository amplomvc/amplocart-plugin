<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/setting.png'); ?>" alt=""/> {{Shipping Policies}}</h1>

			<div class="buttons">
				<a onclick="$('#form').submit();" class="button">{{Save}}</a>
				<a href="<?= $cancel; ?>" class="button">{{Cancel}}</a>
			</div>
		</div>
		<div class="section">
			<form action="<?= $save; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="form">
					<tr>
						<td valign="top"><a id="add_policy" class="button">{{Add Shipping Policy}}</a></td>
						<td>
							<ul id="shipping_policy_list" class="easy_list">
								<? foreach ($shipping_policies as $row => $policy) { ?>
									<li class="shipping_policy" data-row="<?= $row; ?>">
										<input class="title" size="50" type="text" name="shipping_policies[<?= $row; ?>][title]" value="<?= $policy['title']; ?>"/><br/>
										<textarea class="description ckedit" name="shipping_policies[<?= $row; ?>][description]"><?= $policy['description']; ?></textarea>
										<? if (empty($policy['no_delete'])) { ?>
											<a class="delete button text" onclick="$(this).closest('li').remove()">{{Delete}}</a>
										<? } ?>
									</li>
								<? } ?>
							</ul>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#shipping_policy_list').ac_template('sp_list', {defaults: <?= json_encode($shipping_policies['__ac_template__']); ?>});
	$('#add_policy').click(function () {
		$.ac_template('sp_list', 'add')
	});

	$('#shipping_policy_list').sortable();
</script>



<?= $is_ajax ? '' : call('admin/footer'); ?>
