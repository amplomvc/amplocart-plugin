<?= $is_ajax ? '' : call('admin/header'); ?>

<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>

	<form action="<?= site_url('admin/setting/cart/save'); ?>" method="post" enctype="multipart/form-data" class="box ctrl-save">
		<div class="heading">
			<h1>
				<img src="<?= theme_url('image/settings/cart.png'); ?>" alt=""/> {{General Cart Settings}}
			</h1>

			<div class="buttons">
				<button>{{Save}}</button>
				<a href="<?= site_url('admin/settings'); ?>" class="button cancel">{{Cancel}}</a>
			</div>
		</div>

		<div class="section">
			<div id="tabs" class="htabs">
				<a href="#tab-general">{{General}}</a>
				<a href="#tab-local">{{Local}}</a>
				<a href="#tab-image">{{Image}}</a>
				<a href="#tab-fraud">{{Fraud}}</a>
			</div>

			<div id="tab-general">
				<table class="form">
					<tr>
						<td>{{Default Return Policy:}}</td>
						<td>
							<? if (!empty($data_return_policies)) { ?>
								<?=
								build(array(
									'type' => 'select',
									'name'  => 'config_default_return_policy',
									'data'   => $data_return_policies,
									'select' => $config_default_return_policy,
									'value' => false,
									'label' => 'title',
								)); ?>
							<? } ?>
							<p>{{Add Return Policy}}</p>
						</td>
					</tr>
					<tr>
						<td>{{Default Shipping Policy:}}</td>
						<td>
							<? if (!empty($data_shipping_policies)) { ?>
								<?=
								build(array(
									'type' => 'select',
									'name'  => 'config_default_shipping_policy',
									'data'   => $data_shipping_policies,
									'select' => $config_default_shipping_policy,
									'value' => false,
									'label' => 'title',
								)); ?>
							<? } ?>
							<p>{{Add Shipping Policy}}</p>
						</td>
					</tr>
					<tr>
						<td>{{Shipping / Returns Policy Page:}}</td>
						<td>
							<?=
							build(array(
								'type' => 'select',
								'name'  => 'config_shipping_return_page_id',
								'data'   => $data_pages,
								'select' => $config_shipping_return_page_id,
								'value' => 'page_id',
								'label' => 'title',
							)); ?>
						</td>
					</tr>

					<tr>
						<td>{{Show Category Image:}}</td>
						<td><?=
							build(array(
								'type' => 'radio',
								'name'  => 'config_show_category_image',
								'data'   => $data_yes_no,
								'select' => $config_show_category_image
							)); ?>
						</td>
					</tr>
					<tr>
						<td>{{Show Category Description:}}</td>
						<td>
							<?= build(array(
								'type' => 'radio',
								'name'  => 'config_show_category_description',
								'data'   => $data_yes_no,
								'select' => $config_show_category_description
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("Product List Hover Image:<span class=\"help\">For the Product List pages, show an alternate image when moving the mouse over the product block</span>"); ?></td>
						<td><?=
							build(array(
								'type' => 'radio',
								'name'  => 'config_show_product_list_hover_image',
								'data'   => $data_yes_no,
								'select' => $config_show_product_list_hover_image
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("Display Return Policy:<span class=\"help\">(eg: final sale, # days to return, etc.) as a column in the cart</span>"); ?></td>
						<td><?=
							build(array(
								'type' => 'radio',
								'name'  => 'config_cart_show_return_policy',
								'data'   => $data_yes_no,
								'select' => $config_cart_show_return_policy
							)); ?>
						</td>
					</tr>
					<tr>
						<td>{{Display Prices With Tax:}}</td>
						<td>
							<?= build(array(
								'type' => 'radio',
								'name'  => 'config_show_price_with_tax',
								'data'   => $data_yes_no,
								'select' => $config_show_price_with_tax,
							)); ?>
						</td>
					</tr>
					<tr>
						<td>{{Default Tax Class:}}</td>
						<td>
							<?=
							build(array(
								'type' => 'select',
								'name'  => 'config_tax_default_id',
								'data'   => $tax_classes,
								'select' => $config_tax_default_id,
								'value' => 'tax_class_id',
								'label' => 'title',
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("Use Store Tax Address:<br /><span class=\"help\">Use the store address to calculate taxes if no one is logged in. You can choose to use the store address for the customers shipping or payment address.</span>"); ?></td>
						<td>
							<?= build(array(
								'type' => 'select',
								'name'  => 'config_tax_default',
								'data'   => array(
									''         => _l("-- None --"),
									'shipping' => _l("Shipping Address"),
									'payment'  => _l("Payment Address"),
								),
								'select' => $config_tax_default,
							)); ?>

						</td>
					</tr>
					<tr>
						<td><?= _l("Use Customer Tax Address:<br /><span class=\"help\">Use the customers default address when they login to calculate taxes. You can choose to use the default address for the customers shipping or payment address.</span>"); ?></td>
						<td>
							<?= build(array(
								'type' => 'select',
								'name'  => 'config_tax_customer',
								'data'   => array(
									'shipping' => _l("Shipping Address"),
									'payment'  => _l("Payment Address"),
								),
								'select' => $config_tax_customer,
							)); ?>
						</td>
					</tr>
					<tr>
						<td>
							<div>{{Invoice Prefix:}}</div>
							<span class="help">{{Set the invoice prefix (e.g. INV-2011-01 or INV-%Y-m%). Invoice ID's will start at 1 for each unique prefix. Use a date format (eg: %Y-m-d%) anywhere - Invoice IDs will reset automatically to 1 for each unique date.}}</span>
						</td>
						<td><input type="text" name="config_invoice_prefix" value="<?= $config_invoice_prefix; ?>"/></td>
					</tr>
					<tr>
						<td><?= _l("Order Editing:<br /><span class=\"help\">Number of days allowed to edit an order. This is required because prices and discounts may change over time corrupting the order if its edited.</span>"); ?></td>
						<td><input type="text" name="config_order_edit" value="<?= $config_order_edit; ?>" size="3"/></td>
					</tr>
					<tr>
						<td><?= _l("Login Display Prices:<br /><span class=\"help\">Only show prices when a customer is logged in.</span>"); ?></td>
						<td><?=
							build(array(
								'type' => 'radio',
								'name'  => 'config_customer_hide_price',
								'data'   => $data_yes_no,
								'select' => $config_customer_hide_price
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("Approve New Customers:<br /><span class=\"help\">Don\'t allow new customer to login until their account has been approved.</span>"); ?></td>
						<td><?=
							build(array(
								'type' => 'radio',
								'name'  => 'config_customer_approval',
								'data'   => $data_yes_no,
								'select' => $config_customer_approval
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("Guest Checkout:<br /><span class=\"help\">Allow customers to checkout without creating an account. This will not be available when a downloadable product is in the shopping cart.</span>"); ?></td>
						<td><?=
							build(array(
								'type' => 'radio',
								'name'  => 'config_guest_checkout',
								'data'   => $data_yes_no,
								'select' => $config_guest_checkout
							)); ?>
						</td>
					</tr>

					<tr>
						<td><?= _l("Checkout Terms:<br /><span class=\"help\">Forces people to agree to terms before an a customer can checkout.</span>"); ?></td>
						<td>
							<?=
							build(array(
								'type' => 'select',
								'name'  => 'config_checkout_terms_page_id',
								'data'   => $data_pages,
								'select' => $config_checkout_terms_page_id,
								'value' => 'page_id',
								'label' => 'title',
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("Display Stock:<br /><span class=\"help\">Display stock quantity on the product page.</span>"); ?></td>
						<td>
							<?=
							build(array(
								'type' => 'select',
								'name'  => "config_stock_display",
								'data'   => $data_stock_display_types,
								'select' => $config_stock_display,
								'#class' => 'display_stock_radio'
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("Show Out Of Stock Warning:<br /><span class=\"help\">Display out of stock message on the shopping cart page if a product is out of stock but stock checkout is yes. (Warning always shows if stock checkout is no)</span>"); ?></td>
						<td><?=
							build(array(
								'type' => 'select',
								'name'  => 'config_stock_warning',
								'data'   => $data_yes_no,
								'select' => $config_stock_warning
							)); ?></td>
					</tr>
					<tr>
						<td><?= _l("Stock Checkout:<br /><span class=\"help\">Allow customers to still checkout if the products they are ordering are not in stock.</span>"); ?></td>
						<td><?=
							build(array(
								'type' => 'radio',
								'name'  => 'config_stock_checkout',
								'data'   => $data_yes_no,
								'select' => $config_stock_checkout
							)); ?></td>
					</tr>
					<tr>
						<td><?= _l("Out of Stock Status:<br /><span class=\"help\">Set the default out of stock status selected in product edit.</span>"); ?></td>
						<td>
							<?= build(array(
								'type' => 'select',
								'name'  => 'config_stock_status_id',
								'data'   => $data_stock_statuses,
								'select' => $config_stock_status_id,
								'value' => 'stock_status_id',
								'label' => 'name',
							)); ?>
						</td>
					</tr>
					<tr>
						<td>{{Show Related Products on Product Page:}}</td>
						<td><?=
							build(array(
								'type' => 'select',
								'name'  => 'config_show_product_related',
								'data'   => $data_show_product_related,
								'select' => $config_show_product_related
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("Allow Reviews:<br /><span class=\"help\">Enable/Disable new review entry and display of existing reviews</span>"); ?></td>
						<td>
							<?= build(array(
								'type' => 'radio',
								'name'  => 'config_review_status',
								'data'   => $data_yes_no,
								'select' => $config_review_status
							)); ?>
						</td>
					</tr>
					<tr>
						<td>{{Show Product Attributes on Product Page:}}</td>
						<td><?=
							build(array(
								'type' => 'select',
								'name'  => "config_show_product_attributes",
								'data'   => $data_yes_no,
								'select' => $config_show_product_attributes
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("Display Weight on Cart Page:<br /><span class=\"help\">Show the cart weight on the cart page</span>"); ?></td>
						<td>
							<?= build(array(
								'type' => 'radio',
								'name'  => 'config_cart_weight',
								'data'   => $data_yes_no,
								'select' => $config_cart_weight
							)); ?>
						</td>
					</tr>
				</table>
			</div>

			<div id="tab-local">
				<table class="form">
					<tr>
						<td><?= _l("Currency:<br /><span class=\"help\">Change the default currency. Clear your browser cache to see the change and reset your existing cookie.</span>"); ?></td>
						<td>
							<?= build(array(
								'type' => 'select',
								'name'  => 'config_currency',
								'data'   => $data_currencies,
								'select' => $config_currency,
								'value' => 'code',
								'label' => 'title',
							)); ?>
						</td>
					</tr>
					<tr>
						<td><?= _l("Auto Update Currency:<br /><span class=\"help\">Set your store to automatically update currencies daily.</span>"); ?></td>
						<td>
							<?= build(array(
								'type' => 'radio',
								'name'  => 'config_currency_auto',
								'data'   => $data_yes_no,
								'select' => $config_currency_auto,
							)); ?>
						</td>
					</tr>
					<tr>
						<td>{{Length Units:}}</td>
						<td>
							<?= build(array(
								'type' => 'select',
								'name'  => 'config_length_unit',
								'data'   => $data_length_units,
								'select' => $config_length_unit,
								'value' => 'unit',
								'label' => 'plural',
							)); ?>
						</td>
					</tr>
					<tr>
						<td>{{Weight Units:}}</td>
						<td>
							<?= build(array(
								'type' => 'select',
								'name'  => 'config_weight_unit',
								'data'   => $data_weight_units,
								'select' => $config_weight_unit,
								'value' => 'unit',
								'label' => 'plural',
							)); ?>
						</td>
					</tr>
				</table>
			</div>

			<div id="tab-image">
				<table class="form">

					<tr>
						<td class="required"> {{Category Image Size:}}</td>
						<td>
							<input type="text" name="config_image_category_width" value="<?= $config_image_category_width; ?>"
								size="3"/>
							x
							<input type="text" name="config_image_category_height" value="<?= $config_image_category_height; ?>"
								size="3"/>
					</tr>
					<tr>
						<td class="required"> {{Manufacturer Image Size:}}</td>
						<td>
							<input type="text" name="config_image_manufacturer_width" value="<?= $config_image_manufacturer_width; ?>" size="3"/>
							x
							<input type="text" name="config_image_manufacturer_height" value="<?= $config_image_manufacturer_height; ?>" size="3"/>
					</tr>
					<tr>
						<td class="required"> {{Product Image Thumb Size:}}</td>
						<td>
							<input type="text" name="config_image_thumb_width" value="<?= $config_image_thumb_width; ?>" size="3"/>
							x
							<input type="text" name="config_image_thumb_height" value="<?= $config_image_thumb_height; ?>" size="3"/>
					</tr>
					<tr>
						<td class="required"> {{Product Image Popup Size:}}</td>
						<td>
							<input type="text" name="config_image_popup_width" value="<?= $config_image_popup_width; ?>" size="3"/>
							x
							<input type="text" name="config_image_popup_height" value="<?= $config_image_popup_height; ?>" size="3"/>
					</tr>
					<tr>
						<td class="required"> {{Product Image List Size:}}</td>
						<td>
							<input type="text" name="config_image_product_width" value="<?= $config_image_product_width; ?>"
								size="3"/>
							x
							<input type="text" name="config_image_product_height" value="<?= $config_image_product_height; ?>"
								size="3"/>
					</tr>
					<tr>
						<td class="required"> {{Product Options Image Size:}}</td>
						<td>
							<input type="text" name="config_image_product_option_width" value="<?= $config_image_product_option_width; ?>" size="3"/>
							x
							<input type="text" name="config_image_product_option_height" value="<?= $config_image_product_option_height; ?>" size="3"/>
					</tr>
					<tr>
						<td class="required"> {{Additional Product Image Size:}}</td>
						<td>
							<input type="text" name="config_image_additional_width" value="<?= $config_image_additional_width; ?>"
								size="3"/>
							x
							<input type="text" name="config_image_additional_height" value="<?= $config_image_additional_height; ?>"
								size="3"/>
					</tr>
					<tr>
						<td class="required"> {{Related Product Image Size:}}</td>
						<td>
							<input type="text" name="config_image_related_width" value="<?= $config_image_related_width; ?>"
								size="3"/>
							x
							<input type="text" name="config_image_related_height" value="<?= $config_image_related_height; ?>"
								size="3"/>
					</tr>
					<tr>
						<td class="required"> {{Compare Image Size:}}</td>
						<td>
							<input type="text" name="config_image_compare_width" value="<?= $config_image_compare_width; ?>"
								size="3"/>
							x
							<input type="text" name="config_image_compare_height" value="<?= $config_image_compare_height; ?>"
								size="3"/>
					</tr>
					<tr>
						<td class="required"> {{Wish List Image Size:}}</td>
						<td>
							<input type="text" name="config_image_wishlist_width" value="<?= $config_image_wishlist_width; ?>"
								size="3"/>
							x
							<input type="text" name="config_image_wishlist_height" value="<?= $config_image_wishlist_height; ?>"
								size="3"/>
					</tr>
					<tr>
						<td class="required"> {{Cart Image Size:}}</td>
						<td>
							<input type="text" name="config_image_cart_width" value="<?= $config_image_cart_width; ?>" size="3"/>
							x
							<input type="text" name="config_image_cart_height" value="<?= $config_image_cart_height; ?>" size="3"/>
					</tr>
				</table>
			</div>

			<div id="tab-fraud">
				<table class="form">
					<tr>
						<td><?= _l("Use MaxMind Fraud Detection System:<br /><span class=\"help\">MaxMind is a fraud detections service. If you don\'t have a license key you can <a target=\"_blank\" href=\"http://www.maxmind.com/?rId=opencart\">sign up here</a>. Once you have obtained a key copy and paste it into the field below.</span>"); ?></td>
						<td><? if ($config_fraud_detection) { ?>
								<input type="radio" name="config_fraud_detection" value="1" checked="checked"/>
								{{Yes}}
								<input type="radio" name="config_fraud_detection" value="0"/>
								{{No}}
							<? } else { ?>
								<input type="radio" name="config_fraud_detection" value="1"/>
								{{Yes}}
								<input type="radio" name="config_fraud_detection" value="0" checked="checked"/>
								{{No}}
							<? } ?></td>
					</tr>
					<tr>
						<td>{{MaxMind License Key:</span>}}</td>
						<td><input type="text" name="config_fraud_key" value="<?= $config_fraud_key; ?>"/></td>
					</tr>
					<tr>
						<td><?= _l("MaxMind Risk Score:<br /><span class=\"help\">The higher the score the more likly the order is fraudulent. Set a score between 0 - 100.</span>"); ?></td>
						<td><input type="text" name="config_fraud_score" value="<?= $config_fraud_score; ?>"/></td>
					</tr>
				</table>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	$('.table.form .zone_select').ac_zoneselect({listen: '.table.form .country_select'});

	$('[name=config_theme]').change(function () {
		var url = $ac.admin_url + 'setting/setting/theme?theme=' + $(this).val();
		$('#theme').load(url);
	}).change();

	$('.imageinput').ac_imageinput();

	$('#tabs a').tabs();
</script>

<?= $is_ajax ? '' : call('admin/footer'); ?>
