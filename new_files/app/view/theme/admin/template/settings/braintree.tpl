<?= $is_ajax ? '' : call('admin/header'); ?>

<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>

	<form action="<?= site_url('admin/settings/braintree/save'); ?>" method="post" class="box ctrl-save">
		<div class="heading">
			<h1>
				<img src="<?= theme_url('image/setting.png'); ?>" alt=""/>
				{{Braintree Settings}}
			</h1>

			<div class="buttons">
				<button>{{Save}}</button>
				<a href="<?= site_url('admin/settings'); ?>" class="button cancel">{{Cancel}}</a>
			</div>
		</div>

		<div class="section">
			<table class="form">
				<tr>
					<td>{{Merchant ID:}}</td>
					<td><input type="text" name="merchant_id" value="<?= $merchant_id; ?>"/></td>
				</tr>
				<tr>
					<td>{{Public Key:}}</td>
					<td><input type="text" name="public_key" value="<?= $public_key; ?>"/></td>
				</tr>
				<tr>
					<td>{{Private Key:}}</td>
					<td><input type="text" name="private_key" value="<?= $private_key; ?>"/></td>
				</tr>
				<tr>
					<td>{{Client-Side Encryption Key}}</td>
					<td>
						<textarea rows="8" cols="70" name="client_side_encryption_key"><?= $client_side_encryption_key; ?></textarea>
					</td>
				</tr>
				<tr>
					<td>{{Environment Mode}}</td>
					<td><?= build(array(
							'type'   => 'select',
							'name'   => "mode",
							'data'   => $data_modes,
							'select' => $mode
						)); ?></td>
				</tr>
				<tr>
					<td>
						{{Recurring Billing Plan ID}}
						<span class="help">
							{{Log into}}
							<a target="_blank" href="https://sandbox.braintreegateway.com/">{{your braintree account}}</a>
							{{ and }}
							<a target="_blank" href="https://www.braintreepayments.com/docs/php/guide/recurring_billing">{{make a plan}}</a>.
							{{Enter the plan ID here. Set the Price to $1.00.}}
						</span>
					</td>
					<td>
						<? if (!empty($data_braintree_plans)) { ?>
							<?= build(array(
								'type'   => 'select',
								'name'   => "plan_id",
								'data'   => $data_braintree_plans,
								'select' => $plan_id,
								'value'  => 'id',
								'label'  => 'name',
							)); ?>
						<? } else { ?>
							<p>
								<a target="_blank" href="https://sandbox.braintreegateway.com/">{{Go Setup a Recurring Billing Plan Now!}}</a>
							</p>
						<? } ?>
					</td>
				</tr>
			</table>
		</div>
	</form>
</div>

<?= $is_ajax ? '' : call('admin/footer'); ?>
