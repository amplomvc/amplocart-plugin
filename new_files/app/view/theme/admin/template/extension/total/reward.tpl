<?= $is_ajax ? '' : call('admin/header'); ?>
	<div class="section">
		<?= $is_ajax ? '' : breadcrumbs(); ?>
		<? if ($error_warning) { ?>
			<div class="message warning"><?= $error_warning; ?></div>
		<? } ?>
		<div class="box">
			<div class="heading">
				<h1><img src="<?= theme_url('image/total.png'); ?>" alt=""/> <?= $head_title; ?></h1>

				<div class="buttons"><a onclick="$('#form').submit();" class="button">{{Save}}</a><a
						href="<?= $cancel; ?>" class="button">{{Cancel}}</a></div>
			</div>
			<div class="section">
				<form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form">
					<table class="form">
						<tr>
							<td>{{Status}}</td>
							<td><select name="reward_status">
									<? if ($reward_status) { ?>
										<option value="1" selected="selected">{{Enabled}}</option>
										<option value="0">{{Disabled}}</option>
									<? } else { ?>
										<option value="1">{{Enabled}}</option>
										<option value="0" selected="selected">{{Disabled}}</option>
									<? } ?>
								</select></td>
						</tr>
						<tr>
							<td>{{Sort Order}}</td>
							<td><input type="text" name="reward_sort_order" value="<?= $reward_sort_order; ?>" size="1"/></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
<?= $is_ajax ? '' : call('admin/footer'); ?>
