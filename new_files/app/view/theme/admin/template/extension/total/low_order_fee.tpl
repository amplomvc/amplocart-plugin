<table class="form">
	<tr>
		<td>{{Order Total:}}</td>
		<td><input type="text" name="settings[total]" value="<?= $settings['total']; ?>"/></td>
	</tr>
	<tr>
		<td>{{Fee:}}</td>
		<td><input type="text" name="settings[fee]" value="<?= $settings['fee']; ?>"/></td>
	</tr>
	<tr>
		<td>{{Tax Class:}}</td>
		<td>
			<?= build(array(
				'type' => 'select',
				'name'  => "settings[tax_class_id]",
				'data'   => $data_tax_classes,
				'select' => $settings['tax_class_id'],
				'value' => 'tax_class_id',
				'label' => 'title',
			)); ?>
		</td>
	</tr>
</table>
