<?= $is_ajax ? '' : call('admin/header'); ?>

<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/module.png'); ?>" alt=""/> <?= $page_title; ?></h1>

			<div class="buttons">
				<a onclick="$('#form').trigger('saving').submit();" class="button">{{Save}}</a>
				<a href="<?= $cancel; ?>" class="button">{{Cancel}}</a>
			</div>
		</div>
		<div class="section">
			<form action="<?= $save; ?>" method="post" enctype="multipart/form-data" id="form">
				<div id="extension_settings">
					<table class="form">
						<tr>
							<td>{{Title}}</td>
							<td><input type="text" name="title" value="<?= $title; ?>"/></td>
						</tr>
						<? if (!empty($extend_settings)) { ?>
							<tr>
								<td colspan="2"><?= $extend_settings; ?></td>
							</tr>
						<? } ?>
						<tr>
							<td>{{Minimum Order Total}}</td>
							<td><input type="text" name="settings[min_total]" value="<?= $settings['min_total']; ?>"/></td>
						</tr>
						<tr>
							<td>{{Shipping Zone}}</td>
							<td>
							<?= build(array(
								'type' => 'select',
								'name'  => "settings[geo_zone_id]",
								'data'   => $data_geo_zones,
								'select' => $settings['geo_zone_id'],
								'value' => 'geo_zone_id',
								'label' => 'name',
							)); ?>
							</td>
						</tr>
						<tr>
							<td>{{Sort Order}}</td>
							<td><input type="text" name="sort_order" value="<?= $sort_order; ?>"/></td>
						</tr>
						<tr>
							<td>{{Status}}</td>
							<td><?= build(array(
	'type' => 'select',
	'name'  => "status",
	'data'   => $data_statuses,
	'select' => $status
)); ?></td>
						</tr>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>



<?= $is_ajax ? '' : call('admin/footer'); ?>
