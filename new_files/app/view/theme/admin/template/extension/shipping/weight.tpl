<table class="form">
	<tr>
		<td>{{Price Sets:}}</td>
		<td>
			<table id="pricesets" class="list">
				<thead>
				<tr>
					<td class="left">{{Label}}</td>
					<td class="left">{{Total Weight of Cart}}</td>
					<td class="left">{{Shipping Cost}}</td>
					<td class="left">{{Cost Type}}</td>
					<td></td>
				</tr>
				</thead>
				<tbody id="priceset_list">
				<? foreach ($priceset as $row => $set) { ?>
					<tr class="priceset" data-row="<?= $row; ?>">
						<td class="left quote-label">
							<input type="text" name="settings[priceset][<?= $row; ?>][label]" value="<?= $set['label']; ?>" />
						</td>
						<td class="left pricetotal">
							<?=
							build(array(
								'type' => 'select',
								'name'     => "settings[priceset][$row][range]",
								'data'      => $data_ranges,
								'select'    => $set['range'],
								'#onchange' => 'range_values($(this))',
							)); ?>
							<span class="total" <?= $set['range'] == 'range' ? "style=\"display:none\"" : ''; ?>>
								<input type="text" name="settings[priceset][<?= $row; ?>][total]" value="<?= $set['total']; ?>"/>
							</span>
							<span class="pricerange" <?= $set['range'] != 'range' ? "style=\"display:none\"" : ''; ?>>
								{{Min Price}}
								<input type="text" name="settings[priceset][<?= $row; ?>][from]" value="<?= $set['from']; ?>"/>
								{{Max Price}}
								<input type="text" name="settings[priceset][<?= $row; ?>][to]" value="<?= $set['to']; ?>"/>
							</span>
						</td>
						<td class="left">
							<input type="text" name="settings[priceset][<?= $row; ?>][cost]" value="<?= $set['cost']; ?>"/>
						</td>
						<td class="left"><?=
							build(array(
								'type' => 'select',
								'name'  => "settings[priceset][$row][type]",
								'data'   => $data_types,
								'select' => $set['type']
							)); ?>
						</td>
						<td class="left">
							<a onclick="$(this).closest('.priceset').remove();" class="button remove">{{X}}</a>
						</td>
					</tr>
				<? } ?>
				</tbody>
				<tfoot>
				<tr>
					<td colspan="4"></td>
					<td class="left">
						<a onclick="add_price_set()" class="button">{{Add Price Set}}</a>
					</td>
				</tr>
				</tfoot>
			</table>
		</td>
	</tr>
	<tr>
		<td>{{Zone Rules}}</td>
		<td>
			<table id="zonerules" class="list">
				<thead>
				<tr>
					<td class="left">{{Zone}}</td>
					<td class="left">{{Modifier}}</td>
					<td class="left">{{Modified Cost}}</td>
					<td class="left">{{Cost Type}}</td>
					<td></td>
				</tr>
				</thead>
				<tbody id="zonerule_list">
				<? foreach ($zonerule as $row => $rule) { ?>
					<tr class="zonerule" data-row="<?= $row; ?>">
						<td class="left">
							<?=
							build(array(
								'type' => 'select',
								'name'  => "settings[zonerule][$row][country_id]",
								'data'   => $data_countries,
								'select' => $rule['country_id'], array('class' => 'country_select'),
								'key'    => 'country_id',
								'value'  => 'name',
							)); ?>
							<select name="settings[zonerule][<?= $row; ?>][zone_id]" class="zone_select" data-zone_id="<?= $rule['zone_id']; ?>"></select>
						</td>
						<td class="left"><?=
							build(array(
								'type' => 'select',
								'name'  => "settings[zonerule][$row][mod]",
								'data'   => $data_mods,
								'select' => $rule['mod']
							)); ?></td>
						<td class="left">
							<input type="text" name="settings[zonerule][<?= $row; ?>][cost]" value="<?= $rule['cost']; ?>"/>
						</td>
						<td class="left"><?=
							build(array(
								'type' => 'select',
								'name'  => "settings[zonerule][$row][type]",
								'data'   => $data_types,
								'select' => $rule['type']
							)); ?></td>
						<td class="left">
							<a onclick="$(this).closest('.zonerule').remove();" class="button remove">{{X}}</a>
						</td>
					</tr>
				<? } ?>
				</tbody>
				<tfoot>
				<tr>
					<td colspan="4"></td>
					<td class="left">
						<a onclick="add_zone_rule();" class="button">{{Add Zone Rule}}</a>
					</td>
				</tr>
				</tfoot>
			</table>
		</td>
	</tr>
</table>

<script type="text/javascript">
	$('.zonerule .zone_select').ac_zoneselect({listen: '.zonerule .country_select', allow_all: true});

	/* Flat Pricing List */
	var ps_list = $('#priceset_list');
	ps_list.ac_template('ps_list', {defaults: <?= json_encode($priceset['__ac_template__']); ?>});

	function add_price_set() {
		$.ac_template('ps_list', 'add');
	}

	ps_list.sortable({cursor: 'move'});

	function range_values(context) {
		pricetotal = context.closest('.priceset').find('.pricetotal');
		if (context.val() == 'range') {
			pricetotal.find('.pricerange').show();
			pricetotal.find('.total').hide();
		}
		else {
			pricetotal.find('.pricerange').hide();
			pricetotal.find('.total').show();
		}
	}

	/* Zone Rules */
	var zr_list = $('#zonerule_list');
	zr_list.ac_template('zr_list', {defaults: <?= json_encode($zonerule['__ac_template__']); ?>});

	zr_list.sortable({cursor: 'move'});

	function add_zone_rule() {
		var zr = $.ac_template('zr_list', 'add');
		zr.find('.country_select').trigger('change');
	}
</script>
