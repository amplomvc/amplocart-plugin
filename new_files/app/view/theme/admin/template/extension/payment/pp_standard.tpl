<div id="pp_standard_settings">
	<table class="form">
		<tr>
			<td>
				{{Image Title:}}
				<span class="help">{{The Paypal Button Graphic image to replace the Paypal title during checkout. Leave blank to use default title.}}</span>
			</td>
			<td>
				<input type="text" size="80" name="settings[button_graphic]" value="<?= $settings['button_graphic']; ?>"/>
				<img id="button_graphic_img" src="<?= $settings['button_graphic']; ?>"/>
			</td>
		</tr>
		<tr>
			<td class="required"> {{E-Mail:}}</td>
			<td><input type="text" name="settings[email]" size="50" value="<?= $settings['email']; ?>"/></td>
		</tr>
		<tr>
			<td>
				{{Test Account E-Mail:}}
				<span class="help">{{(if different than your primary account)}}</span>
			</td>
			<td><input type="text" name="settings[test_email]" size="50" value="<?= $settings['test_email']; ?>"/></td>
		</tr>
		<tr>
			<td>{{Sandbox Mode:}}</td>
			<td><?= build(array(
	'type' => 'radio',
	'name'  => 'settings[test]',
	'data'   => $data_yes_no,
	'select' => $settings['test']
)); ?></td>
		</tr>
		<tr>
			<td>{{Transaction Method:}}</td>
			<td><?= build(array(
	'type' => 'select',
	'name'  => 'settings[transaction]',
	'data'   => $data_auth_sale,
	'select' => $settings['transaction']
)); ?></td>
		</tr>
		<tr>
			<td>
				{{PDT is enabled?}}
				<span class="help">{{This will allow the user to be instantly returned to your site after payment.}}</span>
			</td>
			<td><?= build(array(
					'type' => 'select',
					'name'  => "settings[pdt_enabled]",
					'data'   => $data_statuses,
					'select' => $settings['pdt_enabled']
				)); ?>
				<span class="help">{{To enabled PDT on your account you must}} <a target="_blank" href="http://www.paypal.com/">{{login to your paypal account}}</a>.<br/>
					{{Go to Profile > Website payments preferences.}}<br/>
					{{From here enable PDT and Auto Return.}}
				</span>
			</td>
		</tr>
		<tr>
			<td class="required">{{PDT Identity Token}}</td>
			<td><input type="text" name="settings[pdt_token]" value="<?= $settings['pdt_token']; ?>" size="80"/></td>
		</tr>
		<tr>
			<td>
				<div>{{Debug Mode:}}</div>
				<span class="help">{{Logs additional information to the system log.}}</span>
			</td>
			<td><?= build(array(
	'type' => 'select',
	'name'  => "settings[debug]",
	'data'   => $data_statuses,
	'select' => $settings['debug']
)); ?></td>
		</tr>
		<tr>
			<td>
				<div>{{Total:}}</div>
				<span class="help">{{The checkout total the order must reach before this payment method becomes active.}}</span>
			</td>
			<td><input type="text" name="settings[total]" value="<?= $settings['total']; ?>"/></td>
		</tr>
		<tr>
			<td>
				{{Page Style:}}
				<span class="help">{{Enter 'primary' for the primary style set on your Paypal account, or enter the name of the style as you named it on your paypal account. Leave blank to use the default}}</span>
			</td>
			<td><input type="text" name="settings[page_style]" value="<?= $settings['page_style']; ?>"/></td>
		</tr>
		<tr>
			<td>{{Canceled Reversal Status:}}</td>
			<td>
			<?= build(array(
				'type' => 'select',
				'name'  => "settings[canceled_reversal_status_id]",
				'data'   => $data_order_statuses,
				'select' => $settings['canceled_reversal_status_id'],
				'value' => false,
				'label' => 'title',
			)); ?>
			</td>
		</tr>
		<tr>
			<td>{{Denied Status:}}</td>
			<td><?= build(array(
	'type' => 'select',
	'name'  => "settings[denied_status_id]",
	'data'   => $data_order_statuses,
	'select' => $settings['denied_status_id']
)); ?></td>
		</tr>
		<tr>
			<td>{{Expired Status:}}</td>
			<td><?= build(array(
	'type' => 'select',
	'name'  => "settings[expired_status_id]",
	'data'   => $data_order_statuses,
	'select' => $settings['expired_status_id']
)); ?></td>
		</tr>
		<tr>
			<td>{{Failed Status:}}</td>
			<td><?= build(array(
	'type' => 'select',
	'name'  => "settings[failed_status_id]",
	'data'   => $data_order_statuses,
	'select' => $settings['failed_status_id']
)); ?></td>
		</tr>
		<tr>
			<td>{{Pending Status:}}</td>
			<td><?= build(array(
	'type' => 'select',
	'name'  => "settings[pending_status_id]",
	'data'   => $data_order_statuses,
	'select' => $settings['pending_status_id']
)); ?></td>
		</tr>
		<tr>
			<td>{{Processed Status:}}</td>
			<td><?= build(array(
	'type' => 'select',
	'name'  => "settings[processed_status_id]",
	'data'   => $data_order_statuses,
	'select' => $settings['processed_status_id']
)); ?></td>
		</tr>
		<tr>
			<td>{{Refunded Status:}}</td>
			<td><?= build(array(
	'type' => 'select',
	'name'  => "settings[refunded_status_id]",
	'data'   => $data_order_statuses,
	'select' => $settings['refunded_status_id']
)); ?></td>
		</tr>
		<tr>
			<td>{{Reversed Status:}}</td>
			<td><?= build(array(
	'type' => 'select',
	'name'  => "settings[reversed_status_id]",
	'data'   => $data_order_statuses,
	'select' => $settings['reversed_status_id']
)); ?></td>
		</tr>
		<tr>
			<td>{{Voided Status:}}</td>
			<td><?= build(array(
	'type' => 'select',
	'name'  => "settings[voided_status_id]",
	'data'   => $data_order_statuses,
	'select' => $settings['voided_status_id']
)); ?></td>
		</tr>
	</table>
</div>

<script type="text/javascript">
	$('[name="settings[button_graphic]"]').change(function () {
		$('#button_graphic_img').attr('src', $(this).val());
	});

	$('[name="settings[pdt_enabled]"]').change(function () {
		token_row = $('[name="settings[pdt_token]"]').closest('tr');

		if ($(this).val() === '1') {
			token_row.show();
		} else {
			token_row.hide();
		}
	}).change();
</script>


