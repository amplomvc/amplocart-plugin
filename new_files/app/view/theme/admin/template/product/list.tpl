<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/setting.png'); ?>" alt=""/> {{Products}}</h1>

			<? if (user_can('w', 'admin/product/batch_action')) { ?>
				<div class="batch_actions">
					<?= block('widget/batch_action', null, $batch_action); ?>
				</div>
				<div class="buttons">
					<? if (!empty($product_classes)) { ?>
						<div class="insert_classes">
							<? foreach ($product_classes as $product_class) { ?>
								<a href="<?= site_url('admin/product/' . ($product_class['class'] ? $product_class['class'] . '/' : '') . 'form'); ?>" class="button"><?= _l("Add %s", $product_class['name']); ?></a>
							<? } ?>
						</div>
					<? } else { ?>
						<a href="<?= site_url('admin/product/form'); ?>" class="button">{{Add Product}}</a>
					<? } ?>
				</div>
			<? } ?>
		</div>
		<div class="section">
			<?= block('widget/views', null, array(
				'name'  => _l("Products"),
				'group' => 'product_list',
				'path'  => 'admin/product/listing',
			)); ?>
		</div>
	</div>
</div>

<?= $is_ajax ? '' : call('admin/footer'); ?>
