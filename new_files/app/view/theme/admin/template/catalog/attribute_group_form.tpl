<?= $is_ajax ? '' : call('admin/header'); ?>

<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>

	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/order.png'); ?>" alt=""/> {{Attribute Groups}}</h1>

			<div class="buttons">
				<a onclick="$('#form').submit();" class="button">{{Save}}</a>
				<a href="<?= $cancel; ?>" class="button">{{Cancel}}</a>
			</div>
		</div>

		<div class="section">
			<form action="<?= $save; ?>" method="post" id="form">
				<table class="form">
					<tr>
						<td class="required"> {{Attribute Group Name:}}</td>
						<td><input type="text" name="name" value="<?= $name; ?>"/></td>
					</tr>
					<tr>
						<td>{{Sort Order:}}</td>
						<td><input type="text" name="sort_order" value="<?= $sort_order; ?>" size="1"/></td>
					</tr>
					<tr>
						<td>{{Attributes:}}</td>
						<td>
							<table class="list">
								<thead>
									<tr>
										<td class="center">{{Attribute Name}}</td>
										<td class="center">{{Sort Order}}</td>
										<td></td>
									</tr>
								</thead>
								<tbody id="attribute_list">
									<? foreach ($attributes as $row => $attribute) { ?>
										<tr class="attribute" data-row="<?= $row; ?>">
											<td class="center">
												<input type="hidden" name="attributes[<?= $row; ?>][attribute_id]" value="<?= $row; ?>"/>
												<input type="text" name="attributes[<?= $row; ?>][name]" value="<?= $attribute['name']; ?>"/>
											</td>
											<td class="center">
												<div class="image">
													<input type="text" class="imageinput" name="attributes[<?= $row; ?>][image]" value="<?= $attribute['image']; ?>" />
												</div>
											</td>
											<td class="center">
												<input type="text" class="sort_order" name="attributes[<?= $row; ?>][sort_order]" value="<?= $attribute['sort_order']; ?>"/>
											</td>
											<td class="center">
												<? if (!empty($attribute['product_count'])) { ?>
													<span class="product_count"><?= $attribute['product_count']; ?></span>
												<? } else { ?>
													<a class="button" onclick="$(this).closest('.attribute').remove()">{{Remove}}</a>
												<? } ?>
											</td>
										</tr>
									<? } ?>

								</tbody>
								<tfoot>
									<tr>
										<td><a id="add_attribute" class="button">{{Add Attribute}}</a></td>
									</tr>
								</tfoot>
							</table>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	var a_list = $('#attribute_list');
	a_list.ac_template('a_list');

	$('#add_attribute').click(function () {
		var a = $.ac_template('a_list', 'add');
		a.find('.imageinput').ac_imageinput();

		a_list.update_index('.sort_order');
	});

	a_list.sortable({cursor: 'move', stop: function () {
		$(this).update_index('.sort_order');
	}});

	$('.imageinput').ac_imageinput();
</script>

<?= $is_ajax ? '' : call('admin/footer'); ?>
