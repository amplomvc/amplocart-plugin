<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/shipping.png'); ?>" alt=""/> {{Manufacturer}}</h1>

			<div class="buttons">
				<a onclick="$('#form').submit();" class="button">{{Save}}</a>
				<a href="<?= $cancel; ?>" class="button">{{Cancel}}</a>
			</div>
		</div>
		<div class="section">
			<div id="tabs" class="htabs">
				<a href="#tab-general">{{General}}</a>
			</div>
			<form action="<?= $action; ?>" method="post" id="form">
				<div id="tab-general">
					<table class="form">
						<tr>
							<td class="required"> {{Manufacturer Name:}}</td>
							<td><input type="text" name="name" value="<?= $name; ?>" size="100"/>
							</td>
						</tr>
						<tr>
							<td class="required">
								<div>{{SEO Url:}}</div>
								<span class="help">{{The Search Engine Optimized URL for this page.}}</span>
							</td>
							<td>
								<input type="text" onfocus="generate_url_warning(this)" name="keyword" value="<?= $keyword; ?>"/>
								<a class="gen_url" onclick="generate_url(this)">{{[Generate URL]}}</a>
							</td>
						</tr>
						<tr>
							<td>{{Teaser:}}</td>
							<td><input type="text" name="teaser" value="<?= $teaser; ?>" size="80"/></td>
						</tr>
						<tr>
							<td>{{Description:}}</td>
							<td><textarea name="description" class="ckedit"/><?= $description; ?></textarea></td>
						</tr>
						<tr>
							<td>{{Default Shipping Return Policy}}</td>
							<td><textarea name="shipping_return" class="ckedit"/><?= $shipping_return; ?></textarea></td>
						</tr>
						<tr>
							<td>{{Stores:}}</td>
							<td>
								<?= build(array(
									'type' => 'multiselect',
									'name'  => "stores",
									'data'   => $data_stores,
									'select' => $stores,
									'value' => 'store_id',
									'label' => 'name',
								)); ?>
							</td>
						</tr>
						<tr>
							<td>{{Image:}}</td>
							<td>
								<input type="text" class="imageinput" name="image" value="<?= $image; ?>" />
							</td>
						</tr>
						<tr>
							<td>{{Active On:}}</td>
							<td><input type="text" class="datetimepicker" name="date_active" value="<?= $date_active; ?>"/>
							</td>
						</tr>
						<tr>
							<td><?= _l("Expires On <span class=\"help\">Leave blank if this Manufacturer does not expire</span>:"); ?></td>
							<td><input type="text" class="datetimepicker" name="date_expires" value="<?= $date_expires; ?>"/></td>
						</tr>
						<tr>
							<td>{{Sort Order:}}</td>
							<td><input type="text" name="sort_order" value="<?= $sort_order; ?>" size="1"/></td>
						</tr>
						<tr>
							<td>{{Status:}}</td>
							<td><?= build(array(
	'type' => 'select',
	'name'  => 'status',
	'data'   => $data_statuses,
	'select' => $status
)); ?></td>
						</tr>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.datetimepicker').ac_datepicker();

	function generate_url_warning(field) {
		if ($('#gen_warn').length == 0)
			$(field).parent().append('<span id="gen_warn" style="color:red">{{Warning! This may cause system instability! Please use the \\\'Generate URL\\\' button}}</span>');
	}
	function generate_url(c) {
		$(c).fadeOut(500, function () {
			$(this).show();
		});
		$('#gen_warn').remove();
		name = $('input[name="name"]').val();
		if (!name)
			alert("Please make a name for this Manufacturer before generating the URL");
		$.post("<?= $url_generate_url; ?>", {manufacturer_id:<?= $manufacturer_id?$manufacturer_id:0; ?>, name: name}, function (json) {
			$('input[name="keyword"]').val(json);
		}, 'json');
	}

	$('.imageinput').ac_imageinput();

	$('#tabs a').tabs();
</script>

<?= $is_ajax ? '' : call('admin/footer'); ?>
