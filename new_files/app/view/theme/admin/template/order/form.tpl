<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>

	<div class="box">
		<form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form">
			<div class="heading">
				<h1>
					<img src="<?= theme_url('image/order.png'); ?>" alt=""/> {{Orders}}</h1>

				<div class="buttons">
					<a onclick="$('#form').submit();" class="button">{{Save}}</a>
					<a href="<?= $cancel; ?>" class="button">{{Cancel}}</a>
				</div>
			</div>
			<div class="section">
				<div id="vtabs" class="vtabs">
					<a href="#tab-customer">{{Customer Details}}</a>
					<a href="#tab-payment">{{Payment Details}}</a>
					<a href="#tab-shipping">{{Shipping Details}}</a>
					<a href="#tab-product">{{Products}}</a>
					<a href="#tab-voucher">{{Vouchers}}</a>
					<a href="#tab-total">{{Totals}}</a>
				</div>

				<div id="tab-customer" class="vtabs-content">
					<table class="form">
						<tr>
							<td class="left">{{Store:}}</td>
							<td class="left">
								<?=
								build(array(
									'type' => 'select',
									'name'  => "store_id",
									'data'   => $data_stores,
									'select' => $store_id,
									'value' => 'store_id',
									'label' => 'name',
								)); ?>
							</td>
						</tr>
						<tr>
							<td>{{Customer:}}</td>
							<td>
								<input type="text" name="customer" value="<?= $customer; ?>"/>
								<input type="hidden" name="customer_id" value="<?= $customer_id; ?>"/>
							</td>
						</tr>
						<tr>
							<td class="required"> {{First Name:}}</td>
							<td><input type="text" name="first_name" value="<?= $first_name; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{Last Name:}}</td>
							<td><input type="text" name="last_name" value="<?= $last_name; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{E-Mail:}}</td>
							<td><input type="text" name="email" value="<?= $email; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{Telephone:}}</td>
							<td><input type="text" name="phone" value="<?= $phone; ?>"/></td>
						</tr>
						<tr>
							<td>{{Fax:}}</td>
							<td><input type="text" name="fax" value="<?= $fax; ?>"/></td>
						</tr>
					</table>
				</div>
				<div id="tab-payment" class="vtabs-content">
					<table class="form">
						<tr>
							<td>{{Choose Address:}}</td>
							<td>
								<select name="payment_address">
									<option value="0" selected="selected">{{ --- None --- }}</option>
									<? foreach ($addresses as $address) { ?>
										<option value="<?= $address['address_id']; ?>"><?= $address['first_name'] . ' ' . $address['last_name'] . ', ' . $address['address'] . ', ' . $address['city'] . ', ' . $address['country']; ?></option>
									<? } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="required"> {{First Name:}}</td>
							<td><input type="text" name="payment_first_name" value="<?= $payment_first_name; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{Last Name:}}</td>
							<td><input type="text" name="payment_last_name" value="<?= $payment_last_name; ?>"/></td>
						</tr>
						<tr>
							<td>{{Company:}}</td>
							<td><input type="text" name="payment_company" value="<?= $payment_company; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{Address 1:}}</td>
							<td><input type="text" name="payment_address" value="<?= $payment_address; ?>"/></td>
						</tr>
						<tr>
							<td>{{Address 2:}}</td>
							<td><input type="text" name="payment_address_2" value="<?= $payment_address_2; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{City:}}</td>
							<td><input type="text" name="payment_city" value="<?= $payment_city; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{Postcode:}}</td>
							<td><input type="text" name="payment_postcode" value="<?= $payment_postcode; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{Country:}}</td>
							<td>
								<?=
								build(array(
									'type' => 'select',
									'name'  => 'payment_country_id',
									'data'   => $countries,
									'select' => $payment_country_id,
									'value' => 'country_id',
									'label' => 'name',
								)); ?>
							</td>
						</tr>
						<tr>
							<td class="required"> {{Region / State:}}</td>
							<td>
								<select name="payment_zone_id" class="zone_select" data-zone_id="<?= $payment_zone_id; ?>"></select>
							</td>
						</tr>
					</table>
				</div>
				<div id="tab-shipping" class="vtabs-content">
					<table class="form">
						<tr>
							<td>{{Choose Address:}}</td>
							<td>
								<select name="shipping_address">
									<option value="0" selected="selected">{{ --- None --- }}</option>
									<? foreach ($addresses as $address) { ?>
										<option value="<?= $address['address_id']; ?>"><?= $address['first_name'] . ' ' . $address['last_name'] . ', ' . $address['address'] . ', ' . $address['city'] . ', ' . $address['country']; ?></option>
									<? } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="required"> {{First Name:}}</td>
							<td><input type="text" name="shipping_first_name" value="<?= $shipping_first_name; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{Last Name:}}</td>
							<td><input type="text" name="shipping_last_name" value="<?= $shipping_last_name; ?>"/></td>
						</tr>
						<tr>
							<td>{{Company:}}</td>
							<td><input type="text" name="shipping_company" value="<?= $shipping_company; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{Address 1:}}</td>
							<td><input type="text" name="shipping_address" value="<?= $shipping_address; ?>"/></td>
						</tr>
						<tr>
							<td>{{Address 2:}}</td>
							<td><input type="text" name="shipping_address_2" value="<?= $shipping_address_2; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{City:}}</td>
							<td><input type="text" name="shipping_city" value="<?= $shipping_city; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{Postcode:}}</td>
							<td><input type="text" name="shipping_postcode" value="<?= $shipping_postcode; ?>"/></td>
						</tr>
						<tr>
							<td class="required"> {{Country:}}</td>
							<td>
								<?=
								build(array(
									'type' => 'select',
									'name'  => 'shipping_country_id',
									'data'   => $countries,
									'select' => $shipping_country_id,
									'value' => 'country_id',
									'label' => 'name',
								)); ?>
							</td>
						</tr>
						<tr>
							<td class="required"> {{Region / State:}}</td>
							<td>
								<select name="shipping_zone_id" data-zone_id="<?= $shipping_zone_id; ?>" class="zone_select"></select>
							</td>
						</tr>
					</table>
				</div>
				<div id="tab-product" class="vtabs-content">
					<table class="list">
						<thead>
						<tr>
							<td></td>
							<td class="left">{{Product}}</td>
							<td class="left">{{Model}}</td>
							<td class="right">{{Quantity}}</td>
							<td class="right">{{Unit Price}}</td>
							<td class="right">{{Total}}</td>
						</tr>
						</thead>
						<? $product_row = 0; ?>
						<? $option_row = 0; ?>
						<? $download_row = 0; ?>
						<tbody id="product">
						<? if ($order_products) { ?>
							<? foreach ($order_products as $order_product) { ?>
								<tr id="product-row<?= $product_row; ?>">
									<td class="center" style="width: 3px;">
										<img src="<?= theme_url('image/delete.png') ?>"
											title="{{Remove}}"
											alt="{{Remove}}" style="cursor: pointer;" onclick="$('#product-row<?= $product_row; ?>').remove(); $('#button-update').trigger('click');"/>
									</td>
									<td class="left"><?= $order_product['name']; ?><br/>
										<input type="hidden" name="order_product[<?= $product_row; ?>][order_product_id]" value="<?= $order_product['order_product_id']; ?>"/>
										<input type="hidden" name="order_product[<?= $product_row; ?>][product_id]" value="<?= $order_product['product_id']; ?>"/>
										<input type="hidden" name="order_product[<?= $product_row; ?>][name]" value="<?= $order_product['name']; ?>"/>
										<? foreach ($order_product['option'] as $option) { ?>
											-
											<small><?= $option['name']; ?>: <?= $option['value']; ?></small><br/>
											<input type="hidden" name="order_product[<?= $product_row; ?>][order_option][<?= $option_row; ?>][order_option_id]" value="<?= $option['order_option_id']; ?>"/>
											<input type="hidden" name="order_product[<?= $product_row; ?>][order_option][<?= $option_row; ?>][product_option_id]" value="<?= $option['product_option_id']; ?>"/>
											<input type="hidden" name="order_product[<?= $product_row; ?>][order_option][<?= $option_row; ?>][product_option_value_id]" value="<?= $option['product_option_value_id']; ?>"/>
											<input type="hidden" name="order_product[<?= $product_row; ?>][order_option][<?= $option_row; ?>][name]" value="<?= $option['name']; ?>"/>
											<input type="hidden" name="order_product[<?= $product_row; ?>][order_option][<?= $option_row; ?>][value]" value="<?= $option['value']; ?>"/>
											<input type="hidden" name="order_product[<?= $product_row; ?>][order_option][<?= $option_row; ?>][type]" value="<?= $option['type']; ?>"/>
											<? $option_row++; ?>
										<? } ?>
										<? foreach ($order_product['download'] as $download) { ?>
											<input type="hidden" name="order_product[<?= $product_row; ?>][order_download][<?= $download_row; ?>][order_download_id]" value="<?= $download['order_download_id']; ?>"/>
											<input type="hidden" name="order_product[<?= $product_row; ?>][order_download][<?= $download_row; ?>][name]" value="<?= $download['name']; ?>"/>
											<input type="hidden" name="order_product[<?= $product_row; ?>][order_download][<?= $download_row; ?>][filename]" value="<?= $download['filename']; ?>"/>
											<input type="hidden" name="order_product[<?= $product_row; ?>][order_download][<?= $download_row; ?>][mask]" value="<?= $download['mask']; ?>"/>
											<input type="hidden" name="order_product[<?= $product_row; ?>][order_download][<?= $download_row; ?>][remaining]" value="<?= $download['remaining']; ?>"/>
											<? $download_row++; ?>
										<? } ?></td>
									<td class="left"><?= $order_product['model']; ?>
										<input type="hidden" name="order_product[<?= $product_row; ?>][model]" value="<?= $order_product['model']; ?>"/>
									</td>
									<td class="right"><?= $order_product['quantity']; ?>
										<input type="hidden" name="order_product[<?= $product_row; ?>][quantity]" value="<?= $order_product['quantity']; ?>"/>
									</td>
									<td class="right"><?= $order_product['price']; ?>
										<input type="hidden" name="order_product[<?= $product_row; ?>][price]" value="<?= $order_product['price']; ?>"/>
									</td>
									<td class="right"><?= $order_product['total']; ?>
										<input type="hidden" name="order_product[<?= $product_row; ?>][total]" value="<?= $order_product['total']; ?>"/>
										<input type="hidden" name="order_product[<?= $product_row; ?>][tax]" value="<?= $order_product['tax']; ?>"/>
										<input type="hidden" name="order_product[<?= $product_row; ?>][reward]" value="<?= $order_product['reward']; ?>"/>
									</td>
								</tr>
								<? $product_row++; ?>
							<? } ?>
						<? } else { ?>
							<tr>
								<td class="center" colspan="6">{{No results!}}</td>
							</tr>
						<? } ?>
						</tbody>
					</table>
					<table class="list">
						<thead>
						<tr>
							<td colspan="2" class="left">{{Add Product(s)}}</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td class="left">{{Choose Product:}}</td>
							<td class="left"><input type="text" name="product" value=""/>
								<input type="hidden" name="product_id" value=""/></td>
						</tr>
						<tr id="option"></tr>
						<tr>
							<td class="left">{{Quantity:}}</td>
							<td class="left"><input type="text" name="quantity" value="1"/></td>
						</tr>
						</tbody>
						<tfoot>
						<tr>
							<td class="left">&nbsp;</td>
							<td class="left">
								<a id="button-product" class="button">{{Add Product}}</a>
							</td>
						</tr>
						</tfoot>
					</table>
				</div>
				<div id="tab-voucher" class="vtabs-content">
					<table class="list">
						<thead>
						<tr>
							<td></td>
							<td class="left">{{Product}}</td>
							<td class="left">{{Model}}</td>
							<td class="right">{{Quantity}}</td>
							<td class="right">{{Unit Price}}</td>
							<td class="right">{{Total}}</td>
						</tr>
						</thead>
						<tbody id="voucher">
						<? $voucher_row = 0; ?>
						<? if ($order_vouchers) { ?>
							<? foreach ($order_vouchers as $order_voucher) { ?>
								<tr id="voucher-row<?= $voucher_row; ?>">
									<td class="center" style="width: 3px;">
										<img src="<?= theme_url('image/loading.gif') ?>"
											title="{{Remove}}"
											alt="{{Remove}}" style="cursor: pointer;" onclick="$('#voucher-row<?= $voucher_row; ?>').remove(); $('#button-update').trigger('click');"/>
									</td>
									<td class="left"><?= $order_voucher['description']; ?>
										<input type="hidden" name="order_voucher[<?= $voucher_row; ?>][order_voucher_id]" value="<?= $order_voucher['order_voucher_id']; ?>"/>
										<input type="hidden" name="order_voucher[<?= $voucher_row; ?>][voucher_id]" value="<?= $order_voucher['voucher_id']; ?>"/>
										<input type="hidden" name="order_voucher[<?= $voucher_row; ?>][description]" value="<?= $order_voucher['description']; ?>"/>
										<input type="hidden" name="order_voucher[<?= $voucher_row; ?>][code]" value="<?= $order_voucher['code']; ?>"/>
										<input type="hidden" name="order_voucher[<?= $voucher_row; ?>][from_name]" value="<?= $order_voucher['from_name']; ?>"/>
										<input type="hidden" name="order_voucher[<?= $voucher_row; ?>][from_email]" value="<?= $order_voucher['from_email']; ?>"/>
										<input type="hidden" name="order_voucher[<?= $voucher_row; ?>][to_name]" value="<?= $order_voucher['to_name']; ?>"/>
										<input type="hidden" name="order_voucher[<?= $voucher_row; ?>][to_email]" value="<?= $order_voucher['to_email']; ?>"/>
										<input type="hidden" name="order_voucher[<?= $voucher_row; ?>][voucher_theme_id]" value="<?= $order_voucher['voucher_theme_id']; ?>"/>
										<input type="hidden" name="order_voucher[<?= $voucher_row; ?>][message]" value="<?= $order_voucher['message']; ?>"/>
										<input type="hidden" name="order_voucher[<?= $voucher_row; ?>][amount]" value="<?= $order_voucher['amount']; ?>"/>
									</td>
									<td class="left"></td>
									<td class="right">1</td>
									<td class="right"><?= $order_voucher['amount']; ?></td>
									<td class="right"><?= $order_voucher['amount']; ?></td>
								</tr>
								<? $voucher_row++; ?>
							<? } ?>
						<? } else { ?>
							<tr>
								<td class="center" colspan="6">{{No results!}}</td>
							</tr>
						<? } ?>
						</tbody>
					</table>
					<table class="list">
						<thead>
						<tr>
							<td colspan="2" class="left">{{Add Voucher(s)}}</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td class="left">
								<span class="required"></span> {{Recipient\'s Name:}}</td>
							<td class="left"><input type="text" name="to_name" value=""/></td>
						</tr>
						<tr>
							<td class="left">
								<span class="required"></span> {{Recipient\'s Email:}}</td>
							<td class="left"><input type="text" name="to_email" value=""/></td>
						</tr>
						<tr>
							<td class="left">
								<span class="required"></span> {{Senders Name:}}</td>
							<td class="left"><input type="text" name="from_name" value=""/></td>
						</tr>
						<tr>
							<td class="left">
								<span class="required"></span> {{Senders Email:}}</td>
							<td class="left"><input type="text" name="from_email" value=""/></td>
						</tr>
						<tr>
							<td class="left">
								<span class="required"></span> {{Gift Certificate Theme:}}</td>
							<td class="left">
								<select name="voucher_theme_id">
									<? foreach ($voucher_themes as $voucher_theme) { ?>
										<option value="<?= $voucher_theme['voucher_theme_id']; ?>"><?= addslashes($voucher_theme['name']); ?></option>
									<? } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="left">{{Message:}}</td>
							<td class="left">
								<textarea name="message" cols="40" rows="5"></textarea>
							</td>
						</tr>
						<tr>
							<td class="left">
								<span class="required"></span> {{Amount:}}</td>
							<td class="left"><input type="text" name="amount" value="25.00" size="5"/></td>
						</tr>
						</tbody>
						<tfoot>
						<tr>
							<td class="left">&nbsp;</td>
							<td class="left">
								<a id="button-voucher" class="button">{{Add Voucher}}</a>
							</td>
						</tr>
						</tfoot>
					</table>
				</div>
				<div id="tab-total" class="vtabs-content">
					<table class="list">
						<thead>
						<tr>
							<td class="left">{{Product}}</td>
							<td class="left">{{Model}}</td>
							<td class="right">{{Quantity}}</td>
							<td class="right">{{Unit Price}}</td>
							<td class="right">{{Total}}</td>
						</tr>
						</thead>
						<tbody id="total">
						<? $total_row = 0; ?>
						<? if ($order_products || $order_vouchers || $order_totals) { ?>
							<? foreach ($order_products as $order_product) { ?>
								<tr>
									<td class="left"><?= $order_product['name']; ?><br/>
										<? foreach ($order_product['option'] as $option) { ?>
											-
											<small><?= $option['name']; ?>: <?= $option['value']; ?></small><br/>
										<? } ?></td>
									<td class="left"><?= $order_product['model']; ?></td>
									<td class="right"><?= $order_product['quantity']; ?></td>
									<td class="right"><?= $order_product['price']; ?></td>
									<td class="right"><?= $order_product['total']; ?></td>
								</tr>
							<? } ?>
							<? foreach ($order_vouchers as $order_voucher) { ?>
								<tr>
									<td class="left"><?= $order_voucher['description']; ?></td>
									<td class="left"></td>
									<td class="right">1</td>
									<td class="right"><?= $order_voucher['amount']; ?></td>
									<td class="right"><?= $order_voucher['amount']; ?></td>
								</tr>
							<? } ?>
							<? foreach ($order_totals as $order_total) { ?>
								<tr id="total-row<?= $total_row; ?>">
									<td class="right" colspan="4"><?= $order_total['title']; ?>:
										<input type="hidden" name="order_total[<?= $total_row; ?>][order_total_id]" value="<?= $order_total['order_total_id']; ?>"/>
										<input type="hidden" name="order_total[<?= $total_row; ?>][code]" value="<?= $order_total['code']; ?>"/>
										<input type="hidden" name="order_total[<?= $total_row; ?>][title]" value="<?= $order_total['title']; ?>"/>
										<input type="hidden" name="order_total[<?= $total_row; ?>][text]" value="<?= $order_total['text']; ?>"/>
										<input type="hidden" name="order_total[<?= $total_row; ?>][value]" value="<?= $order_total['value']; ?>"/>
										<input type="hidden" name="order_total[<?= $total_row; ?>][sort_order]" value="<?= $order_total['sort_order']; ?>"/>
									</td>
									<td class="right"><?= $order_total['value']; ?></td>
								</tr>
								<? $total_row++; ?>
							<? } ?>
						<? } else { ?>
							<tr>
								<td class="center" colspan="5">{{No results!}}</td>
							</tr>
						<? } ?>
						</tbody>
					</table>
					<table class="list">
						<thead>
						<tr>
							<td class="left" colspan="2">{{Order Details}}</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td class="left">{{Shipping Method:}}</td>
							<td class="left">
								<select name="shipping">
									<option value="">{{ --- Please Select --- }}</option>
									<? if ($shipping_code) { ?>
										<option value="<?= $shipping_code; ?>" selected="selected"><?= $shipping_method; ?></option>
									<? } ?>
								</select>
								<input type="hidden" name="shipping_method" value="<?= $shipping_method; ?>"/>
								<input type="hidden" name="shipping_code" value="<?= $shipping_code; ?>"/></td>
						</tr>
						<tr>
							<td class="left">{{Payment Method:}}</td>
							<td class="left">
								<select name="payment">
									<option value="">{{ --- Please Select --- }}</option>
									<? if ($payment_code) { ?>
										<option value="<?= $payment_code; ?>" selected="selected"><?= $payment_method; ?></option>
									<? } ?>
								</select>
								<input type="hidden" name="payment_method" value="<?= $payment_method; ?>"/>
								<input type="hidden" name="payment_code" value="<?= $payment_code; ?>"/></td>
						</tr>
						<tr>
							<td class="left">{{Coupon:}}</td>
							<td class="left"><input type="text" name="coupon" value=""/></td>
						</tr>
						<tr>
							<td class="left">{{Voucher:}}</td>
							<td class="left"><input type="text" name="voucher" value=""/></td>
						</tr>
						<tr>
							<td class="left">{{Reward:}}</td>
							<td class="left"><input type="text" name="reward" value=""/></td>
						</tr>
						<tr>
							<td class="left">{{Order Status:}}</td>
							<td class="left">
								<select name="order_status_id">
									<? foreach ($order_statuses as $order_status) { ?>
										<? if ($order_status['order_status_id'] == $order_status_id) { ?>
											<option value="<?= $order_status['order_status_id']; ?>"
												selected="selected"><?= $order_status['name']; ?></option>
										<? } else { ?>
											<option value="<?= $order_status['order_status_id']; ?>"><?= $order_status['name']; ?></option>
										<? } ?>
									<? } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="left">{{Comment:}}</td>
							<td class="left">
								<textarea name="comment" cols="40" rows="5"><?= $comment; ?></textarea>
							</td>
						</tr>
						</tbody>
						<tfoot>
						<tr>
							<td class="left">&nbsp;</td>
							<td class="left">
								<a id="button-update" class="button">{{Update Totals}}</a>
							</td>
						</tr>
						</tfoot>
					</table>
				</div>

			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$.widget('custom.catcomplete', $.ui.autocomplete, {
		_renderMenu: function (ul, items) {
			var self = this, currentCategory = '';

			$.each(items, function (index, item) {
				if (item['category'] != currentCategory) {
					ul.append('<li class="ui-autocomplete-category">' + item['category'] + '</li>');

					currentCategory = item['category'];
				}

				self._renderItem(ul, item);
			});
		}
	});

	$('input[name=\'customer\']').catcomplete({
		delay:  0,
		source: function (request, response) {
			$.ajax({
				url:     "<?= $url_autocomplete; ?>" + '&filter_name=" + encodeURIComponent(request.term),
				dataType: "json',
				success: function (json) {
					response($.map(json, function (item) {
						return {
							category:          item['customer_group'],
							label:             item['name'],
							value:             item['customer_id'],
							first_name:         item['first_name'],
							last_name:          item['last_name'],
							email:             item['email'],
							phone:         item['phone'],
							fax:               item['fax'],
							address:           item['address']
						}
					}));
				}
			});
		},
		select: function (event, ui) {
			$('input[name=\'customer\']').attr('value', ui.item['label']);
			$('input[name=\'customer_id\']').attr('value', ui.item['value']);
			$('input[name=\'customer_group_id\']').attr('value', ui.item['customer_group_id']);
			$('input[name=\'first_name\']').attr('value', ui.item['first_name']);
			$('input[name=\'last_name\']').attr('value', ui.item['last_name']);
			$('input[name=\'email\']').attr('value', ui.item['email']);
			$('input[name=\'phone\']').attr('value', ui.item['phone']);
			$('input[name=\'fax\']').attr('value', ui.item['fax']);

			html = '<option value="0">{{ --- None --- }}</option>';

			for (i = 0; i < ui.item['address'].length; i++) {
				html += '<option value="' + ui.item['address'][i]['address_id'] + '">' + ui.item['address'][i]['first_name'] + ' ' + ui.item['address'][i]['last_name'] + ', ' + ui.item['address'][i]['address'] + ', ' + ui.item['address'][i]['city'] + ', ' + ui.item['address'][i]['country'] + '</option>';
			}

			$('select[name=\'shipping_address\']').html(html);
			$('select[name=\'payment_address\']').html(html);

			return false;
		}
	});

	$('select[name=\'payment_address\']').bind('change', function () {
		$.ajax({
			url:     "<?= URL_SITE . "admin/index.php?route=sale/customer/address"; ?>" + '&address_id=" + this.value,
			dataType: "json',
			success: function (json) {
				if (json != '') {
					$('input[name=\'payment_first_name\']').attr('value', json['first_name']);
					$('input[name=\'payment_last_name\']').attr('value', json['last_name']);
					$('input[name=\'payment_company\']').attr('value', json['company']);
					$('input[name=\'payment_address\']').attr('value', json['address']);
					$('input[name=\'payment_address_2\']').attr('value', json['address_2']);
					$('input[name=\'payment_city\']').attr('value', json['city']);
					$('input[name=\'payment_postcode\']').attr('value', json['postcode']);
					$('select[name=\'payment_country_id\']').attr('value', json['country_id']);
					$('select[name=\'payment_zone_id\']').load("<?= URL_SITE . "admin/index.php?route=tool/data/load_zones"; ?>" + '&country_id=" + json["country_id'
				]
					+'&zone_id=" + json["zone_id'
				])
					;
				}
			}
		});
	});

	$('select[name=\'shipping_address\']').bind('change', function () {
		$.ajax({
			url:     "<?= URL_SITE . "admin/index.php?route=sale/customer/address"; ?>" + '&address_id=" + this.value,
			dataType: "json',
			success: function (json) {
				if (json != '') {
					$('input[name=\'shipping_first_name\']').attr('value', json['first_name']);
					$('input[name=\'shipping_last_name\']').attr('value', json['last_name']);
					$('input[name=\'shipping_company\']').attr('value', json['company']);
					$('input[name=\'shipping_address\']').attr('value', json['address']);
					$('input[name=\'shipping_address_2\']').attr('value', json['address_2']);
					$('input[name=\'shipping_city\']').attr('value', json['city']);
					$('input[name=\'shipping_postcode\']').attr('value', json['postcode']);
					$('select[name=\'shipping_country_id\']').attr('value', json['country_id']);
					$('select[name=\'shipping_zone_id\']').load("<?= URL_SITE . "admin/index.php?route=tool/data/load_zones"; ?>" + '&country_id=" + json["country_id'
				]
					+'&zone_id=" + json["zone_id'
				])
					;
				}
			}
		});
	});
</script>

<script type="text/javascript">
	$('.table.form .zone_select').ac_zoneselect({listen: '.table.form .country_select'});

	$('input[name=\'product\']').autocomplete({
		delay:  0,
		source: function (request, response) {
			$.ajax({
				url:     "<?= URL_SITE . "admin/index.php?route=catalog/product/autocomplete"; ?>" + '&filter_name=" + encodeURIComponent(request.term),
				dataType: "json',
				success: function (json) {
					response($.map(json, function (item) {
						return {
							label:  item.name,
							value:  item.product_id,
							model:  item.model,
							option: item.option,
							price:  item.price
						}
					}));
				}
			});
		},
		select: function (event, ui) {
			$('input[name=\'product\']').attr('value', ui.item['label']);
			$('input[name=\'product_id\']').attr('value', ui.item['value']);

			if (ui.item['option'] != '') {
				html = '';

				for (i = 0; i < ui.item['option'].length; i++) {
					option = ui.item['option'][i];

					if (option['type'] == 'select') {
						html += '<div id="option-' + option['product_option_id'] + '">';

						if (option['required']) {
							html += '<span class="required"></span> ';
						}

						html += option['name'] + '<br />';
						html += '<select name="option[' + option['product_option_id'] + ']">';
						html += '<option value="">{{ --- Please Select --- }}</option>';

						for (j = 0; j < option['option_value'].length; j++) {
							option_value = option['option_value'][j];

							html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['name'];

							if (option_value['price']) {
								html += ' (' + option_value['price'] + ')';
							}

							html += '</option>';
						}

						html += '</select>';
						html += '</div>';
						html += '<br />';
					}

					if (option['type'] == 'radio') {
						html += '<div id="option-' + option['product_option_id'] + '">';

						if (option['required']) {
							html += '<span class="required"></span> ';
						}

						html += option['name'] + '<br />';
						html += '<select name="option[' + option['product_option_id'] + ']">';
						html += '<option value="">{{ --- Please Select --- }}</option>';

						for (j = 0; j < option['option_value'].length; j++) {
							option_value = option['option_value'][j];

							html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['name'];

							if (option_value['price']) {
								html += ' (' + option_value['price'] + ')';
							}

							html += '</option>';
						}

						html += '</select>';
						html += '</div>';
						html += '<br />';
					}

					if (option['type'] == 'checkbox') {
						html += '<div id="option-' + option['product_option_id'] + '">';

						if (option['required']) {
							html += '<span class="required"></span> ';
						}

						html += option['name'] + '<br />';

						for (j = 0; j < option['option_value'].length; j++) {
							option_value = option['option_value'][j];

							html += '<input type="checkbox" name="option[' + option['product_option_id'] + '][]" value="' + option_value['product_option_value_id'] + '" id="option-value-' + option_value['product_option_value_id'] + '" />';
							html += '<label for="option-value-' + option_value['product_option_value_id'] + '">' + option_value['name'];

							if (option_value['price']) {
								html += ' (' + option_value['price'] + ')';
							}

							html += '</label>';
							html += '<br />';
						}

						html += '</div>';
						html += '<br />';
					}

					if (option['type'] == 'image') {
						html += '<div id="option-' + option['product_option_id'] + '">';

						if (option['required']) {
							html += '<span class="required"></span> ';
						}

						html += option['name'] + '<br />';
						html += '<select name="option[' + option['product_option_id'] + ']">';
						html += '<option value="">{{ --- Please Select --- }}</option>';

						for (j = 0; j < option['option_value'].length; j++) {
							option_value = option['option_value'][j];

							html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['name'];

							if (option_value['price']) {
								html += ' (' + option_value['price'] + ')';
							}

							html += '</option>';
						}

						html += '</select>';
						html += '</div>';
						html += '<br />';
					}

					if (option['type'] == 'text') {
						html += '<div id="option-' + option['product_option_id'] + '">';

						if (option['required']) {
							html += '<span class="required"></span> ';
						}

						html += option['name'] + '<br />';
						html += '<input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['option_value'] + '" />';
						html += '</div>';
						html += '<br />';
					}

					if (option['type'] == 'textarea') {
						html += '<div id="option-' + option['product_option_id'] + '">';

						if (option['required']) {
							html += '<span class="required"></span> ';
						}

						html += option['name'] + '<br />';
						html += '<textarea name="option[' + option['product_option_id'] + ']" cols="40" rows="5">' + option['option_value'] + '</textarea>';
						html += '</div>';
						html += '<br />';
					}

					if (option['type'] == 'file') {
						html += '<div id="option-' + option['product_option_id'] + '">';

						if (option['required']) {
							html += '<span class="required"></span> ';
						}

						html += option['name'] + '<br />';
						html += '<a id="button-option-' + option['product_option_id'] + '" class="button">{{Upload}}</a>';
						html += '<input type="hidden" name="option[' + option['product_option_id'] + ']" value="' + option['option_value'] + '" />';
						html += '</div>';
						html += '<br />';
					}

					if (option['type'] == 'date') {
						html += '<div id="option-' + option['product_option_id'] + '">';

						if (option['required']) {
							html += '<span class="required"></span> ';
						}

						html += option['name'] + '<br />';
						html += '<input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['option_value'] + '" class="datepicker" />';
						html += '</div>';
						html += '<br />';
					}

					if (option['type'] == 'datetime') {
						html += '<div id="option-' + option['product_option_id'] + '">';

						if (option['required']) {
							html += '<span class="required"></span> ';
						}

						html += option['name'] + '<br />';
						html += '<input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['option_value'] + '" class="datetimepicker" />';
						html += '</div>';
						html += '<br />';
					}

					if (option['type'] == 'time') {
						html += '<div id="option-' + option['product_option_id'] + '">';

						if (option['required']) {
							html += '<span class="required"></span> ';
						}

						html += option['name'] + '<br />';
						html += '<input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['option_value'] + '" class="time" />';
						html += '</div>';
						html += '<br />';
					}
				}

				$('#option').html('<td class="left">{{Choose Option(s):}}</td><td class="left">' + html + '</td>');

				for (i = 0; i < ui.item.option.length; i++) {
					option = ui.item.option[i];

					if (option['type'] == 'file') {
						new AjaxUpload('#button-option-' + option['product_option_id'], {
							action:       "<?= URL_SITE . "admin/index.php?route=sale/order/upload"; ?>",
							name:         'file',
							autoSubmit:   true,
							responseType: 'json',
							data:         option,
							onSubmit:     function (file, extension) {
								$('#button-option-' + (this._settings.data['product_option_id'] + '-' + this._settings.data['product_option_id'])).after('<img src="<?= theme_url('image/loading.gif')?>" class="loading" />');
							},
							onComplete:   function (file, json) {

								$('.error').remove();

								if (json['success']) {
									alert(json['success']);

									$('input[name=\'option[' + this._settings.data['product_option_id'] + ']\']').attr('value', json['file']);
								}

								if (json.error) {
									$('#option-' + this._settings.data['product_option_id']).after('<span class="error">' + json['error'] + '</span>');
								}

								$('.loading').remove();
							}
						});
					}
				}

				$('.date').datepicker({dateFormat: 'yy-mm-dd'});
				$('.datetime').datetimepicker({
					dateFormat: 'yy-mm-dd',
					timeFormat: 'h:m'
				});
				$('.time').timepicker({timeFormat: 'h:m'});
			} else {
				$('#option td').remove();
			}

			return false;
		}
	});
</script>
<script type="text/javascript">
	$('select[name=\'payment\']').bind('change', function () {
		if (this.value) {
			$('input[name=\'payment_method\']').attr('value', $('select[name=\'payment\'] option:selected').text());
		} else {
			$('input[name=\'payment_method\']').attr('value', '');
		}

		$('input[name=\'payment_code\']').attr('value', this.value);
	});

	$('select[name=\'shipping\']').bind('change', function () {
		if (this.value) {
			$('input[name=\'shipping_method\']').attr('value', $('select[name=\'shipping\'] option:selected').text());
		} else {
			$('input[name=\'shipping_method\']').attr('value', '');
		}

		$('input[name=\'shipping_code\']').attr('value', this.value);
	});
</script>
<script type="text/javascript">
	$('#button-product, #button-voucher, #button-update').live('click', function () {
		data = '#tab-customer input[type=\'text\'], #tab-customer input[type=\'hidden\'], #tab-customer input[type=\'radio\']:checked, #tab-customer input[type=\'checkbox\']:checked, #tab-customer select, #tab-customer textarea, ';
		data += '#tab-payment input[type=\'text\'], #tab-payment input[type=\'hidden\'], #tab-payment input[type=\'radio\']:checked, #tab-payment input[type=\'checkbox\']:checked, #tab-payment select, #tab-payment textarea, ';
		data += '#tab-shipping input[type=\'text\'], #tab-shipping input[type=\'hidden\'], #tab-shipping input[type=\'radio\']:checked, #tab-shipping input[type=\'checkbox\']:checked, #tab-shipping select, #tab-shipping textarea, ';

		if ($(this).attr('id') == 'button-product') {
			data += '#tab-product input[type=\'text\'], #tab-product input[type=\'hidden\'], #tab-product input[type=\'radio\']:checked, #tab-product input[type=\'checkbox\']:checked, #tab-product select, #tab-product textarea, ';
		} else {
			data += '#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea, ';
		}

		if ($(this).attr('id') == 'button-voucher') {
			data += '#tab-voucher input[type=\'text\'], #tab-voucher input[type=\'hidden\'], #tab-voucher input[type=\'radio\']:checked, #tab-voucher input[type=\'checkbox\']:checked, #tab-voucher select, #tab-voucher textarea, ';
		} else {
			data += '#voucher input[type=\'text\'], #voucher input[type=\'hidden\'], #voucher input[type=\'radio\']:checked, #voucher input[type=\'checkbox\']:checked, #voucher select, #voucher textarea, ';
		}

		data += '#tab-total input[type=\'text\'], #tab-total input[type=\'hidden\'], #tab-total input[type=\'radio\']:checked, #tab-total input[type=\'checkbox\']:checked, #tab-total select, #tab-total textarea';

		$.ajax({
			url:        '<?= $store_url; ?>index.php?route=checkout/manual',
			type:       'post',
			data:       $(data),
			dataType:   'json',
			beforeSend: function () {
				$('.success, .warning, .attention, .error').remove();

				$('.box').before('<div class="attention"><img src="<?= theme_url('image/delete.png'); ?>" alt="" /> {{Please Wait!}}</div>');
			},
			success:    function (json) {
				$('.success, .warning, .attention, .error').remove();

				// Check for errors
				if (json['error']) {
					if (json['error']['warning']) {
						$('.box').before('<div class="message warning">' + json['error']['warning'] + '</div>');
					}

					// Order Details
					if (json['error']['customer']) {
						$('.box').before('<span class="error">' + json['error']['customer'] + '</span>');
					}

					if (json['error']['first_name']) {
						$('input[name=\'first_name\']').after('<span class="error">' + json['error']['first_name'] + '</span>');
					}

					if (json['error']['last_name']) {
						$('input[name=\'last_name\']').after('<span class="error">' + json['error']['last_name'] + '</span>');
					}

					if (json['error']['email']) {
						$('input[name=\'email\']').after('<span class="error">' + json['error']['email'] + '</span>');
					}

					if (json['error']['phone']) {
						$('input[name=\'phone\']').after('<span class="error">' + json['error']['phone'] + '</span>');
					}

					// Payment Address
					if (json['error']['payment']) {
						if (json['error']['payment']['first_name']) {
							$('input[name=\'payment_first_name\']').after('<span class="error">' + json['error']['payment']['first_name'] + '</span>');
						}

						if (json['error']['payment']['last_name']) {
							$('input[name=\'payment_last_name\']').after('<span class="error">' + json['error']['payment']['last_name'] + '</span>');
						}

						if (json['error']['payment']['address']) {
							$('input[name=\'payment_address\']').after('<span class="error">' + json['error']['payment']['address'] + '</span>');
						}

						if (json['error']['payment']['city']) {
							$('input[name=\'payment_city\']').after('<span class="error">' + json['error']['payment']['city'] + '</span>');
						}

						if (json['error']['payment']['country']) {
							$('select[name=\'payment_country_id\']').after('<span class="error">' + json['error']['payment']['country'] + '</span>');
						}

						if (json['error']['payment']['zone']) {
							$('select[name=\'payment_zone_id\']').after('<span class="error">' + json['error']['payment']['zone'] + '</span>');
						}

						if (json['error']['payment']['postcode']) {
							$('input[name=\'payment_postcode\']').after('<span class="error">' + json['error']['payment']['postcode'] + '</span>');
						}
					}

					// Shipping	Address
					if (json['error']['shipping']) {
						if (json['error']['shipping']['first_name']) {
							$('input[name=\'shipping_first_name\']').after('<span class="error">' + json['error']['shipping']['first_name'] + '</span>');
						}

						if (json['error']['shipping']['last_name']) {
							$('input[name=\'shipping_last_name\']').after('<span class="error">' + json['error']['shipping']['last_name'] + '</span>');
						}

						if (json['error']['shipping']['address']) {
							$('input[name=\'shipping_address\']').after('<span class="error">' + json['error']['shipping']['address'] + '</span>');
						}

						if (json['error']['shipping']['city']) {
							$('input[name=\'shipping_city\']').after('<span class="error">' + json['error']['shipping']['city'] + '</span>');
						}

						if (json['error']['shipping']['country']) {
							$('select[name=\'shipping_country_id\']').after('<span class="error">' + json['error']['shipping']['country'] + '</span>');
						}

						if (json['error']['shipping_zone']) {
							$('select[name=\'shipping_zone_id\']').after('<span class="error">' + json['error']['shipping']['zone'] + '</span>');
						}

						if (json['error']['shipping']['postcode']) {
							$('input[name=\'shipping_postcode\']').after('<span class="error">' + json['error']['shipping']['postcode'] + '</span>');
						}
					}

					// Products
					if (json['error']['product']) {
						if (json['error']['product']['option']) {
							for (i in json['error']['product']['option']) {
								$('#option-' + i).after('<span class="error">' + json['error']['product']['option'][i] + '</span>');
							}
						}

						if (json['error']['product']['stock']) {
							$('.box').before('<div class="message warning">' + json['error']['product']['stock'] + '</div>');
						}

						if (json['error']['product']['minimum']) {
							for (i in json['error']['product']['minimum']) {
								$('.box').before('<div class="message warning">' + json['error']['product']['minimum'][i] + '</div>');
							}
						}
					} else {
						$('input[name=\'product\']').attr('value', '');
						$('input[name=\'product_id\']').attr('value', '');
						$('#option td').remove();
						$('input[name=\'quantity\']').attr('value', '1');
					}

					// Voucher
					if (json['error']['vouchers']) {
						if (json['error']['vouchers']['from_name']) {
							$('input[name=\'from_name\']').after('<span class="error">' + json['error']['vouchers']['from_name'] + '</span>');
						}

						if (json['error']['vouchers']['from_email']) {
							$('input[name=\'from_email\']').after('<span class="error">' + json['error']['vouchers']['from_email'] + '</span>');
						}

						if (json['error']['vouchers']['to_name']) {
							$('input[name=\'to_name\']').after('<span class="error">' + json['error']['vouchers']['to_name'] + '</span>');
						}

						if (json['error']['vouchers']['to_email']) {
							$('input[name=\'to_email\']').after('<span class="error">' + json['error']['vouchers']['to_email'] + '</span>');
						}

						if (json['error']['vouchers']['amount']) {
							$('input[name=\'amount\']').after('<span class="error">' + json['error']['vouchers']['amount'] + '</span>');
						}
					} else {
						$('input[name=\'from_name\']').attr('value', '');
						$('input[name=\'from_email\']').attr('value', '');
						$('input[name=\'to_name\']').attr('value', '');
						$('input[name=\'to_email\']').attr('value', '');
						$('textarea[name=\'message\']').attr('value', '');
						$('input[name=\'amount\']').attr('value', '25.00');
					}

					// Shipping Method
					if (json['error']['shipping_method']) {
						$('.box').before('<div class="message warning">' + json['error']['shipping_method'] + '</div>');
					}

					// Payment Method
					if (json['error']['payment_method']) {
						$('.box').before('<div class="message warning">' + json['error']['payment_method'] + '</div>');
					}

					// Coupon
					if (json['error']['coupon']) {
						$('.box').before('<div class="message warning">' + json['error']['coupon'] + '</div>');
					}

					// Voucher
					if (json['error']['voucher']) {
						$('.box').before('<div class="message warning">' + json['error']['voucher'] + '</div>');
					}

					// Reward Points
					if (json['error']['reward']) {
						$('.box').before('<div class="message warning">' + json['error']['reward'] + '</div>');
					}
				} else {
					$('input[name=\'product\']').attr('value', '');
					$('input[name=\'product_id\']').attr('value', '');
					$('#option td').remove();
					$('input[name=\'quantity\']').attr('value', '1');

					$('input[name=\'from_name\']').attr('value', '');
					$('input[name=\'from_email\']').attr('value', '');
					$('input[name=\'to_name\']').attr('value', '');
					$('input[name=\'to_email\']').attr('value', '');
					$('textarea[name=\'message\']').attr('value', '');
					$('input[name=\'amount\']').attr('value', '25.00');
				}

				if (json['success']) {
					$('.box').before('<div class="message success" style="display: none;">' + json['success'] + '</div>');

					$('.success').fadeIn('slow');
				}

				if (json['order_product'] != '') {
					var product_row = 0;
					var option_row = 0;
					var download_row = 0;

					html = '';

					for (i = 0; i < json['order_product'].length; i++) {
						product = json['order_product'][i];

						html += '<tr id="product-row' + product_row + '">';
						html += '	<td class="center" style="width: 3px;"><img src="<?= theme_url('image/delete.png'); ?>" title="{{Remove}}" alt="{{Remove}}" style="cursor: pointer;" onclick="$(\'#product-row' + product_row + '\').remove(); $(\'#button-update\').trigger(\'click\');" /></td>';
						html += '	<td class="left">' + product['name'] + '<br /><input type="hidden" name="order_product[' + product_row + '][order_product_id]" value="" /><input type="hidden" name="order_product[' + product_row + '][product_id]" value="' + product['product_id'] + '" /><input type="hidden" name="order_product[' + product_row + '][name]" value="' + product['name'] + '" />';

						if (product['option']) {
							for (j = 0; j < product['option'].length; j++) {
								option = product['option'][j];

								html += '	- <small>' + option['name'] + ': ' + option['value'] + '</small><br />';
								html += '	<input type="hidden" name="order_product[' + product_row + '][order_option][' + option_row + '][order_option_id]" value="' + option['order_option_id'] + '" />';
								html += '	<input type="hidden" name="order_product[' + product_row + '][order_option][' + option_row + '][product_option_id]" value="' + option['product_option_id'] + '" />';
								html += '	<input type="hidden" name="order_product[' + product_row + '][order_option][' + option_row + '][product_option_value_id]" value="' + option['product_option_value_id'] + '" />';
								html += '	<input type="hidden" name="order_product[' + product_row + '][order_option][' + option_row + '][name]" value="' + option['name'] + '" />';
								html += '	<input type="hidden" name="order_product[' + product_row + '][order_option][' + option_row + '][value]" value="' + option['value'] + '" />';
								html += '	<input type="hidden" name="order_product[' + product_row + '][order_option][' + option_row + '][type]" value="' + option['type'] + '" />';

								option_row++;
							}
						}

						if (product['download']) {
							for (j = 0; j < product['download'].length; j++) {
								download = product['download'][j];

								html += '	<input type="hidden" name="order_product[' + product_row + '][order_download][' + download_row + '][order_download_id]" value="' + download['order_download_id'] + '" />';
								html += '	<input type="hidden" name="order_product[' + product_row + '][order_download][' + download_row + '][name]" value="' + download['name'] + '" />';
								html += '	<input type="hidden" name="order_product[' + product_row + '][order_download][' + download_row + '][filename]" value="' + download['filename'] + '" />';
								html += '	<input type="hidden" name="order_product[' + product_row + '][order_download][' + download_row + '][mask]" value="' + download['mask'] + '" />';
								html += '	<input type="hidden" name="order_product[' + product_row + '][order_download][' + download_row + '][remaining]" value="' + download['remaining'] + '" />';

								download_row++;
							}
						}

						html += '	</td>';
						html += '	<td class="left">' + product['model'] + '<input type="hidden" name="order_product[' + product_row + '][model]" value="' + product['model'] + '" /></td>';
						html += '	<td class="right">' + product['quantity'] + '<input type="hidden" name="order_product[' + product_row + '][quantity]" value="' + product['quantity'] + '" /></td>';
						html += '	<td class="right">' + product['price'] + '<input type="hidden" name="order_product[' + product_row + '][price]" value="' + product['price'] + '" /></td>';
						html += '	<td class="right">' + product['total'] + '<input type="hidden" name="order_product[' + product_row + '][total]" value="' + product['total'] + '" /><input type="hidden" name="order_product[' + product_row + '][tax]" value="' + product['tax'] + '" /><input type="hidden" name="order_product[' + product_row + '][reward]" value="' + product['reward'] + '" /></td>';
						html += '</tr>';

						product_row++;
					}

					$('#product').html(html);
				} else {
					html = '</tr>';
					html += '	<td colspan="6" class="center">{{No results!}}</td>';
					html += '</tr>';

					$('#product').html(html);
				}

				// Vouchers
				if (json['order_voucher'] != '') {
					var voucher_row = 0;

					html = '';

					for (i in json['order_voucher']) {
						voucher = json['order_voucher'][i];

						html += '<tr id="voucher-row' + voucher_row + '">';
						html += '	<td class="center" style="width: 3px;"><img src="<?= theme_url('image/delete.png'); ?>" title="{{Remove}}" alt="{{Remove}}" style="cursor: pointer;" onclick="$(\'#voucher-row' + voucher_row + '\').remove(); $(\'#button-update\').trigger(\'click\');" /></td>';
						html += '	<td class="left">' + voucher['description'];
						html += '	<input type="hidden" name="order_voucher[' + voucher_row + '][order_voucher_id]" value="" />';
						html += '	<input type="hidden" name="order_voucher[' + voucher_row + '][voucher_id]" value="' + voucher['voucher_id'] + '" />';
						html += '	<input type="hidden" name="order_voucher[' + voucher_row + '][description]" value="' + voucher['description'] + '" />';
						html += '	<input type="hidden" name="order_voucher[' + voucher_row + '][code]" value="' + voucher['code'] + '" />';
						html += '	<input type="hidden" name="order_voucher[' + voucher_row + '][from_name]" value="' + voucher['from_name'] + '" />';
						html += '	<input type="hidden" name="order_voucher[' + voucher_row + '][from_email]" value="' + voucher['from_email'] + '" />';
						html += '	<input type="hidden" name="order_voucher[' + voucher_row + '][to_name]" value="' + voucher['to_name'] + '" />';
						html += '	<input type="hidden" name="order_voucher[' + voucher_row + '][to_email]" value="' + voucher['to_email'] + '" />';
						html += '	<input type="hidden" name="order_voucher[' + voucher_row + '][voucher_theme_id]" value="' + voucher['voucher_theme_id'] + '" />';
						html += '	<input type="hidden" name="order_voucher[' + voucher_row + '][message]" value="' + voucher['message'] + '" />';
						html += '	<input type="hidden" name="order_voucher[' + voucher_row + '][amount]" value="' + voucher['amount'] + '" />';
						html += '	</td>';
						html += '	<td class="left"></td>';
						html += '	<td class="right">1</td>';
						html += '	<td class="right">' + voucher['amount'] + '</td>';
						html += '	<td class="right">' + voucher['amount'] + '</td>';
						html += '</tr>';

						voucher_row++;
					}

					$('#voucher').html(html);
				}
				else {
					html = '</tr>';
					html += '	<td colspan="6" class="center">{{No results!}}</td>';
					html += '</tr>';

					$('#voucher').html(html);
				}

				// Totals
				if (json{
					['order_product'] != '' || json
				}
				['order_voucher'] != '' || json['order_total'] != ''
				)
				{
					html = '';

					if (json{
						['order_product'] != ''
					}
				)
					{
						for (i = 0; i < json['order_product'].length{;
					}
					i++
				)
					{
						product = json['order_product'][i];

						html += '<tr>';
						html += '	<td class="left">' + product['name'] + '<br />';

						if (product{
							['option']
						}
					)
						{
							for (j = 0; j < product['option'].length{;
						}
						j++
					)
						{
							option = product['option'][j];

							html += '	- <small>' + option['name'] + ': ' + option['value'] + '</small><br />';
						}
					}

					html += '	</td>';
					html += '	<td class="left">' + product['model'] + '</td>';
					html += '	<td class="right">' + product['quantity'] + '</td>';
					html += '	<td class="right">' + product['price'] + '</td>';
					html += '	<td class="right">' + product['total'] + '</td>';
					html += '</tr>';
				}
			}

			if (json
		{
			['order_voucher'] != ''
		}
		)
		{
			for (i in json{
				['order_voucher']
			}
		)
			{
				voucher = json['order_voucher'][i];

				html += '<tr>';
				html += '	<td class="left">' + voucher['description'] + '</td>';
				html += '	<td class="left"></td>';
				html += '	<td class="right">1</td>';
				html += '	<td class="right">' + voucher['amount'] + '</td>';
				html += '	<td class="right">' + voucher['amount'] + '</td>';
				html += '</tr>';
			}
		}

		var total_row = 0;

		for (i in json{
			['order_total']
		}
		)
		{
			total = json['order_total'][i];

			html += '<tr id="total-row' + total_row + '">';
			html += '	<td class="right" colspan="4"><input type="hidden" name="order_total[' + total_row + '][order_total_id]" value="" /><input type="hidden" name="order_total[' + total_row + '][code]" value="' + total['code'] + '" /><input type="hidden" name="order_total[' + total_row + '][title]" value="' + total['title'] + '" /><input type="hidden" name="order_total[' + total_row + '][text]" value="' + total['text'] + '" /><input type="hidden" name="order_total[' + total_row + '][value]" value="' + total['value'] + '" /><input type="hidden" name="order_total[' + total_row + '][sort_order]" value="' + total['sort_order'] + '" />' + total['title'] + ':</td>';
			html += '	<td class="right">' + total['value'] + '</td>';
			html += '</tr>';

			total_row++;
		}

		$('#total').html(html);
	}
	else
	{
		html = '</tr>';
		html += '	<td colspan="6" class="center">{{No results!}}</td>';
		html += '</tr>';

		$('#total').html(html);
	}

	// Shipping Methods
	if (json{
		['shipping_method']
	}
	)
	{
		html = '<option value="">{{ --- Please Select --- }}</option>';

		for (i in json{
			['shipping_method']
		}
	)
		{
			html += '<optgroup label="' + json['shipping_method'][i]['title'] + '">';

			if (!json{
				['shipping_method'][i]['error']
			}
		)
			{
				for (j in json{
					['shipping_method'][i]['quote']
				}
			)
				{
					if (json{
						['shipping_method'][i]['quote'][j]['code'] == $('input[name=\'shipping_code\']').attr('value')
					}
				)
					{
						html += '<option value="' + json['shipping_method'][i]['quote'][j]['code'] + '" selected="selected">' + json['shipping_method'][i]['quote'][j]['title'] + '</option>';
					}
				else
					{
						html += '<option value="' + json['shipping_method'][i]['quote'][j]['code'] + '">' + json['shipping_method'][i]['quote'][j]['title'] + '</option>';
					}
				}
			}
		else
			{
				html += '<option value="" style="color: #F00;" disabled="disabled">' + json['shipping_method'][i]['error'] + '</option>';
			}

			html += '</optgroup>';
		}

		$('select[name=\'shipping\']').html(html);

		if ($('select[name=\'shipping\'] option:selected').attr('value')) {
			$('input[name=\'shipping_method\']').attr('value', $('select[name=\'shipping\'] option:selected').text());
		} else {
			$('input[name=\'shipping_method\']').attr('value', '');
		}

		$('input[name=\'shipping_code\']').attr('value', $('select[name=\'shipping\'] option:selected').attr('value'));
	}

	// Payment Methods
	if (json{
		['payment_method']
	}
	)
	{
		html = '<option value="">{{ --- Please Select --- }}</option>';

		for (i in json{
			['payment_method']
		}
	)
		{
			if (json{
				['payment_method'][i]['code'] == $('input[name=\'payment_code\']').attr('value')
			}
		)
			{
				html += '<option value="' + json['payment_method'][i]['code'] + '" selected="selected">' + json['payment_method'][i]['title'] + '</option>';
			}
		else
			{
				html += '<option value="' + json['payment_method'][i]['code'] + '">' + json['payment_method'][i]['title'] + '</option>';
			}
		}

		$('select[name=\'payment\']').html(html);

		if ($('select[name=\'payment\'] option:selected').attr('value')) {
			$('input[name=\'payment_method\']').attr('value', $('select[name=\'payment\'] option:selected').text());
		} else {
			$('input[name=\'payment_method\']').attr('value', '');
		}

		$('input[name=\'payment_code\']').attr('value', $('select[name=\'payment\'] option:selected').attr('value'));
	}
	},
	error:      function (xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	}
	})
	;
	})
	;
</script>

<script type="text/javascript">
	$.ac_datepicker();
	$('.vtabs a').tabs();
</script>
<?= $is_ajax ? '' : call('admin/footer'); ?>
