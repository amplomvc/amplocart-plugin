<?= $is_ajax ? '' : call('admin/header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/order.png'); ?>" alt=""/> {{Orders}}</h1>

			<div class="buttons"><a onclick="window.open('<?= $invoice; ?>');" class="button">{{Invoice}}</a><a
					href="<?= $cancel; ?>" class="button">{{Cancel}}</a></div>
		</div>
		<div class="section">
			<div class="vtabs">
				<a href="#tab-order">{{Order}}</a>
				<a href="#tab-product"><?= _("Product"); ?></a>
				<a href="#tab-history">{{Order History}}</a>
			</div>

			<div id="tab-order" class="vtabs-content">
				<table class="form">
					<tr>
						<td>{{Order ID:}}</td>
						<td>#<?= $order_id; ?></td>
					</tr>
					<tr>
						<td>{{Invoice No.:}}</td>
						<td><?= $invoice_id; ?></td>
					</tr>
					<tr>
						<td>{{Store:}}</td>
						<td><a target="_blank" href="<?= $store['url']; ?>"><?= $store['name']; ?></a></td>
					</tr>
					<tr>
						<td>{{Customer:}}</td>
						<td>
							<? if (!empty($url_customer)) { ?>
								<a target="_blank" href="<?= $url_customer; ?>"><?= $first_name; ?> <?= $last_name; ?></a>
							<? } else { ?>
								<?= $first_name; ?> <?= $last_name; ?>
							<? } ?>
						</td>
					</tr>
					<tr>
						<td>{{E-Mail:}}</td>
						<td><?= $email; ?></td>
					</tr>
					<tr>
						<td>{{Telephone:}}</td>
						<td><?= $phone; ?></td>
					</tr>
					<? if (!empty($fax)) { ?>
						<tr>
							<td>{{Fax:}}</td>
							<td><?= $fax; ?></td>
						</tr>
					<? } ?>
					<tr>
						<td>{{Payment Details:}}</td>
						<td>
							<p><?= $payment_method['title']; ?></p>

							<p><?= $payment_address; ?></p>
						</td>
					</tr>
					<? if (!empty($shipping_address)) { ?>
						<tr>
							<td>{{Shipping Details:}}</td>
							<td>
								<p><?= $shipping_method['title']; ?></p>

								<p><?= $shipping_address; ?></p>
							</td>
						</tr>
					<? } ?>
					<tr>
						<td>{{Total:}}</td>
						<td><?= $total; ?>
							<? if ($credit && $customer_id) { ?>
								<? if (!$credit_total) { ?>
									<span id="credit"><b>[</b> <a id="credit-add">{{Add Credit}}</a> <b>]</b></span>
								<? } else { ?>
									<span id="credit"><b>[</b> <a id="credit-remove">{{Remove Credit}}</a> <b>]</b></span>
								<? } ?>
							<? } ?>
						</td>
					</tr>
					<? if (!empty($reward) && $customer_id) { ?>
						<tr>
							<td>{{Reward Points:}}</td>
							<td><?= $reward; ?>
								<? if (!$reward_total) { ?>
									<span id="reward"><b>[</b> <a id="reward-add">{{Add Reward Points}}</a> <b>]</b></span>
								<? } else { ?>
									<span id="reward"><b>[</b> <a id="reward-remove">{{Remove Reward Points}}</a> <b>]</b></span>
								<? } ?></td>
						</tr>
					<? } ?>
					<? if ($order_status) { ?>
						<tr>
							<td>{{Order Status:}}</td>
							<td id="order-status"><?= $order_status['title']; ?></td>
						</tr>
					<? } ?>
					<? if (!empty($comment)) { ?>
						<tr>
							<td>{{Comment:}}</td>
							<td><?= $comment; ?></td>
						</tr>
					<? } ?>
					<? if ($ip) { ?>
						<tr>
							<td>{{IP Address:}}</td>
							<td><?= $ip; ?></td>
						</tr>
					<? } ?>
					<? if ($forwarded_ip) { ?>
						<tr>
							<td>{{Forwarded IP:}}</td>
							<td><?= $forwarded_ip; ?></td>
						</tr>
					<? } ?>
					<? if ($user_agent) { ?>
						<tr>
							<td>{{User Agent:}}</td>
							<td><?= $user_agent; ?></td>
						</tr>
					<? } ?>
					<? if ($accept_language) { ?>
						<tr>
							<td>{{Accept Language:}}</td>
							<td><?= $accept_language; ?></td>
						</tr>
					<? } ?>
					<tr>
						<td>{{Date Added:}}</td>
						<td><?= $date_added; ?></td>
					</tr>
					<tr>
						<td>{{Date Modified:}}</td>
						<td><?= $date_modified; ?></td>
					</tr>
				</table>
			</div>
			<!-- /tab-order -->

			<div id="tab-product" class="vtabs-content">
				<table class="list">
					<thead>
					<tr>
						<td class="left">{{Product}}</td>
						<td class="left">{{Model}}</td>
						<td class="right">{{Quantity}}</td>
						<td class="right">{{Unit Price}}</td>
						<td class="right">{{Total}}</td>
					</tr>
					</thead>
					<tbody>
					<? foreach ($products as $product) { ?>
						<tr>
							<td class="left">
								<a href="<?= $product['href']; ?>"><?= $product['name']; ?></a>
								<? foreach ($product['options'] as $option) { ?>
									<div class="product_option">
										<span class="name"><?= $option['name']; ?>:</span>
										<span class="value"><?= $option['value']; ?></span>
									</div>
								<? } ?>
							</td>
							<td class="left"><?= $product['model']; ?></td>
							<td class="right"><?= $product['quantity']; ?></td>
							<td class="right"><?= $product['price_display']; ?></td>
							<td class="right"><?= $product['total_display']; ?></td>
						</tr>
					<? } ?>
					<? foreach ($vouchers as $voucher) { ?>
						<tr>
							<td class="left"><a href="<?= $voucher['href']; ?>"><?= $voucher['description']; ?></a></td>
							<td class="left"></td>
							<td class="right">1</td>
							<td class="right"><?= $voucher['amount']; ?></td>
							<td class="right"><?= $voucher['amount']; ?></td>
						</tr>
					<? } ?>
					<? foreach ($totals as $totals) { ?>
						<tr class="totals">
							<td colspan="4" class="right"><?= $totals['title']; ?>:</td>
							<td class="right"><?= $totals['value_display']; ?></td>
						</tr>
					<? } ?>
					</tbody>
				</table>

				<? if (!empty($downloads)) { ?>
					<h3>{{Order Downloads}}</h3>
					<table class="list">
						<thead>
						<tr>
							<td class="left"><b>{{Download Name}}</b></td>
							<td class="left"><b>{{Filename}}</b></td>
							<td class="right"><b>{{Remaining Downloads}}</b></td>
						</tr>
						</thead>
						<tbody>
						<? foreach ($downloads as $download) { ?>
							<tr>
								<td class="left"><?= $download['name']; ?></td>
								<td class="left"><?= $download['filename']; ?></td>
								<td class="right"><?= $download['remaining']; ?></td>
							</tr>
						<? } ?>
						</tbody>
					</table>
				<? } ?>
			</div>
			<!-- /tab-products -->

			<div id="tab-history" class="vtabs-content">
				<div id="history">
					<table class="list">
						<thead>
						<tr>
							<td class="left"><b>{{Date Added}}</b></td>
							<td class="left"><b>{{Comment}}</b></td>
							<td class="left"><b>{{Status}}</b></td>
							<td class="left"><b>{{Customer Notified}}</b></td>
						</tr>
						</thead>
						<tbody>
						<? if (!empty($histories)) { ?>
							<? foreach ($histories as $history) { ?>
								<tr>
									<td class="left"><?= $history['date_added']; ?></td>
									<td class="left"><?= $history['comment']; ?></td>
									<td class="left"><?= $history['status']; ?></td>
									<td class="left"><?= $history['notify']; ?></td>
								</tr>
							<? } ?>
						<? } else { ?>
							<tr>
								<td class="center" colspan="4">{{There are no results to display.}}</td>
							</tr>
						<? } ?>
						</tbody>
					</table>
				</div>

				<form id="add_history_form" action="" method="post">
					<table class="form">
						<tr>
							<td>{{Order Status:}}</td>
							<td>
								<select name="order_status_id">
									<? foreach ($data_order_statuses as $os_id => $order_status) { ?>
										<option value="<?= $os_id; ?>" <?= $os_id == $order_status_id ? 'selected="selected"' : ''; ?>><?= $order_status['title']; ?></option>
									<? } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td>{{Notify Customer:}}</td>
							<td><input type="checkbox" name="notify" value="1"/></td>
						</tr>
						<tr>
							<td>{{Comment:}}</td>
							<td>
								<textarea name="comment" cols="40" rows="8"></textarea>

								<div>
									<input type="submit" id="button-history" class="button" value="{{Add History}}"/>
								</div>
							</td>
						</tr>
					</table>
				</form>

			</div>
			<!-- /tab-history -->
		</div>
	</div>
</div>


<script type="text/javascript">
	$('.vtabs a').tabs();
</script>

<?= $is_ajax ? '' : call('admin/footer'); ?>
