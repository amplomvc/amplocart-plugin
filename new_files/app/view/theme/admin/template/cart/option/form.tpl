<?= $is_ajax ? '' : call('admin/header'); ?>

<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>

	<div class="box">
		<div class="heading">
			<h1>
				<img src="<?= theme_url('image/information.png'); ?>" alt=""/> {{Options}}</h1>

			<div class="buttons">
				<a onclick="$('#form').submit();" class="button">{{Save}}</a>
				<a href="<?= $cancel; ?>" class="button">{{Cancel}}</a>
			</div>
		</div>

		<div class="section">
			<form action="<?= $save; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="form">
					<tr>
						<td class="required"> {{Option Name:}}</td>
						<td><input type="text" name="name" value="<?= $name; ?>"/></td>
					</tr>
					<tr>
						<td class="required"> {{Option Display Name:}}</td>
						<td><input type="text" name="display_name" value="<?= $display_name; ?>"/></td>
					</tr>
					<tr>
						<td>{{Type:}}</td>
						<td><?=
							build(array(
								'type' => 'select',
								'name'  => "type",
								'data'   => $data_option_types,
								'select' => $type
							)); ?>
						</td>
					</tr>
					<tr>
						<td>{{Sort Order:}}</td>
						<td><input type="text" name="sort_order" value="<?= $sort_order; ?>" size="1"/></td>
					</tr>
				</table>

				<table class="list">
					<thead>
					<tr>
						<td class="center required">{{Option Value:}}</td>
						<td class="center">
							{{Option Value Display: (optional)}}
							<span class="help">{{The value to show to customers.<br/>Leave blank to use Option Value. HTML enabled.}}</span>
						</td>
						<td class="center">
							{{Info: (optional)}}
							<span class="help">{{Additional Information about the option. Typically a popup info box.}}</span>
						</td>
						<td class="center">{{Image:}}</td>
						<td class="center">{{Sort Order:}}</td>
						<td></td>
					</tr>
					</thead>
					<tbody id="option_value_list">
					<? foreach ($option_values as $row => $option_value) { ?>
						<tr class="optionvaluerow" data-row="<?= $row; ?>">
							<td class="center">
								<input type="hidden" name="option_value[<?= $row; ?>][option_value_id]" value="<?= $option_value['option_value_id']; ?>"/>
								<input type="text" name="option_value[<?= $row; ?>][value]" value="<?= $option_value['value']; ?>"/>
							</td>
							<td class="center">
								<input type="text" name="option_value[<?= $row; ?>][display_value]" size="50" value="<?= $option_value['display_value']; ?>"/>
							</td>
							<td class="center">
								<textarea name="option_value[<?= $row; ?>][info]" rows="5" cols="60"><?= $option_value['info']; ?></textarea>
							</td>
							<td class="center">
								<input type="text" class="imageinput" name="option_value[<?= $row; ?>][image]" value="<?= $option_value['image']; ?>" data-thumb="<?= $option_value['thumb']; ?>"/>
							</td>
							<td class="center">
								<input class="sort_order" type="text" name="option_value[<?= $row; ?>][sort_order]" value="<?= $option_value['sort_order']; ?>" size="1"/>
							</td>

							<td class="center">
								<a onclick="$(this).closest('tr').remove();" class="button">{{Remove}}</a>
							</td>
						</tr>
					<? } ?>
					</tbody>
					<tfoot>
					<tr>
						<td colspan="3"></td>
						<td class="left">
							<a onclick="add_option_value();" class="button">{{Add Option Value}}</a>
						</td>
					</tr>
					</tfoot>
				</table>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#option_value_list').ac_template('ov_list');

	function add_option_value() {
		var ov = $.ac_template('ov_list', 'add');
		ov.find('.imageinput').ac_imageinput();
		$('#option_value_list').update_index('.sort_order');
	}

	$('#option_value_list').sortable({cursor: "move", stop: function () {
		$('#option_value_list').update_index();
	} });

	$('.imageinput').ac_imageinput();
</script>

<?= $is_ajax ? '' : call('admin/footer'); ?>
