<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>
	<div class="content">
		<?= $is_ajax ? '' : breadcrumbs(); ?>
		<?= area('top'); ?>

		<h1>{{Account Downloads}}</h1>
		<? foreach ($downloads as $download) { ?>
			<div class="download-list">
				<div class="download-id"><b>{{Order ID:}}</b> <?= $download['order_id']; ?></div>
				<div class="download-status"><b>{{Size:}}</b> <?= $download['size']; ?></div>
				<div class="download-content">
					<div><b>{{Name:}}</b> <?= $download['name']; ?><br/>
						<b>{{Date Added:}}</b> <?= $download['date_added']; ?></div>
					<div><b>{{Remaining:}}</b> <?= $download['remaining']; ?></div>
					<div class="download-info">
						<? if ($download['remaining'] > 0) { ?>
							<a href="<?= $download['href']; ?>"><img src="<?= theme_url('image/download.png'); ?>"
									alt="{{Download}}"
									title="{{Download}}"/></a>
						<? } ?>
					</div>
				</div>
			</div>
		<? } ?>
		<div class="pagination"><?= $pagination; ?></div>
		<div class="buttons">
			<div class="right"><a href="<?= $continue; ?>" class="button">{{Continue}}</a></div>
		</div>

		<?= area('bottom'); ?>
	</div>

<?= $is_ajax ? '' : call('footer'); ?>
