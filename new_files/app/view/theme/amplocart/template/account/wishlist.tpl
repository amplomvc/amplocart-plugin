<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>
	<div class="content">
		<?= $is_ajax ? '' : breadcrumbs(); ?>
		<?= area('top'); ?>

		<h1>{{My Wish List}}</h1>
		<? if ($products) { ?>
			<div class="wishlist-info">
				<table>
					<thead>
						<tr>
							<td class="image">{{Image}}</td>
							<td class="name">{{Product Name}}</td>
							<td class="model">{{Model}}</td>
							<td class="stock">{{Stock}}</td>
							<td class="price">{{Unit Price}}</td>
							<td class="action">{{Action}}</td>
						</tr>
					</thead>
					<? foreach ($products as $product) { ?>
						<tbody id="wishlist-row<?= $product['product_id']; ?>">
							<tr>
								<td class="image"><? if ($product['thumb']) { ?>
										<a href="<?= $product['href']; ?>"><img src="<?= $product['thumb']; ?>"
												alt="<?= $product['name']; ?>"
												title="<?= $product['name']; ?>"/></a>
									<? } ?></td>
								<td class="name"><a href="<?= $product['href']; ?>"><?= $product['name']; ?></a></td>
								<td class="model"><?= $product['model']; ?></td>
								<td class="stock"><?= $product['stock']; ?></td>
								<td class="price"><? if ($product['price']) { ?>
										<div class="price">
											<? if (!$product['special']) { ?>
												<?= $product['price']; ?>
											<? } else { ?>
												<s><?= $product['price']; ?></s> <b><?= $product['special']; ?></b>
											<? } ?>
										</div>
									<? } ?></td>
								<td class="action"><img src="<?= theme_url('image/cart-add.png'); ?>" alt="{{Add to Cart}}"
										title="{{Add to Cart}}" onclick="addToCart('<?= $product['product_id']; ?>');"/>&nbsp;&nbsp;<a
										href="<?= $product['remove']; ?>"><img src="<?= theme_url('image/remove.png'); ?>"
											alt="{{Remove}}"
											title="{{Remove}}"/></a></td>
							</tr>
						</tbody>
					<? } ?>
				</table>
			</div>
			<div class="buttons">
				<div class="right"><a href="<?= $continue; ?>" class="button">{{Continue}}</a></div>
			</div>
		<? } else { ?>
			<div class="section">{{Your wish list is empty.}}</div>
			<div class="buttons">
				<div class="right"><a href="<?= $continue; ?>" class="button">{{Continue}}</a></div>
			</div>
		<? } ?>

		<?= area('bottom'); ?>
	</div>

<?= $is_ajax ? '' : call('footer'); ?>
