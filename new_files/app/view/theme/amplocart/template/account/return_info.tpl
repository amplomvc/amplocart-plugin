<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>

<div class="content">
	<?= area('top'); ?>
	<?= $is_ajax ? '' : breadcrumbs(); ?>

	<h1>{{Return Information}}</h1>
	<table class="list">
		<thead>
			<tr>
				<td class="left" colspan="2">{{Return Details}}</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="left" style="width: 50%;"><b>{{RMA #:}}</b> #<?= $rma; ?><br/>
					<b>{{Date Added:}}</b> <?= $date_added; ?></td>
				<td class="left" style="width: 50%;"><b>{{Order ID:}}</b> #<?= $order_id; ?><br/>
					<b>{{Order Date:}}</b> <?= $date_ordered; ?></td>
			</tr>
		</tbody>
	</table>
	<h2>{{Product Information &amp; Reason for Return}}</h2>
	<table class="list">
		<thead>
			<tr>
				<td class="left">{{Product Name}}</td>
				<td class="left">{{Model}}</td>
				<td class="right">{{Quantity}}</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="left"><?= $subscription['name']; ?></td>
				<td class="left"><?= $subscription['model']; ?></td>
				<td class="right"><?= $quantity; ?></td>
			</tr>
		</tbody>
	</table>
	<table class="list">
		<thead>
			<tr>
				<td class="left">{{Reason}}</td>
				<td class="left">{{Opened}}</td>
				<td class="left">{{Action}}</td>
				<td class="left">{{Status}}</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="left"><?= $reason['title']; ?></td>
				<td class="left"><?= $opened; ?></td>
				<td class="left"><?= $action; ?></td>
				<td class="left"><?= $return_status['title']; ?></td>
			</tr>
		</tbody>
	</table>
	<table class="list">
		<? if ($comment) { ?>
		<thead>
			<tr>
				<td class="left">{{Return Comments}}</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="left"><?= $comment; ?></td>
			</tr>
		</tbody>
	</table>
<? } ?>
	<? if ($histories) { ?>
		<h2>{{Return History}}</h2>
		<table class="list">
			<thead>
				<tr>
					<td class="left" style="width: 33.3%;">{{Date Added}}</td>
					<td class="left" style="width: 33.3%;">{{Status}}</td>
					<td class="left" style="width: 33.3%;">{{Comment}}</td>
				</tr>
			</thead>
			<tbody>
				<? foreach ($histories as $history) { ?>
					<tr>
						<td class="left"><?= $history['date_added']; ?></td>
						<td class="left"><?= $history['status']; ?></td>
						<td class="left"><?= $history['comment']; ?></td>
					</tr>
				<? } ?>
			</tbody>
		</table>
	<? } ?>
	<div class="buttons">
		<div class="right"><a href="<?= $continue; ?>" class="button">{{Continue}}</a></div>
	</div>

	<?= area('bottom'); ?>
</div>

<?= $is_ajax ? '' : call('footer'); ?>
