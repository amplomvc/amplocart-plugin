<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>
	<div class="content">
		<?= $is_ajax ? '' : breadcrumbs(); ?>
		<?= area('top'); ?>

		<h1>{{Product Returns}}</h1>

		<div class="success-message">{{<p>Thank you for submitting your return request. Your request has been sent to the relevant department for processing.</p><p> You will be notified via e-mail as to the status of your request.</p>}}</div>

		<? if (!empty($returns)) { ?>
			<div class="rma-description">{{Please ship your products back to us with the following RMA number(s) included inside each package with the associated product.}}</div>
			<ul class="return-success-list">
				<? foreach ($returns as $return) { ?>
					<li>
						<div class="product-name">
							<span class="label">{{Product Name:}}</span>
							<span class="value"><?= $return['product']['name']; ?></span>
						</div>
						<div class="product-model">
							<span class="label">{{Product Model:}}</span>
							<span class="value"><?= $return['product']['model']; ?></span>
						</div>
						<div class="rma">
							<span class="label">{{RMA #:}}</span>
							<span class="value"><?= $return['rma']; ?></span>
						</div>
					</li>
				<? } ?>
			</ul>
		<? } ?>

		<div class="buttons">
			<div class="right"><a href="<?= $continue; ?>" class="button">{{Continue}}</a></div>
		</div>


		<?= area('bottom'); ?>
	</div>

<?= $is_ajax ? '' : call('footer'); ?>
