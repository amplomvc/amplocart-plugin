<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>
<div class="content">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<?= area('top'); ?>

	<h1>{{Your Transactions}}</h1>

	<p>{{Your current balance is:}}<b> <?= $total; ?></b>.</p>
	<table class="list">
		<thead>
			<tr>
				<td class="left">{{Date Added}}</td>
				<td class="left">{{Description}}</td>
				<td class="right"><?= _l("Amount (%s)", $amount); ?></td>
			</tr>
		</thead>
		<tbody>
			<? if ($transactions) { ?>
				<? foreach ($transactions as $transaction) { ?>
					<tr>
						<td class="left"><?= $transaction['date_added']; ?></td>
						<td class="left"><?= $transaction['description']; ?></td>
						<td class="right"><?= $transaction['amount']; ?></td>
					</tr>
				<? } ?>
			<? } else { ?>
				<tr>
					<td class="center" colspan="5">{{You do not have any transactions!}}</td>
				</tr>
			<? } ?>
		</tbody>
	</table>
	<div class="pagination"><?= $pagination; ?></div>
	<div class="buttons">
		<div class="right"><a href="<?= $continue; ?>" class="button">{{Continue}}</a></div>
	</div>

	<?= area('bottom'); ?>
</div>

<?= $is_ajax ? '' : call('footer'); ?>
