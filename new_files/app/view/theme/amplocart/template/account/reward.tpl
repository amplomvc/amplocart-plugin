<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>
	<div class="content">
		<?= $is_ajax ? '' : breadcrumbs(); ?>
		<?= area('top'); ?>

		<h1>{{Your Reward Points}}</h1>

		<p>{{Your total number of reward points is:}}<b> <?= $total; ?></b>.</p>
		<table class="list">
			<thead>
				<tr>
					<td class="left">{{Date Added}}</td>
					<td class="left">{{Description}}</td>
					<td class="right">{{Points}}</td>
				</tr>
			</thead>
			<tbody>
				<? if ($rewards) { ?>
					<? foreach ($rewards as $reward) { ?>
						<tr>
							<td class="left"><?= $reward['date_added']; ?></td>
							<td class="left"><? if ($reward['order_id']) { ?>
									<a href="<?= $reward['href']; ?>"><?= $reward['description']; ?></a>
								<? } else { ?>
									<?= $reward['description']; ?>
								<? } ?></td>
							<td class="right"><?= $reward['points']; ?></td>
						</tr>
					<? } ?>
				<? } else { ?>
					<tr>
						<td class="center" colspan="5">{{You do not have any reward points!}}</td>
					</tr>
				<? } ?>
			</tbody>
		</table>
		<div class="pagination"><?= $pagination; ?></div>
		<div class="buttons">
			<div class="right"><a href="<?= $continue; ?>" class="button">{{Continue}}</a></div>
		</div>

		<?= area('bottom'); ?>
	</div>

<?= $is_ajax ? '' : call('footer'); ?>
