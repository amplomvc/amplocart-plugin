<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>
<div class="content">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<?= area('top'); ?>

	<h1>{{Purchase a Gift Certificate}}</h1>

	<p>{{This gift certificate will be emailed to the recipient after your order has been paid for.}}</p>

	<form action="<?= $action; ?>" method="post" enctype="multipart/form-data">
		<table class="form">
			<tr>
				<td class="required"> {{Recipient's Name:}}</td>
				<td><input type="text" name="to_name" value="<?= $to_name; ?>"/>
					<? if (_l("Recipient's Name must be between 1 and 64 characters!")) { ?>
						<span class="error">{{Recipient's Name must be between 1 and 64 characters!}}</span>
					<? } ?></td>
			</tr>
			<tr>
				<td class="required"> {{Recipient's Email:}}</td>
				<td><input type="text" name="to_email" value="<?= $to_email; ?>"/></td>
			</tr>
			<tr>
				<td class="required"> {{Your Name:}}</td>
				<td><input type="text" name="from_name" value="<?= $from_name; ?>"/>
					<? if (_l("Your Name must be between 1 and 64 characters!")) { ?>
						<span class="error">{{Your Name must be between 1 and 64 characters!}}</span>
					<? } ?></td>
			</tr>
			<tr>
				<td class="required"> {{Your Email:}}</td>
				<td><input type="text" name="from_email" value="<?= $from_email; ?>"/></td>
			</tr>
			<tr>
				<td class="required"> {{Gift Certificate Theme:}}</td>
				<td><? foreach ($voucher_themes as $voucher_theme) { ?>
						<? if ($voucher_theme['voucher_theme_id'] == $voucher_theme_id) { ?>
							<input type="radio" name="voucher_theme_id" value="<?= $voucher_theme['voucher_theme_id']; ?>"
								id="voucher-<?= $voucher_theme['voucher_theme_id']; ?>" checked="checked"/>
							<label
								for="voucher-<?= $voucher_theme['voucher_theme_id']; ?>"><?= $voucher_theme['name']; ?></label>
						<? } else { ?>
							<input type="radio" name="voucher_theme_id" value="<?= $voucher_theme['voucher_theme_id']; ?>"
								id="voucher-<?= $voucher_theme['voucher_theme_id']; ?>"/>
							<label
								for="voucher-<?= $voucher_theme['voucher_theme_id']; ?>"><?= $voucher_theme['name']; ?></label>
						<? } ?>
						<br/>
					<? } ?>
					<? if (_l("You must select a theme!")) { ?>
						<span class="error">{{You must select a theme!}}</span>
					<? } ?></td>
			</tr>
			<tr>
				<td><?= _l("Message:<br /><span class=\"help\">(Optional)</span>"); ?></td>
				<td><textarea name="message" cols="40" rows="5"><?= $message; ?></textarea></td>
			</tr>
			<tr>
				<td class="required">
					{{Amount:}}
					<span class="help"><?= _l("(Value must be between %s and %s)", $max_value, $min_value); ?></span>
				</td>
				<td><input type="text" name="amount" value="<?= $amount; ?>" size="5"/></td>
			</tr>
		</table>
		<div class="buttons">
			<div class="right">{{I understand that gift certificates are non-refundable.}}
				<? if ($agree) { ?>
					<input type="checkbox" name="agree" value="1" checked="checked"/>
				<? } else { ?>
					<input type="checkbox" name="agree" value="1"/>
				<? } ?>
				<input type="submit" value="{{Continue}}" class="button"/>
			</div>
		</div>
	</form>

	<?= area('bottom'); ?>
</div>



<?= $is_ajax ? '' : call('footer'); ?>
