<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?>
<?= area('right'); ?>

<section id="account-update-page" class="content">
	<header class="row top-row">
		<div class="wrap">
			<?= $is_ajax ? '' : breadcrumbs(); ?>

			<h1>{{My Account Information}}</h1>
		</div>
	</header>

	<?= area('top'); ?>

	<form action="<?= $save; ?>" class="form labels" method="post" autocomplete="off" enctype="multipart/form-data">
		<div class="row account-update-form">
			<div class="wrap">

				<div class="col xs-12 md-6 account-details">
					<section class="form-section">
						<h2>{{Your Personal Details}}</h2>

						<div class="form-item">
							<label for="first_name">{{First Name}}</label>
							<input id="first_name" type="text" name="first_name" value="<?= $first_name; ?>"/>
						</div>
						<div class="form-item">
							<label for="last_name">{{Last Name}}</label>
							<input id="last_name" type="text" name="last_name" value="<?= $last_name; ?>"/>
						</div>
						<div class="form-item">
							<label for="email">{{Email}}</label>
							<input id="email" type="text" name="email" value="<?= $email; ?>"/>
						</div>
						<div class="form-item">
							<label for="birthdate">{{Birth Date}}</label>
							<input id="birthdate" type="text" class="datepicker" autocomplete="off" name="metadata[birthdate]" value="<?= !empty($metadata['birthdate']) ? $metadata['birthdate'] : ''; ?>"/>
						</div>
					</section>

					<section class="form-section">
						<h2>{{Change Password}}</h2>

						<div class="form-item">
							<label for="password">{{Password}}</label>
							<input id="password" autocomplete="off" type="password" name="password"/>
							<span class="help">{{Leave blank to keep the same.}}</span>
						</div>
						<div class="form-item">
							<label for="password-confirm">{{Confirm}}</label>
							<input id="password-confirm" autocomplete="off" type="password" name="confirm"/>
						</div>
					</section>

					<section class="form-section">
						<h2>{{Newsletter}}</h2>

						<label for="newsletter">{{Join our mailing list?}}</label>
						<input id="newsletter" type="checkbox" name="newsletter" value="1" <?= $newsletter ? 'checked="checked"' : ''; ?> />
					</section>
				</div>

				<div class="col xs-12 md-6 account-address">
					<section class="form-section shipping-address">
						<h2>{{Default Shipping Address:}}</h2>
						<? if (empty($data_addresses)) { ?>
							<h3>{{You do not have an address registered with us.}}</h3>
							<a href="<?= site_url('account/address/form'); ?>" class="button colorbox">{{Register New Address}}</a>
						<? } else { ?>
							<div class="address-list">
								<?= build(array(
									'type'   => 'radio',
									'name'   => 'metadata[default_shipping_address_id]',
									'data'   => format_all('address', $data_addresses),
									'select' => $metadata['default_shipping_address_id'],
									'value' => 'address_id',
									'label' => 'formatted',
								)); ?>

								<a href="<?= site_url('account/address/form'); ?>" class="add-address colorbox">{{Add New Address}}</a>
							</div>
						<? } ?>
					</section>

					<section class="form-section credit-card">
						<h2>{{Default Credit Card:}}</h2>

						<div class="credit-card-list">
							<?= block('account/card/select'); ?>
						</div>
					</section>
				</div>

			</div>
		</div>
		<div class="button-row row">
			<div class="wrap">
				<div class="col xs-10 sm-7 center">
					<div class="left">
						<a href="<?= site_url('account'); ?>" class="button">{{Back}}</a>
					</div>
					<div class="right">
						<button class="button">{{Save}}</button>
					</div>
				</div>
			</div>
		</div>
	</form>

	<?= area('bottom'); ?>

</section>

<script type="text/javascript">
	$('.address-list .remove').click(function () {
		var address = $(this);
		$.get(address.attr('href'), {}, function (json) {
			if (json['error']) {
				show_msgs(json['error'], 'error');
			} else {
				location.reload();
			}
		});
		return false;
	});

	$.ac_datepicker({changeYear: true, yearRange: "c-150:c", changeMonth: true});
</script>

<?= $is_ajax ? '' : call('footer'); ?>
