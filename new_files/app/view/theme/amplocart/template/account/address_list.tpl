<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>
	<div class="content">
		<?= $is_ajax ? '' : breadcrumbs(); ?>
		<?= area('top'); ?>

		<h1>{{Address Book}}</h1>

		<h2>{{Address Book Entries}}</h2>
		<? foreach ($addresses as $result) { ?>
			<div class="section">
				<table style="width: 100%;">
					<tr>
						<td><?= $result['address']; ?></td>
						<td style="text-align: right;"><a href="<?= $result['update']; ?>"
								class="button">{{Edit}}</a> &nbsp; <a
								href="<?= $result['delete']; ?>" class="button">{{Delete}}</a></td>
					</tr>
				</table>
			</div>
		<? } ?>
		<div class="buttons">
			<div class="left"><a href="<?= $back; ?>" class="button">{{Back}}</a></div>
			<div class="right"><a href="<?= $insert; ?>" class="button">{{New Address}}</a></div>
		</div>

		<?= area('bottom'); ?>
	</div>
<?= $is_ajax ? '' : call('footer'); ?>
