<div class="account-info">
	<h1>{{Order History}}</h1>


	<? if (!empty($orders)) { ?>
		<div class="order-list">

			<? foreach ($orders as $order) { ?>
				<div class="order-item info-list">
					<div class="order-entry">
						<div class="info-item date-added">
							<span class="label">{{Date:}}</span>
							<span class="info"><?= format('date', $order['date_added'], 'short'); ?></span>
						</div>
						<div class="info-item order-id">
							<span class="label">{{Order ID:}}</span>
							<span class="info"><?= $order['order_id']; ?></span>
						</div>
						<div class="info-item status">
							<span class="label">{{Status:}}</span>
							<span class="info"><?= $order['order_status']['title']; ?></span>
						</div>
					</div>

					<div class="order-content">
						<div class="info-item products">
							<span class="label">{{Products:}}</span>
							<span class="info"><?= $order['products']; ?></span>
						</div>
						<div class="info-item customer">
							<span class="label">{{Customer:}}</span>
							<span class="info"><?= $order['name']; ?></span>
						</div>
						<div class="info-item total">
							<span class="label">{{Total:}}</span>
							<span class="info"><?= format('currency', $order['total']); ?></span>
						</div>
						<div class="order-buttons">
							<a class="view" href="<?= $order['href']; ?>">
								<img src="<?= theme_url('image/view.png'); ?>" alt="{{View}}" title="{{View Order}}"/>
							</a>
							<a class="reorder" href="<?= $order['reorder']; ?>">
								<img src="<?= theme_url('image/reorder.png'); ?>" alt="{{Reorder}}" title="{{Reorder}}"/>
							</a>
						</div>
					</div>
				</div>
			<? } ?>
		</div>

		<div class="pagination"><?= $pagination; ?></div>
	<? } else { ?>
		<div class="section">{{You have not made any previous orders!}}</div>
	<? } ?>
</div>
