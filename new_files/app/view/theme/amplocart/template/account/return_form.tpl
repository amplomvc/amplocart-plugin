<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>
<div class="content">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<?= area('top'); ?>

	<h1>{{Product Returns}}</h1>

	<div class="description">{{<p>Please complete the form below to request an RMA number.</p>}}</div>
	<form id="order-lookup" method="post" action="<?= $order_lookup_action; ?>">
		<div class="section">
			<h2>{{Lookup Order Information}}</h2>

			<div class="form-item ol-order_id">
				<label for="ol_order_id">{{Order ID:}}</label>
				<input type="text" size="2" name="ol_order_id" value=""/>
			</div>
			<div class="form-item ol-email">
				<label for="ol_email">{{E-Mail:}}</label>
				<input type="text" size="25" name="ol_email" value=""/>
			</div>
			<input type="submit" name="order_lookup" class="button" value="{{Find Order}}"/>
		</div>
	</form>

	<? if (!empty($return_products)) { ?>
		<form id="return-form" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
			<div class="section">
				<h2>{{Order Information}}</h2>

				<div class="order-info order-id">
					<label>{{Order ID:}}</label>
					<? if (count($customer_orders) > 1) { ?>
						<?=
						build(array(
							'type' => 'select',
							'name'  => 'order_id',
							'data'   => $customer_orders,
							'select' => $order_id,
							'value' => 'order_id',
							'label' => 'display',
						)); ?>
					<? } elseif ($order_id) { ?>
						<span class="value"><?= $order_id; ?></span>
					<? } ?>
					<input type="hidden" name="order_id" value="<?= $order_id; ?>"/>

				</div>

				<div class="order-info date-ordered">
					<label>{{Order Date:}}</label>
					<span class="value"><?= $date_ordered_display; ?></span>
					<input type="hidden" name="date_ordered" value="<?= $date_ordered; ?>"/>
				</div>
				<div class="form-item">
					<label for="first_name" class="required">{{First Name:}}</label>
					<input id="first_name" type="text" name="first_name" value="<?= $first_name; ?>"/>
				</div>
				<div class="form-item">
					<label for="last_name" class="required">{{Last Name:}}</label>
					<input id="last_name" type="text" name="last_name" value="<?= $last_name; ?>"/>
				</div>
				<div class="form-item">
					<label for="email" class="required">{{E-Mail:}}</label>
					<input id="email" type="text" name="email" value="<?= $email; ?>"/>
				</div>
				<div class="form-item">
					<label for="phone" class="required">{{Telephone:}}</label>
					<input id="phone" type="text" name="phone" value="<?= $phone; ?>"/>
				</div>
			</div>

			<div class="section">
				<h2>{{Product Information &amp; Reason for Return}}</h2>
				<table class="list return-product">
					<thead>
					<tr>
						<td>{{Product Name}}</td>
						<td>{{Model #}}</td>
						<td>{{Price}}</td>
						<td>{{Return Quantity}}</td>
						<td>{{Reason for Return}}</td>
						<td>{{Details About Return}}</td>
						<td>{{Product is Opened?}}</td>
					</tr>
					</thead>
					<tbody>
					<? foreach ($return_products as $product) { ?>
						<? $product_id = $product['product_id']; ?>
						<tr class="return-product">
							<td class="product">
								<?= $product['name']; ?>
								<input type="hidden" name="return_products[<?= $product_id; ?>][product_id]" value="<?= $product_id; ?>"/>
							</td>
							<td class="model">
								<?= $product['model']; ?>
							</td>
							<td class="price">
								<?= $product['price']; ?>
							</td>
							<td class="quantity">
								<? if (!empty($product['no_return'])) { ?>
									<span><?= $product['no_return']; ?></span>
									<input type="hidden" name="return_products[<?= $product_id; ?>][return_quantity]" value="0"/>
								<? } else { ?>
									<?=
									build(array(
										'type' => 'select',
										'name'  => "return_products[$product_id][return_quantity]",
										'data'   => range(0, (int)$product['quantity']),
										'select' => $product['return_quantity'],
									)); ?>
								<? } ?>
							</td>
							<td class="reason">
								<?=
								build(array(
									'type' => 'select',
									'name'  => "return_products[$product_id][return_reason_id]",
									'data'   => $data_return_reasons,
									'select' => $product['return_reason_id'],
									'value' => false,
									'label' => 'title',
								)); ?>
							</td>
							<td class="comment">
								<textarea name="return_products[<?= $product_id; ?>][comment]"><?= $product['comment']; ?></textarea>
							</td>
							<td
								class="opened"><?=
								build(array(
									'type' => 'select',
									'name'  => "return_products[$product_id][opened]",
									'data'   => $data_yes_no,
									'select' => $product['opened']
								)); ?></td>
						</tr>
					<? } ?>
					</tbody>
				</table>
			</div>

			<? if (!$order_lookup) { ?>
				<div class="return-captcha">
					<label>{{Enter the code in the box below:}}</label>
					<img src="<?= $url_captcha_image; ?>" alt=""/>
					<input type="text" name="captcha" value="<?= $captcha; ?>"/>
				</div>
				<div class="buttons clear">
					<div class="left">
						<a href="<?= $back; ?>" class="button">{{Back}}</a>
					</div>
					<div class="right">
						<input type="submit" value="{{Continue}}" class="button"/>
					</div>
				</div>
			<? } ?>
		</form>
	<? }//end if ((!empty($return_products))) ?>

	<?= area('bottom'); ?>
</div>

<script type="text/javascript">
	$('.order_info.order_id select').change(function () {
		location = "<?= $return_product_url; ?>" + '&order_id=' + $(this).val();
	});

	$.ac_datepicker();

</script>
<?= $is_ajax ? '' : call('footer'); ?>
