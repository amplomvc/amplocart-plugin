<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>
	<div class="content">
		<?= $is_ajax ? '' : breadcrumbs(); ?>
		<?= area('top'); ?>

		<h1>{{Product Returns}}</h1>
		<? if (!empty($returns)) { ?>
			<? foreach ($returns as $return) { ?>
				<div class="return-list">
					<div class="return-id"><b>{{RMA #:}}</b> #<?= $return['rma']; ?></div>
					<div class="return-status"><b>{{Status:}}</b> <?= $return['status']['title']; ?></div>
					<div class="return-content">
						<div><b>{{Date Added:}}</b> <?= $return['date_added']; ?><br/>
							<b>{{Order ID:}}</b> <?= $return['order_id']; ?></div>
						<div><b>{{Customer:}}</b> <?= $return['name']; ?></div>
						<div class="return-info"><a href="<?= $return['href']; ?>"><img
									src="<?= theme_url('image/info.png'); ?>" alt="{{View}}"
									title="{{View}}"/></a></div>
					</div>
				</div>
			<? } ?>
			<div class="pagination"><?= $pagination; ?></div>
		<? } else { ?>
			<div class="section">{{You have not made any previous returns!}}</div>
		<? } ?>

		<div class="buttons">
			<div class="right"><a href="<?= $continue; ?>" class="button">{{Continue}}</a></div>
		</div>

		<?= area('bottom'); ?>
	</div>

<?= $is_ajax ? '' : call('footer'); ?>
