<section id="address-form-page" class="content">
	<header class="row top-row">
		<div class="wrap">
			<h2>
				<? if ($address_id) { ?>
					{{Edit Address}}
				<? } else { ?>
					{{Add Address}}
				<? } ?>
			</h2>
		</div>
	</header>

	<div class="address-row row">
		<div class="wrap">
			<form id="address-form" class="form" action="<?= $save; ?>" method="post" enctype="multipart/form-data">
				<div class="form-item first-name">
					<input type="text" placeholder="{{First Name}}" name="first_name" value="<?= $first_name; ?>"/>
				</div>
				<div class="form-item last-name">
					<input type="text" placeholder="{{Last Name}}" name="last_name" value="<?= $last_name; ?>"/>
				</div>
				<div class="form-item company">
					<input type="text" placeholder="{{Company}}" name="company" value="<?= $company; ?>"/>
				</div>
				<div class="form-item address">
					<input type="text" placeholder="{{Address}}" name="address" value="<?= $address; ?>"/>
				</div>
				<div class="form-item address-line-2">
					<input type="text" placeholder="{{Address Line 2}}" name="address_2" value="<?= $address_2; ?>"/>
				</div>
				<div class="form-item city">
					<input type="text" placeholder="{{City}}" name="city" value="<?= $city; ?>"/>
				</div>
				<div class="form-item postcode">
					<input type="text" placeholder="{{Post Code}}" name="postcode" value="<?= $postcode; ?>"/>
				</div>
				<div class="form-item country-select">
					<?= build(array(
						'type'   => 'select',
						'name'   => 'country_id',
						'data'   => $data_countries,
						'select' => $country_id,
						'value' => 'country_id',
						'label' => 'name',
					)); ?>
				</div>
				<div class="form-item zone-select">
					<select name="zone_id" data-zone-id="<?= $zone_id; ?>"></select>
				</div>
				<div class="form-item default-address">
					<div class="text">{{Set as Default Address?}}</div>
					<?= build(array(
						'type'   => 'radio',
						'name'   => 'default',
						'data'   => $data_yes_no,
						'select' => $default,
					)); ?>
				</div>

				<div class="form-item submit">
					<button data-loading="{{Saving...}}">{{Save}}</button>
				</div>
			</form>
		</div>
	</div>
</section>

<script type="text/javascript">
	$('#address-form').submit(function () {
		var $this = $(this);

		$this.find('button').loading();

		$.post($(this).attr('action'), $(this).serialize(), function (json) {
			$this.find('button').loading('stop');

			if (json) {
				if (json.error) {
					$.ac_errors(json.error);
				} else {
					location.reload()
				}
			}
		}, 'json');
		return false;
	});

	$('#address-form [name=zone_id]').ac_zoneselect({listen: '#address-form [name=country_id]'});

</script>
