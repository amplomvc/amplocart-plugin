<div class="shipping-amount-form">
	<? $amount_format = function ($a) {
		return $a['title'] . ' - ' . format('currency', $a['cost']);
	} ?>

	<?= build(array(
		'type'   => 'radio',
		'name'   => 'shipping_key',
		'data'   => format_all($amount_format, $quotes),
		'select' => $shipping_key,
		'value' => 'shipping_key',
		'label' => 'formatted',
	)); ?>
</div>
