<div class="shipping-weight-form">
	<? $weight_format = function ($a) {
		return $a['title'] . ' - ' . format('currency', $a['cost']);
	} ?>

	<? $build = array(
		'name'   => 'shipping_key',
		'data'   => format_all($weight_format, $quotes),
		'select' => $shipping_key,
		'value' => 'shipping_key',
		'label' => 'formatted',
	); ?>

	<?= build('ac-radio', $build); ?>
</div>
