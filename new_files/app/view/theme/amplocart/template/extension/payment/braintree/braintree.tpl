<div class="braintree-form">
	<div class="braintree-checkout">
		<? if (!empty($cards)) { ?>
			<h1>{{Existing Credit Card}}</h1>
		<? } ?>

		<? $card_format = function ($card, $key) {
			return <<<HTML
				<div class="card">
					<div class="card-type">
						<img src="$card[image]" alt="$card[type]"/>
					</div>
					<div class="name">$card[name]</div>
					<div class="number">$card[masked]</div>
				</div>
HTML;
		}; ?>

		<?= build(array(
			'type'   => 'radio',
			'name'   => 'payment_key',
			'data'   => format_all($card_format, $cards),
			'select' => $payment_key,
			'value' => 'id',
			'label' => 'formatted',
		)); ?>

		<label class="new-card ac-radio" for="bt-new-card">
			<input type="radio" name="payment_key" value="new" <?= !$payment_key ? 'checked' : ''; ?> />

			<div class="label">
				<?= call('extension/payment/braintree/register_card', array('template' => 'extension/payment/braintree/register_fields')); ?>
			</div>
		</label>
	</div>
</div>
