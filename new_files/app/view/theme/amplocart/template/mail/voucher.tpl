<?= call('mail/header', $header); ?>

<? if (!empty($voucher_image)) { ?>
	<div style="float: right; margin-left: 20px;">
		<a href="<?= $store['url']; ?>" title="<?= $store['name']; ?>">
			<img width="<?= $image_width; ?>" height="<?= $image_height; ?>" src="<?= $voucher_image; ?>" alt="<?= $store['name']; ?>"
				style="margin-bottom: 20px; border: none;"/>
		</a>
	</div>
<? } ?>

<div>
	<p style="margin-top: 0px; margin-bottom: 20px;">{{Congratulations, You have received a Gift Certificate!}}</p>

	<p style="margin-top: 0px; margin-bottom: 20px;"><?= _l("This Gift Certificate has been sent to you by %s", $from_name); ?></p>
	<? if ($message) { ?>
		<p style="margin-top: 0px; margin-bottom: 20px;"><?= $message; ?></p>
	<? } ?>
	<div style="margin-top: 0px; margin-bottom: 20px;">
		<span style="font-size: 16px;">{{Voucher Value:}}</span>
		<span style="font-size: 20px; color: green;"><?= format('currency', $amount, $currency_code, $currency_value); ?></span>
	</div>
	<div style="margin-top: 0px; margin-bottom: 20px;">
		<span style="font-size: 16px;">{{Voucher Code:}}</span>
		<span style="font-size: 20px; font-weight: bold;"><?= $code; ?></span>
	</div>

	<p style="margin-top: 0px; margin-bottom: 20px;">
		{{To redeem this Voucher, visit}}
		<a href="<?= $redeem_url; ?>"><?= $store['name']; ?></a>
		{{, find something you like and proceed to checkout. During the checkout you may enter the voucher code.}}
	</p>

	<p style="margin-top: 0px; margin-bottom: 20px;">
		<a href="<?= $store['url']; ?>" title="<?= $store['name']; ?>"><?= $store['url']; ?></a>
	</p>

	<p style="margin-top: 0px; margin-bottom: 20px;">{{Please reply to this email if you have any questions.}}</p>
</div>

<?= call('mail/footer'); ?>
