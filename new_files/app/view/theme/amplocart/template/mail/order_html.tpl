<?= call('mail/header', $header); ?>

<p style="margin-top: 0px; margin-bottom: 20px;">
	<?= _l("Thank you for your purchase from %s! Your order has been received and will be processed once payment has been confirmed.", $store['name']); ?>
</p>

<? if ($customer_id) { ?>
	<p style="margin-top: 0px; margin-bottom: 20px;">{{To view your order click on the link below:}}</p>
	<p style="margin-top: 0px; margin-bottom: 20px;">
		<a href="<?= $link; ?>"><?= $link; ?></a>
	</p>
<? } ?>
<? if (!empty($order_downloads)) { ?>
	<p style="margin-top: 0px; margin-bottom: 20px;">{{Once your payment has been confirmed you can click on the link below to access your downloadable products:}}</p>
	<p style="margin-top: 0px; margin-bottom: 20px;">
		<a href="<?= store_url($store['store_id'], 'account/download'); ?>"><?= store_url($store['store_id'], 'account/download'); ?></a>
	</p>
<? } ?>
<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
	<thead>
	<tr>
		<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;" colspan="2">
			{{Order Details:}}
		</td>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
			<b>{{Order ID:}}</b> <?= $order_id; ?><br/>
			<b>{{Date Added:}}</b> <?= format('date', $date_added, 'short'); ?><br/>
			<b>{{Payment Method:}}</b> <?= $payment_method['title']; ?><br/>
			<? if (!empty($shipping_method)) { ?>
				<b>{{Shipping Method:}}</b> <?= $shipping_method['title']; ?>
			<? } ?>
		</td>

		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
			<b>{{Email:}}</b> <?= $email; ?><br/>
			<b>{{Phone:}}</b> <?= $phone; ?><br/>
			<b>{{Customer IP Address:}}</b> <?= $ip; ?><br/>
		</td>
	</tr>
	</tbody>
</table>
<? if ($comment) { ?>
	<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
		<thead>
		<tr>
			<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">
				{{Order Comments:}}
			</td>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
				<?= $comment; ?>
			</td>
		</tr>
		</tbody>
	</table>
<? } ?>
<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
	<thead>
	<tr>
		<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">
			{{Billing Address}}
		</td>
		<? if ($shipping_address) { ?>
			<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">
				{{Delivery Address}}
			</td>
		<? } ?>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
			<?= format('address', $payment_address); ?>
		</td>
		<? if ($shipping_address) { ?>
			<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
				<?= format('address', $shipping_address); ?>
			</td>
		<? } ?>
	</tr>
	</tbody>
</table>
<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
	<thead>
	<tr>
		<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">
			{{Product}}
		</td>
		<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">
			{{Model #}}
		</td>
		<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">
			{{Quantity}}
		</td>
		<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">
			{{Price}}
		</td>
		<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">
			{{Total}}
		</td>
	</tr>
	</thead>
	<tbody>
	<? foreach ($order_products as $product) { ?>
		<tr>
			<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
				<?= $product['name']; ?>

				<? foreach ($product['options'] as $option) { ?>
					<br/>
					&nbsp;
					<small> - <?= $option['name']; ?>: <?= $option['display_value'] ? $option['display_value'] : $option['value']; ?></small>
				<? } ?>
			</td>
			<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
				<?= $product['model']; ?>
			</td>
			<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
				<?= $product['quantity']; ?>
			</td>
			<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
				<?= format('currency', $product['price'], $currency_code, $currency_value); ?>
			</td>
			<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
				<?= format('currency', $product['total'], $currency_code, $currency_value); ?>
			</td>
		</tr>
	<? } ?>
	</tbody>
	<tfoot>
	<? foreach ($order_totals as $total) { ?>
		<tr>
			<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="4">
				<b><?= $total['title']; ?>:</b>
			</td>
			<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">
				<?= format('currency', $total['amount'], $currency_code, $currency_value); ?>
			</td>
		</tr>
	<? } ?>
	</tfoot>
</table>
<p style="margin-top: 0px; margin-bottom: 20px;">{{Please reply to this email if you have any questions.}}</p>

<?= call('mail/footer'); ?>
