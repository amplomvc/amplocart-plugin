<? if ($has_shipping) { ?>
	<div id="shipping-method" class="col xs-12 md-6">
		<? if (!empty($shipping_methods)) { ?>
			<div class="method-list">
				<h3>{{Delivery Method}}</h3>

				<? $method_format = function ($a) {
					return call('extension/shipping/' . $a['code']);
				}; ?>

				<?= build(array(
					'type'   => 'radio',
					'name'   => 'shipping_code',
					'data'   => format_all($method_format, $shipping_methods),
					'select' => $shipping_code,
					'value' => 'code',
					'label' => 'formatted',
				)); ?>
			</div>
			<a class="change-method">{{Change Method}}</a>
		<? } else { ?>
			<? if ($shipping_key) { ?>
				<div class="error"><?= _l("There are no available Delivery Methods for your order! Please contact <a href=\"mailto:%s\">Customer Support</a> to complete your order.", option('site_email')); ?></div>
			<? } else { ?>
				<div class="notify">{{Please select your Delivery Address}}</div>
			<? } ?>
		<? } ?>
	</div>
<? } ?>

<div id="payment-method" class="col xs-12 <?= $has_shipping ? 'md-6' : ''; ?>">
	<? if (!empty($payment_methods)) { ?>
		<div class="method-list">
			<h3>{{Payment Method}}</h3>

			<? $method_format = function ($a) {
				return call('extension/payment/' . $a['code']);
			}; ?>

			<?= build(array(
				'type'   => 'radio',
				'name'   => 'payment_code',
				'data'   => format_all($method_format, $payment_methods),
				'select' => $payment_code,
				'value' => 'code',
				'label' => 'formatted',
			)); ?>
		</div>
		<a class="change-method">{{Change Method}}</a>
	<? } else { ?>
		<? if ($payment_key) { ?>
			<div class="error"><?= _l("There are no available Payment Methods for your order! Please contact <a href=\"mailto:%s\">Customer Support</a> to complete your order.", option('site_email')); ?></div>
		<? } else { ?>
			<div class="notify">{{Please select your Billing Address}}</div>
		<? } ?>
	<? } ?>
</div>
