<div id="checkout-confirmation-form">
	<? if (option('coupon_status')) { ?>
		<div class="checkout-coupon col xs-12 md-6 center">
			<?= block('cart/code'); ?>
		</div>
	<? } ?>

	<form action="<?= $action; ?>" class="form clear" method="post">
		<div class="checkout-totals">
			<?= block('cart/total'); ?>
		</div>

		<div class="checkout-payment">
			<? if ($has_confirmation) { ?>
				<?= call('extension/payment/' . $payment_code . '/confirmation'); ?>
			<? } else { ?>
				<button class="checkout-confirm">{{Confirm Order}}</button>
			<? } ?>
		</div>
	</form>
</div>
