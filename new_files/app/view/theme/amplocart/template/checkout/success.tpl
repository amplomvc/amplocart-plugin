<?= $is_ajax ? '' : call('header'); ?>

<?= area('left'); ?>
<?= area('right'); ?>

<section id="checkout-success" class="content">
	<header class="row top-row">
		<div class="wrap">
			<?= $is_ajax ? '' : breadcrumbs(); ?>
			<h1>{{Your Order Has Been Processed}}</h1>
		</div>
	</header>

	<?= area('top'); ?>

	<div class="checkout-success-row row">
		<div class="wrap">
			<div class="success-message">
				<? if (is_logged()) { ?>
					<p>
						<?= _l("You can view your order history by going to the <a href=\"%s\">my account</a> page and by clicking on <a href=\"%s\">history</a>.", site_url('account'), site_url('account/order')); ?>
					</p>
				<? } ?>

				<p>
					<?= _l("If you have any questions or concerns please feel free to <a href=\"%s\">contact us</a>.", site_url('contact')); ?>
				</p>
				<p>
					<?= _l("Thanks for shopping with %s!", option('site_name')); ?>
				</p>
			</div>

			<div class="buttons">
				<div class="right"><a href="<?= site_url(); ?>" class="button">{{Continue}}</a></div>
			</div>
		</div>
	</div>

	<?= area('bottom'); ?>

</section>

<?= $is_ajax ? '' : call('footer'); ?>
