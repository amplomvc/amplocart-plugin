<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?>
<?= area('right'); ?>

<section id="checkout-page" class="content">

	<header class="row top-row">
		<div class="wrap">
			<?= $is_ajax ? '' : breadcrumbs(); ?>

			<h1>{{Checkout}}</h1>
		</div>
	</header>

	<?= area('top'); ?>

	<? if (!is_logged() && !$is_guest) {
		$step = 'login';
	} elseif (($has_shipping && !$shipping_address_id) || !$payment_address_id) {
		$step = 'address';
	} elseif (($has_shipping && !$shipping_key) || !$payment_key) {
		$step = 'methods';
	} else {
		$step = 'confirmation';
	} ?>

	<? if ($step === 'login') { ?>
		<div class="row checkout-login-row">
			<div class="wrap">
				<? $this->request->setRedirect(site_url('checkout')); ?>
				<?= call('customer/login', null, true); ?>
			</div>
		</div>
	<? } else { ?>
		<form id="checkout-form" action="<?= site_url('checkout/add_order'); ?>" class="form <?= $step; ?>" method="post">
			<div class="row checkout-row">
				<div class="wrap">
					<? if ($is_guest) { ?>
						<?= call('checkout/guest'); ?>
					<? } else { ?>
						<? if ($has_shipping) { ?>
							<div class="checkout-delivery col xs-12 md-6">
								<div id="shipping-address" class="address-list">
									<h3>{{Delivery Address}}</h3>
									<?= build(array(
										'type'   => 'radio',
										'name'   => 'shipping_address_id',
										'data'   => format_all('address', $shipping_addresses),
										'select' => $shipping_address_id,
										'value' => 'address_id',
										'label' => 'formatted',
									)); ?>

									<a href="<?= site_url('account/address/form', 'select=shipping_address_id'); ?>" class="add-address colorbox">{{Add New Address}}</a>
								</div>
								<a class="change-address">{{Change Address}}</a>
							</div>
						<? } ?>

						<div class="checkout-payment col xs-12 <?= $has_shipping ? 'md-6' : ''; ?>">
							<div id="payment-address" class="address-list">
								<h3>{{Billing Address}}</h3>
								<?= build(array(
									'type'   => 'radio',
									'name'   => 'payment_address_id',
									'data'   => format_all('address', $payment_addresses),
									'select' => $payment_address_id,
									'value' => 'address_id',
									'label' => 'formatted',
								)); ?>

								<a href="<?= site_url('account/address/form', 'select=payment_address_id'); ?>" class="add-address colorbox">{{Add New Address}}</a>
							</div>
							<a class="change-address">{{Change Address}}</a>
						</div>
					<? } ?>
				</div>
			</div>

			<div class="row checkout-methods">
				<div class="wrap">
					<?= call('checkout/methods'); ?>
				</div>
			</div>

			<div class="checkout-submit-row row">
				<div class="wrap">
					<div class="checkout-submit col xs-12">
						<div class="form-item submit">
							<button data-loading="{{Submitting...}}">{{Continue Checkout}}</button>
						</div>
					</div>
				</div>
			</div>
		</form>

	<? } ?>

	<div class="row checkout-confirmation">
		<div class="wrap">
			<? if ($step === 'confirmation') { ?>
				<?= call('checkout/confirmation'); ?>
			<? } ?>
		</div>
	</div>

	<?= area('bottom'); ?>

</section>

<script type="text/javascript">
	var $co_form = $('#checkout-form');
	$co_form.submit(function () {
		var $this = $(this);
		var $button = $this.find('button');
		$button.loading();
		$co_form.loading();

		$.post($this.attr('action'), $this.serialize(), function (response) {
			$button.loading('stop');
			$co_form.loading('stop');

			if (!response) {
				response = {error: "{{There was a problem checking you out. Please try again.}}"}
			}

			if (response.success) {
				$co_form.removeClass("methods address").addClass("confirmation");
				$co_form.loading();
				$button.loading();

				refresh_methods();

				$.post("<?= site_url('checkout/confirmation'); ?>", {}, function (response) {
					$co_form.loading('stop');
					$button.loading('stop');
					$('#checkout-page').find('.checkout-confirmation .wrap').html(response);
				}, 'html');
			} else if (response.error) {
				$co_form.find('.checkout-submit').ac_errors(response.error);
			}

			$co_form.find('.checkout-submit').show_msg(response);
		}, 'json');

		return false;
	});

	$('.address-list label').click(function () {
		var $this = $(this);

		$this.closest('.address-list').find('label').removeClass('checked');
		$this.addClass('checked');
		$this.find('input[type=radio]').prop('checked', true);

		refresh_methods();

		return false;
	});

	function refresh_methods() {
		if ($co_form.find('[name=payment_address_id]:checked').length <?= $has_shipping ? "&& \$co_form.find('[name=shipping_address_id]:checked').length" : ''; ?>) {
			$co_form.find('button').loading({text: "{{Loading}}"});
			var $co_methods = $co_form.find('.checkout-methods .wrap');
			$co_methods.children().fadeOut();
			$co_methods.loading();

			$.post("<?= site_url('checkout/methods'); ?>", $co_form.serialize(), function (response) {
				$co_form.find('button').loading('stop');
				$co_form.addClass('methods');
				$co_methods.html(response);

				if (!$co_methods.find('[name=payment_code]:checked').length <?= $has_shipping ? "|| !\$co_methods.find('[name=shipping_code]:checked').length" : ''; ?>) {
					$co_form.removeClass('confirmation').addClass('methods');
				}

				$co_methods.find('.change-method').click(function () {
					$co_form.removeClass('confirmation').addClass('methods');
				});
			});
		}
	}

	refresh_methods();

	$('.change-address').click(function () {
		$co_form.removeClass('methods confirmation').addClass('address');
	});
</script>

<?= $is_ajax ? '' : call('footer'); ?>
