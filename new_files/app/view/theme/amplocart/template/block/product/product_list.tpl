<div class="catalog-listing">

	<div class="list-grid-toggle">
		<span>{{Display}}</span>
		<a class="toggle-list">{{List}}</a>
		<b>/</b>
		<a class="active" class="toggle-grid">{{Grid}}</a>
	</div>

	<div class="listing-products" class="grid">
		<? foreach ($products as $product) { ?>
			<a class="listing-product" href="<?= site_url('product/product', 'product_id=' . $product['product_id']); ?>">

				<? if ($product['image']) { ?>
					<div class="image">
						<img class="primary" src="<?= image($product['image'], $image_width, $image_height, true); ?>" title="<?= $product['name']; ?>" alt="<?= $product['name']; ?>"/>

						<? if (!empty($product['hover_image'])) { ?>
							<img class="backup" src="<?= image($product['hover_image'], $image_width, $image_height); ?>" title="<?= $product['name']; ?>" alt="<?= $product['name']; ?>"/>
						<? } ?>
					</div>
				<? } ?>

				<div class="item-text">
					<div class="name"><?= $product['name']; ?></div>

					<? if (!empty($product['teaser'])) { ?>
						<div class="teaser"><?= charlimit(html_entity_decode($product['teaser']), 80); ?></div>
					<? } ?>
				</div>
				<? if (!option('config_customer_hide_price') || is_logged()) { ?>
					<div class="price">
						<? if (empty($product['special'])) { ?>
							<?= format('currency', $product['price']); ?>
						<? } else { ?>
							<span class="retail"><?= format('currency', $product['price']); ?></span>
							<span class="special"><?= format('currency', $product['special']); ?></span>
						<? } ?>

						<? if ($show_price_with_tax) { ?>
							<br/>
							<span class="price-tax">{{Tax:}} <?= format('currency', $product['tax']); ?></span>
						<? } ?>
					</div>
				<? } ?>

				<? if ($review_status) { ?>
					<div class="rating">
						<img src="<?= theme_image("stars-$product[rating].png"); ?>" alt="<?= _l("Based on %s reviews.", (int)$product['reviews']); ?>"/>
					</div>
				<? } ?>

				<? if ($list_show_add_to_cart) { ?>
					<div class="cart">
						<input type="button" value="{{Add to Cart}}" onclick="addToCart('<?= $product['product_id']; ?>');"
							class="button"/>
					</div>
				<? } ?>

				<? if ($wishlist_status) { ?>
					<div class="wishlist">
						<a onclick="addToWishList('<?= $product['product_id']; ?>');">{{Add to Wishlist}}</a>
					</div>
				<? } ?>
				<? if ($compare_status) { ?>
					<div class="compare">
						<a onclick="addToCompare('<?= $product['product_id']; ?>');">{{Add to Compare}}</a>
					</div>
				<? } ?>
			</a>
		<? } ?>
	</div>

</div>
<script type="text/javascript">
	(function () {
		var $listing = $('.catalog-listing').not('.activated').addClass('activated');

		$listing.find('.list-grid-toggle > a').click(function () {
			var $this = $(this);

			if ($this.hasClass('active')) return;

			var view = $this.hasClass('toggle-list') ? 'list' : 'grid';

			$listing('.list-grid-toggle a.active').removeClass('active');
			$this.addClass('active');

			$listing.find('.listing-items').removeClass('grid list').addClass(view);

			$.cookie('display', view);
		});

		view = $.cookie('display') || 'grid';

		$listing.find('.toggle-' + view).click();
	})();
</script>
