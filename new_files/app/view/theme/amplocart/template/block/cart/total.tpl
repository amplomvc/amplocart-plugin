<div class="block-cart-total">
	<div class="cart-total row">
		<div class="col xs-12 sm-8 md-6 lg-4 center">
			<div class="total-line-items">
				<? foreach ($totals as $total) { ?>
					<div class="line-item">
						<div class="title"><?= $total['title']; ?>:</div>
						<div class="text"><?= format('currency', $total['amount']); ?></div>
					</div>
				<? } ?>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('body').on('reload_totals cart_loaded', function () {
			var $block = $('.block-cart-total').loading();
			$.get("<?= site_url('block/cart/total/build'); ?>",{},function(response){
				$block.replaceWith(response);
			});
		});
	</script>
</div>
