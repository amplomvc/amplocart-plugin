<div class="shipping-quote">
	<h2>{{Please select the preferred shipping method to use on this order.}}</h2>

	<form class="shipping-quote-form" action="<?= $apply; ?>" method="post">
			<? foreach ($shipping_methods as $method) { ?>
				<? html_dump($method, 'method'); ?>
			<? } ?>

		<input type="hidden" name="add_address" value="0"/>

		<input type="hidden" name="country_id" value=""/>
		<input type="hidden" name="zone_id" value=""/>
		<input type="hidden" name="postcode" value=""/>

		<div class="quote-address"></div>

		<button class="apply-quote">{{Apply Shipping}}</button>
	</form>
</div>

<script type="text/javascript">
	<? if (!empty($message)) { ?>
		$('.shipping-quote').show_msg(<?= $message; ?>);
	<? } ?>

	$('.shipping-quote .shipping-quote-form').submit(function() {
		var $form = $(this);

		$.post($form.attr('action'), $form.serialize(), function (response) {
			console.log(response);
			if (json['request_address']) {
				$form.find('[name=add_address]').val(1);
				$form.find('#quote_full_address').slideDown();
				$form.show_msg('notify', '{{Please provide additional shipping information.}}');
				return;
			}

			$form.show_msg(response);
		});

		return false;
	});
</script>
