<form class="cart-shipping-estimate action="<?= $action; ?>" class="section">
	<p>{{Enter your destination to get a shipping estimate.}}</p>
	<table>
		<tr>
			<td class="required"> {{Country:}}</td>
			<td>
				<?= build(array(
					'type' => 'select',
					'name'  => "country_id",
					'data'   => $data_countries,
					'select' => $country_id,
					'value' => 'country_id',
					'label' => 'name',
				)); ?>
			</td>
		</tr>
		<tr>
			<td class="required"> {{Region / State:}}</td>
			<td><select name="zone_id" class="zone_select" data-zone_id="<?= $zone_id; ?>"></select></td>
		</tr>
		<tr>
			<td class="required"> {{Post Code:}}</td>
			<td><input type="text" name="postcode" value="<?= $postcode; ?>"/></td>
		</tr>
	</table>
	<button class="get-quote colorbox" data-loading="{{Calculating...}}">{{Get Quotes}}</button>
</form>



<script type="text/javascript">
	var $estimate = $('.cart-shipping-estimate');
	$estimate.find('.zone_select').ac_zoneselect({listen: '.cart-shipping-estimate [name="country_id"]'});

	$estimate.find('.get-quote').click(function () {
		var $this = $(this);

		$this.loading();
		$.post("<?= site_url('block/cart/shipping/quote'); ?>", $('#cart-shipping').serialize(), function (response) {
			$this.loading('stop');
			$.colorbox({html: response});
		},'html');

		return false;
	});
</script>
