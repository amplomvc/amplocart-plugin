<div id="the-cart">
	<? if (!$show_price) { ?>
		<div class="no-price"><?= _l("Please <a href=\"%s\">Login</a> or <a href=\"%s\">Register</a> to see Prices.", site_url('customer/login'), site_url('customer/registration')); ?></div>
	<? } ?>

	<form id="cart-form" action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="block_cart_form" value="1"/>

		<? if (!empty($cart_products)) { ?>
			<div class="cart-info">
				<table>
					<thead>
					<tr>
						<td class="image">{{Image}}</td>
						<td class="name">{{Product Name}}</td>
						<td class="model">{{Model}}</td>
						<td class="quantity">{{Quantity}}</td>

						<? if (option('config_cart_weight')) { ?>
							<td class="weight">{{Weight}}</td>
						<? } ?>

						<? if (!empty($return_policy)) { ?>
							<td class="return-policy">{{Return Period}}</td>
						<? } ?>

						<? if ($show_price) { ?>
							<td class="price">{{Unit Price}}</td>
							<td class="total">{{Total}}</td>
						<? } ?>

						<td class="center remove">{{Remove}}</td>
					</tr>
					</thead>

					<tbody>
					<? if (!empty($cart_products)) { ?>
						<? foreach ($cart_products as $cart_product) { ?>
							<? $product = $cart_product['product']; ?>
							<? if (empty($product)) {
								continue;
							} ?>
							<tr class="product">
								<td class="image">
									<? if (!empty($product['image'])) { ?>
										<a href="<?= site_url('product/product', 'product_id=' . $product['product_id']); ?>">
											<img src="<?= image($cart_product['product']['image'], option('config_image_cart_width'), option('config_image_cart_height')); ?>" alt="<?= $product['name']; ?>" title="<?= $product['name']; ?>"/>
										</a>
									<? } ?>
								</td>
								<td class="name">
									<a href="<?= site_url('product/product', 'product_id=' . $product['product_id']); ?>"><?= $product['name']; ?></a>
									<? if (!$cart_product['in_stock']) { ?>
										<span class="out-of-stock"></span>
									<? } ?>

									<? if (!empty($cart_product['options'])) { ?>
										<div class="product-option-description">
											<? foreach ($cart_product['options'] as $product_option_id => $product_option_values) { ?>
												<? foreach ($product_option_values as $product_option_value) { ?>
													<? if ($product_option_value['display_value']) { ?>
														<div class="cart-product-option-value">
															<?= $product_option_value['display_value']; ?>
														</div>
													<? } else { ?>
														<div class="cart-product-option-value">
															<span class="name"><?= $product_option_value['name']; ?></span>
															<span class="value"><?= $product_option_value['value']; ?></span>
														</div>
													<? } ?>
												<? } ?>
											<? } ?>
										</div>
									<? } ?>

									<? if ($product['reward']) { ?>
										<span class="cart-product-reward"><?= _l("Total Points: %s", $product['reward']); ?></span>
									<? } ?>
								</td>
								<td class="model"><?= $product['model']; ?></td>
								<td class="quantity">
									<input type="text" name="quantity[<?= $cart_product['key']; ?>]" value="<?= $cart_product['quantity']; ?>" size="1"/>
									<input id="update-<?= $cart_product['key']; ?>" class="update" type="image" name="cart_update" value="1" src="<?= theme_url('image/update.png'); ?>" alt="{{Update}}" title="{{Update your Cart}}"/>
									<label for="update-<?= $cart_product['key']; ?>" data-loading="{{Updating...}}">{{Update}}</label>
								</td>

								<? if (option('config_cart_weight')) { ?>
									<td class="weight">
										<?= format('weight', $product['weight']); ?>
									</td>
								<? } ?>

								<? if (!empty($return_policy)) { ?>
									<td class="return_policy">
										<? if ($return_policy['days'] > 0) {
											echo _l("%s Days", $policy['days']);
										} elseif ((int)$return_policy['days'] === 0) {
											echo _l("You may return at anytime");
										} else { ?>
											<div class="extra-info-block">
												<span class="final-sale"></span>
												<span class="help-icon">
													<span class="help_icon_popup"><?= _l("A Product Marked as Final Sale cannot be returned. Read our <a href=\"%s\" class=\"colorbox\">Return Policy</a> for details.", site_url('page/content', 'page_id=' . option('config_shipping_return_page_id'))); ?></span>
												</span>
											</div>
										<? } ?>
									</td>
								<? } ?>

								<? if (!isset($no_price_display)) { ?>
									<td class="price"><?= format('currency', $cart_product['price']); ?></td>
									<td class="total"><?= format('currency', $cart_product['total']); ?></td>
								<? } ?>

								<td class="center">
									<a href="<?= site_url("cart/remove", 'cart_key=' . $cart_product['key']); ?>" class="button remove" data-loading="O">{{X}}</a>
								</td>
							</tr>
						<? } ?>
					<? } ?>

					<?= $cart_inline; ?>

					</tbody>
				</table>
			</div>
		<? } elseif ($is_empty) { ?>
			<div class="center">
				<h2>{{Your shopping cart is empty! Please check back here after you have added something to your cart!}}</h2>
			</div>
		<? } ?>

		<? if ($is_ajax) { ?>
			<?= $this->message->render(); ?>
		<? } ?>
	</form>

	<div id="cart-extended">
		<?= $cart_extend; ?>
	</div>
</div>

<script type="text/javascript">
	$('[name=cart_update]').click(function () {
		var $this = $(this);
		var data = $this.closest('form').serializeArray();

		data.push({name: 'cart_update', value: $this.val()});

		$this.siblings('label').loading();
		$this.closest('td').addClass('loading');
		$.post("<?= site_url('cart/update'); ?>", data, function (html) {
			if (!html) {
				html = "<div id=\"the-cart\">{{The Cart is empty.}}</div>";
			}

			$('body').trigger('cart_loaded');
			$('#the-cart').replaceWith(html);
		}, 'html');

		return false;
	});

	$('#cart-form .remove').click(function () {
		var $this = $(this);

		$this.loading();
		$.get($this.attr('href'), {}, function (response) {
			$this.loading('stop');

			$this.closest('form').show_msg(response);

			if (response.success) {
				$this.closest('tr').remove();
			}
		}, 'json');

		return false;
	});
</script>
