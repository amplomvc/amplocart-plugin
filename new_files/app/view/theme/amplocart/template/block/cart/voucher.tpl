<div id="voucher" class="section">
	<form action="<?= $action; ?>" method="post" enctype="multipart/form-data">
		{{Enter your gift voucher code here:}}
		<input type="text" name="voucher" value="<?= $voucher; ?>"/>
		<input type="hidden" name="next" value="voucher"/>
		<input type="submit" value="{{Apply Voucher}}" class="button"/>
	</form>
</div>
