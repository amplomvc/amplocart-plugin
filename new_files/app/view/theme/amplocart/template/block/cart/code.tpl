<div class="block-cart-code">
	<form action="<?= $action; ?>" class="apply-code-form form" method="post">
		<div class="form-item apply-code">
			<input type="text" name="code" placeholder="{{Enter Code}}" value=""/>
			<button data-loading="{{Applying...}}">{{Apply}}</button>
		</div>
	</form>
</div>

<script type="text/javascript">
	$('.apply-code-form').not('.activated').addClass('activated').submit(function () {
		var $this = $(this);

		$this.find('button').loading();
		$.post($this.attr('action'), $this.serialize(), function (response) {
			$this.find('button').loading('stop');

			if (response.success) {
				$('body').trigger('reload_totals');
			}

			$this.show_msg(response);
		}, 'json');

		return false;
	});
</script>
