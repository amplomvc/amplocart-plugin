<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?>
<?= area('right'); ?>

<section id="product-<?= $product_id; ?>" class="product-content product-voucher content">
	<header class="row top-row">
		<div class="wrap">
			<?= $is_ajax ? '' : breadcrumbs(); ?>

			<h1><?= $name; ?></h1>
		</div>
	</header>

	<?= area('top'); ?>

	<div class="row product-row">
		<div class="wrap">
			<div class="product-image col xs-12 md-6 top">
				<? if (!empty($thumb)) { ?>
					<div class="image clearfix">
						<a href="<?= $popup; ?>" title="<?= $name; ?>" class="colorbox clearfix" rel="gal1">
							<img src="<?= $thumb; ?>" title="<?= $name; ?>" alt="<?= $name; ?>" id="image"/>
						</a>
					</div>
				<? } ?>
			</div>

			<div class="product-info col xs-12 md-6 top">
				<div class="product-top">
					<? if ($show_price && $is_purchasable) { ?>
						<div class="price">
							<? if (empty($special)) { ?>
								<span class="regular"><?= $price; ?></span>
							<? } else { ?>
								<span class="special"><?= $special; ?></span>
								<span class="retail"><?= _l("%s retail", $price); ?></span>
							<? } ?>

							<? if (!empty($tax)) { ?>
								<div class="price-tax"><?= _l("Ex Tax: %s", $tax); ?></div>
							<? } ?>

							<? if (!empty($points)) { ?>
								<div class="price-reward"><?= _l("Price in reward points: %s", $points); ?></div>
							<? } ?>

							<? if (!empty($discounts)) { ?>
								<div class="discounts">
									<? foreach ($discounts as $discount) { ?>
										<div class="discount"><?= _l("Discount for %s: %s", $discount['quantity'], $discount['price']); ?></div>
									<? } ?>
								</div>
							<? } ?>
						</div>
					<? } ?>
				</div>

				<div class="product-tabs tab-header htabs">
					<? if ($description) { ?>
						<a class="tab" href="#tab-description">{{Description}}</a>
					<? } ?>
				</div>

				<div class="tab-contents">
					<div id="tab-description" class="description">
						<? if (!empty($description)) { ?>
							<div class="product-description">
								<div class="scroll-wrapper">
									<?= $description; ?>
								</div>
							</div>
						<? } ?>
					</div>
				</div>

				<? if ($is_purchasable) { ?>
					<form id="product-form" class="form full-width product-voucher" action="<?= site_url('product/voucher/add_to_cart'); ?>" method="post">

						<div class="voucher-details">
							<div class="form-item">
								<input type="text" placeholder="{{Recipient's Name}}" name="voucher[to_name]" value="<?= $voucher['to_name']; ?>"/>
								<input type="text" placeholder="{{Recipient's Email}}" name="voucher[to_email]" value="<?= $voucher['to_email']; ?>"/>
								<input type="text" placeholder="{{Gifter's Name}}" name="voucher[from_name]" value="<?= $voucher['from_name']; ?>"/>
								<input type="text" placeholder="{{Gifter's Email}}" name="voucher[from_email]" value="<?= $voucher['from_email']; ?>"/>
								<textarea rows="5" name="voucher[message]" placeholder="{{Message to Recipient}}"><?= $voucher['message']; ?></textarea>
								<br/>
							</div>
							<div class="form-item">
								<label for="voucher-amount">{{Gift Amount}}</label>
								<?=
								build(array(
									'type' => 'select',
									'name'  => 'voucher[amount]',
									'data'   => $data_amounts,
									'select' => $voucher['amount'],
								)); ?>
							</div>

						</div>

						<div class="option-list">
							<?= block('product/options', null, array('product_id' => $product_id)); ?>
						</div>

						<div class="cart">

							<div id="product-submit-box" class="clear">
								<div class="quantity form-item">
									<label>{{Quantity}}</label>
									<input type="text" name="quantity" id="quantity" size="2" value="<?= $minimum; ?>"/>
									<input type="hidden" id="product-id" name="product_id" size="2" value="<?= $product_id; ?>"/>
								</div>
								<div id="product-buttons-box">
									<div id="buy-product-buttons">
										<input type="submit" name="buy_now" value="{{Buy Now}}" id="button-buy-now" class="button medium"/>
										<button id="add-to-cart" class="button medium" data-loading="{{Adding...}}">{{Add to Cart}}</button>
									</div>
								</div>
							</div>

							<div class="product-nav">
								<a href="<?= site_url('cart'); ?>">{{View Cart}}</a>
								<a href="<?= site_url('checkout'); ?>">{{Checkout}}</a>
								<a href="<?= $this->breadcrumb->prevUrl(); ?>">{{Continue Shopping}}</a>
							</div>
							<? if ($minimum > 1) { ?>
								<div class="minimum"><?= _l("This product has a minimum quantity of %s", $minimum); ?></div>
							<? } ?>
						</div>
					</form>

				<? } else { ?>
					<div id="product-inactive">{{This product is currently unavailable.}}</div>
				<? } ?>

				<? if ($show_sharing) { ?>
					<div class="product-sharing">
						<?= block('extras/sharing'); ?>
					</div>
				<? } ?>
			</div>
		</div>

	</div>

	<?= area('bottom'); ?>

</section>


<script type="text/javascript">
	//Check if Product description is overflowed
	pd = $('.product_info .product_description')[0];
	if (pd && pd.scrollHeight > pd.clientHeight) {
		$(pd).addClass('overflowed');
		$(pd).click(function () {
			$(this).toggleClass('hover');
		})
	}

	function option_select_post_before() {
		$('#product_form input[type=submit]').attr('disabled', true);
		$('#buy_product_buttons').addClass('hidden');
		$('#processing_product').removeClass('hidden');
	}

	function option_select_post_after() {
		$('#product_form input[type=submit]').attr('disabled', false);
		$('#buy_product_buttons').removeClass('hidden');
		$('#processing_product').addClass('hidden');
	}

	data = {
		form:   $('#product_form'),
		before: option_select_post_before,
		after:  option_select_post_after
	}

	$('#add-to-cart').click(function () {
		var $this = $(this);
		var $form = $('#product-form');

		$this.loading();
		$.post($form.attr('action'), $form.serialize(), function (response) {
			$this.loading('stop');
			$form.show_msg(response);
		}, 'json');

		return false;
	});

	$('#zoombox-image-link').click(function () {
		if (!screen_sm) {
			$.colorbox({href: $(this).attr('href'), width: '70%', height: '90%'});
		}

		return false;
	});

	$(document).ready(function () {
		$('.image-additional a img, .option-image a img').click(function () {
			if ($(this).attr('src').replace(/-\d+x\d+/, '') == $('#the-zoombox .zoomPad > img').attr('src').replace(/-\d+x\d+/, '')) {
				event.preventDefault();
				return false;
			}
		});

		if (screen_md || screen_lg) {
			$('.zoombox').jqzoom({
				zoomWidth:   $ac.image_thumb_width,
				zoomHeight:  $ac.image_thumb_height,
				position:    'right',
				xOffset:     25,
				yOffset:     0,
				preloadText: '{{Loading High Resolution Image}}'
			});
		}
	});

	$('.product-tabs a').tabs();
</script>

<?= $is_ajax ? '' : call('footer'); ?>
