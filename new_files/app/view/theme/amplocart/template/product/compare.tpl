<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>
	<div class="content">
		<?= $is_ajax ? '' : breadcrumbs(); ?>
		<?= area('top'); ?>

		<h1>{{Product Comparison}}</h1>
		<? if ($products) { ?>
			<table class="compare-info">
				<thead>
					<tr>
						<td class="compare-product" colspan="<?= count($products) + 1; ?>">{{Product Details}}</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{{Product}}</td>
						<? foreach ($products as $product) { ?>
							<td class="name"><a
									href="<?= $products[$product['product_id']]['href']; ?>"><?= $products[$product['product_id']]['name']; ?></a>
							</td>
						<? } ?>
					</tr>
					<tr>
						<td>{{Image}}</td>
						<? foreach ($products as $product) { ?>
							<td><? if ($products[$product['product_id']]['thumb']) { ?>
									<img src="<?= $products[$product['product_id']]['thumb']; ?>"
										alt="<?= $products[$product['product_id']]['name']; ?>"/>
								<? } ?></td>
						<? } ?>
					</tr>
					<tr>
						<td>{{Price}}</td>
						<? foreach ($products as $product) { ?>
							<td><? if ($products[$product['product_id']]['price']) { ?>
									<? if (!$products[$product['product_id']]['special']) { ?>
										<?= $products[$product['product_id']]['price']; ?>
									<? } else { ?>
										<span class="retail"><?= $products[$product['product_id']]['price']; ?></span> <span
											class="special"><?= $products[$product['product_id']]['special']; ?></span>
									<? } ?>
								<? } ?></td>
						<? } ?>
					</tr>
					<tr>
						<td>{{Model}}</td>
						<? foreach ($products as $product) { ?>
							<td><?= $products[$product['product_id']]['model']; ?></td>
						<? } ?>
					</tr>
					<tr>
						<td>{{Brand}}</td>
						<? foreach ($products as $product) { ?>
							<td><?= $products[$product['product_id']]['manufacturer']; ?></td>
						<? } ?>
					</tr>
					<tr>
						<td>{{Availability}}</td>
						<? foreach ($products as $product) { ?>
							<td><?= $products[$product['product_id']]['availability']; ?></td>
						<? } ?>
					</tr>
					<tr>
						<td>{{Rating}}</td>
						<? foreach ($products as $product) { ?>
							<td><img
									src="<?= URL_THEME_IMAGE . "stars-" . $products[$product['product_id']]['rating'] . ".png"; ?>"
									alt="<?= $products[$product['product_id']]['reviews']; ?>"/><br/>
								<?= $products[$product['product_id']]['reviews']; ?></td>
						<? } ?>
					</tr>
					<tr>
						<td>{{Summary}}</td>
						<? foreach ($products as $product) { ?>
							<td class="description"><?= $products[$product['product_id']]['description']; ?></td>
						<? } ?>
					</tr>
					<tr>
						<td>{{Weight}}</td>
						<? foreach ($products as $product) { ?>
							<td><?= $products[$product['product_id']]['weight']; ?></td>
						<? } ?>
					</tr>
					<tr>
						<td>{{Dimensions (L x W x H)}}</td>
						<? foreach ($products as $product) { ?>
							<td><?= $products[$product['product_id']]['length']; ?>
								x <?= $products[$product['product_id']]['width']; ?>
								x <?= $products[$product['product_id']]['height']; ?></td>
						<? } ?>
					</tr>
				</tbody>
				<? foreach ($attribute_groups as $attribute_group) { ?>
					<thead>
						<tr>
							<td class="compare-attribute"
								colspan="<?= count($products) + 1; ?>"><?= $attribute_group['name']; ?></td>
						</tr>
					</thead>
					<? foreach ($attribute_group['attributes'] as $key => $attribute) { ?>
						<tbody>
							<tr>
								<td><?= $attribute['name']; ?></td>
								<? foreach ($products as $product) { ?>
									<? if (isset($products[$product['product_id']]['attribute'][$key])) { ?>
										<td><?= $products[$product['product_id']]['attribute'][$key]; ?></td>
									<? } else { ?>
										<td></td>
									<? } ?>
								<? } ?>
							</tr>
						</tbody>
					<? } ?>
				<? } ?>
				<tr>
					<td></td>
					<? foreach ($products as $product) { ?>
						<td><input type="button" value="{{Add to Cart}}" onclick="addToCart('<?= $product['product_id']; ?>');" class="button"/></td>
					<? } ?>
				</tr>
				<tr>
					<td></td>
					<? foreach ($products as $product) { ?>
						<td class="remove"><a href="<?= $product['remove']; ?>" class="button">{{Remove}}</a></td>
					<? } ?>
				</tr>
			</table>
			<div class="buttons">
				<div class="right"><a href="<?= $continue; ?>" class="button">{{Continue}}</a></div>
			</div>
		<? } else { ?>
			<div class="section">{{You have not chosen any products to compare.}}</div>
			<div class="buttons">
				<div class="right"><a href="<?= $continue; ?>" class="button">{{Continue}}</a></div>
			</div>
		<? } ?>

		<?= area('bottom'); ?>
	</div>

<?= $is_ajax ? '' : call('footer'); ?>
