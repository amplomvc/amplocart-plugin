<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?>
<?= area('right'); ?>

<section id="category-page" class="content">
	<header class="row top-row">
		<div class="wrap">
			<?= $is_ajax ? '' : breadcrumbs(); ?>

			<h1><?= $name; ?></h1>
		</div>
	</header>

	<?= area('top'); ?>

	<div class="row category-listing">
		<div class="wrap">
			<div class="category_info">
				<? if (option('config_show_category_image') && $image) { ?>
					<div class="image">
						<img src="<?= image($image, option('config_image_category_width'), option('config_image_category_height')); ?>" alt="<?= $name; ?>"/>
					</div>
				<? } ?>

				<? if (option('config_show_category_description')) { ?>
					<div class="description"><?= html_entity_decode($description); ?></div>
				<? } ?>
			</div>

			<div class="item-filter">
				<div class="limit"><?= $limits; ?></div>
				<!-- <div class="sort"><?= $sorts; ?></div> -->
			</div>


			<? $params = array(
				'products'     => $products,
				'template' => 'block/product/product_list',
			); ?>

			<?= block('product/list', null, $params); ?>

			<div class="pagination"><?= block('widget/pagination', null, array('total' => $product_total)); ?></div>
		</div>
	</div>

	<?= area('bottom'); ?>

</section>

<script type="text/javascript">
	$('[name=sort_list]').change(function () {
		location = "<?= $sort_url ; ?>" + $(this).val();
	});
</script>

<?= $is_ajax ? '' : call('footer'); ?>
