<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?><?= area('right'); ?>
<div class="content">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<?= area('top'); ?>

	<? if (!empty($manufacturers)) { ?>
		<h1><?= $page_title; ?></h1>

		<div class="manufacturers">
			<div class="pagination"><?= $pagination; ?></div>
		</div>

	<? } else { ?>
		<h1>{{Find Your Favorite Brand}}</h1>

		<div class="section">{{There are no manufacturers to list.}}</div>

		<div class="buttons">
			<div class="right"><a href="<?= $continue; ?>" class="button">{{Continue}}</a></div>
		</div>
	<? } ?>

	<?= area('bottom'); ?>
</div>
