<?= $is_ajax ? '' : call('header'); ?>

<?= area('left'); ?>
<?= area('right'); ?>

<section id="checkout-success" class="content">
	<header class="row top-row">
		<div class="wrap">
			<?= $is_ajax ? '' : breadcrumbs(); ?>
			<h1>{{Registration Successful!}}</h1>
		</div>
	</header>

	<?= area('top'); ?>

	<div class="checkout-success-row row">
		<div class="wrap">

			<div class="success-message">
				<? if (option('config_customer_approval')) { ?>
					<p>{{Thank you for registering with us!}}</p>
					<p>{{You will be notified by email once your account has been activated by the store owner.}}</p>
					<p>
						{{If you have ANY questions about the operation of this online shop, please}}
						<a href="<?= site_url('contact'); ?>">{{contact the store owner}}</a>.
					</p>
				<? } else { ?>
					<p>{{Congratulations! Your new account has been successfully created!}}</p>
					<p>{{You can now take advantage of member privileges to enhance your online shopping experience with us.}}</p>
					<p>{{If you have ANY questions about the operation of this online shop, please email us.}}</p>
					<p>
						{{A confirmation has been sent to the provided email address. If you have not received it within the hour, please}}
						<a href="<?= site_url('contact'); ?>">{{contact us}}</a>.
					</p>
				<? } ?>
			</div>

			<div class="buttons">
				<div class="right">
					<a href="<?= site_url('account'); ?>" class="button">{{Continue}}</a>
				</div>
			</div>
		</div>
	</div>

	<?= area('bottom'); ?>

</section>

<?= $is_ajax ? '' : call('footer'); ?>
