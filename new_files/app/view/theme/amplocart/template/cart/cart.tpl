<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?>
<?= area('right'); ?>

<section id="cart-page" class="content">
	<header class="row top-row">
		<div class="wrap">
		<?= $is_ajax ? '' : breadcrumbs(); ?>

		<h1>{{Shopping Cart}}</h1>
		</div>
	</header>

	<?= area('top'); ?>

	<div class="cart-row row">
		<div class="wrap">
			<? if (!$is_empty) { ?>

				<?= block('cart/cart'); ?>

				<? //TODO: Need to update total weight when cart updates before enabling this
				/*

				<? if (option('config_cart_weight')) { ?>
					<div class="cart-weight">
						<label>{{Total Weight:}}</label>
						<span class="value">(<?= format('weight', $weight); ?>)</span>
					</div>
				<? } ?>
				*/ ?>

				<? if (option('config_show_cart_total', true) && $can_checkout) { ?>
					<div id="cart-block-total">
						<?= block('cart/total'); ?>
					</div>
				<? } ?>

				<div id="cart-actions">
					<h2>{{What would you like to do next?}}</h2>

					<? if (option('coupon_status') || option('voucher_status')) { ?>
						<div>
							<a id="text_block_coupon" onclick="$('#toggle_block_coupon').slideToggle();">{{Use Coupon / Voucher Code}}</a>

							<div id="toggle_block_coupon" class="toggle-block content">
								<?= block('cart/code'); ?>
							</div>
						</div>
					<? } ?>

					<? if (option('reward_status') && $customer_points && $cart_points > 0) { ?>
						<div id="cart-rewards">
							<a class="toggle" onclick="$('#block-reward').slideToggle();">{{Use Reward}}</a>

							<div id="block-reward" class="content">
								<?= block('cart/reward'); ?>
							</div>
						</div>
					<? } ?>

					<? if ($has_shipping) { ?>
						<div id="cart-shipping-estimate">
							<a class="toggle" onclick="$('#block-shipping').slideToggle();">{{Estimate Shipping}}</a>

							<div id="block-shipping" class="content">
								<?= block('cart/shipping'); ?>
							</div>
						</div>
					<? } ?>
				</div>

				<div class="buttons">
					<? if ($can_checkout) { ?>
						<div class="right"><a href="<?= $checkout; ?>" class="button">{{Checkout}}</a></div>
					<? } ?>

					<div class="center"><a href="<?= $continue; ?>" class="button">{{Continue Shopping}}</a></div>
				</div>

			<? } else { ?>
				{{Your shopping cart is empty!}}
				<div class="center"><a href="<?= $continue; ?>" class="button">{{Continue Shopping}}</a></div>
			<? } ?>
		</div>
	</div>

	<?= area('bottom'); ?>

</section>

<?= $is_ajax ? '' : call('footer'); ?>
