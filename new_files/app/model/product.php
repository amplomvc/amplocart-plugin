<?php

class App_Model_Product extends App_Model_Table
{
	protected $table = 'product', $primary_key = 'product_id';

	public function save($product_id, $product, $strict = false)
	{
		if (isset($product['name'])) {
			if (!validate('text', $product['name'], 3, 255)) {
				$this->error['name'] = _l("Product Name must be between 3 and 255 characters!");
			}
		} elseif (!$product_id) {
			$this->error['name'] = _l("Product Name is required.");
		}

		if (isset($product['model']) && !$product_id) {
			if (empty($product['model'])) {
				$product['model'] = $this->generateModel(0, $product['name']);
			}

			if ($this->queryVar("SELECT COUNT(*) FROM {$this->t['product']} WHERE model = '" . $this->escape($product['model']) . "'")) {
				$this->error['model'] = _l("Product Model %s already exists. Please create a unique Model #", $product['model']);
			}
		}

		//validate the quantities
		if (!empty($product['product_options'])) {
			foreach ($product['product_options'] as $option_id => &$product_option) {
				if (empty($product_option['product_option_values'])) {
					$this->error["option_value$option_id"] = _l("You must specify at least 1 Option Value for %s!", $product_option['name']);
					continue;
				}

				//Validate Product Option Value Restrictions
				foreach ($product_option['product_option_values'] as $product_option_value_id => &$product_option_value) {
					if (!isset($product_option_value['subtract'])) {
						$product_option_value['subtract'] = 0;
					}

					if (!empty($product_option_value['restrictions'])) {
						foreach ($product_option_value['restrictions'] as $r_key => $restriction) {
							if ($restriction['restrict_option_value_id'] == $product_option_value_id) {
								$this->error["product_options[$option_id][product_option_value][$product_option_value_id][restrictions][$r_key][restrict_option_value_id]"] = _l("You cannot restrict a Product Option Value with itself!");
							}

							foreach ($product_option_value['restrictions'] as $r_key2 => $restriction2) {
								if ($r_key != $r_key2) {
									if ($restriction['restrict_option_value_id'] == $restriction2['restrict_option_value_id']) {
										$this->error["product_options[$option_id][product_option_value][$product_option_value_id][restrictions][$r_key][restrict_option_value_id]"] = _l("You have duplicate restriction values on 1 Product Option Value!");
									}
								}
							}

							if (isset($product_option['product_option_values'][$restriction['restrict_option_value_id']])) {
								$this->error["product_options[$option_id][product_option_value][$product_option_value_id][restrictions][$r_key][restrict_option_value_id]"] = sprintf(_l("You cannot restrict a Product Option Value with another Product Option value of the same Option Category For Type '%s'!"), ucfirst($product_option['type']));
							}
						}
					}
				}
				unset($product_option_value);
			}
			unset($product_option);
		}

		if ($this->error) {
			return false;
		}

		if ($product_id) {
			$product['date_modified'] = $this->date->now();

			$product_id = $this->update('product', $product, $product_id);
		} else {
			$product['date_added'] = $this->date->now();

			$product_id = $this->insert('product', $product);
		}

		if (!$product_id) {
			return false;
		}

		//Product Attributes
		if (isset($product['product_attributes'])) {
			$this->delete('product_attribute', array('product_id' => $product_id));

			if ($product['product_attributes']) {
				foreach ($product['product_attributes'] as $product_attribute) {
					$product_attribute['product_id'] = $product_id;

					$this->insert('product_attribute', $product_attribute);
				}
			}
		}

		//Product Options
		if (isset($product['product_options'])) {
			$this->delete('product_option_value_restriction', array('product_id' => $product_id));

			$product_option_ids       = array(0);
			$product_option_value_ids = array(0);

			if ($product['product_options']) {
				foreach ($product['product_options'] as $product_option) {
					$product_option['product_id'] = $product_id;

					$product_option_id = $this->update('product_option', $product_option);

					if (!empty($product_option['product_option_values'])) {
						foreach ($product_option['product_option_values'] as $product_option_value) {
							$product_option_value['product_id']        = $product_id;
							$product_option_value['product_option_id'] = $product_option_id;
							$product_option_value['option_id']         = $product_option['option_id'];

							if (empty($product_option_value['default'])) {
								$product_option_value['default'] = 0;
							}

							$product_option_value_id = $this->update('product_option_value', $product_option_value);

							if (!empty($product_option_value['restrictions'])) {
								foreach ($product_option_value['restrictions'] as $restriction) {
									$restriction['product_id']              = $product_id;
									$restriction['product_option_value_id'] = $product_option_value_id;

									$this->insert('product_option_value_restriction', $restriction);
								}
							}

							$product_option_value_ids[] = $product_option_value_id;
						}
					}

					$product_option_ids[] = $product_option_id;
				}
			}

			$this->query("DELETE FROM {$this->t['product_option']} WHERE product_id = '$product_id' AND product_option_id NOT IN (" . implode(',', $product_option_ids) . ")");
			$this->query("DELETE FROM {$this->t['product_option_value']} WHERE product_id = '$product_id' AND product_option_value_id NOT IN (" . implode(',', $product_option_value_ids) . ")");
		}

		//Product Additional Images
		if (isset($product['images'])) {
			$this->delete('product_image', array('product_id' => $product_id));

			if ($product['images']) {
				foreach ($product['images'] as $image) {
					if (empty($image['image'])) {
						$image['product_id'] = $product_id;

						$this->insert('product_image', $image);
					}
				}
			}
		}

		//Product Categories
		if (isset($product['product_categories'])) {
			$this->delete('product_category', array('product_id' => $product_id));

			if ($product['product_categories']) {
				foreach (array_unique($product['product_categories']) as $category_id) {
					$values = array(
						'product_id'  => $product_id,
						'category_id' => $category_id
					);
					$this->insert('product_category', $values);
				}
			}
		}

		//Product Attributes
		if (isset($product['product_attributes'])) {
			$this->delete('product_attribute', array('product_id' => $product_id));

			if ($product['product_attributes']) {
				$product_attributes = array_unique($product['product_attributes'], SORT_REGULAR);

				foreach ($product_attributes as $product_attribute) {
					$product_attribute['product_id'] = $product_id;

					$this->insert('product_attribute', $product_attribute);
				}
			}
		}

		//Product Discount
		if (isset($product['product_discounts'])) {
			$this->delete('product_discount', array('product_id' => $product_id));

			if ($product['product_discounts']) {
				foreach ($product['product_discounts'] as $product_discount) {
					$product_discount['product_id'] = $product_id;

					$this->insert('product_discount', $product_discount);
				}
			}
		}

		//Product Special
		if (isset($product['product_specials'])) {
			$this->delete('product_special', array('product_id' => $product_id));

			if ($product['product_specials']) {
				foreach ($product['product_specials'] as $product_special) {
					$product_special['product_id'] = $product_id;

					$this->insert('product_special', $product_special);
				}
			}
		}

		//Product Downloads
		if (isset($product['product_downloads'])) {
			$this->delete('product_to_download', array('product_id' => $product_id));

			if ($product['product_downloads']) {
				foreach ($product['product_downloads'] as $download_id) {
					$values = array(
						'download_id' => $download_id,
						'product_id'  => $product_id
					);
					$this->insert('product_to_download', $values);
				}
			}
		}


		//Product Related
		if (isset($product['product_related'])) {
			$this->delete('product_related', array('product_id' => $product_id));
			$this->delete('product_related', array('related_id' => $product_id));

			if ($product['product_related']) {
				foreach ($product['product_related'] as $related_id) {
					$values = array(
						'product_id' => $product_id,
						'related_id' => $related_id
					);

					$this->insert('product_related', $values);

					//the inverse so the other product is related to this product too!
					$values = array(
						'product_id' => $related_id,
						'related_id' => $product_id
					);

					$this->insert('product_related', $values);
				}
			}
		}

		//Product Templates
		if (isset($product['product_templates'])) {
			$this->delete('product_template', array('product_id' => $product_id));

			if ($product['product_templates']) {
				foreach ($product['product_templates'] as $store_id => $themes) {
					foreach ($themes as $theme => $template) {
						if (empty($template['template'])) {
							continue;
						}

						$template['product_id'] = $product_id;
						$template['theme']      = $theme;
						$template['store_id']   = $store_id;

						$this->insert('product_template', $template);
					}
				}
			}
		}

		//Product Tags
		if (isset($product['product_tags'])) {
			$this->delete('product_tag', array('product_id' => $product_id));

			if ($product['product_tags']) {
				$tag_ids = $this->tag->addAll($product['product_tags']);

				foreach ($tag_ids as $tag_id) {
					$product_tag = array(
						'product_id' => $product_id,
						'tag_id'     => $tag_id,
					);

					$this->insert('product_tag', $product_tag);
				}
			}
		}

		//Product URL Alias
		if (isset($product['alias'])) {
			$this->url->setAlias($product['alias'], 'product/product', 'product_id=' . (int)$product_id);
		}

		//Translations
		if (!empty($product['translations'])) {
			$this->translation->setTranslations('product', $product_id, $product['translations']);
		}

		clear_cache("product.$product_id");
		clear_cache('product.rows');

		return $product_id;
	}

	public function generateModel($product_id, $name)
	{
		$model = strtoupper(slug($name, '-'));

		$count        = 1;
		$unique_model = $model;

		while ($this->queryVar("SELECT COUNT(*) FROM {$this->t['product']} WHERE model = '" . $this->escape($unique_model) . "' AND product_id != " . (int)$product_id)) {
			$unique_model = $model . '-' . $count++;
		}

		return $unique_model;
	}

	public function copy($product_id)
	{
		$product = $this->getProduct($product_id);

		if (!$product) {
			return false;
		}

		$product['model']  = $this->generateModel(null, $product['name']);
		$product['alias']  = '';
		$product['status'] = 0;

		$product['product_attributes'] = $this->getProductAttributes($product_id);
		$product['product_discounts']  = $this->getProductDiscounts($product_id);
		$product['images']             = $this->getImages($product_id);
		$product['product_options']    = $this->getProductOptions($product_id);
		$product['product_related']    = $this->getProductRelated($product_id);
		$product['product_specials']   = $this->getProductSpecials($product_id);
		$product['product_tags']       = $this->getProductTags($product_id);
		$product['product_categories'] = $this->getCategories($product_id);
		$product['product_downloads']  = $this->getProductDownloads($product_id);
		$product['product_templates']  = $this->getProductTemplates($product_id);

		$name_count = $this->queryVar("SELECT COUNT(*) FROM {$this->t['product']} WHERE `name` like '" . $this->escape($product['name']) . "%'");

		$product['name'] .= ' - Copy' . ($name_count > 1 ? "($name_count)" : '');

		$product['translations'] = $this->translation->getTranslations('product', $product_id);

		return $this->save(null, $product);
	}

	public function remove($product_id)
	{
		if ($this->order->productInConfirmedOrder($product_id)) {
			$this->error['product_id'] = _l("The product %s cannot be deleted after it is associated with a confirmed order! Please deactivate this product instead of deleting it.", $this->Model_Product->getField($product_id, 'name'));
			return false;
		}

		$this->delete('product', array('product_id' => $product_id));
		$this->delete('product_attribute', array('product_id' => $product_id));
		$this->delete('product_discount', array('product_id' => $product_id));
		$this->delete('product_image', array('product_id' => $product_id));
		$this->delete('product_option', array('product_id' => $product_id));
		$this->delete('product_option_value', array('product_id' => $product_id));
		$this->delete('product_option_value_restriction', array('product_id' => $product_id));
		$this->delete('product_related', array('product_id' => $product_id));
		$this->delete('product_related', array('related_id' => $product_id));
		$this->delete('product_reward', array('product_id' => $product_id));
		$this->delete('product_special', array('product_id' => $product_id));
		$this->delete('product_tag', array('product_id' => $product_id));
		$this->delete('product_category', array('product_id' => $product_id));
		$this->delete('product_to_download', array('product_id' => $product_id));
		$this->delete('product_template', array('product_id' => $product_id));
		$this->delete('review', array('product_id' => $product_id));

		$this->url->removeAlias('product/product', 'product_id=' . (int)$product_id);

		$this->translation->deleteTranslation('product', $product_id);

		clear_cache("product.$product_id");

		return true;
	}

	public function getRecords($sort = array(), $filter = array(), $options = array(), $total = false)
	{
		if (isset($filter['category_id']) || isset($filter['!category_id'])) {
			$options['join'][]   = "LEFT JOIN {$this->t['product_category']} pc ON pc.product_id = p.product_id";
			$filter['#category'] = 'AND ' . $this->extractWhere('product_category pc', $filter);
		}

		return parent::getRecords($sort, $filter, $options, $total);

		if (!empty($data['downloads'])) {
			$from .= " LEFT JOIN {$this->t['product_to_download']} p2dl ON (p.product_id=p2dl.product_id)";

			$where .= " AND p2dl.download_id IN (" . implode(',', $data['downloads']) . ")";
		}

		if (!empty($data['attributes'])) {
			$from .= " LEFT JOIN {$this->t['product_attribute']} pa ON (p.product_id=pa.product_id)";

			$where .= " AND pa.attribute_id IN (" . implode(',', $data['attributes']) . ")";
		}

		if (!empty($data['options'])) {
			$from .= " LEFT JOIN {$this->t['product_option']} po ON (p.product_id=po.product_id)";

			$where .= " AND po.option_id IN (" . implode(',', $data['options']) . ")";
		}

		//Reviews / Ratings
		if (option('config_review_status') && (isset($data['rating_min']) || isset($data['rating_max']))) {
			$select .= "(SELECT AVG(rating) AS total FROM {$this->t['review']} r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating";

			if (isset($data['rating_min'])) {
				$where .= 'rating >= ' . (int)$data['rating_min'];
			}

			if (isset($data['rating_max'])) {
				$where .= 'rating <= ' . (int)$data['rating_max'];
			}
		}

		//Product Tag
		if (!empty($data['product_tag'])) {
			$from .= " LEFT JOIN {$this->t['product_tag']} pt ON (p.product_id = pt.product_id)";
			$from .= " LEFT JOIN {$this->t['tag']} t ON (pt.tag_id=t.tag_id)";

			$where .= " AND LCASE(t.text) = '" . $this->escape(strtolower(trim($data['product_tag']))) . "'";
		}

		//Product Related
		if (!empty($data['related_ids'])) {
			$from .= " LEFT JOIN {$this->t['product_related']} pr ON (pr.product_id=p.product_id)";

			$where .= " AND pr.related_id IN (" . implode(',', $data['related_ids']) . ")";
		}
	}

	public function getProduct($product_id)
	{
		$product = $this->queryRow("SELECT * FROM {$this->t['product']} p WHERE p.product_id = '" . (int)$product_id . "'");

		if ($product) {
			$product['alias'] = $this->url->getAlias('product/product', 'product_id=' . (int)$product_id);

			$this->translation->translate('product', $product_id, $product);
		}

		return $product;
	}

	public function getActiveProduct($product_id)
	{
		$product_id  = (int)$product_id;
		$language_id = option('config_language_id');

		$product = cache("product.$product_id.$language_id");

		//Validate Product time constraints to allow for caching
		if ($product) {
			if ($this->date->isInFuture($product['date_available']) || $this->date->isInPast($product['date_expires'], false)) {
				$product = false;
			}
		}

		if (!$product) {
			$discount     = "(SELECT price FROM {$this->t['product_discount']} pdc WHERE pdc.product_id = p.product_id AND pdc.quantity >= '0' AND ((pdc.date_start = '0000-00-00' OR pdc.date_start < NOW()) AND (pdc.date_end = '0000-00-00' OR pdc.date_end > NOW())) ORDER BY pdc.priority ASC, pdc.price ASC LIMIT 1) AS discount";
			$special      = "(SELECT price FROM {$this->t['product_special']} ps WHERE ps.product_id = p.product_id AND (ps.date_start <= NOW() AND (ps.date_end = '" . DATETIME_ZERO . "' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special";
			$reward       = "(SELECT points FROM {$this->t['product_reward']} pr WHERE pr.product_id = p.product_id) AS reward";
			$stock_status = "(SELECT ss.name FROM {$this->t['stock_status']} ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '$language_id') AS stock_status";
			$rating       = "(SELECT AVG(rating) AS total FROM {$this->t['review']} r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating";
			$reviews      = "(SELECT COUNT(*) AS total FROM {$this->t['review']} r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews";
			$template     = "(SELECT template FROM {$this->t['product_template']} pt WHERE pt.product_id = p.product_id AND pt.theme = '" . option('config_theme') . "' AND store_id = '$store_id') as template";

			$manufacturer = DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)";
			$category     = DB_PREFIX . "product_category p2c ON (p2c.product_id=p.product_id)";

			$query =
				"SELECT p.*, p2c.category_id, p2s.*, $special, p.image, m.name AS manufacturer, m.keyword, m.status as manufacturer_status, $discount, $reward, $stock_status, $rating, $reviews, $template, p.sort_order " .
				" FROM {$this->t['product']} p LEFT JOIN $category LEFT JOIN $manufacturer" .
				" WHERE p.product_id='$product_id' AND p.status = '1' AND p2s.store_id = $store_id AND (m.manufacturer_id IS NULL OR m.status='1') AND (p.date_available <= NOW() OR p.date_available = '" . DATETIME_ZERO . "') AND (p.date_expires > NOW() OR p.date_expires = '" . DATETIME_ZERO . "')";

			$product = $this->queryRow($query);

			if (!empty($product)) {
				$product['name']        = html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8');
				$product['price']       = ($product['discount'] ? $product['discount'] : $product['price']);
				$product['rating']      = (int)$product['rating'];
				$product['teaser']      = html_entity_decode($product['teaser'], ENT_QUOTES, 'UTF-8');
				$product['description'] = html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8');
				$product['information'] = html_entity_decode($product['information'], ENT_QUOTES, 'UTF-8');

				$this->translation->translate('product', $product_id, $product);
			}

			cache("product.$product_id.$language_id.$store_id", $product);
		}

		return $product;
	}

	public function getProductInfo($product_id)
	{
		return $this->queryRow("SELECT * FROM {$this->t['product']} WHERE product_id = '" . (int)$product_id . "'");
	}

	public function getProductName($product_id)
	{
		return $this->queryVar("SELECT name FROM {$this->t['product']} WHERE product_id='" . (int)$product_id . "'");
	}

	public function getActiveProducts($sort = array(), $filter = array(), $select = '*', $total = false, $index = 'product_id')
	{
		$filter += array(
				'status'              => 1,
				'manufacturer_status' => 1,
				'date_available'      => array(
					'lte' => $this->date->now(),
				),
				'date_expires'        => array(
					'gte' => $this->date->now(),
				),
			) + $filter;

		$products = $this->getRecords($sort, $filter, 'product_id', $total, $index);

		$total ? $rows = &$products[0] : $rows = &$products;

		foreach ($rows as &$row) {
			$row += $this->getActiveProduct($row['product_id']);
		}
		unset($row);

		return $products;
	}

	public function getProductTranslations($product_id)
	{
		$translate_fields = array(
			'name',
			'teaser',
			'description',
			'information',
			'meta_description',
			'meta_keywords',
		);

		return $this->translation->getTranslations('product', $product_id, $translate_fields);
	}

	public function isEditable($product_id)
	{
		return (int)$this->queryVar("SELECT editable FROM {$this->t['product']} WHERE product_id='$product_id'");
	}

	public function getProductSuggestions($product, $limit = 4)
	{
		if (!is_array($product)) {
			$product = $this->getActiveProduct($product);
		}

		$sort = array(
			'limit' => $limit,
			'sort'  => 'RAND()',
		);

		$filter = array(
			'category_ids' => array($product['category_id']),
		);

		return $this->getActiveProducts($sort, $filter);
	}

	public function getProductAttributeGroups($product_id)
	{
		$query =
			"SELECT ag.* FROM {$this->t['product_attribute']} pa" .
			" LEFT JOIN {$this->t['attribute']} a ON (pa.attribute_id = a.attribute_id)" .
			" LEFT JOIN {$this->t['attribute_group']} ag ON (a.attribute_group_id=ag.attribute_group_id)" .
			" WHERE pa.product_id = '" . (int)$product_id . "' GROUP BY ag.attribute_group_id ORDER BY ag.sort_order, ag.name";

		$attribute_groups = $this->queryRows($query, 'attribute_group_id');

		if (!empty($attribute_groups)) {
			$this->translation->translateAll('attribute_group', 'attribute_group_id', $attribute_groups);

			foreach ($attribute_groups as &$attribute_group) {
				$query =
					"SELECT a.name, pa.* FROM {$this->t['product_attribute']} pa" .
					" LEFT JOIN {$this->t['attribute']} a ON (pa.attribute_id = a.attribute_id)" .
					" WHERE pa.product_id = '" . (int)$product_id . "' AND a.attribute_group_id = '" . (int)$attribute_group['attribute_group_id'] . "' ORDER BY a.sort_order, a.name";

				$attributes = $this->queryRows($query, 'attribute_id');

				$this->translation->translateAll('attribute', 'attribute_id', $attributes);

				$attribute_group['attributes'] = $attributes;
			}
		}

		return $attribute_groups;
	}

	public function getProductAttributes($product_id)
	{
		$attributes = $this->queryRows("SELECT pa.*, a.name FROM {$this->t['product_attribute']} pa LEFT JOIN {$this->t['attribute']} a ON (pa.attribute_id = a.attribute_id) WHERE pa.product_id = '" . (int)$product_id . "' GROUP BY pa.attribute_id", 'attribute_id');

		$this->translation->translateAll('attribute', 'attribute_id', $attributes);

		return $attributes;
	}

	public function getProductOption($product_id, $product_option_id)
	{
		$product_option = $this->queryRow("SELECT * FROM {$this->t['product_option']} WHERE product_id = " . (int)$product_id . " AND product_option_id = " . (int)$product_option_id);

		if ($product_option) {
			$product_option['product_option_values'] = $this->queryRows("SELECT * FROM {$this->t['product_option_value']} WHERE product_option_id = " . (int)$product_option_id, 'product_option_value_id');
		}

		return $product_option;
	}

	public function getProductOptionValue($product_id, $product_option_id, $product_option_value_id)
	{
		$product_option_value = $this->queryRow("SELECT * FROM {$this->t['product_option_value']} WHERE product_id = " . (int)$product_id . " AND product_option_id = " . (int)$product_option_id . " AND product_option_value_id = " . (int)$product_option_value_id);

		$this->translation->translate('product_option_value', $product_option_value_id, $product_option_value);

		return $product_option_value;
	}

	public function getProductOptions($product_id)
	{
		$product_options = $this->queryRows("SELECT * FROM {$this->t['product_option']} WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC", 'product_option_id');

		$restrictions = $this->getProductOptionValueRestrictions($product_id);

		foreach ($product_options as &$product_option) {
			$product_option_values = $this->queryRows("SELECT * FROM {$this->t['product_option_value']} WHERE product_option_id = " . (int)$product_option['product_option_id'] . " ORDER BY sort_order ASC", 'product_option_value_id');

			foreach ($product_option_values as &$product_option_value) {
				$product_option_value['restrictions'] = array_search_key('product_option_value_id', $product_option_value['product_option_value_id'], $restrictions);

				$product_option_value['available'] = !$product_option_value['subtract'] || (int)$product_option_value['quantity'] > 0;
			}
			unset($product_option_value);

			$product_option['product_option_values'] = $product_option_values;
		}
		unset($product_option);

		return $product_options;
	}

	public function getProductOptionValueRestrictions($product_id)
	{
		return $this->queryRows("SELECT * FROM {$this->t['product_option_value_restriction']} WHERE product_id = " . (int)$product_id);
	}

	public function getProductDiscounts($product_id)
	{
		return $this->queryRows("SELECT * FROM {$this->t['product_discount']} WHERE product_id = " . (int)$product_id . " ORDER BY quantity, priority, price");
	}

	public function getProductActiveDiscounts($product_id)
	{
		return $this->queryRows("SELECT * FROM {$this->t['product_discount']} WHERE product_id = " . (int)$product_id . " AND quantity > 0 AND (date_start <= NOW() AND (date_end = '" . DATETIME_ZERO . "' OR date_end > NOW())) ORDER BY quantity ASC, priority ASC, price ASC");
	}

	public function getProductSpecialPrice($product_id)
	{
		return $this->queryVar("SELECT price FROM {$this->t['product_special']} WHERE product_id = " . (int)$product_id . " AND (date_start <= NOW() AND (date_end = '" . DATETIME_ZERO . "' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");
	}

	public function getImages($product_id)
	{
		return $this->queryRows("SELECT * FROM {$this->t['product_image']} WHERE product_id = " . (int)$product_id . " ORDER BY sort_order");
	}

	public function getProductDownloads($product_id)
	{
		$downloads = $this->queryRows("SELECT * FROM {$this->t['product_to_download']} p2d LEFT JOIN {$this->t['download']} d ON (p2d.download_id = d.download_id) WHERE p2d.product_id = " . (int)$product_id);

		$this->translation->translateAll('download', 'download_id', $downloads);

		return $downloads;
	}

	public function getProductReward($product_id)
	{
		return (int)$this->queryVar("SELECT points FROM {$this->t['product_reward']} WHERE product_id = " . (int)$product_id);
	}

	public function getProductActiveRelated($product_id)
	{
		return $this->queryRows("SELECT * FROM {$this->t['product_related']} pr LEFT JOIN {$this->t['product']} p ON (pr.related_id = p.product_id) WHERE pr.product_id = " . (int)$product_id . " AND p.status = 1 AND p.date_available <= NOW() AND p2s.store_id = " . (int)option('store_id'));
	}

	public function getProductRelated($product_id)
	{
		return $this->queryColumn("SELECT related_id FROM {$this->t['product_related']} WHERE product_id = " . (int)$product_id);
	}

	public function getProductTags($product_id)
	{
		return $this->queryRows("SELECT t.* FROM {$this->t['product_tag']} pt LEFT JOIN {$this->t['tag']} t ON (pt.tag_id=t.tag_id) WHERE product_id = " . (int)$product_id, 'tag_id');
	}

	public function getCategories($product_id)
	{
		return $this->queryColumn("SELECT category_id FROM {$this->t['product_category']} WHERE product_id = " . (int)$product_id);
	}

	public function getProductClass($product_id)
	{
		return $this->queryVar("SELECT product_class FROM " . $this->prefix . "product WHERE product_id = " . (int)$product_id);
	}

	public function getClasses()
	{
		$file_list = get_files(DIR_SITE . '/app/controller/admin/product/', array('php'), FILELIST_STRING);

		$classes = array();

		foreach ($file_list as $file) {
			$directives = get_comment_directives($file);

			$class = pathinfo($file, PATHINFO_FILENAME);

			$classes[$class] = array(
				'class' => $class,
				'name'  => !empty($directives['name']) ? $directives['name'] : strtoupper($class),
			);
		}

		return $classes;
	}

	public function getProductSpecials($product_id)
	{
		return $this->queryRows("SELECT * FROM {$this->t['product_special']} WHERE product_id = " . (int)$product_id . " ORDER BY priority, price");
	}

	public function getProductActiveSpecial($product_id)
	{
		return $this->queryVar("SELECT price FROM {$this->t['product_special']} WHERE product_id = " . (int)$product_id . " AND date_start <= NOW() AND (date_end = '" . DATETIME_ZERO . "' OR date_end > NOW()) ORDER BY priority, price LIMIT 1");
	}

	public function getProductTemplates($product_id)
	{
		$product_template_list = $this->queryRows("SELECT * FROM {$this->t['product_template']} WHERE product_id = " . (int)$product_id);

		$product_templates = array();

		foreach ($product_template_list as $product_template) {
			$product_templates[$product_template['store_id']][$product_template['theme']] = $product_template;
		}

		return $product_templates;
	}

	public function getTotalProducts($data = array())
	{
		return $this->getProducts($data, '', true);
	}

	public function getTotalActiveProducts($filter = array())
	{
		return $this->getActiveProducts(null, $filter, 'COUNT(*)');
	}

	public function getTotalProductSpecials()
	{
		return (int)$this->queryVar("SELECT COUNT(DISTINCT ps.product_id) AS total FROM {$this->t['product_special']} ps LEFT JOIN {$this->t['product']} p ON (ps.product_id = p.product_id) LEFT JOIN {$this->t['product_to_store']} p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)option('store_id') . "' AND ((ps.date_start = '" . DATETIME_ZERO . "' OR ps.date_start < NOW()) AND (ps.date_end = '" . DATETIME_ZERO . "' OR ps.date_end > NOW()))");
	}

	public function optionIdToProductOptionId($product_id, $option_id)
	{
		return $this->queryVar("SELECT product_option_id FROM {$this->t['product_option']} WHERE product_id = " . (int)$product_id . " AND option_id = " . (int)$option_id);
	}

	public function fillProductDetails(&$details, $product_id, $quantity, &$options = array(), $ignore_status = false)
	{
		$product = isset($details['product']) ? $details['product'] : $this->getActiveProduct($product_id, $ignore_status);

		if (!$product) {
			return false;
		}

		$product['special']   = $this->getProductActiveSpecial($product_id);
		$product['discounts'] = $this->getProductActiveDiscounts($product_id);
		$product['reward']    = $this->getProductReward($product_id);

		// Product Specials / Discounts
		if ($product['special']) {
			$price = $product['special'];
		} elseif ($product['discounts']) {
			$price = $product['discount'][0]['price'];
		} else {
			$price = $product['price'];
		}

		// Stock
		$in_stock = $product['subtract'] ? (int)$product['quantity'] >= $quantity : true;

		// Calculate Option totals
		$option_cost   = 0;
		$option_price  = 0;
		$option_points = 0;
		$option_weight = 0;

		if (!empty($options)) {
			if (!$this->fillProductOptions($product_id, $options)) {
				return false;
			}

			foreach ($options as $key => $values) {
				if (empty($values)) {
					unset($options[$key]);
					continue;
				}

				foreach ($values as $product_option_value) {
					if (empty($product_option_value)) {
						continue;
					}

					$option_cost += $product_option_value['cost'];
					$option_price += $product_option_value['price'];
					$option_points += $product_option_value['points'];
					$option_weight += $product_option_value['weight'];

					if ($product_option_value['subtract'] && $product_option_value['quantity'] < $quantity) {
						$in_stock = false;
					}
				}
			}
		}

		// Downloads
		$downloads = $this->getProductDownloads($product_id);

		//Product Details
		$details['price']        = $price + $option_price;
		$details['total']        = $details['price'] * $quantity;
		$details['cost']         = $product['cost'] + $option_cost;
		$details['total_cost']   = $details['cost'] * $quantity;
		$details['in_stock']     = $in_stock;
		$details['points']       = ((int)$product['points'] + $option_points) * $quantity;
		$details['weight']       = ((float)$product['weight'] + $option_weight) * $quantity;
		$details['total_reward'] = $product['reward'] * $quantity;
		$details['product']      = $product;
		$details['options']      = $options;
		$details['downloads']    = $downloads;

		return true;
	}

	private function fillProductOptions($product_id, &$options)
	{
		$filter = array(
			'product_option_id' => array_keys($options),
		);

		$product_option_data = $this->Model_Product_Option->getRecords(null, $filter);

		foreach ($options as $product_option_id => &$product_option_values) {
			if (empty($product_option_values)) {
				continue;
			} elseif (!is_array($product_option_values)) {
				$product_option_values = array($product_option_values);
			}

			foreach ($product_option_values as $pov_key => &$product_option_value) {
				//Check if information already filled.
				if (is_array($product_option_value)) {
					//We verify option_id as this is not included with a typical Product Option Value, so make sure we load all necessary information
					if (!isset($product_option_value['option_id'])) {
						$product_option_value = $product_option_value['product_option_value_id'];
					} else {
						continue;
					}
				}

				$product_option_value = $this->getProductOptionValue($product_id, $product_option_id, $product_option_value);

				if ($product_option_value) {
					$data = array_search_key('product_option_id', $product_option_id, $product_option_data);

					//Validate that all options exist for this product
					if (!$data) {
						return false;
					}

					$product_option_value += $data;
				} else {
					//Option not found! Probably was deleted, and therefore no longer available.
					unset($product_option_values[$pov_key]);
				}
			}
			unset($product_option_value);

			//Clean up
			if (empty($product_option_values)) {
				unset($options[$product_option_id]);
			}
		}
		unset($product_option_values);

		return true;
	}

	public function getColumns($filter = array())
	{
		$columns = array(
			'thumb'         => array(
				'type'         => 'image',
				'display_name' => _l("Image"),
				'filter'       => false,
				'sortable'     => true,
				'sort_value'   => '__image_sort__image',
			),
			'product_class' => array(
				'type'         => 'select',
				'display_name' => _l("Class"),
				'build_data'   => $this->Model_Product->getClasses(),
				'build_config' => array(
					'class',
					'name',
				),
				'filter'       => true,
				'sortable'     => true,
			),
			'categories'    => array(
				'type'         => 'multiselect',
				'display_name' => _l("Categories"),
				'filter'       => true,
				'build'        => array(
					'value' => 'category_id',
					'label' => 'pathname',
					'data'  => $this->Model_Category->getCategoriesWithParents(),
				),
				'sortable'     => false,
			),
			'status'        => array(
				'type'         => 'select',
				'display_name' => _l("Status"),
				'build_data'   => array(
					0 => _l("Disabled"),
					1 => _l("Enabled"),
				),
				'filter'       => true,
				'sortable'     => true,
			),
		);

		$columns = $this->getTableColumns('product', $columns, $filter);

		unset($columns['password']);

		return $columns;
	}
}

