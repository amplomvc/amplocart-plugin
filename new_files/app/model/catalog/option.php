<?php
class App_Model_Catalog_Option extends App_Model_Table
{
	protected $table = 'option', $primary_key = 'option_id';

	public function addOption($data)
	{
		$option_id = $this->insert('option', $data);

		if (!empty($data['option_value'])) {
			foreach ($data['option_value'] as $option_value) {
				$option_value['option_id'] = $option_id;

				$this->insert('option_value', $option_value);
			}
		}

		clear_cache('option');

		return $option_id;
	}

	public function editOption($option_id, $data)
	{
		$this->update('option', $data, $option_id);


		if (isset($data['option_value'])) {
			foreach ($data['option_value'] as $option_value) {
				$option_value['option_id'] = $option_id;

				if ($option_value['option_value_id']) {
					$this->update('option_value', $option_value, $option_value['option_value_id']);
				} else {
					$option_value['option_value_id'] = $this->insert('option_value', $option_value);
				}

				if (!empty($attribute['translations'])) {
					$this->translation->setTranslations('option_value', $option_value['option_value_id'], $option_value['translations']);
				}
			}
		}

		clear_cache('option');
	}

	public function deleteOption($option_id)
	{
		$this->delete('option', $option_id);
		$this->delete('option_value', array('option_id' => $option_id));

		clear_cache('option');
	}

	public function getOption($option_id)
	{
		return $this->queryRow("SELECT * FROM `{$this->t['option']}` o WHERE o.option_id = " . (int)$option_id);
	}

	public function getOptionTranslations($option_id)
	{
		$translate_fields = array(
			'name',
			'display_name',
		);

		return $this->translation->getTranslations('option', $option_id, $translate_fields);
	}

	public function countProducts($option_id)
	{
		$filter = array(
			'options' => array($option_id),
		);

		return $this->Model_Product->getTotalProducts($filter);
	}
}
