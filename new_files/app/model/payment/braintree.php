<?php

class App_Model_Payment_Braintree extends App_Model_Payment
{
	private $cards;

	public function __construct()
	{
		parent::__construct('braintree');

		require_once DIR_RESOURCES . '/braintree/lib/Braintree.php';

		$this->settings = option('braintree_settings');

		try {
			Braintree_Configuration::environment($this->settings['mode']);
			Braintree_Configuration::merchantId($this->settings['merchant_id']);
			Braintree_Configuration::publicKey($this->settings['public_key']);
			Braintree_Configuration::privateKey($this->settings['private_key']);
		} catch (Exception $e) {
			trigger_error($e->getMessage());
		}
	}

	public function loadCustomer($customer_id)
	{
		$braintree_id = $this->Model_Customer->getMeta($customer_id, 'braintree_id');

		if ($braintree_id) {
			try {
				return Braintree_Customer::find($braintree_id);
			} catch (Braintree_Exception $e) {
				$this->customer->removeMeta('braintree_id');
				$this->error[] = _l("Your Customer information was not found. Please try <a href=\"%s\">registering a credit card</a>", site_url('extension/payment/braintree/register_card'));
				write_log('error', __METHOD__ . _l("(): The customer with ID %s was not found!", $braintree_id));
			}
		} else {
			$customer = $this->Model_Customer->getRecord($customer_id);

			$data = array(
				'first_name' => $customer['first_name'],
				'last_name'  => $customer['last_name'],
				'email'      => $customer['email'],
				'phone'      => $customer['phone'],
				'fax'        => $customer['fax'],
				//TODO: Add website to customers ... 'website' => $customer['website'],
			);

			try {
				$result = Braintree_Customer::create($data);

				if ($result->success) {
					//If new customer created, save customer ID for braintree (NOTE: $result->customer->id returned only for new customers)
					if (!empty($result->customer) && !empty($result->customer->id)) {
						$this->customer->setMeta('braintree_id', $result->customer->id);
					}

					return $result->customer;

				} elseif (!empty($result->errors)) {
					$this->resultError($result);
				} else {
					$this->error['unknown'] = _l("A problem occurred creating a customer with Braintree. Please try again.");
				}
			} catch (Braintree_Exception $e) {
				$this->error[] = $e->getMessage();
			}
		}

		return false;
	}

	public function createProfile($customer_id, $profile)
	{
		$bt_customer = $this->loadCustomer($customer_id);

		if ($bt_customer) {
			$result = Braintree_PaymentMethod::create(array(
				'customerId'         => $bt_customer->id,
				'paymentMethodNonce' => $profile['nonce'],
				'options'            => array(
					'verifyCard' => true,
				),
			));

			if (!empty($result->success)) {
				unset($profile['nonce']);

				$payment_profile = array(
						'method'    => 'braintree',
						'token'     => $result->paymentMethod->token,
						'last4'     => $result->paymentMethod->last4,
						'card_type' => $result->paymentMethod->cardType,
					) + $profile;

				return parent::createProfile($customer_id, $payment_profile);
			} else {
				$this->error = $result->message;
			}
		} else {
			$this->error['bt_customer'] = _l("Unable to create profile for customer.");
		}

		return false;
	}

	public function removeProfile($customer_id, $profile_id)
	{
		$profile = $this->getProfile($customer_id, $profile_id);

		if ($profile) {
			try {
				Braintree_CreditCard::delete($profile['token']);
			} catch (Braintree_Exception $e) {
				$this->error[] = $e->getMessage();
			}
		}

		return empty($this->error);
	}

	public function charge($transaction)
	{
		//0 balance or negative balance automatically succeeds
		if (empty($transaction['amount']) || (float)$transaction['amount'] <= 0) {
			return true;
		}

		if (!$transaction['payment_key']) {
			$this->error = _l("Payment Method was not specified.");
			return false;
		}
		//Charge New Card without saving card to account (save customer details for reference)
		if (strpos($transaction['payment_key'], 'new_card') === 0) {
			$order_id = (int)str_replace('new_card_', '', $transaction['payment_key']);
			$order    = $this->order->get($order_id);

			//Load New Card data
			$new_card = $this->config->load('braintree', $transaction['payment_key']);

			//Remove temporary New Card data
			$this->config->remove('braintree', $transaction['payment_key'], 0);

			if (!$order) {
				$this->error = _l("Failed to load Order information. Transaction failed.");
				return false;
			}

			if (!$new_card) {
				$this->error = _l("Failed to load Payment Card data. Unable to process transaction.");
				return false;
			}

			$sale = array(
				"amount"     => $transaction['amount'],
				"creditCard" => array(
					"number"          => $new_card["number"],
					"cvv"             => $new_card["cvv"],
					"expirationMonth" => $new_card["month"],
					"expirationYear"  => $new_card["year"]
				),
				'customer'   => array(
					'first_name' => $order['first_name'],
					'last_name'  => $order['last_name'],
					'email'      => $order['email'],
					'phone'      => $order['phone'],
					'fax'        => $order['fax'],
				),
				"options"    => array(
					"submitForSettlement" => true,
					'storeInVault'        => false,
				),
			);
		} else {
			$this->loadCustomer();

			$sale = array(
				'customerId'         => $this->bt_customer->id,
				'amount'             => (string)round($transaction['amount'], 2),
				'paymentMethodToken' => $transaction['payment_key'],
				'recurring'          => $transaction['type'] === 'subscription',
				'options'            => array(
					'submitForSettlement' => true,
				),
			);
		}

		try {
			$result = Braintree_Transaction::sale($sale);

			if ($result->success) {
				return true;
			}

			$this->resultError($result);
		} catch (Braintree_Exception $e) {
			$this->error[] = $e->getMessage();
		}

		return false;
	}

	/********************
	 * Card Transaction *
	 ********************/

	public function hasCard($id)
	{
		$cards = $this->getCards();

		return isset($cards[$id]);
	}

	public function getCard($id)
	{
		$cards = $this->getCards();

		return isset($cards[$id]) ? $cards[$id] : null;
	}

	public function getCards()
	{
		if (!$this->cards) {
			$this->cards = array();

			$this->loadCustomer();

			if ($this->bt_customer && !empty($this->bt_customer->creditCards)) {
				foreach ($this->bt_customer->creditCards as $card) {
					foreach ($this->bt_customer->creditCards as $card2) {
						if ($card2->token !== $card->token && $card2->uniqueNumberIdentifier === $card->uniqueNumberIdentifier) {
							$this->removeCard($card2->token);
							continue 2;
						}
					}

					$this->cards[$card->token] = array(
						'id'      => $card->token,
						'type'    => $card->cardType,
						'name'    => $card->cardholderName,
						'month'   => $card->expirationMonth,
						'year'    => $card->expirationYear,
						'masked'  => $card->maskedNumber,
						'last4'   => $card->last4,
						'image'   => $card->imageUrl,
						'default' => $card->isDefault(),
					);
				}
			}
		}

		return $this->cards;
	}

	public function addCard($card = array())
	{
		//Handle POST
		if (empty($card) && IS_POST) {
			$card = $_POST;
		}

		if (empty($card['number'])) {
			$this->error[] = _l("There was a problem while processing your card. Please make sure Javascript is enabled and try again.");
		} else {
			$this->loadCustomer();

			//TODO: Use unique number identifier to clean up duplicates!
			if (!empty($this->bt_customer->id)) {
				$data = array(
					'customerId'      => $this->bt_customer->id,
					'number'          => $card['number'],
					'expirationMonth' => $card['month'],
					'expirationYear'  => $card['year'],
					'cvv'             => $card['cvv'],
					'options'         => array(
						'verifyCard' => true,
					),
				);

				if (!empty($card['name'])) {
					$data['cardholderName'] = $card['name'];
				} elseif (!empty($card['last_name'])) {
					$data['cardholderName'] = $card['first_name'] . ' ' . $card['last_name'];
				}

				try {
					$result = Braintree_CreditCard::create($data);

					if (!empty($result->success)) {
						return $result->creditCard->_attributes['token'];
					} elseif (!empty($result->errors)) {
						$this->error['create'] = _l("The card information was invalid");
						$this->resultError($result);
					} else {
						$this->error[] = _l("There was a problem creating the credit card");
					}
				} catch (Braintree_Exception $e) {
					$this->error[] = $e->getMessage();
				}
			}
		}

		return empty($this->error);
	}

	public function updateCard($id = null, $data)
	{
		if (!empty($data['default'])) {
			unset($data['default']);
			$data['options'] = array(
				'makeDefault' => true,
			);
		}

		try {
			Braintree_CreditCard::update($id, $data);
		} catch (Braintree_Exception $e) {
			$this->error[] = $e->getMessage();
		}

		return empty($this->error);
	}

	public function getSubscription($id)
	{
		try {
			return Braintree_Subscription::find($id);
		} catch (Braintree_Exception $e) {
			$this->error[] = $e->getMessage();
		}

		return false;
	}

	public function getPlans()
	{
		try {
			$results = Braintree_Plan::all();

			if ($results) {
				$plans = array();
				foreach ($results as $plan) {
					$plans[$plan->_attributes['id']] = $plan->_attributes;
				}

				return $plans;
			}
		} catch (Exception $e) {
			write_log('error', $e);
			$error_log   = site_url('admin/tool/logs', 'log=error');
			$this->error = _l("There was a problem while communicating with Braintree. See more details in the <a target=\"_blank\" href=\"%s\">Error Log.</a>", $error_log);
		}
	}

	public function validatePaymentKey($payment_key)
	{
		if (!parent::validatePaymentKey($payment_key)) {
			return false;
		}

		$cards = $this->getCards();

		if (!isset($cards[$payment_key])) {
			return false;
		}

		return true;
	}

	public function validateCard($card)
	{
		if (empty($card['number'])) {
			$this->error['number'] = _l("You must provide a credit card number");
		}

		if (empty($card['cvv'])) {
			$this->error['cvv'] = _l("You must provide the credit card CVV");
		}

		if (empty($card['month'])) {
			$this->error['month'] = _l("You must provide the 2 digit credit card Expiration Month");
		}

		if (empty($card['year'])) {
			$this->error['year'] = _l("You must provide the 4 digit credit card Expiration Year");
		}

		return empty($this->error);
	}

	public function generateClientToken()
	{
		return Braintree_ClientToken::generate();
	}

	protected function resultError($result)
	{
		if ($result->_attributes && !empty($result->_attributes['message'])) {
			$this->error[] = $result->_attributes['message'];
		} elseif ($result->errors) {
			foreach ($result->errors->deepAll() as $error) {
				$this->error[] = $error->message;
			}
		} else {
			$this->error[] = _l("Transaction failed. Unable to activate subscription.");
		}

		write_log('error', 'Braintree Result Error: ' . implode('; ', $this->error) . get_caller(0, 8));
	}
}
