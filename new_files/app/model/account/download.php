<?php
class App_Model_Account_Download extends Model
{
	public function getDownload($order_download_id)
	{
		$query = $this->query("SELECT * FROM {$this->t['order_download']} od LEFT JOIN `{$this->t['order']}` o ON (od.order_id = o.order_id) WHERE o.customer_id = '" . (int)customer_info('customer_id') . "' AND o.order_status_id > '0' AND o.order_status_id = '" . (int)option('config_complete_status_id') . "' AND od.order_download_id = '" . (int)$order_download_id . "' AND od.remaining > 0");

		return $query->row;
	}

	public function getDownloads($start = 0, $limit = 20)
	{
		if ($start < 0) {
			$start = 0;
		}

		$query = $this->query("SELECT o.order_id, o.date_added, od.order_download_id, od.name, od.filename, od.remaining FROM {$this->t['order_download']} od LEFT JOIN `{$this->t['order']}` o ON (od.order_id = o.order_id) WHERE o.customer_id = '" . (int)customer_info('customer_id') . "' AND o.order_status_id > '0' AND o.order_status_id = '" . (int)option('config_complete_status_id') . "' ORDER BY o.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function updateRemaining($order_download_id)
	{
		$this->query("UPDATE {$this->t['order_download']} SET remaining = (remaining - 1) WHERE order_download_id = '" . (int)$order_download_id . "'");
	}

	public function getTotalDownloads()
	{
		$query = $this->query("SELECT COUNT(*) AS total FROM {$this->t['order_download']} od LEFT JOIN `{$this->t['order']}` o ON (od.order_id = o.order_id) WHERE o.customer_id = '" . (int)customer_info('customer_id') . "' AND o.order_status_id > '0' AND o.order_status_id = '" . (int)option('config_complete_status_id') . "'");

		return $query->row['total'];
	}
}
