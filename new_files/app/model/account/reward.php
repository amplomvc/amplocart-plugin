<?php
class App_Model_Account_Reward extends Model
{
	public function getRewards($data = array())
	{
		$sql = "SELECT * FROM `{$this->t['customer_reward']}` WHERE customer_id = '" . (int)customer_info('customer_id') . "'";

		$sort_data = array(
			'points',
			'description',
			'date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->query($sql);

		return $query->rows;
	}

	public function getTotalRewards()
	{
		$query = $this->query("SELECT COUNT(*) AS total FROM `{$this->t['customer_reward']}` WHERE customer_id = '" . (int)customer_info('customer_id') . "'");

		return $query->row['total'];
	}

	public function getTotalPoints()
	{
		$query = $this->query("SELECT SUM(points) AS total FROM `{$this->t['customer_reward']}` WHERE customer_id = '" . (int)customer_info('customer_id') . "' GROUP BY customer_id");

		if ($query->num_rows) {
			return $query->row['total'];
		} else {
			return 0;
		}
	}
}
