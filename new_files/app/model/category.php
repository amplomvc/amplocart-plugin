<?php

class App_Model_Category extends App_Model_Table
{
	protected $table = 'category', $primary_key = 'category_id';

	public function save($category_id, $category)
	{
		if (isset($category['name'])) {
			if (!validate('text', $category['name'], 2, 64)) {
				$this->error['name'] = _l("Category Name must be between 2 and 64 characters!");
			}
		} elseif (!$category_id) {
			$this->error['name'] = _l("Category name is required.");
		}

		if ($this->error) {
			return false;
		}

		$category['date_modified'] = $this->date->now();;

		if ($category_id) {
			$category_id = $this->update($this->table, $category, $category_id);
		} else {
			$category['date_added'] = $this->date->now();

			$category_id = $this->insert($this->table, $category);
		}

		if ($category_id) {
			if (!empty($category['alias'])) {
				$this->url->setAlias($category['alias'], 'product/category', 'category_id=' . (int)$category_id);
			}
		}

		clear_cache('category');

		return $category_id;
	}

	public function remove($category_id)
	{
		$this->delete('category', $category_id);

		$this->url->removeAlias('product/category', 'category_id=' . (int)$category_id);

		$children = $this->queryRows("SELECT category_id FROM {$this->t['category']} WHERE parent_id = '" . (int)$category_id . "'");

		foreach ($children as $category) {
			$this->deleteCategory($category['category_id']);
		}

		clear_cache('category');

		return true;
	}

	public function copy($category_id)
	{
		$category = $this->getCategory($category_id);

		$copy_count = $this->queryVar("SELECT COUNT(*) FROM {$this->t['category']} WHERE `name` like '$category[name]%'");
		$category['name'] .= ' - Copy(' . $copy_count . ')';

		return $this->save(null, $category);
	}

	public function getActiveCategory($category_id)
	{
		if ($category_id === 0) {
			$category = array(
				'name'             => _l("All Categories"),
				'image'            => '',
				'description'      => '',
				'parent_id'        => 0,
				'sort_order'       => 0,
				'meta_description' => '',
				'meta_keywords'    => '',
				'status'           => 1,
				'date_added'       => DATETIME_ZERO,
				'date_modified'    => DATETIME_ZERO,
			);

		} else {
			$query = "SELECT * FROM {$this->t['category']} c" .
				" LEFT JOIN {$this->t['category_to_store']} c2s ON (c.category_id = c2s.category_id)" .
				" WHERE c.category_id = '" . (int)$category_id . "' AND c2s.store_id = '" . (int)option('store_id') . "' AND c.status = '1'";

			$category = $this->queryRow($query);
		}

		if ($category) {
			$this->translation->translate('category', $category_id, $category);
		}

		return $category;
	}

	public function getCategory($category_id)
	{
		$result = $this->queryRow("SELECT * FROM {$this->t['category']} WHERE category_id = '" . (int)$category_id . "'");

		$result['alias'] = $this->url->getAlias('product/category', 'category_id=' . $category_id);

		return $result;
	}

	public function getChildrenIds($category_tree)
	{
		if (!is_array($category_tree)) {
			$category_tree = $this->getCategoryTree($category_tree);
		}

		$children_ids = array();

		foreach ($category_tree['children'] as $child) {
			$children_ids[] = $child['category_id'];

			$children_ids = array_merge($children_ids, $this->getChildrenIds($child));
		}

		return $children_ids;
	}

	public function getParent($category)
	{
		return current($this->getParents($category));
	}

	public function getParents($category)
	{
		$category_tree = $this->getCategoryTree($category);

		if (!is_array($category)) {
			$category = array_search_key('category_id', (int)$category, $category_tree);
		}

		//An array of parent ID's
		$parent_path = explode(',', $category['parent_path']);
		//minus the 0 root ID
		array_shift($parent_path);

		$parents = array();

		//Although this is a lot of extra data, PHP returns arrays via copy on write, making this optimized performance wise
		foreach ($parent_path as $parent_id) {
			$parents[] = $this->getCategoryTree($parent_id);
		}

		return $parents;
	}

	//TODO: Update Categories in Admin to Category Tree style (see front end!)
	public function getCategoriesWithParents($sort = array(), $filter = array(), $options = array(), $delimeter = ' > ')
	{
		$categories = $this->getRecords($sort, $filter, $options);

		foreach ($categories as &$category) {
			if ($category['parent_id'] > 0) {
				$parents = $this->getParents($category['category_id']);

				if (!empty($parents)) {
					$category['pathname'] = implode($delimeter, array_column($parents, 'name')) . $delimeter . $category['name'];
				}
			} else {
				$category['pathname'] = $category['name'];
			}
		}

		return $categories;
	}

	public function getCategoryTree_TODO()
	{
		$categories = $this->getRecords();

		$tree = new Tree();
		$tree->addNodes($categories, 'category_id');

		$tree->printTree();
	}

	//TODO: Make this into a real OO tree with nodes, array is not the best way....
	public function getCategoryTree($category_id = 0)
	{
		$language_id = option('config_language_id');
		$store_id    = option('store_id');

		$category_tree = cache("category.tree.$store_id.$language_id");

		if (!$category_tree) {
			$categories = $this->getRecords();

			$category_tree = array(
				'category_id'      => 0,
				'name'             => _l("All Categories"),
				'description'      => '',
				'meta_description' => '',
				'meta_keywords'    => '',
				'image'            => '',
				'children'         => array(),
				'parent_path'      => '0',
				'parent_id'        => '',
				'depth'            => 0,
			);

			$parent_ref = array();

			foreach ($categories as &$category) {
				$category['children']                 = array();
				$parent_ref[$category['category_id']] = &$category;
			}
			unset($category);

			foreach ($categories as &$category) {
				if ($category['parent_id']) {
					$parent_ref[$category['parent_id']]['children'][$category['category_id']] = &$category;
				} elseif ((int)$category['parent_id'] === 0) {
					$category_tree['children'][$category['category_id']] = &$category;
				}
			}
			unset($category);

			$this->resolvePaths($category_tree['children']);

			cache("category.tree.$store_id.$language_id", $category_tree);
		}

		if ($category_id) {
			return array_search_key('category_id', $category_id, $category_tree);
		}

		return $category_tree;
	}

	private function resolvePaths(&$category_tree, $parent_path = '0', $path = '', $depth = 0, $delimeter = ' > ')
	{
		foreach ($category_tree as &$category) {
			$category['depth']       = $depth;
			$category['pathname']    = $path . $category['name'];
			$category['parent_path'] = $parent_path;

			if (!empty($category['children'])) {
				$this->resolvePaths($category['children'], $parent_path . ',' . $category['category_id'], $path . $category['name'] . $delimeter, $depth + 1, $delimeter);
			}
		}
	}

	public function getCategoryTranslations($category_id)
	{
		$translate_fields = array(
			'name',
			'meta_keywords',
			'meta_description',
			'description',
		);

		return $this->translation->getTranslations('category', $category_id, $translate_fields);
	}

	public function getCategoryName($category_id)
	{
		$category = $this->queryRow("SELECT name FROM {$this->t['category']} WHERE category_id='" . (int)$category_id . "'");

		$this->translation->translate('category', $category_id, $category);

		return $category['name'];
	}

	public function getCategoryLayoutId($category_id)
	{
		return $this->queryVar("SELECT layout_id FROM {$this->t['category_to_layout']} WHERE category_id = '" . (int)$category_id . "' AND store_id = '" . (int)option('store_id') . "'");
	}

	public function getCategoryStores($category_id)
	{
		$category_store_data = array();

		$query = $this->query("SELECT * FROM {$this->t['category_to_store']} WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_store_data[] = $result['store_id'];
		}

		return $category_store_data;
	}

	public function getCategoryLayouts($category_id)
	{
		$category_layout_data = array();

		$query = $this->query("SELECT * FROM {$this->t['category_to_layout']} WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $category_layout_data;
	}

	public function getColumns($filter = array())
	{
		//The Table Columns
		$columns = array(
			'status' => array(
				'type'         => 'select',
				'display_name' => _l("Status"),
				'build_data'   => array(
					0 => _l("Disabled"),
					1 => _l("Enabled"),
				),
				'filter'       => true,
				'sortable'     => true,
			),
		);

		return $this->getTableColumns($this->table, $columns, $filter);
	}
}
