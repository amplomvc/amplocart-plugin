<?php

class App_Model_Sale_Order extends App_Model_Table
{
	protected $table = 'order', $primary_key = 'order_id';

	public function save($order_id, $order)
	{
		$this->cleanInactiveOrders();

		$order['date_modified'] = $this->date->now();

		if ($order_id) {
			$order_id = $this->update('order', $order, $order_id);
		} else {
			$order['invoice_id'] = $this->generateInvoiceId($order);
			$order['date_added'] = $this->date->now();

			$order += array(
				'customer_group_id' => option('config_customer_group_id'),
			);

			$order_id = $this->insert('order', $order);
		}

		if ($order_id) {
			foreach ($order['products'] as $cart_product) {
				$order_product             = $cart_product + $cart_product['product'];
				$order_product['order_id'] = $order_id;

				$order_product_id = $this->insert('order_product', $order_product);

				foreach ($order_product['options'] as $product_option_values) {
					foreach ($product_option_values as $product_option_value) {
						$product_option_value['order_id']         = $order_id;
						$product_option_value['order_product_id'] = $order_product_id;

						$this->insert('order_option', $product_option_value);
					}
				}

				if (!empty($cart_product['downloads'])) {
					foreach ($cart_product['downloads'] as $download) {
						$download['order_id']         = $order_id;
						$download['order_product_id'] = $order_product_id;
						$download['remaining']        = $download['remaining'] * $cart_product['quantity'];

						$this->insert('order_download', $download);
					}
				}

				if (!empty($cart_product['meta'])) {
					foreach ($cart_product['meta'] as $key => $value) {
						if (is_array($value)) {
							$value      = serialize($value);
							$serialized = 1;
						} else {
							$serialized = 0;
						}

						$meta = array(
							'order_id'         => $order_id,
							'order_product_id' => $order_product_id,
							'key'              => $key,
							'value'            => $value,
							'serialized'       => $serialized,
						);

						$this->insert('order_product_meta', $meta);
					}
				}
			}

			foreach ($order['totals'] as $total) {
				$total['order_id'] = $order_id;

				$this->insert('order_total', $total);
			}

			//Add Entry to Order History
			$history_data = array(
				'order_id'        => $order_id,
				'order_status_id' => $order['order_status_id'],
				'comment'         => _l("Order Updated by %s", $this->user->info('username')),
				'notify'          => 0,
				'date_added'      => $this->date->now(),
			);

			$this->insert('order_history', $history_data);
		}

		return $order_id;
	}

	public function deleteOrder($order_id)
	{
		$order_query = $this->query("SELECT * FROM `{$this->t['order']}` WHERE order_status_id > '0' AND order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$product_query = $this->query("SELECT * FROM {$this->t['order_product']} WHERE order_id = '" . (int)$order_id . "'");

			foreach ($product_query->rows as $product) {
				$this->query("UPDATE `{$this->t['product']}` SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");

				$option_query = $this->query("SELECT * FROM {$this->t['order_option']} WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

				foreach ($option_query->rows as $option) {
					$this->query("UPDATE {$this->t['product_option_value']} SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
				}
			}
		}

		$this->query("DELETE FROM `{$this->t['order']}` WHERE order_id = '" . (int)$order_id . "'");
		$this->query("DELETE FROM {$this->t['order_product']} WHERE order_id = '" . (int)$order_id . "'");
		$this->query("DELETE FROM {$this->t['order_option']} WHERE order_id = '" . (int)$order_id . "'");
		$this->query("DELETE FROM {$this->t['order_download']} WHERE order_id = '" . (int)$order_id . "'");
		$this->query("DELETE FROM {$this->t['order_total']} WHERE order_id = '" . (int)$order_id . "'");
		$this->query("DELETE FROM {$this->t['order_history']} WHERE order_id = '" . (int)$order_id . "'");
		$this->query("DELETE FROM {$this->t['order_fraud']} WHERE order_id = '" . (int)$order_id . "'");
		$this->query("DELETE FROM {$this->t['customer_transaction']} WHERE order_id = '" . (int)$order_id . "'");
		$this->query("DELETE FROM {$this->t['customer_reward']} WHERE order_id = '" . (int)$order_id . "'");
	}

	public function generateInvoiceId()
	{
		$invoice_prefix = option('config_invoice_prefix');

		$date_format = null;
		preg_match("/%(.*)%/", $invoice_prefix, $date_format);

		if ($date_format) {
			$invoice_prefix = preg_replace("/%.*%/", $this->date->format(null, $date_format[1]), $invoice_prefix);
		}

		$count = $this->queryVar("SELECT COUNT(*) FROM {$this->t['order']} WHERE invoice_id like '" . $this->db->escape($invoice_prefix) . "%'");

		return $invoice_prefix . $count;
	}

	public function getOrder($order_id)
	{
		return $this->queryRow("SELECT * FROM `{$this->t['order']}` WHERE order_id = " . (int)$order_id);
	}

	public function getOrderHistories($data = array(), $select = '', $total = false)
	{
		//Select
		if ($total) {
			$select = "COUNT(*) as total";
		} elseif (empty($select)) {
			$select = '*';
		}

		//From
		$from = DB_PREFIX . "order_history oh";

		//Where
		$where = "1";

		if (!empty($data['order_id'])) {
			$where .= " AND oh.order_id = " . (int)$data['order_id'];
		} elseif (!empty($data['order_ids'])) {
			$where .= " AND oh.order_id IN (" . implode(',', $data['order_ids']) . ")";
		}

		if (!empty($data['order_status_ids'])) {
			$where .= " AND oh.order_status_id IN (" . implode(',', $data['order_status_ids']) . ")";
		}

		//Order By and Limit
		list($order, $limit) = $this->extractOrderLimit($data);

		//The Query
		$query = "SELECT $select FROM $from WHERE $where $order $limit";

		$result = $this->query($query);

		if ($total) {
			return $result->row['total'];
		}

		return $result->rows;
	}

	public function getOrderProducts($order_id)
	{
		return $this->queryRows("SELECT * FROM {$this->t['order_product']} WHERE order_id = " . (int)$order_id);
	}

	public function getOrderProductOptions($order_id, $order_product_id)
	{
		$options = $this->queryRows("SELECT * FROM {$this->t['order_option']} WHERE order_id = " . (int)$order_id . " AND order_product_id = " . (int)$order_product_id);

		foreach ($options as &$option) {
			$option += $this->Model_Product->getProductOptionValue($option['product_id'], $option['product_option_id'], $option['product_option_value_id']);
			$option += $this->Model_Product->getProductOption($option['product_id'], $option['product_option_id']);
		}
		unset($option);

		return $options;
	}

	public function getOrderProductMeta($order_id, $order_product_id)
	{
		$meta = $this->queryRows("SELECT * FROM " . $this->prefix . "order_product_meta WHERE order_product_id = " . (int)$order_product_id . " AND order_id = " . (int)$order_id, 'key');

		foreach ($meta as &$m) {
			if ($m['serialized']) {
				$m = unserialize($m['value']);
			} else {
				$m = $m['value'];
			}
		}
		unset($m);

		return $meta;
	}

	public function getOrderTotals($order_id)
	{
		return $this->queryRows("SELECT * FROM {$this->t['order_total']} WHERE order_id = " . (int)$order_id . " ORDER BY sort_order ASC");
	}

	public function getOrderDownload($order_id, $order_product_id)
	{
		return $this->queryRows("SELECT * FROM {$this->t['order_download']} WHERE order_id = " . (int)$order_id . " AND order_product_id = " . (int)$order_product_id);
	}

	public function getOrderDownloads($order_id)
	{
		return $this->queryRows("SELECT * FROM {$this->t['order_download']} WHERE order_id = " . (int)$order_id);
	}

	public function getTotalOrderHistories($data = array())
	{
		return $this->getOrderHistories($data, '', true);
	}

	public function getTotalOrderProducts($order_id)
	{
		return $this->queryVar("SELECT COUNT(*) FROM {$this->t['order_product']} WHERE order_id = " . (int)$order_id);
	}

	public function getGrossSales($data = array())
	{
		$data['!order_status_ids'] = array(0);

		return $this->getOrders($data, '', true);
	}

	public function cleanInactiveOrders()
	{
		$inactive_orders = $this->queryColumn("SELECT order_id FROM {$this->t['order']} WHERE order_status_id = 0 AND date_modified < DATE_SUB(NOW(), INTERVAL 2 DAY)");

		foreach ($inactive_orders as $order_id) {
			$this->delete('order', $order_id);

			$where = array('order_id' => $order_id);

			$this->delete('order_download', $where);
			$this->delete('order_fraud', $where);
			$this->delete('order_history', $where);
			$this->delete('order_option', $where);
			$this->delete('order_product', $where);
			$this->delete('order_product_meta', $where);
			$this->delete('order_total', $where);
		}
	}

	public function getColumns($filter = array())
	{
		$columns['customer'] = array(
			'type'         => 'text',
			'display_name' => _l("Customer"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['order_status_id'] = array(
			'type'         => 'select',
			'display_name' => _l("Status"),
			'filter'       => true,
			'build_config' => array(
				false,
				'title'
			),
			'build_data'   => Order::$statuses,
			'sortable'     => true,
		);

		return $this->getTableColumns($this->table, $columns, $filter);
	}
}
