<?php

class App_Model_Sale_Coupon extends Model
{
	public function save($coupon_id, $coupon)
	{
		if (!$this->validate($coupon_id, $coupon)) {
			return false;
		}

		clear_cache('coupon.' . $coupon_id);

		if (!$coupon_id) {
			$coupon['date_added'] = $this->date->now();

			$coupon_id = $this->insert('coupon', $coupon);
		} else {
			$coupon_id = $this->update('coupon', $coupon, $coupon_id);
		}

		//Error Saving coupon
		if (!$coupon_id) {
			return false;
		}

		if (isset($coupon['products'])) {
			$this->delete('coupon_product', array('coupon_id' => $coupon_id));

			foreach ($coupon['products'] as $product_id) {
				$values = array(
					'product_id' => $product_id,
					'coupon_id'  => $coupon_id
				);
				$this->insert('coupon_product', $values);
			}
		}

		if (isset($coupon['categories'])) {
			$this->delete('coupon_category', array('coupon_id' => $coupon_id));

			foreach ($coupon['categories'] as $category_id) {
				$values = array(
					'category_id' => $category_id,
					'coupon_id'   => $coupon_id
				);

				$this->insert('coupon_category', $values);
			}
		}

		if (isset($coupon['customers'])) {
			$this->delete('coupon_customer', array('coupon_id' => $coupon_id));

			foreach ($coupon['customers'] as $customer_id) {
				$values = array(
					'customer_id' => $customer_id,
					'coupon_id'   => $coupon_id
				);

				$this->insert('coupon_customer', $values);
			}
		}

		return $coupon_id;
	}

	public function deleteCoupon($coupon_id)
	{
		clear_cache('coupon.' . $coupon_id);

		$this->delete('coupon_product', array('coupon_id' => $coupon_id));
		$this->delete('coupon_category', array('coupon_id' => $coupon_id));
		$this->delete('coupon_customer', array('coupon_id' => $coupon_id));
		$this->delete('coupon_history', array('coupon_id' => $coupon_id));

		return $this->delete('coupon', $coupon_id);
	}

	public function getCoupon($coupon_id)
	{
		$coupon = cache('coupon.' . $coupon_id);

		if (!$coupon) {
			$coupon = $this->queryRow("SELECT * FROM {$this->t['coupon']} WHERE coupon_id = " . (int)$coupon_id);

			$coupon['products']   = $this->getCouponProducts($coupon_id);
			$coupon['customers']  = $this->getCouponCustomers($coupon_id);
			$coupon['categories'] = $this->getCouponCategories($coupon_id);

			cache('coupon.' . $coupon_id, $coupon);
		}

		return $coupon;
	}

	public function getCouponByCode($code)
	{
		$coupon_id = $this->queryVar("SELECT coupon_id FROM {$this->t['coupon']} WHERE code = '" . $this->escape($code) . "'");

		if ($coupon_id) {
			return $this->getCoupon($coupon_id);
		}
	}

	public function getColumns($filter = array())
	{
		//The Table Columns
		$columns = array();

		$columns['name'] = array(
			'type'         => 'text',
			'display_name' => _l("Name"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['code'] = array(
			'type'         => 'text',
			'display_name' => _l("Code"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['auto_apply'] = array(
			'type'         => 'select',
			'display_name' => _l("Auto Apply"),
			'build_data'   => array(
				0 => _l("No"),
				1 => _l("Yes"),
			),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['type'] = array(
			'type'         => 'select',
			'display_name' => _l("Discount Type"),
			'build_data'   => array(
				'P' => _l("Percentage"),
				'F' => _l("Fixed Amount"),
			),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['discount'] = array(
			'type'         => 'text',
			'display_name' => _l("Discount"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['date_start'] = array(
			'type'         => 'datetime',
			'display_name' => _l("Start Date"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['date_end'] = array(
			'type'         => 'datetime',
			'display_name' => _l("End Date"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['status'] = array(
			'type'         => 'select',
			'display_name' => _l("Status"),
			'filter'       => true,
			'build_data'   => array(
				0 => _l("Disabled"),
				1 => _l("Enabled"),
			),
			'sortable'     => true,
		);

		return $this->getTableColumns('coupon', $columns, $filter);
	}

	public function getCoupons($filter = array(), $select = '*', $index = null)
	{
		//Select
		if ($index === false) {
			$select = "COUNT(*)";
		}

		//From
		$from = $this->prefix . "coupon";

		//Where
		$columns = array(
			'Status' => 'equals',
		);

		$where = $this->extractWhere('coupon', $filter, $columns);

		//Order and Limit
		list($order, $limit) = $this->extractOrderLimit($filter);

		//The Query
		$query = "SELECT $select FROM $from WHERE $where $order $limit";

		if ($index === false) {
			return $this->queryVar($query);
		}

		return $this->queryRows($query, $index);
	}

	public function getCouponProducts($coupon_id)
	{
		return $this->queryColumn("SELECT product_id FROM {$this->t['coupon_product']} WHERE coupon_id = " . (int)$coupon_id);
	}

	public function getCouponCategories($coupon_id)
	{
		return $this->queryColumn("SELECT category_id FROM {$this->t['coupon_category']} WHERE coupon_id = " . (int)$coupon_id);
	}

	public function getCouponCustomers($coupon_id)
	{
		return $this->queryColumn("SELECT customer_id FROM {$this->t['coupon_customer']} WHERE coupon_id = " . (int)$coupon_id);
	}

	public function getTotalCoupons($filter = array())
	{
		return $this->getCoupons($filter, '', false);
	}

	public function getCouponHistories($coupon_id, $start = 0, $limit = 10)
	{
		return $this->queryRows("SELECT ch.order_id, CONCAT(c.first_name, ' ', c.last_name) AS customer, ch.amount, ch.date_added FROM {$this->t['coupon_history']} ch LEFT JOIN {$this->t['customer']} c ON (ch.customer_id = c.customer_id) WHERE ch.coupon_id = '" . (int)$coupon_id . "' ORDER BY ch.date_added ASC LIMIT " . (int)$start . "," . (int)$limit);
	}

	public function getTotalCouponHistories($coupon_id)
	{
		return $this->queryVar("SELECT COUNT(*) FROM {$this->t['coupon_history']} WHERE coupon_id = " . (int)$coupon_id);
	}

	public function validate($coupon_id, $coupon)
	{
		if (!$coupon_id || isset($coupon['name'])) {
			if (!validate('text', $coupon['name'], 3, 128)) {
				$this->error['name'] = _l("Coupon Name must be between 3 and 128 characters!");
			}
		}

		if (!$coupon_id || isset($coupon['code'])) {
			if (!validate('text', $coupon['code'], 3, 32)) {
				$this->error['code'] = _l("Code must be between 3 and 32 characters!");
			} else {
				$duplicate = $this->queryVar("SELECT COUNT(*) FROM " . $this->prefix . "coupon WHERE code = '" . $this->escape($coupon['code']) . "' AND coupon_id != " . (int)$coupon_id);

				if ($duplicate) {
					$this->error['code'] = _l("The Code %s is already in use.", $coupon['code']);
				}
			}
		}

		return empty($this->error);
	}

	public function verifyCoupon($code)
	{
		$coupon = $this->getCouponByCode($code);

		if (empty($coupon['status']) || !$this->date->isInPast($coupon['date_start'])) {
			$this->error['status'] = _l("The coupon is inactive.");
			return;
		}

		if ($coupon['date_end'] !== DATETIME_ZERO && $this->date->isInPast($coupon['date_end'])) {
			$this->error['expired'] = _l("The coupon is expired! %s %s", $coupon['date_end'], $coupon['date_start']);
			return false;
		}

		if ($coupon['total'] > $this->cart->getSubTotal()) {
			$this->error['coupon_total'] = _l("You must have at least %s in your cart for this coupon.", $this->currency->format($coupon['total']));
			return false;
		}

		if ($coupon['uses_total'] > 0) {
			$use_count = $this->queryVar("SELECT COUNT(*) FROM {$this->t['coupon_history']} WHERE coupon_id = " . (int)$coupon['coupon_id']);

			if ($use_count >= $coupon['uses_total']) {
				$this->error['coupon_uses'] = _l("This coupon has reached its usage limit.");
				return false;
			}
		}

		if (($coupon['uses_customer'] > 0 || $coupon['logged']) && !is_logged()) {
			$this->error['coupon_logged'] = _l("You must be logged in to use this coupon");
			return false;
		}

		if ($coupon['uses_customer'] > 0) {
			$customer_uses = $this->queryVar("SELECT COUNT(*) FROM {$this->t['coupon_history']} WHERE coupon_id = " . (int)$coupon['coupon_id'] . " AND customer_id = " . (int)customer_info('customer_id'));

			if ($customer_uses >= $coupon['uses_customer']) {
				$this->error['coupon_customer_uses'] = _l("You have reached the usage limit for this coupon.");
				return false;
			}
		}

		if (!empty($coupon['products'])) {
			$required = array_diff($coupon['products'], $this->cart->getProductIds());

			if ($required) {
				$products = $this->queryColumn("SELECT id, name FROM " . $this->prefix . "product WHERE product_id IN (" . implode(',', $required) . ")");

				$list = array();
				foreach ($products as $product) {
					$list[] = "<a href=\"" . site_url("product/product", 'product_id=' . $product['product_id']) . "\">" . $product['name'] . "</a>";
				}

				$this->error['coupon_product'] = _l("To use this coupon, please add %s to your cart.", implode(', ', $list));
				return false;
			}
		}

		return $coupon;
	}

	public function loadAutoCoupons()
	{
		$coupons = $this->queryColumn("SELECT code FROM " . $this->prefix . "coupon WHERE auto_apply = 1 AND status = 1");

		foreach ($coupons as $code) {
			$this->cart->applyCoupon($code);
		}

		$this->cart->clearErrors();
	}

	public function redeem($coupon_id, $order_id, $customer_id, $amount)
	{
		$redeem = array(
			'coupon_id'   => $coupon_id,
			'order_id'    => $order_id,
			'customer_id' => $customer_id,
			'amount'      => $amount,
			'date_added'  => $this->date->now(),
		);

		$this->insert('coupon_history', $redeem);
	}
}
