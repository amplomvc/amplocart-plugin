<?php

class App_Model_Sale_Voucher extends Model
{
	public function save($voucher_id, $voucher)
	{
		$voucher['amount'] = (int)$voucher['amount'];

		if ($voucher['amount'] <= 0) {
			$this->error['amount'] = _l("The amount must be greater than 0");
		}

		if ($this->error) {
			return false;
		}

		if (!$voucher_id) {
			$voucher['code'] = tokengen(10);

			$voucher['data'] = serialize($voucher['data']);

			$voucher_id = $this->insert('voucher', $voucher);

			$this->addHistory($voucher_id, 0, 0, _l("Voucher created"));

			call('mail/voucher', $voucher_id);
		} else {
			$voucher_id = $this->update('voucher', $voucher, $voucher_id);

			$this->addHistory($voucher_id, 0, 0, _l("Voucher updated"));
		}

		if (!$voucher_id) {
			return false;
		}

		return $voucher_id;
	}

	public function remove($voucher_id)
	{
		$where = array(
			'voucher_id' => $voucher_id,
		);

		$this->delete('voucher_history', $where);

		return $this->delete('voucher', $voucher_id);
	}

	public function getVoucher($voucher_id)
	{
		$voucher = $this->queryRow("SELECT * FROM {$this->t['voucher']} WHERE voucher_id = " . (int)$voucher_id);

		if ($voucher) {
			$spent                = $this->queryVar("SELECT SUM(amount) FROM {$this->t['voucher_history']} WHERE voucher_id = " . (int)$voucher_id);
			$voucher['remaining'] = $voucher['amount'] - $spent;
			$voucher['data']      = unserialize($voucher['data']);
		}

		return $voucher;
	}

	public function getVoucherByCode($code)
	{
		$voucher_id = $this->queryVar("SELECT voucher_id FROM {$this->t['voucher']} WHERE code = '" . $this->escape($code) . "' LIMIT 1");

		return $this->getVoucher($voucher_id);
	}

	public function getVouchers($filter = array(), $select = '*', $index = null)
	{
		//Select
		if ($index === false) {
			$select = "COUNT(*)";
		}

		//From
		$from = DB_PREFIX . "voucher";

		//Where
		$where = '1';

		if (isset($filter['order_ids'])) {
			$where .= " AND order_id IN (" . implode(',', $filter['order_ids']) . ")";
		}

		if (isset($filter['code'])) {
			$where .= " AND LCASE(code) like '%" . $this->escape(strtolower($filter['code'])) . "%'";
		}

		if (isset($filter['amount'])) {
			if (strpos($filter['amount'], ',')) {
				list($low, $high) = explode(',', $filter['amount'], 2);
			} else {
				$low  = $filter['amount'];
				$high = false;
			}

			if ($low) {
				$where .= " AND amount >= " . (int)$low;
			}

			if ($high) {
				$where .= " AND amount < " . (int)$high;
			}
		}

		if (!empty($filter['date_added']['gte'])) {
			$where .= " AND date_added >= '" . $this->date->format($filter['date_added']['gte']) . "'";
		}

		if (!empty($filter['date_added']['lte'])) {
			$where .= " AND date_added < '" . $this->date->add($filter['date_added']['lte'], '1 day') . "'";
		}

		//Order and Limit
		list($order, $limit) = $this->extractOrderLimit($filter);

		//The Query
		$query = "SELECT $select FROM $from WHERE $where $order $limit";

		if ($index === false) {
			return $this->queryVar($query);
		}

		return $this->queryRows($query, $index);
	}

	public function getTotalVouchers($filter = array())
	{
		return $this->getVouchers($filter, '', false);
	}

	public function verifyVoucher($code)
	{
		$voucher = $this->getVoucherByCode($code);

		if (!$voucher) {
			$this->error['code'] = _l("The voucher does not exist");
			return false;
		}

		if ($voucher['remaining'] <= 0) {
			$this->error['remaining'] = _l("This voucher has already been used.");
			return false;
		}

		return $voucher;
	}

	public function redeem($voucher_id, $order_id, $amount)
	{
		$voucher = $this->getVoucher($voucher_id);

		if (!$voucher) {
			$this->error['voucher_id'] = _l("Voucher did not exist");
			return false;
		}

		if ($voucher['remaining'] < $amount) {
			$this->error['remaining'] = _l("Voucher does not have enough funds.");
			return false;
		}

		$this->addHistory($voucher_id, $order_id, $amount, _l("Voucher has been redeemed for %s", format('currency', $amount)));

		return true;
	}

	public function addHistory($voucher_id, $order_id, $amount, $message = '')
	{
		$voucher_history = array(
			'voucher_id' => $voucher_id,
			'order_id'   => $order_id,
			'amount'     => $amount,
			'message'    => $message,
			'date'       => $this->date->now(),
		);

		return $this->insert('voucher_history', $voucher_history);
	}
}
