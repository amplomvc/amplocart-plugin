<?php

abstract class App_Model_Payment extends Model
{
	protected $extension;
	protected $settings = array();

	public function __construct($code = null)
	{
		parent::__construct();

		//TODO: reimplement this

		/*
		if ($code) {
			$this->extension = cache('extension.' . $code);

			if (!$this->extension) {
				$this->extension = $this->queryRow("SELECT * FROM {$this->t['extension']} WHERE `code` = '" . $this->escape($code) . "'");
				cache('extension.' . $code, $this->extension);
			}

			$this->settings = unserialize($this->extension['settings']);
		}
		*/
	}

	public function createProfile($customer_id, $profile)
	{
		if (empty($profile['address'])) {
			$this->error['address'] = _l("A valid address is required for creating a payment profile.");
			return false;
		}

		if (empty($profile['address']['address_id'])) {
			$this->Model_Customer->saveAddress($customer_id, null, $profile['address']);
		} else {
			$profile['address'] = $this->Model_Address->getRecord($profile['address']['address_id']);
		}

		unset($profile['address']['address_id']);

		$profiles = $this->getProfiles($customer_id);

		$profile_id = empty($profiles) ? 1 : (int)max(array_keys($profiles)) + 1;

		$profiles[$profile_id] = $profile;

		$this->Model_Customer->setMeta($customer_id, 'payment_profiles', $profiles);

		return $profile_id;
	}

	public function getProfile($customer_id, $profile_id)
	{
		$profiles = $this->getProfiles($customer_id);
		return isset($profiles[$profile_id]) ? $profiles[$profile_id] : null;
	}

	public function getProfiles($customer_id)
	{
		return (array)$this->Model_Customer->getMeta($customer_id, 'payment_profiles');
	}

	public function charge($transaction)
	{

	}
}
