<?php

class App_Controller_Account_Order extends Controller
{
	static $allow = array(
		'access' => '.*',
	);

	public function index()
	{
		//Page Head
		set_page_info('title', _l("Order History"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Account"), site_url('account'));
		breadcrumb(_l("Order History"), site_url('account/order'));

		$data['path'] = $this->route->getPath();

		//Get Sorted / Filtered Data
		$sort = (array)_get('sort', array('order_id' => 'DESC'));

		$filter = array(
			'customer_ids'     => array(customer_info('customer_id')),
			'!order_status_id' => 0,
		);

		$options = array(
			'index' => 'order_id',
			'page'  => _get('page', 1),
			'limit' => _get('limit', option('site_list_limit', 20)),
		);

		list($orders, $total) = $this->Model_Sale_Order->getRecords($sort, $filter, $options, true);

		foreach ($orders as &$order) {
			$product_total = $this->Model_Sale_Order->getTotalOrderProducts($order['order_id']);

			$order['name']         = $order['first_name'] . ' ' . $order['last_name'];
			$order['products']     = $product_total;
			$order['order_status'] = $this->order->getOrderStatus($order['order_status_id']);
			$order['href']         = site_url('account/order/info', 'order_id=' . $order['order_id']);
			$order['reorder']      = site_url('account/order/reorder', 'order_id=' . $order['order_id']);
		}
		unset($order);

		$data['orders'] = $orders;

		//Pagination
		$pagination = array(
			'total' => $total,
		);

		$data['pagination'] = block('widget/pagination', null, $pagination);

		//Render
		$content = $this->render('account/order/list', $data);

		if ($this->is_ajax) {
			output($content);
		} else {
			$data['content'] = $content;

			output($this->render('account/account', $data));
		}
	}

	public function info()
	{
		//Order ID
		$order_id = isset($_GET['order_id']) ? $_GET['order_id'] : 0;

		//Login Validation
		if (!is_logged()) {
			$this->request->setRedirect(site_url('account/order/info', 'order_id=' . $order_id), 'login');

			redirect('customer/login');
		}

		//Order Validation
		$order                = $this->order->get($order_id);
		$order['transaction'] = $this->transaction->get($order['transaction_id']);
		$order['shipping']    = $this->shipping->get($order['shipping_id']);

		if (!$order) {
			message('warning', _l("Unable to find requested order. Please choose an order to view from the list."));

			redirect('account/order');
		}

		//Page Head
		set_page_info('title', _l("Order Information"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Account"), site_url('account'));
		breadcrumb(_l("Order History"), site_url('account/order'));
		breadcrumb(_l("Order Information"), $this->url->here());

		//Shipping / Payment Addresses
		$order['payment_address'] = $this->order->getPaymentAddress($order_id);
		$order['payment_method']  = $this->System_Extension_Payment->get($order['transaction']['payment_code'])->info();

		if ($order['shipping']) {
			$order['shipping_address'] = $this->order->getShippingAddress($order_id);
			$order['shipping_method']  = $this->System_Extension_Shipping->get($order['shipping']['shipping_code'])->info();
		}


		//Order Products
		$products = $this->Model_Sale_Order->getOrderProducts($order_id);

		foreach ($products as &$product) {
			$product += $this->Model_Product->getProduct($product['product_id']);

			$options = $this->Model_Sale_Order->getOrderProductOptions($order_id, $product['order_product_id']);

			foreach ($options as &$option) {
				$option = $this->Model_Product->getProductOptionValue($product['product_id'], $option['product_option_id'], $option['product_option_value_id']);
				$option += $this->Model_Product->getProductOption($product['product_id'], $option['product_option_id']);
			}
			unset($option);

			$product['options'] = $options;

			$product['return_policy']   = $this->cart->getReturnPolicy($product['return_policy_id']);
			$product['shipping_policy'] = $this->cart->getShippingPolicy($product['shipping_policy_id']);

			if ($product['return_policy']['days'] >= 0) {
				$product['return'] = site_url('account/return/insert', 'order_id=' . $order['order_id'] . '&product_id=' . $product['product_id']);
			}
		}
		unset($product);

		$order['products'] = $products;

		//History
		$history_filter = array(
			'order_id' => $order_id,
		);

		$histories = $this->Model_Sale_Order->getOrderHistories($history_filter);

		foreach ($histories as &$history) {
			$history['order_status'] = $this->order->getOrderStatus($history['order_status_id']);
		}
		unset($history);

		$order['histories'] = $histories;

		//Totals
		$order['totals'] = $this->Model_Sale_Order->getOrderTotals($order_id);

		//Render
		output($this->render('account/order_info', $order));
	}

	public function reorder()
	{
		$order_id = isset($_GET['order_id']) ? (int)$_GET['order_id'] : false;

		if ($order_id) {
			$order = $this->order->get($order_id);

			if ($order) {
				$order_products = $this->Model_Sale_Order->getOrderProducts($order_id);

				foreach ($order_products as $order_product) {
					$order_options = $this->Model_Sale_Order->getOrderProductOptions($order_id, $order_product['order_product_id']);

					$options = array();

					foreach ($order_options as $order_option) {
						$options[$order_option['product_option_id']][$order_option['product_option_value_id']] = $order_option;
					}

					message('success', _l("You have successfully added the products from order ID #%s to your cart!", $order_id));

					$this->cart->addProduct($order_product['product_id'], $order_product['quantity'], $options);
				}

				redirect('cart');
			}
		}

		message('warning', _l("Unable to locate the order you have requested to reorder."));

		$this->index();
	}
}
