<?php

class App_Controller_Account_Return extends Controller
{
	public function index()
	{
		if (!is_logged()) {
			$this->session->set('redirect', site_url('account/return'));

			redirect('customer/login');
		}

		set_page_info('title', _l("Product Returns"));

		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Account"), site_url('account'));
		breadcrumb(_l("Product Returns"), site_url('account/return'));

		$sort_filter = $this->sort->getQueryDefaults('date_added', 'ASC');

		$return_total = $this->Model_Account_Return->getTotalReturns($sort_filter);
		$returns      = $this->Model_Account_Return->getReturns($sort_filter);

		foreach ($returns as &$return) {
			$return['name']       = $return['first_name'] . ' ' . $return['last_name'];
			$return['date_added'] = $this->date->format($return['date_added'], 'short');
			$return['href']       = site_url('account/return/info', 'return_id=' . $return['return_id']);
		}

		$data['returns'] = $returns;

		//Template Data
		$data['data_yes_no'] = array(
			1 => _l("Yes"),
			0 => _l("No"),
		);

		//Pagination
		$this->pagination->init();
		$this->pagination->total = $return_total;

		$data['pagination'] = $this->pagination->render();

		$data['continue'] = site_url('account');

		output($this->render('account/return_list', $data));
	}

	public function info()
	{
		$return_id = isset($_GET['return_id']) ? $_GET['return_id'] : 0;

		if (!is_logged()) {
			$query = array(
				'redirect' => site_url('account/return/info', 'return_id=' . $return_id)
			);

			redirect('customer/login', $query);
		}

		//Page Title
		set_page_info('title', _l("Return Information"));

		$url_query = $this->url->getQuery('page');

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Account"), site_url('account'));
		breadcrumb(_l("Product Returns"), site_url('account/return', $url_query));
		breadcrumb(_l("Return Information"), site_url('account/return/info', 'return_id=' . $return_id . '&' . $url_query));

		$return_info = $this->Model_Account_Return->getReturn($return_id);

		if ($return_info) {
			$return_info['comment']       = nl2br($return_info['comment']);
			$return_info['opened']        = $return_info['opened'] ? _l("Yes") : _l("No");
			$return_info['return_status'] = $this->order->getReturnStatus($return_info['return_status_id']);

			$data = $return_info;

			$data['date_ordered'] = $this->date->format($return_info['date_ordered'], 'date_format_short');
			$data['date_added']   = $this->date->format($return_info['date_added'], 'date_format_short');

			$histories = $this->Model_Account_Return->getReturnHistories($return_id);

			foreach ($histories as &$history) {
				$history['return_status'] = $this->order->getReturnStatus($history['return_status_id']);
				$history['date_added']    = $this->date->format($history['date_added'], 'date_format_short');
				$history['comment']       = nl2br($history['comment']);
			}
			unset($history);

			$data['histories'] = $histories;

			$data['continue'] = site_url('account/return', $url_query);

			output($this->render('account/return_info', $data));
		} else {
			$data['page_title'] = _l("Return Information");

			$data['continue'] = site_url('account/return');

			output($this->render('error/not_found', $data));
		}
	}

	public function insert()
	{
		//Order ID
		$order_id = isset($_GET['order_id']) ? $_GET['order_id'] : 0;

		$order_lookup = isset($_GET['order_lookup']) ? $_GET['order_lookup'] : 0;

		if (IS_POST && $this->validate()) {
			$return_data = $_POST;

			foreach ($return_data['return_products'] as &$product) {
				$product['rma']      = $this->Model_Account_Return->generateRma($return_data + $product);
				$product['quantity'] = $product['return_quantity'];

				$product['return_id'] = $this->Model_Account_Return->addReturn($return_data + $product);
			}
			unset($product);

			call('mail/return', $return_data);

			$url_query = array(
				'return_ids' => array_column($_POST['return_products'], 'return_id'),
			);

			redirect('account/return/success', $url_query);
		}

		//Page Head
		set_page_info('title', _l("Product Returns"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Account"), site_url('account'));
		breadcrumb(_l("Returns"), site_url('account/return'));
		breadcrumb(_l("Product Returns"), site_url('account/return/insert'));

		//The Data
		$order_info = $_POST;

		$customer_orders = $this->customer->getOrders();

		if ($customer_orders) {
			if ($order_lookup) {
				//If order does not belong to this customer, lookup the order info
				if (!empty($customer_orders) && !in_array($order_id, array_column($customer_orders, 'order_id'))) {
					$order_info = $this->order->get($order_id, false);

					//If the lookup email does not match the order email, customer may not view this order
					if (empty($_GET['email']) || $_GET['email'] !== $order_info['email']) {
						message('warning', _l("This order ID %s is associated with another account! Please login to that account to request a return.", $order_id));
						redirect('account/return/insert');
					}
				} //This order belongs to this customer, so they may request an exchange
				else {
					$order_lookup = false;
				}
			}

			if ($order_id) {
				foreach ($customer_orders as $order) {
					if ((int)$order['order_id'] === (int)$order_id) {
						$order_info += $order;
						break;
					}
				}
			} else {
				$order_info = reset($customer_orders);
			}
		}

		if ($order_info) {
			$order_info['date_ordered'] = $this->date->format($order_info['date_added']);

			$order_products = $this->Model_Sale_Order->getOrderProducts($order_info['order_id']);

			foreach ($order_products as $key => &$product) {
				$product_info = $this->Model_Product->getProductInfo($product['product_id']);

				if ($product_info) {
					$product['name']  = $product_info['name'];
					$product['model'] = $product_info['model'];
					$product['price'] = $this->currency->format($product['price']);

					$return_policy = $this->cart->getReturnPolicy($product_info['return_policy_id']);

					if ($return_policy['days'] < 0) {
						$product['no_return'] = _l("Final Sale");
					} else {
						$return_date = $this->date->add($order_info['date_added'], $return_policy['days'] . ' days');

						if ($this->date->isInPast($return_date)) {
							$product['no_return'] = _l("Past Policy Return Date (%s)", $this->date->format($return_date, 'short'));
						}
					}

					$product['return_policy'] = $return_policy;

					$product_defaults = array(
						'return_quantity'  => 0,
						'return_reason_id' => '',
						'comment'          => '',
						'opened'           => 0,
					);

					foreach ($product_defaults as $key => $default) {
						if (isset($_POST['return_products'][$product['product_id']][$key])) {
							$product[$key] = $_POST['return_products'][$product['product_id']][$key];
						} else {
							$product[$key] = $default;
						}
					}

				} else {
					unset($order_products[$key]);
				}
			}
			unset($product);

			$order_info['return_products'] = $order_products;
		}

		$defaults = array(
			'order_id'     => $order_id,
			'date_ordered' => '',
			'first_name'    => $this->customer->info('first_name'),
			'last_name'     => $this->customer->info('last_name'),
			'email'        => $this->customer->info('email'),
			'phone'        => $this->customer->info('phone'),
			'captcha'      => '',
		);

		$data = $order_info + $defaults;

		if (!empty($customer_orders)) {
			foreach ($customer_orders as &$order) {
				$product_count = $this->Model_Sale_Order->getTotalOrderProducts($order['order_id']);

				$order['display'] = _l("%s - (%s products)", $order['order_id'], $product_count);
			}
			unset($order);
		}

		$data['customer_orders'] = $customer_orders;

		$data['date_ordered_display'] = $this->date->format($data['date_ordered'], 'short');
		$data['data_return_reasons']  = Order::$return_reasons;

		$data['back']               = site_url('account');
		$data['return_product_url'] = site_url('account/return/insert');

		$data['order_lookup']        = $order_lookup;
		$data['order_lookup_action'] = site_url('account/return/find');

		if (!is_logged()) {
			message('warning', _l("You must be logged in to request a return. Your orders will automatically be associated to your account via your email address"));
		}

		$data['data_yes_no'] = array(
			0 => _l("No"),
			1 => _l("Yes"),
		);

		//Action Buttons
		$data['action']            = site_url('account/return/insert');
		$data['url_captcha_image'] = site_url('account/return/captcha');

		//Render
		output($this->render('account/return_form', $data));
	}

	public function find()
	{
		$url_query = '';

		if (IS_POST && !empty($_POST['ol_order_id']) && !empty($_POST['ol_email'])) {
			$order = $this->order->get($_POST['ol_order_id']);

			if (!empty($order)) {
				if ($order['email'] === $_POST['ol_email']) {
					$query = array(
						'order_id'     => $order['order_id'],
						'email'        => $order['email'],
						'order_lookup' => 1,
					);

					$url_query = http_build_query($query);


					message('notify', _l("The order was found! To request a return, you must first login or register a new account with the email %s.", $order['email']));
				} else {
					message('warning', _l("That order is associated with another email account!"));
				}
			} else {
				message("warning", _l("We were unable to find the order requested!"));
			}
		} else {
			message("warning", _l("We were unable to find the order requested!"));
		}

		redirect('account/return/insert', $url_query);
	}

	public function success()
	{
		set_page_info('title', _l("Return Success"));

		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Returns"), site_url('account/return'));
		breadcrumb(_l("Product Returns"), site_url('account/return/insert'));
		breadcrumb(_l("Return Success"), site_url('account/return/success'));

		$returns = array();

		if (!empty($_GET['return_ids'])) {
			foreach ($_GET['return_ids'] as $return_id) {
				$returns[] = $this->Model_Account_Return->getReturn($return_id);
			}
		}

		$data['returns'] = $returns;

		$data['continue'] = site_url();

		output($this->render('account/return_success', $data));
	}


	//TODO: Add to Library::Order->returnProducts();
	private function validate()
	{
		if (empty($_POST['order_id'])) {
			$this->error['order_id'] = _l("Order ID required!");
		}

		if (!$this->validation->text($_POST['first_name'], 1, 64)) {
			$this->error['first_name'] = _l("First Name must be between 1 and 64 characters!");
		}

		if (!$this->validation->text($_POST['last_name'], 1, 64)) {
			$this->error['last_name'] = _l("Last Name must be between 1 and 64 characters!");
		}

		if (!$this->validation->email($_POST['email'])) {
			$this->error['email'] = _l("E-Mail Address does not appear to be valid!");
		}

		if (!$this->validation->phone($_POST['phone'])) {
			$this->error['phone'] = _l("Telephone must be between 3 and 32 characters!");
		}

		$has_product = false;

		if (!empty($_POST['return_products'])) {
			foreach ($_POST['return_products'] as $key => $product) {
				if (!empty($product['return_quantity'])) {
					$has_product = true;

					if (!isset($product['return_reason_id']) || (!$product['return_reason_id'] && $product['return_reason_id'] !== '0')) {
						$this->error["return_products[$product[product_id]][return_reason_id"] = _l("You must select a Reason For Return!");
					}
				}
			}
		}

		if (!$has_product) {
			$this->error['return_products'] = _l("You must select at least 1 product to return!");
		}

		if (!$this->captcha->validate($_POST['captcha'])) {
			$this->error['captcha'] = _l("Verification code does not match the image!");
		}

		return empty($this->error);
	}

	public function captcha()
	{
		$this->captcha->generate();
	}
}
