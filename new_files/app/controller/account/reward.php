<?php
class App_Controller_Account_Reward extends Controller
{
	public function index()
	{
		if (!is_logged()) {
			$this->session->set('redirect', site_url('account/reward'));

			redirect('customer/login');
		}

		set_page_info('title', _l("Your Reward Points"));

		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Account"), site_url('account'));
		breadcrumb(_l("Reward Points"), site_url('account/reward'));

		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		} else {
			$page = 1;
		}

		$data['rewards'] = array();

		$data = array(
			'sort'  => 'date_added',
			'order' => 'DESC',
			'start' => ($page - 1) * 10,
			'limit' => 10
		);

		$reward_total = $this->Model_Account_Reward->getTotalRewards($data);

		$results = $this->Model_Account_Reward->getRewards($data);

		foreach ($results as $result) {
			$data['rewards'][] = array(
				'order_id'    => $result['order_id'],
				'points'      => $result['points'],
				'description' => $result['description'],
				'date_added'  => $this->date->format($result['date_added'], 'short'),
				'href'        => site_url('account/order/info', 'order_id=' . $result['order_id'])
			);
		}

		$this->pagination->init();
		$this->pagination->total  = $reward_total;
		$data['pagination'] = $this->pagination->render();

		$data['total'] = (int)$this->customer->getRewardPoints();

		$data['continue'] = site_url('account');

		output($this->render('account/reward', $data));
	}
}
