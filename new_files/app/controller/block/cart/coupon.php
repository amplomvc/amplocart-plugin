<?php
class App_Controller_Block_Cart_Coupon extends Controller
{
	public function build($settings)
	{
		$settings['action'] = site_url('block/cart/coupon/apply_coupon');

		output($this->render('block/cart/coupon', $settings));
	}

	public function apply_coupon()
	{
		if ($this->cart->applyCoupon($_POST['coupon_code'])) {
			message('success', _l("Your coupon has been applied!"));
		} else {
			message('error', $this->cart->getError());
		}

		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} else {
			redirect('cart/cart');
		}
	}
}
