<?php
class App_Controller_Block_Cart_Voucher extends Controller
{
	public function build($settings)
	{
		$settings += array(
			'voucher' => '',
		);

		//Render
		output($this->render('block/cart/voucher', $settings));
	}

	public function add_voucher()
	{
		if ($this->cart->applyVoucher($_POST['voucher'])) {
			message('success', _l("Your Voucher has been added to your order"));
		} else {
			message('error', $this->cart->getError());
		}

		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} else {
			redirect('checkout/checkout');
		}
	}
}
