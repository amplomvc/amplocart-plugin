<?php
class App_Controller_Block_Cart_Cart extends Controller
{
	public function build($settings = array())
	{
		//Check if the shipping estimate was invalidated and that we are not in the checkout process
		// -> update the shipping estimate to the first Shipping option
		if (!$this->order->hasOrder() && !$this->cart->validateShippingMethod()) {
			$shipping_methods = $this->cart->getShippingMethods();

			if (!empty($shipping_methods)) {
				$this->cart->setShippingMethod(key($shipping_methods));
			}
		}

		//Clear errors from cart that we dont care about..
		$this->cart->clearErrors();

		if (!$this->cart->validate()) {
			if ($this->cart->getErrorCode() !== Cart::ERROR_CART_EMPTY) {
				message('error', $this->cart->getError());
			}
		}

		$settings['show_price'] = !option('config_customer_hide_price') || is_logged();

		//Get the cart Products
		if ($this->cart->hasProducts()) {
			$cart_products = $this->cart->getProducts();

			foreach ($cart_products as $key => &$cart_product) {
				$product = & $cart_product['product'];

				if (empty($product) || !isset($product['tax_class_id'])) {
					$this->cart->removeProduct($key);
					continue;
				}

				$cart_product['price'] = $this->tax->calculate($cart_product['price'], $product['tax_class_id']);
				$cart_product['total'] = $this->tax->calculate($cart_product['total'], $product['tax_class_id']);

				if (option('config_cart_show_return_policy')) {
					$product['return_policy'] = $this->cart->getReturnPolicy($product['return_policy_id']);
				}
			}
			unset($product);

			$settings['cart_products'] = $cart_products;
		}

		//Render Additional Carts
		$carts = $this->System_Extension_Cart->renderCarts();

		$settings['cart_inline'] = $carts['inline'];
		$settings['cart_extend'] = $carts['extend'];

		$settings['is_empty'] = $this->cart->isEmpty();
		$settings['$this->is_ajax'] = $this->is_ajax;

		//Render
		$this->render('block/cart/cart', $settings);
	}
}
