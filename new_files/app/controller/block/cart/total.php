<?php
class App_Controller_Block_Cart_Total extends Controller
{
	public function build()
	{
		$data['totals'] = $this->cart->getTotals();

		//Render
		output($this->render('block/cart/total', $data));
	}
}
