<?php
class App_Controller_Block_Cart_Code extends Controller
{
	public function build($settings = array())
	{
		$settings['action'] = site_url('block/cart/code/apply');

		output($this->render('block/cart/code', $settings));
	}

	public function apply()
	{
		$error = array();

		if ($this->cart->applyVoucher(_post('code'))) {
			message('success', _l("Your Voucher has been added to your order"));
		} else {
			$error = $this->cart->getError();
		}

		//If Voucher does not exist, apply coupon
		if (isset($error['code'])) {
			$this->cart->clearErrors();
			$error = array();

			if ($this->cart->applyCoupon(_post('code'))) {
				message('success', _l("Your coupon has been applied!"));
			} else {
				$error = $this->cart->getError();
			}
		}

		if ($error) {
			message('error', $error);
		}

		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} else {
			redirect('checkout/checkout');
		}
	}
}
