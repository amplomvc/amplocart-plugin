<?php

class App_Controller_Block_Cart_Shipping extends Controller
{
	public function build($settings = array())
	{
		$settings += $_SESSION;

		$settings += array(
			'action'          => '',
			'country_id'      => option('config_country_id'),
			'zone_id'         => '',
			'postcode'        => '',
			'shipping_method' => '',
		);


		$settings['__ac_template__'] =

		$settings['data_countries'] = $this->Model_Localisation_Country->getActiveCountries();

		output($this->render('block/cart/shipping', $settings));
	}

	public function quote()
	{
		if (!$this->cart->hasProducts()) {
			message('error', _l("Your cart is empty!"));
		}

		if (!$this->cart->hasShipping()) {
			message('error', _l("No Shipping options are available. Please <a href=\"%s\">contact us</a> for assistance!", site_url('contact')));
		}

		//Template Data
		$data = $_POST;

		$data += array(
			'country_id'       => '',
			'zone_id'          => '',
			'postcode'         => '',
			'shipping_methods' => array(),
		);

		if (!$this->message->has('error')) {
			$shipping_methods = $this->cart->getShippingMethods($data);

			if ($shipping_methods) {
				$data['shipping_methods'] = $shipping_methods;
			} else {
				message('error', _l("No Shipping options are available. Please <a href=\"%s\">contact us</a> for assistance!", site_url('contact')));
			}
		}

		//Action Buttons
		$data['apply'] = site_url('block/cart/shipping/apply');

		//Messages
		$data['message'] = $this->message->toJSON();

		//Response
		output($this->render('block/cart/shipping_quote', $data));
	}

	public function apply()
	{
		$json = array();

		if (empty($_POST['shipping_method'])) {
			if (!empty($_POST['redirect'])) {
				message('warning', _l("Please choose a shipping method!"));
				redirect(urldecode($_POST['redirect']));
			} else {
				$json['error'] = _l("Please choose a shipping method!");
				output(json_encode($json));
				return;
			}
		}

		if (!empty($_POST['add_address'])) {
			if (!$this->cart->canShipTo($_POST)) {
				$json['error'] = $this->cart->getError('shipping_address');
			} else {
				$address_id = $this->Model_Address->save(null, $_POST);
			}
		} else {
			$address = array(
				'customer_id' => customer_info('customer_id'),
				'country_id'  => _post('country_id'),
				'zone_id'     => _post('zone_id'),
				'postcode'    => _post('postcode'),
			);

			$address_id = $this->Model_Address->exists($address);
		}

		if (!empty($address_id)) {
			if (!$this->cart->setShippingAddress($address_id)) {
				$json['error']['shipping_address'] = $this->cart->getError('shipping_address') . $address_id;
			} else {
				$result = $this->cart->setShippingMethod($_POST['shipping_method']);

				if (!empty($_POST['redirect'])) {
					if ($result) {
						message('success', _l("Success: Your shipping estimate has been applied!"));
					} else {
						message('warning', $this->cart->getError('shipping_method'));
					}

					redirect(urldecode($_POST['redirect']));
				}

				if ($result) {
					$json['success'] = _l("Success: Your shipping estimate has been applied!");
				} else {
					$json['error'] = $this->cart->getError('shipping_method');
				}
			}

		} else {
			$json['request_address'] = true;
		}

		output(json_encode($json));
	}
}
