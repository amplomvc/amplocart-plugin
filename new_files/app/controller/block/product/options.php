<?php
class App_Controller_Block_Product_Options extends Controller
{
	public function build($settings)
	{
		$product_id = !empty($settings['product_id']) ? $settings['product_id'] : null;

		if (!$product_id) {
			return '';
		}

		$product_options = $this->Model_Product->getProductOptions($product_id);

		//return a blank template if no options were found
		if (!$product_options) {
			return '';
		}

		$image_width        = option('config_image_product_option_width');
		$image_height       = option('config_image_product_option_height');
		$image_thumb_width  = option('config_image_thumb_width');
		$image_thumb_height = option('config_image_thumb_height');
		$image_popup_width  = option('config_image_popup_width');
		$image_popup_height = option('config_image_popup_height');

		foreach ($product_options as $key => &$product_option) {
			if (empty($product_option['product_option_values'])) {
				unset($product_options[$key]);
				continue;
			}

			$product_option['default'] = array();

			foreach ($product_option['product_option_values'] as $key => &$product_option_value) {
				//if this product is still in stock
				if ($product_option_value['available']) {
					//Hide Price for non-logged customers
					if (option('config_customer_hide_price') && !is_logged()) {
						$product_option_value['price'] = false;
					} else {
						if (option('config_show_price_with_tax')) {
							$product_option_value['price'] = $this->tax->calculate($product_option_value['price'], $settings['tax_class_id']);
						}

						//Show the price with the Product Option Name
						if ($product_option_value['price'] > 0) {
							$product_option_value['display_price'] = _l(" <span class=\"option_price\">+%s</span>", $this->currency->format($product_option_value['price']));
						} elseif ($product_option_value['price'] < 0) {
							$product_option_value['display_price'] = _l(" <span class=\"option_price\">-%s</span>", $this->currency->format($product_option_value['price']));
						} else {
							$product_option_value['display_price'] = '';
						}

						$product_option_value['value'] .= $product_option_value['display_price'];
					}

					if ($product_option['type'] == 'image') {
						$image                         = $product_option_value['image'];
						$product_option_value['thumb'] = image($image, $image_width, $image_height);

						$small_image                 = image($image, $image_thumb_width, $image_thumb_height);
						$popup_image                 = image($image, $image_popup_width, $image_popup_height);
						$product_option_value['rel'] = "{gallery:'gal1', smallimage:'$small_image', largeimage:'$popup_image'}";
					}

					if ($product_option_value['default']) {
						$product_option['default'][] = $product_option_value['product_option_value_id'];
					}
				} else {
					unset($product_option['product_option_values'][$key]);
				}
			}
			unset($product_option_value);

			$blank_option = array();

			switch ($product_option['type']) {
				case 'select':
					$blank_option[''] = array(
						'option_value_id'         => '',
						'product_option_value_id' => '',
						'value'                   => _l("( Please Select )")
					);
					break;

				case 'radio':
				case 'checkbox':
					break;

				case 'image':
					if (!(int)$product_option['required']) {
						$image            = image('data/no_image_select.png', $image_width, $image_height);
						$blank_option[''] = array(
							'option_value_id'         => '',
							'product_option_value_id' => '',
							'rel'                     => '',
							'thumb'                   => $image,
							'value'                   => _l("( Please Select )")
						);
					}
					break;

				default:
					break;
			}

			if ($blank_option) {
				$product_option['product_option_values'] = $blank_option + $product_option['product_option_values'];
			}
		}
		unset($product_option);

		$data['product_options'] = $product_options;

		$data['no_image'] = image('no_image.png', option('config_image_product_option_width'), option('config_image_product_option_height'));

		output($this->render('block/product/options', $data));
	}
}
