<?php

class App_Controller_Block_Product_List extends Controller
{
	public function build($settings = array())
	{
		//TODO: need to implement these options in admin panel!
		$settings += array(
			'process_data'          => true,
			'wishlist_status'       => option('config_wishlist_status'),
			'compare_status'        => option('config_compare_status'),
			'list_show_add_to_cart' => option('config_list_show_add_to_cart'),
			'show_price_with_tax'   => option('config_show_price_with_tax'),
			'review_status'         => option('config_review_status'),
			'image_width'           => option('config_image_product_width'),
			'image_height'          => option('config_image_product_height'),
		);

		if ($settings['process_data']) {
			foreach ($settings['products'] as &$product) {

				if (option('config_show_product_list_hover_image')) {
					if (!empty($product['images'])) {
						reset($product['images']);
						$product['hover_image'] = current($product['images']);
					}
				}

				$product['tax'] = $this->tax->calculate($product['special'] ? $product['special'] : $product['price'], $product['tax_class_id']);
			}
			unset($product);
		}

		$template = !empty($settings['template']) ? $settings['template'] : 'block/product/product_list';

		$this->render($template, $settings);
	}
}
