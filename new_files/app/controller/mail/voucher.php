<?php

class App_Controller_Mail_Voucher extends Controller
{
	public function index($voucher_id)
	{
		$voucher = $this->Model_Sale_Voucher->getVoucher($voucher_id);

		if (!$voucher) {
			return;
		}

		$voucher += $voucher['data'];

		if (!empty($voucher['order_id'])) {
			$order = $this->order->get($voucher['order_id']);
			$store = $this->config->getStore($order['store_id']);

			$voucher += array(
				'currency_code'  => $order['currency_code'],
				'currency_value' => $order['currency_value'],
				'message'        => _l("Please accept this Gift for use at %s", option('site_name')),
				'to_email'       => $order['email'],
			);
		} else {
			$store = $this->config->getDefaultStore();

			$voucher += array(
				'currency_code'  => null,
				'currency_value' => null,
			);
		}

		$voucher['header'] = array(
			'store' => $store,
		   'title' => _l("Gift Voucher"),
		);

		$voucher['store'] = $store;

		$voucher['redeem_url'] = $this->url->store($store['store_id']);

		if (!empty($voucher['template'])) {
			$voucher['voucher_image'] = $this->image->get($voucher['image']);

			$image_size = getimagesize($voucher['image']);

			$voucher['image_width']  = $image_size[0];
			$voucher['image_height'] = $image_size[1];
		}

		$this->mail->init();
		$this->mail->setTo($voucher['to_email']);

		if ($voucher['from_email'] !== $voucher['to_email']) {
			$this->mail->setCc($voucher['from_email']);
		}

		$bcc = array(
			option('site_email')
		);

		if ($order['email'] !== $voucher['to_email'] && $order['email'] !== $voucher['from_email']) {
			$bcc[] = $order['email'];
		}

		$this->mail->setBcc($bcc);
		$this->mail->setFrom($voucher['from_email']);
		$this->mail->setSender($store['name']);
		$this->mail->setSubject(_l("You have been sent a gift voucher from %s", $voucher['from_name']));

		$this->mail->setHtml($this->render('mail/voucher', $voucher));

		$this->mail->send();
	}
}
