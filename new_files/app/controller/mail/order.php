<?php

class App_Controller_Mail_Order extends Controller
{
	public function index($order)
	{
		$order_id = $order['order_id'];

		//Order Information
		$order['link'] = store_url($order['store_id'], 'account/order/info', 'order_id=' . $order_id);

		//Shipping / Payment addresses
		$order['payment_address']  = $this->order->getPaymentAddress($order_id);
		$order['shipping_address'] = $this->order->getShippingAddress($order_id);

		//Shipping / Payment Methods
		$order['payment_method'] = $this->System_Extension_Payment->get($order['transaction']['payment_code'])->info();

		if (!empty($order['shipping'])) {
			$order['shipping_method'] = $this->System_Extension_Shipping->get($order['shipping']['shipping_code'])->info();
		}

		//Products
		foreach ($order['order_products'] as &$product) {
			$product += $this->Model_Product->getActiveProduct($product['product_id']);
		}
		unset($product);

		//Urls
		$order['order_info_url'] = $this->url->store($order['store_id'], 'account/order/info', 'order_id=' . $order_id);

		$store          = $this->config->getStore($order['store_id']);
		$order['store'] = $store;

		$order['header'] = array(
			'store' => $store,
			'title' => _l("Order Confirmation"),
		);

		$this->mail->init();

		$subject = _l('%s - Order %s', $store['name'], $order_id);

		if (empty($order['email'])) {
			$order['email'] = option('site_email_error');
			$subject .= " (No Order Email was found!)";
		}

		$this->mail->setTo($order['email']);
		$this->mail->setCc(option('site_email'));
		$this->mail->setFrom(option('site_email'));
		$this->mail->setSender($store['name']);
		$this->mail->setSubject($subject);

		$this->mail->setText(html_entity_decode($this->render('mail/order_text', $order), ENT_QUOTES, 'UTF-8'));

		//HTML email
		$this->mail->setHtml($this->render('mail/order_html', $order));

		$this->mail->send();

		// Admin Alert Mail
		if (option('config_alert_mail')) {
			$to = option('site_email');

			if (option('config_alert_emails')) {
				$to .= ',' . option('config_alert_emails');
			}

			$this->mail->init();

			$this->mail->setTo($to);
			$this->mail->setFrom(option('site_email'));
			$this->mail->setSender($store['name']);
			$this->mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$this->mail->setText(html_entity_decode($this->render('mail/order_text_admin', $order), ENT_QUOTES, 'UTF-8'));
			$this->mail->send();
		}
	}
}
