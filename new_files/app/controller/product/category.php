<?php

class App_Controller_Product_Category extends Controller
{
	public function index()
	{
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("All Categories"), site_url('product/category'));

		$category_id = _get('category_id', 0);
		$attributes  = _get('attribute', 0);

		$category = $this->Model_Category->getActiveCategory($category_id);

		if (!$category) {
			return call('error/not_found');
		}

		//Layout override (only if set)
		$layout_id = $this->Model_Category->getCategoryLayoutId($category_id);

		if ($layout_id) {
			$this->config->set('config_layout_id', $layout_id);
		}

		set_page_info('title', $category['name']);
		set_page_meta('description', $category['meta_description']);

		//Page Title
		$parents = $this->Model_Category->getParents($category_id);

		foreach ($parents as $parent) {
			breadcrumb($parent['name'], site_url('product/category', 'category_id=' . $parent['category_id']));
		}

		$category['parents'] = $parents;

		breadcrumb($category['name'], site_url('product/category', 'category_id=' . $category_id));

		//TODO: How do we handle sub categories....?

		//Sorting / Filtering
		$sort_col = $category_id ? 'p.sort_order' : 'c.sort_order';
		$sort    = (array)_get('sort', array($sort_col => 'ASC'));

		$filter = array();

		if ($category_id) {
			$filter['category_ids'] = array($category_id);
		}

		if ($attributes) {
			$filter['attribute'] = $attributes;
		}

		list($products, $product_total) = $this->Model_Product->getActiveProducts($sort, $filter, '*', true);

		if (option('config_show_product_list_hover_image')) {
			foreach ($products as &$product) {
				$product['images'] = $this->Model_Product->getImages($product['product_id']);
			}
			unset($product);
		}

		$category['products']      = $products;
		$category['product_total'] = $product_total;


		//Sorting
		$sorts = array(
			'sort=p.sort_order&order=ASC' => _l("Default"),
			'sort=p.name&order=ASC'       => _l("Name (A - Z)"),
			'sort=p.name&order=DESC'      => _l("Name (Z - A)"),
			'sort=p.price&order=ASC'      => _l("Price (Low &gt; High)"),
			'sort=p.price&order=DESC'     => _l("Price (High &gt; Low)"),
			'sort=p.model&order=ASC'      => _l("Model (A - Z)"),
			'sort=p.model&order=DESC'     => _l("Model (Z - A)"),
		);

		if (option('config_review_status')) {
			$sorts['sort=rating&order=ASC']  = _l("Rating (Lowest)");
			$sorts['sort=rating&order=DESC'] = _l("Rating (Highest)");
		}

		$category['sorts'] = $this->sort->render_sort($sorts);

		$category['sort_url'] = $this->sort->get_sort_url();

		$category['limits'] = $this->sort->renderLimits();

		//Render
		output($this->render('product/category', $category));
	}
}
