<?php

class App_Controller_Product_Voucher extends App_Controller_Product_Product
{
	public function index($data = array(), $render = true)
	{
		$voucher = !empty($_POST['voucher']) ? $_POST['voucher'] : array();

		$defaults = array(
			'from_name'  => trim($this->customer->info('first_name') . ' ' . $this->customer->info('last_name')),
			'from_email' => $this->customer->info('email'),
			'to_name'    => '',
			'to_email'   => '',
			'message'    => _l("Please accept this gift from %s", option('site_name')),
			'amount'     => 100,
		);

		$voucher += $defaults;

		$data['voucher'] = $voucher;


		$data['data_amounts'] = array(
			50  => format('currency', 50),
			100 => format('currency', 100),
			150 => format('currency', 150),
			200 => format('currency', 200),
			250 => format('currency', 250),
		);

		$data['template'] = 'product/voucher';

		return parent::index($data, $render);
	}

	public function add_to_cart()
	{
		$product_id = _post('product_id', 0);
		$quantity   = _post('quantity', 1);
		$options    = _post('options', array());

		$key = $this->cart->addProduct($product_id, $quantity, $options, _post('voucher'));

		if (!$key) {
			message('error', $this->cart->getError());
		} else {
			$this->request->setRedirect(site_url('product/product', 'product_id=' . $product_id));

			$name        = $this->Model_Product->getProductName($product_id);
			$url_product = site_url('product/product', 'product_id=' . $product_id);
			$url_cart    = site_url('cart');

			$name = strip_tags(html_entity_decode($name));
			message('success', _l('<a href="%s">%s</a> has been added to <a href="%s">your cart</a>', $url_product, $name, $url_cart));
		}

		//Response
		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} else {
			if ($this->message->has('error')) {
				redirect('product/product', 'product_id=' . $product_id);
			}

			redirect('checkout');
		}
	}
}
