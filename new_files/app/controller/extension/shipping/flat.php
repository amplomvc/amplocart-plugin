<?php
class App_ControllerExtension_Shipping_Flat extends Controller
{
	public function index()
	{
		//TODO: recover from GIT maybe?
	}

	public function settings()
	{
		set_page_info('title', _l("Flat Rate Shipping"));

		if (IS_POST && $this->validate()) {
			$this->config->saveGroup('shipping_flat', $_POST);

			message('success', _l("You have successfully updated Flat Rate Shipping settings"));

			redirect('admin/extension/shipping');
		}

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Shipping Extensions"), site_url('admin/extension/shipping'));
		breadcrumb(_l("Flat Rate Shipping"), site_url('admin/shipping/flat'));

		$data['action'] = site_url('admin/shipping/flat');

		//Entry Data
		if (IS_POST) {
			$flat_info = $_POST;
		} else {
			$flat_info = $this->config->loadGroup('shipping_flat');
		}

		$defaults = array(
			'rates'      => array(),
		);

		$data += $flat_info + $defaults;

		//Template Data
		$data['data_tax_classes'] = $this->Model_Localisation_TaxClass->getTaxClasses();
		$data['data_geo_zones']   = $this->Model_Localisation_GeoZone->getGeoZones();

		$data['data_rule_types'] = array(
			'item_qty' => _l("Product Quantity"),
			'weight'   => _l("Weight of Cart"),
		);

		//Render
		output($this->render('shipping/flat', $data));
	}

	private function validate()
	{
		if (!user_can('w',  'shipping/flat')) {
			$this->error['warning'] = _l("You do not have permission to modify Flat Rate Shipping");
		}

		if (empty($_POST['flat_rates'])) {
			$this->error['flat_rates'] = _l("You must specify at least 1 flat rate rule");
		} else {
			foreach ($_POST['flat_rates'] as $key => $rate) {
				if (empty($rate['title'])) {
					$this->error["flat_rates[$key][title]"] = _l("Title is required");
				} else {
					$_POST['flat_rates'][$key]['method'] = slug($rate['title']);

					foreach ($_POST['flat_rates'] as $key2 => $rate2) {
						if ($rate2['method'] == $rate['title']) {
							$_POST['flat_rates'][$key]['method'] .= "_" . uniqid();
						}
					}
				}

				switch ($rate['rule']['type']) {
					case 'item_qty':
						if (!preg_match("/^[0-9]+,?[0-9]*$/", $rate['rule']['value'])) {
							$this->error["flat_rates[$key][rule][value]"] = _l("You must specify the rule");
						} else {
							if (preg_match("/^[0-9]+$/", $rate['rule']['value'])) {
								$_POST['flat_rates'][$key]['rule']['value'] .= ",0";
							}
						}
						break;
					case 'weight':
						break;
					default:
						break;
				}
			}
		}

		return empty($this->error);
	}
}
