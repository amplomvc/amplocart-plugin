<?php

class App_Controller_Extension_Payment_Cod extends Controller
{
	public function index()
	{
		//Action Buttons
		$data['confirm'] = site_url('extension/payment/cod/confirm', 'order_id=' . $this->order->getId());

		//Render
		$this->render('extension/payment/cod', $data);
	}
}
