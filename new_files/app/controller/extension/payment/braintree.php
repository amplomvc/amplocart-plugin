<?php

class App_Controller_Extension_Payment_Braintree extends Controller
{
	private $settings;

	public function __construct()
	{
		parent::__construct();

		$this->settings = $this->System_Extension_Payment_Braintree->settings();
	}

	public function index()
	{
		//Entry Data
		$data['encryption_key'] = $this->settings['client_side_encryption_key'];

		$cards = $this->System_Extension_Payment_Braintree->getCards();

		if (!$cards) {
			$cards = array();
		}

		$data['cards'] = $cards;

		//Action Buttons
		$data['confirm'] = site_url('extension/payment/braintree/confirm', 'order_id=' . $this->order->getId());

		$data['user_logged'] = is_logged();

		$data['payment_key'] = $this->cart->getPaymentKey();

		//Render
		$this->render('extension/payment/braintree/braintree', $data);
	}

	public function register_card($settings = array())
	{
		//Default Settings
		$settings += array(
			'template' => 'extension/payment/braintree/register_card',
		);

		//Entry Data
		$card_info = array();

		if (IS_POST) {
			$card_info = $_POST;
		}

		$payment_address = $this->customer->getDefaultPaymentAddress();

		$defaults = array(
			'first_name' => $this->customer->info('first_name'),
			'last_name'  => $this->customer->info('last_name'),
			'postcode'  => $payment_address ? $payment_address['postcode'] : '',
		);

		$settings += $card_info + $defaults;

		//Template Data
		$settings['encryption_key'] = $this->settings['client_side_encryption_key'];

		//Action Buttons
		$settings['submit'] = site_url('extension/payment/braintree/add_card');

		//Render
		output($this->render($settings['template'], $settings));
	}

	public function select_card($settings = array())
	{
		//Default Settings
		$settings += array(
			'new_card'    => false,
			'remove_card' => false,
			'payment_key' => null,
		);

		//Data
		$cards = $this->System_Extension_Payment_Braintree->getCards();

		if (!empty($settings['new_card'])) {
			$template = 'extension/payment/braintree/' . (is_string($settings['new_card']) ? $settings['new_card'] : 'register_fields');

			$cards['new'] = array(
				'id'       => 'new',
				'settings' => array(
					'template' => $template,
				),
			);
		}

		$settings['cards'] = $cards;

		if (is_null($settings['payment_key'])) {
			$settings['payment_key'] = $this->customer->getDefaultPaymentMethod('braintree');
		}

		//Render
		output($this->render('extension/payment/braintree/select_card', $settings));
	}

	public function add_card()
	{
		//Handle POST
		if (!$this->System_Extension_Payment_Braintree->addCard($_POST)) {
			message('error', $this->System_Extension_Payment_Braintree->getError());
		} else {
			message('success', _l("You have successfully registered your card with us!"));
		}

		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} else {
			if ($this->request->hasRedirect()) {
				$this->request->doRedirect();
			}

			redirect('account');
		}
	}

	public function remove_card()
	{
		if (!empty($_GET['card_id'])) {
			if ($this->System_Extension_Payment_Braintree->removeCard($_GET['card_id'])) {
				message('notify', _l("You have successfully removed the card from your account"));
			} else {
				message('warning', $this->System_Extension_Payment_Braintree->getError());
			}
		} else {
			message('warning', _l("There was no card selected to be removed."));
		}

		redirect('account');
	}
}
