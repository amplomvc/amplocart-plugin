<?php

class App_Controller_Cart extends Controller
{
	public function index($data = array())
	{
		//Page Head
		set_page_info('title', _l("Shopping Cart"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Shopping Cart"), site_url('cart'));

		//We remove any active orders to allow shipping estimates to be updated
		if ($this->order->hasOrder()) {
			$this->order->clear();
		}

		$data += array(
			'has_shipping'    => option('shipping_status') && $this->cart->hasShipping(),
			'weight'          => $this->cart->getWeight(),
			'customer_points' => $this->customer->getRewardPoints(),
			'cart_points'     => $this->cart->getTotalPoints(),
			'is_empty'        => $this->cart->isEmpty(),
			'can_checkout'    => $this->cart->canCheckout(),
		);

		//Set Continue to the redirect unless we are redirecting to the cart page
		if (isset($_GET['redirect']) && !preg_match("/cart\\/?[^a-z]/", $_GET['redirect'])) {
			$continue = urldecode($_GET['redirect']);
		} else {
			$continue = site_url('product/category');
		}

		//Action
		$data += array(
			'continue' => $continue,
		   'checkout' => site_url('checkout'),
		);

		//Render
		output($this->render('cart/cart', $data));
	}

	public function add()
	{
		$product_id = _post('product_id', 0);
		$type       = _post('type', Cart::PRODUCTS);

		$key = $this->addToCart();

		if ($key) {
			if ($type === Cart::PRODUCTS || $type === Cart::VOUCHERS) {
				$name = $this->Model_Product->getProductName($product_id);
			}

			$this->request->setRedirect(site_url('product/product', 'product_id=' . $product_id));

			$url_product = site_url('product/product', 'product_id=' . $product_id);
			$url_cart    = site_url('cart');
			$name = strip_tags(html_entity_decode($name));
			message('success', _l('<a href="%s">%s</a> has been added to <a href="%s">your cart</a>', $url_product, $name, $url_cart));
		} else {
			message('error', $this->cart->getError('add'));
		}

		//Response
		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} else {
			if ($this->message->has('error')) {
				redirect('product/product', 'product_id=' . $product_id);
			}

			redirect('checkout');
		}
	}

	private function addToCart()
	{
		$product_id = _post('product_id', 0);
		$type       = _post('type', Cart::PRODUCTS);
		$quantity   = _post('quantity', 1);
		$options    = _post('options', array());

		if ($type === Cart::PRODUCTS) {
			$key = $this->cart->addProduct($product_id, $quantity, $options);
		} else {
			$key = $this->cart->addItem($type, $product_id, $quantity, $options);
		}

		return $this->cart->hasError('add') ? false : $key;
	}

	public function update()
	{
		if (!empty($_POST['quantity'])) {
			foreach ($_POST['quantity'] as $key => $quantity) {
				$this->cart->updateProduct($key, $quantity);
			}

			message('success', _l("Your cart has been updated!"));
		}

		if ($this->is_ajax) {
			$return = !empty($_REQUEST['return_type']) ? $_REQUEST['return_type'] : 'html';

			if ($return === 'html') {
				$output = block('cart/cart');
			} else {
				$output = $this->message->toJSON();
			}

			output($output);
		} else {
			redirect('cart');
		}
	}

	public function remove()
	{
		$cart_product = $this->cart->getProduct($_GET['cart_key'], true);

		if ($this->cart->removeProduct($_GET['cart_key'])) {
			message('success', _l('<a href="%s">%s</a> has been removed from your cart.', site_url('product/product', 'product_id=' . $cart_product['id']), $cart_product['product']['name']));
		} else {
			message('error', $this->cart->getError());
		}

		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} else {
			redirect('cart');
		}
	}
}
