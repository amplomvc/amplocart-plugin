<?php

class App_Controller_Admin_Category extends Controller
{
	public function index()
	{
		//Page Head
		set_page_info('title', _l("Category"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Category"), site_url('admin/category'));

		//Batch Actions
		$actions = array(
			'enable'  => array(
				'label' => _l("Enable")
			),
			'disable' => array(
				'label' => _l("Disable"),
			),
			'copy'    => array(
				'label' => _l("Copy"),
			),
			'delete'  => array(
				'label' => _l("Delete"),
			),
		);

		$data['batch_action'] = array(
			'actions' => $actions,
			'path'    => 'admin/category/batch_action',
		);

		//Render
		output($this->render('category/list', $data));
	}

	public function listing()
	{
		$sort    = (array)_request('sort', array('name' => 'ASC'));
		$filter  = (array)_request('filter');
		$options = array(
			'index'   => 'category_id',
			'page'    => _get('page', 1),
			'limit'   => _get('limit', option('admin_list_limit', 20)),
			'columns' => $this->Model_Category->getColumns((array)_request('columns')),
		);

		list($categories, $category_total) = $this->Model_Category->getRecords($sort, $filter, $options, true);

		$image_width  = option('admin_list_image_width');
		$image_height = option('admin_list_image_height');

		foreach ($categories as $category_id => &$category) {
			if (user_can('w', 'admin/category/form')) {
				$category['actions'] = array(
					'edit'   => array(
						'text' => _l("Edit"),
						'href' => site_url('admin/category/form', 'category_id=' . $category_id)
					),
					'delete' => array(
						'text' => _l("Delete"),
						'href' => site_url('admin/category/remove', 'category_id=' . $category_id)
					)
				);
			}

			$category['thumb'] = image($category['image'], $image_width, $image_height);
		}
		unset($category);

		$listing = array(
			'extra_cols'     => $this->Model_Category->getColumns(false),
			'records'        => $categories,
			'sort'           => $sort,
			'filter_value'   => $filter,
			'pagination'     => true,
			'total_listings' => $category_total,
			'listing_path'   => 'admin/category/listing',
			'save_path'      => 'admin/category/save',
		);

		$output = block('widget/listing', null, $listing + $options);

		//Response
		if ($this->is_ajax) {
			output($output);
		}

		return $output;
	}

	public function form()
	{
		//Page Head
		set_page_info('title', _l("Category"));

		//Insert or Update
		$category_id = (int)_get('category_id');

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Category"), site_url('admin/category'));
		breadcrumb($category_id ? _l("Edit") : _l("Add"), site_url('admin/category/form', 'category_id=' . $category_id));

		//Load Information
		$category = $_POST;

		if (!IS_POST && $category_id) {
			$category = $this->Model_Category->getCategory($category_id);
		}

		//Set Values or Defaults
		$defaults = array(
			'category_id'      => $category_id,
			'parent_id'        => 0,
			'layout_id'        => '',
			'name'             => '',
			'description'      => '',
			'meta_keywords'    => '',
			'meta_description' => '',
			'alias'            => '',
			'image'            => '',
			'sort_order'       => 0,
			'status'           => 1,
		);

		$category += $defaults;

		//All other categories to select parent
		$categories = $this->Model_Category->getCategoriesWithParents();

		// Remove own id from list
		foreach ($categories as $key => $cat) {
			if ($cat['category_id'] === $category_id) {
				unset($categories[$key]);
				break;
			}
		}

		//Template Data
		$category['data_categories'] = array(0 => _l(" --- None --- ")) + $categories;

		$category['data_layouts'] = $this->Model_Layout->getRecords(null, null, array('cache' => true));

		$category['data_statuses'] = array(
			0 => _l("Disabled"),
			1 => _l("Enabled"),
		);

		//Render
		output($this->render('category/form', $category));
	}

	public function save()
	{
		if ($category_id = $this->Model_Category->save(_request('category_id'), $_POST)) {
			message('success', _l("The category has been saved"));
			message('data', array('category_id' => $category_id));
		} else {
			message('error', $this->Model_Category->getError());
		}

		if ($this->is_ajax) {
			output_message();
		} elseif ($this->message->has('error')) {
			post_redirect('admin/category/form', 'category_id=' . _request('category_id'));
		} else {
			redirect('admin/category');
		}
	}

	public function remove()
	{
		if ($this->Model_Category->remove(_get('category_id'))) {
			message('success', _l("The Category has been removed"));
		} else {
			message('error', $this->Model_Category->getError());
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('admin/category');
		}
	}

	public function batch_action()
	{
		foreach ($_POST['batch'] as $category_id) {
			switch ($_POST['action']) {
				case 'enable':
					$this->Model_Category->save($category_id, array('status' => 1));
					break;

				case 'disable':
					$this->Model_Category->save($category_id, array('status' => 0));
					break;

				case 'delete':
					$this->Model_Category->remove($category_id);
					break;

				case 'copy':
					$this->Model_Category->copy($category_id);
					break;
			}
		}

		if ($this->Model_Category->hasError()) {
			message('error', $this->Model_Category->getError());
		} else {
			message('success', _l("Categories have been modified!"));
		}

		if ($this->is_ajax) {
			$this->listing();
		} else {
			redirect('admin/category');
		}
	}

	public function generate_url()
	{
		output($this->Model_UrlAlias->getUniqueAlias(_post('name'), 'category', 'category_id=' . (int)_request('category_id')));
	}
}
