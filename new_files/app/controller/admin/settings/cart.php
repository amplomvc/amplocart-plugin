<?php

/**
 * Title: Cart Settings
 * Icon: cart.png
 *
 */
class App_Controller_Admin_Settings_Cart extends Controller
{
	public function index()
	{
		//Page Head
		set_page_info('title', _l("Cart Settings"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Settings"), site_url('admin/settings'));
		breadcrumb(_l("Cart Settings"), site_url('admin/settings/cart'));

		//Load Information
		$config_data = $_POST;

		if (!IS_POST) {
			$config_data = $this->config->loadGroup('cart_settings', 0);
		}

		$defaults = array(
			'config_currency'                         => '',
			'config_currency_auto'                    => '',
			'config_length_unit'                      => 'cm',
			'config_weight_unit'                      => 'kg',
			'config_default_return_policy'            => 0,
			'config_default_shipping_policy'          => 0,
			'config_shipping_return_page_id'          => 0,
			'config_cart_show_return_policy'          => 1,
			'config_show_price_with_tax'              => '',
			'config_tax_default_id'                   => '',
			'config_tax_default'                      => '',
			'config_tax_customer'                     => '',
			'config_invoice_prefix'                   => 'INV-%Y-M%',
			'config_order_edit'                       => 7,
			'config_customer_approval'                => 0,
			'config_customer_hide_price'              => 0,
			'config_guest_checkout'                   => 1,
			'config_checkout_terms_page_id'           => 0,
			'config_show_category_image'              => 1,
			'config_show_category_description'        => 1,
			'config_show_product_list_hover_image'    => 0,
			'config_stock_display'                    => 1,
			'config_stock_warning'                    => 1,
			'config_stock_checkout'                   => 0,
			'config_stock_status_id'                  => 0,
			'config_show_product_related'             => 1,
			'config_review_status'                    => 1,
			'config_share_status'                     => 1,
			'config_show_product_attributes'          => 1,
			'config_download'                         => 1,
			'config_upload_allowed'                   => 1,
			'config_upload_images_allowed'            => '',
			'config_upload_images_mime_types_allowed' => '',
			'config_cart_weight'                      => 1,
			'config_image_category_width'             => 240,
			'config_image_category_height'            => 240,
			'config_image_manufacturer_width'         => 240,
			'config_image_manufacturer_height'        => 240,
			'config_image_popup_width'                => 1024,
			'config_image_popup_height'               => 1024,
			'config_image_product_width'              => 420,
			'config_image_product_height'             => 420,
			'config_image_product_option_width'       => 50,
			'config_image_product_option_height'      => 50,
			'config_image_additional_width'           => 120,
			'config_image_additional_height'          => 120,
			'config_image_related_width'              => 120,
			'config_image_related_height'             => 120,
			'config_image_compare_width'              => 120,
			'config_image_compare_height'             => 120,
			'config_image_wishlist_width'             => 120,
			'config_image_wishlist_height'            => 120,
			'config_image_cart_width'                 => 80,
			'config_image_cart_height'                => 80,
			'config_fraud_detection'                  => '',
			'config_fraud_key'                        => '',
			'config_fraud_score'                      => '',
		);

		$data = $config_data + $defaults;

		//Template Data
		$data['data_countries']         = $this->Model_Localisation_Country->getCountries();
		$data['data_currencies']        = $this->Model_Localisation_Currency->getCurrencies();
		$data['data_length_units']      = $this->length->getUnits();
		$data['data_weight_units']      = $this->weight->getUnits();
		$data['tax_classes']            = $this->Model_Localisation_TaxClass->getTaxClasses();
		$data['data_pages']             = array('' => _l(" --- None --- ")) + $this->Model_Page->getPages();
		$data['data_stock_statuses']    = $this->Model_Localisation_StockStatus->getStockStatuses();

		$data['data_order_statuses']    = Order::$statuses;
		$data['data_return_statuses']   = Order::$return_statuses;
		$data['data_return_policies']   = $this->cart->getReturnPolicies();
		$data['data_shipping_policies'] = $this->cart->getShippingPolicies();

		$data['data_stock_display_types'] = array(
			'hide'   => _l("Do not display stock"),
			'status' => _l("Only show stock status"),
			-1       => _l("Display stock quantity available"),
			10       => _l("Display quantity up to 10"),
		);

		$data['data_show_product_related'] = array(
			0 => _l('Never'),
			1 => _l('Only When Unavailable'),
			2 => _l('Always'),
		);

		$data['text_add_return_policy']   = _l("Add a new <a href=\"%s\" target=\"_blank\">Return Policy</a>", site_url('admin/setting/return_policy'));
		$data['text_add_shipping_policy'] = _l("Add a new <a href=\"%s\" target=\"_blank\">Shipping Policy</a>", site_url('admin/setting/shipping_policy'));

		$data['data_statuses'] = array(
			0 => _l("Disabled"),
			1 => _l("Enabled"),
		);

		$data['data_yes_no'] = array(
			1 => _l("Yes"),
			0 => _l("No"),
		);

		//Render
		output($this->render('setting/cart_setting', $data));
	}

	public function save()
	{
		if ($this->config->saveGroup('cart_settings', $_POST, 0, true)) {
			message('success', _l("Success: You have modified settings!"));
		} else {
			message('error', $this->config->getError());
		}

		if (IS_AJAX) {
			output_json($this->message->fetch());
		} elseif ($this->message->has('error')) {
			post_redirect('admin/setting/cart');
		} else {
			redirect('admin/settings');
		}
	}
}
