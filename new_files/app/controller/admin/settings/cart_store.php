<?php

/**
 * Name: Cart Store
 * Icon: cart.png
 */
class App_Controller_Admin_Settings_CartStore extends Controller
{
	public function index()
	{
		//Page Head
		set_page_info('title', _l("Cart Store Settings"));

		//Insert or Update
		$store_id = isset($_GET['store_id']) ? $_GET['store_id'] : 0;

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Settings"), site_url('admin/settings'));

		if ($store_id && !IS_POST) {
			$store = $this->Model_Setting_Store->getStore($store_id);

			if (!$store) {
				message('warning', _l("You attempted to access a store that does not exist!"));
				redirect('admin/settings');
			}

			$store_config = $this->config->loadGroup('config', $store_id);

			if (empty($store_config)) {
				$store_config = $this->config->loadGroup('config', 0);
			}

			$store_info = $store + $store_config;
		} else {
			$store_info = $_POST;
		}

		$data = array();

		$defaults = array(
			'name'                            => 'Store ' . $store_id,
			'url'                             => '',
			'ssl'                             => '',
			'config_owner'                    => '',
			'config_address'                  => '',
			'config_email'                    => '',
			'config_phone'                => '',
			'config_fax'                      => '',
			'config_title'                    => '',
			'config_meta_description'         => '',
			'config_default_layout_id'        => '',
			'config_theme'                    => '',
			'config_country_id'               => option('config_country_id'),
			'config_zone_id'                  => option('config_zone_id'),
			'config_language'                 => option('config_language'),
			'config_currency'                 => option('config_currency'),
			'config_catalog_limit'            => '12',
			'config_allowed_shipping_zone'    => 0,
			'config_show_price_with_tax'      => '',
			'config_tax_default'              => '',
			'config_tax_customer'             => '',
			'config_customer_group_id'        => '',
			'config_customer_hide_price'      => '',
			'config_show_product_model'       => 1,
			'config_customer_approval'        => '',
			'config_guest_checkout'           => '',
			'config_account_terms_page_id'               => '',
			'config_checkout_terms_page_id'              => '',
			'config_stock_display'            => '',
			'config_stock_checkout'           => '',
			'config_cart_weight'              => '',
			'config_logo'                     => '',
			'config_icon'                     => null,
			'config_logo_width'               => 0,
			'config_logo_height'              => 0,
			'config_email_logo_width'         => 300,
			'config_email_logo_height'        => 0,
			'config_image_thumb_width'        => 228,
			'config_image_thumb_height'       => 228,
			'config_image_popup_width'        => 500,
			'config_image_popup_height'       => 500,
			'config_image_product_width'      => 80,
			'config_image_product_height'     => 80,
			'config_image_category_width'     => 80,
			'config_image_category_height'    => 80,
			'config_image_additional_width'   => 74,
			'config_image_additional_height'  => 74,
			'config_image_related_width'      => 80,
			'config_image_related_height'     => 80,
			'config_image_compare_width'      => 90,
			'config_image_compare_height'     => 90,
			'config_image_wishlist_width'     => 50,
			'config_image_wishlist_height'    => 50,
			'config_image_cart_width'         => 80,
			'config_image_cart_height'        => 80,
			'config_use_ssl'                  => '',
			'config_contact_page_id'          => '',
		);

		$data += $store_info + $defaults;

		//Current Page Breadcrumb
		breadcrumb($data['name'], site_url('admin/setting/cart_store/form', 'store_id=' . $store_id));

		//Additional Info
		$data['layouts']              = $this->Model_Design_Layout->getLayouts();
		$data['data_themes']               = $this->theme->getThemes();
		$data['geo_zones']            = array_merge(array(0 => "--- All Zones ---"), $this->Model_Localisation_GeoZone->getGeoZones());
		$data['countries']            = $this->Model_Localisation_Country->getCountries();
		$data['languages']            = $this->Model_Localisation_Language->getLanguages();
		$data['currencies']           = $this->Model_Localisation_Currency->getCurrencies();
		$data['data_customer_groups'] = $this->Model_Sale_CustomerGroup->getRecords(null, null, array('cache' => true));
		$data['data_pages']             = array('' => _l(" --- None --- ")) + $this->Model_Page->getPages();
		$data['data_order_statuses']  = Order::$statuses;
		$data['data_pages']           = array('' => _l(" --- Please Select --- ")) + $this->Model_Page->getPages();

		$data['data_stock_display_types'] = array(
			'hide'   => _l("Do not display stock"),
			'status' => _l("Only show stock status"),
			-1       => _l("Display stock quantity available"),
			10       => _l("Display quantity up to 10"),
		);

		$data['data_yes_no'] = array(
			1 => _l("Yes"),
			0 => _l("No"),
		);

		//Logo Sizing
		$data['logo_thumb'] = $this->image->get($data['config_logo']);

		//Website Icon Sizes
		if (!is_array($data['config_icon'])) {
			$data['config_icon'] = array(
				'orig' => '',
				'ico'  => '',
			);
		}

		$data['data_icon_sizes'] = array(
			array(
				152,
				152
			),
			array(
				120,
				120
			),
			array(
				76,
				76
			),
		);

		foreach ($data['data_icon_sizes'] as $size) {
			$key = $size[0] . 'x' . $size[1];

			if (!isset($data['config_icon'][$key])) {
				$data['config_icon'][$key] = '';
			}
		}

		foreach ($data['config_icon'] as &$icon) {
			$icon = array(
				'thumb' => $this->image->get($icon),
				'src'   => $icon,
			);
		}
		unset($icon);

		//Action Buttons
		$data['save']               = site_url('admin/setting/cart_store/save', 'store_id=' . $store_id);
		$data['cancel']             = site_url('admin/settings');
		$data['url_generate_icons'] = site_url('admin/settings/generate_icons');

		//Render
		output($this->render('setting/cart_store_form', $data));
	}

	public function update()
	{
		set_page_info('title', _l("Cart Store Settings"));

		if (IS_POST && $this->validateForm()) {
			//Insert
			if (empty($_GET['store_id'])) {
				$store_id = $this->Model_Setting_Store->save($_POST);
				$this->config->saveGroup('config', $_POST, $store_id);
			} //Update
			else {
				$this->Model_Setting_Store->editStore($_GET['store_id'], $_POST);
				$this->config->saveGroup('config', $_POST, $_GET['store_id']);
			}

			if (!$this->message->has('error', 'warning')) {
				message('success', _l("The Store settings have been saved!"));

				redirect('admin/settings');
			}
		}

		$this->getForm();
	}

	public function generate_icons()
	{
		if (!empty($_POST['icon'])) {
			$sizes = array(
				array(
					152,
					152
				),
				array(
					120,
					120
				),
				array(
					76,
					76
				),
			);

			$icon_files = array();

			foreach ($sizes as $size) {
				$url = image($_POST['icon'], $size[0], $size[1]);

				$icon_files[$size[0] . 'x' . $size[1]] = array(
					'url'     => $url,
					'relpath' => str_replace(URL_IMAGE, '', $url),
				);
			}

			$url = $this->image->ico($_POST['icon']);

			$icon_files['ico'] = array(
				'relpath' => str_replace(URL_IMAGE, '', $url),
				'url'     => $url,
			);

			output(json_encode($icon_files));
		}
	}

	private function validateForm()
	{
		if (!user_can('modify', 'setting/store')) {
			$this->error['warning'] = _l("Warning: You do not have permission to modify stores!");
		}

		if (!validate('text', $_POST['name'], 1, 64)) {
			$this->error['name'] = _l("Store Name must be between 1 and 64 characters!");
		}

		if (!validate('url', $_POST['url'])) {
			$this->error['url'] = _l("Store URL invalid! Please provide a properly formatted URL (eg: http://yourstore.com)");
		}

		if (!validate('url', $_POST['ssl'])) {
			$this->error['ssl'] = _l("Store SSL invalid!  Please provide a properly formatted URL (eg: http://yourstore.com). NOTE: you may set this to the same value as URL, does not have to be HTTPS protocol.");
		}

		if ((strlen($_POST['config_owner']) < 3) || (strlen($_POST['config_owner']) > 64)) {
			$this->error['config_owner'] = _l("Store Owner must be between 3 and 64 characters!");
		}

		if ((strlen($_POST['config_address']) < 3) || (strlen($_POST['config_address']) > 256)) {
			$this->error['config_address'] = _l("Store Address must be between 10 and 256 characters!");
		}

		if ((strlen($_POST['config_email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $_POST['config_email'])) {
			$this->error['config_email'] = _l("E-Mail Address does not appear to be valid!");
		}

		if ((strlen($_POST['config_phone']) < 3) || (strlen($_POST['config_phone']) > 32)) {
			$this->error['config_phone'] = _l("Telephone must be between 3 and 32 characters!");
		}

		if (!$_POST['config_title']) {
			$this->error['config_title'] = _l("Title must be between 3 and 32 characters!");
		}

		$image_sizes = array(
			'image_category'   => "Category List",
			'image_thumb'      => "Product Thumb",
			'image_popup'      => "Product Popup",
			'image_product'    => "Product List",
			'image_additional' => "Product Additional",
			'image_related'    => "Product Related",
			'image_compare'    => "Product Compare",
			'image_wishlist'   => "Product Wishlist",
			'image_cart'       => "Cart",
		);

		foreach ($image_sizes as $image_key => $image_size) {
			$image_width  = 'config_' . $image_key . '_width';
			$image_height = 'config_' . $image_key . '_height';

			if ((int)$_POST[$image_width] <= 0 || (int)$_POST[$image_height] <= 0) {
				$this->error[$image_height] = _l("%s image dimensions are required.", $image_size);
			}
		}

		if ((int)$_POST['config_catalog_limit'] <= 0) {
			$this->error['config_catalog_limit'] = _l("Limit required!");
		}

		return empty($this->error);
	}
}
