<?php
class App_Controller_Admin_Extension_Shipping extends Controller
{
	private $extension_controller;

	public function index()
	{
		if (!empty($_GET['code'])) {
			if (!$this->System_Extension_Shipping->has($_GET['code'])) {
				message('warning', _l("The extension %s does not exist!", $_GET['code']));

				redirect('admin/extension/shipping');
			}

			$this->getForm();
		} else {
			$this->getList();
		}
	}

	//TODO: Implement the Add / Delete functionality for Extensions
	public function delete()
	{
		if (user_can('w',  'extension/shipping')) {
			if (!empty($_GET['code'])) {
				$this->System_Extension_Shipping->deleteExtension($_GET['code']);

				if (!$this->message->has('error', 'warning')) {
					message('success', _l("You have successfully removed the %s extension.", $_GET['code']));
				}

				redirect('admin/extension/shipping', $this->url->getQueryExclude('name'));
			}
		} else {
			$this->error['permission'] = _l("You do not have permission to modify the Shipping extensions");
		}

		$this->index();
	}

	private function getList()
	{
		//Page Head
		set_page_info('title', _l("Shipping Extensions"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Shipping Extensions"), site_url('admin/extension/shipping'));

		//The Table Columns
		$columns = array();

		$columns['title'] = array(
			'type'         => 'text',
			'display_name' => _l("Shipping Title"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['code'] = array(
			'type'         => 'text',
			'display_name' => _l("Code"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['sort_order'] = array(
			'type'         => 'int',
			'display_name' => _l("Sort Order"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['status'] = array(
			'type'         => 'select',
			'display_name' => _l("Status"),
			'filter'       => true,
			'build_data'   => array(
				0 => _l("Disabled"),
				1 => _l("Enabled"),
			),
			'sortable'     => true,
		);

		//The Sort & Filter Data
		$sort   = $this->sort->getQueryDefaults('sort_order', 'ASC');
		$filter = !empty($_GET['filter']) ? $_GET['filter'] : array();

		//Table Row Data
		$extension_total = $this->System_Extension_Model->getTotal('shipping', $filter);
		$extensions      = $this->System_Extension_Model->getExtensions('shipping', $sort + $filter);

		foreach ($extensions as &$extension) {
			if ($extension['installed']) {
				$actions = array(
					'edit'      => array(
						'text' => _l("Edit"),
						'href' => site_url('admin/extension/shipping/edit', 'code=' . $extension['code'])
					),
					'settings'  => array(
						'text' => _l("Settings"),
						'href' => site_url('admin/extension/shipping', 'code=' . $extension['code'])
					),
					'uninstall' => array(
						'text' => _l("Uninstall"),
						'href' => site_url('admin/extension/shipping/uninstall', 'code=' . $extension['code']),
					),
				);
			} else {
				$actions = array(
					'install' => array(
						'text' => _l("Install"),
						'href' => site_url('admin/extension/shipping/install', 'code=' . $extension['code'])
					),
				);
			}

			$extension['actions'] = $actions;
		}
		unset($extension);

		//Build The Table

		$this->table->init();
		$this->table->setTemplate('table/list_view');
		$this->table->setColumns($columns);
		$this->table->setRows($extensions);
		$this->table->mapAttribute('filter_value', $filter);

		$data['list_view'] = $this->table->render();

		//Action Buttons
		$data['insert'] = site_url('admin/extension/add');

		//Render limit Menu
		$data['limits'] = $this->sort->renderLimits();

		//Pagination
		$this->pagination->init();
		$this->pagination->total = $extension_total;

		$data['pagination'] = $this->pagination->render();

		//Render
		output($this->render('extension/shipping_list', $data));
	}

	private function getForm()
	{
		$code = $_GET['code'];

		$this->loadExtensionController($code);

		//Handle POST
		if (IS_POST && $this->validate()) {
			//If Extension needs to customize the way data is stored
			if (method_exists($this->extension_controller, 'saveSettings')) {
				$this->extension_controller->saveSettings($_POST['settings']);
			}

			$this->System_Extension_Model->updateExtension('shipping', $code, $_POST);

			message('success', _l("You have successfully modified the %s extension!", $code));

			redirect('admin/extension/shipping');
		}

		$shipping_extension = $this->System_Extension_Shipping->get($code);

		//Page Head
		set_page_info('title', $shipping_extension->info('title'));

		//Page Title
		$data['page_title'] = $shipping_extension->info('title');

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Shipping Extensions"), site_url('admin/extension/shipping'));
		breadcrumb($shipping_extension->info('title'), site_url('admin/extension/shipping', 'code=' . $code));

		//Load Information
		if (IS_POST) {
			$extension_info = $_POST;
		} else {
			$extension_info = $shipping_extension->info();

			$extension_info['settings']= $shipping_extension->settings();
		}

		//NOTE: 'settings', 'sort_order', and 'status' defaults are never used (in theory. Defaults are set in System_Extension_Model.
		$defaults = array(
			'settings'   => array(),
			'sort_order' => 0,
			'status'     => 1,
		);

		$data += $extension_info + $defaults;

		$settings_defaults = array(
			'min_total'                => 0,
			'geo_zone_id'              => 0,
		);

		$data['settings'] += $settings_defaults;

		//Get additional extension settings and profile data (this is the plugin part)
		if (method_exists($this->extension_controller, 'settings')) {
			$this->extension_controller->settings($data['settings']);
			$data['extend_settings'] = $this->extension_controller->output;
		}

		//Template Data
		$data['data_order_statuses'] = Order::$statuses;
		$data['data_geo_zones']      = array(0 => _l("All Zones")) + $this->Model_Localisation_GeoZone->getGeoZones();

		$data['data_statuses'] = array(
			0 => _l("Disabled"),
			1 => _l("Enabled"),
		);

		//Action Buttons
		$data['save']   = site_url('admin/extension/shipping', 'code=' . $code);
		$data['cancel'] = site_url('admin/extension/shipping');

		//Render
		output($this->render('extension/shipping', $data));
	}

	public function edit()
	{
		$code = !empty($_GET['code']) ? $_GET['code'] : '';

		//Verify File
		$file = DIR_SYSTEM . "extension/shipping/" . $code . '.php';

		if (!is_file($file)) {
			message('warning', _l("Unable to locate %s for editing!", $file));
			redirect('admin/extension/shipping');
		}

		//Handle POST
		if (IS_POST) {
			if (file_put_contents($file, html_entity_decode($_POST['contents']))) {
				message('success', _l("Successfully modified %s", $file));
			} else {
				message('warning', _l("Failed to save changes to %s", $file));
			}

			redirect('admin/extension/shipping');
		}

		//Load extension
		$shipping_extension = $this->System_Extension_Shipping->get($code);

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Shipping Extensions"), site_url('admin/extension/shipping'));
		breadcrumb($shipping_extension->info('title'), site_url('admin/extension/shipping/edit', 'code=' . $code));

		//Load Contents
		$data['contents'] = file_get_contents($file);

		//Template Data
		$data['page_title'] = $shipping_extension->info('title');
		$data['edit_file']  = $file;

		//Action Buttons
		$data['save']   = site_url('admin/extension/shipping/edit', 'code=' . $code);
		$data['cancel'] = site_url('admin/extension/shipping');

		//Render
		output($this->render('extension/edit', $data));
	}

	private function loadExtensionController($code)
	{
		if (!$this->extension_controller && !empty($code)) {
			$action = new Action('extension/shipping/' . $code);

			if ($action->isValid()) {
				$this->extension_controller = $action->getController();
			}
		}
	}

	private function validate()
	{
		if (!user_can('w',  'extension/shipping')) {
			$this->error['warning'] = _l("Warning: You do not have permission to modify shipping!");
		}

		if (method_exists($this->extension_controller, 'validate')) {
			if (!$this->extension_controller->validate()) {
				return false;
			}
		}

		return empty($this->error);
	}

	public function install()
	{
		if ($this->System_Extension_Model->install('shipping', $_GET['code'])) {
			$this->loadExtensionController($_GET['code']);

			$settings = array(
				'min_total'   => 0,
				'geo_zone_id' => 0,
			);

			//Save Default Settings From Extension
			if (method_exists($this->extension_controller, 'settings')) {
				$this->extension_controller->settings($settings);
			}

			$this->System_Extension_Model->updateExtension('shipping', $_GET['code'], array('settings' => $settings));

			message('success', _l("You have successfully installed %s!", $_GET['code']));
		} elseif ($this->System_Extension_Model->hasError()) {
			message('error', $this->System_Extension_Model->getError());
		}

		redirect('admin/extension/shipping');
	}

	public function uninstall()
	{
		if ($this->System_Extension_Model->uninstall('shipping', $_GET['code'])) {
			message('notify', _l("%s has been uninstalled.", $_GET['code']));
		} elseif ($this->System_Extension_Model->hasError()) {
			message('error', $this->System_Extension_Model->getError());
		}

		redirect('admin/extension/shipping');
	}
}
