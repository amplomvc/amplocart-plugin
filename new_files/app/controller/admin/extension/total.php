<?php
class App_Controller_Admin_Extension_Total extends Controller
{
	private $extension_controller;

	public function index()
	{
		if (!empty($_GET['code'])) {
			if (!$this->System_Extension_Total->has($_GET['code'])) {
				message('warning', _l("The extension %s does not exist!", $_GET['code']));

				redirect('admin/extension/total');
			}

			$this->getForm();
		} else {
			$this->getList();
		}
	}

	private function getList()
	{
		//Page Head
		set_page_info('title', _l("Order Totals"));

		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Order Totals"), site_url('admin/extension/total'));

		//The Table Columns
		$columns = array();

		$columns['title'] = array(
			'type'         => 'text',
			'display_name' => _l("Label"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['code'] = array(
			'type'         => 'text',
			'display_name' => _l("Extension Code"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['sort_order'] = array(
			'type'         => 'int',
			'display_name' => _l("Sort Order"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['status'] = array(
			'type'         => 'select',
			'display_name' => _l("Status"),
			'filter'       => true,
			'build_data'   => array(
				0 => _l("Disabled"),
				1 => _l("Enabled"),
			),
			'sortable'     => true,
		);

		//The Sort & Filter Data
		$sort   = $this->sort->getQueryDefaults('sort_order', 'ASC');
		$filter = !empty($_GET['filter']) ? $_GET['filter'] : array();

		//Table Row Data
		$extension_total = $this->System_Extension_Model->getTotal('total', $filter);
		$extensions      = $this->System_Extension_Model->getExtensions('total', $sort + $filter);

		foreach ($extensions as &$extension) {
			if ($extension['installed']) {
				$actions = array(
					'edit'      => array(
						'text' => _l("Edit"),
						'href' => site_url('admin/extension/total/edit', 'code=' . $extension['code'])
					),
					'settings'  => array(
						'text' => _l("Settings"),
						'href' => site_url('admin/extension/total', 'code=' . $extension['code'])
					),
					'uninstall' => array(
						'text' => _l("Uninstall"),
						'href' => site_url('admin/extension/total/uninstall', 'code=' . $extension['code']),
					),
				);
			} else {
				$actions = array(
					'install' => array(
						'text' => _l("Install"),
						'href' => site_url('admin/extension/total/install', 'code=' . $extension['code'])
					),
				);
			}

			$extension['actions'] = $actions;
		}
		unset($extension);

		//Build The Table

		$this->table->init();
		$this->table->setTemplate('table/list_view');
		$this->table->setColumns($columns);
		$this->table->setRows($extensions);
		$this->table->mapAttribute('filter_value', $filter);

		$data['list_view'] = $this->table->render();

		//Action Buttons
		$data['insert'] = site_url('admin/extension/add');

		//Render limit Menu
		$data['limits'] = $this->sort->renderLimits();

		//Pagination
		$this->pagination->init();
		$this->pagination->total = $extension_total;

		$data['pagination'] = $this->pagination->render();

		//Render
		output($this->render('extension/total_list', $data));
	}

	private function getForm()
	{
		$code = $_GET['code'];

		$this->loadExtensionController($code);

		if (IS_POST && $this->validate()) {
			//If Extension needs to customize the way data is stored
			if (method_exists($this->extension_controller, 'saveSettings')) {
				$this->extension_controller->saveSettings($_POST['settings']);
			}

			$this->System_Extension_Model->updateExtension('total', $code, $_POST);

			message('success', _l("Successfully saved settings for %s!", $code));

			redirect('admin/extension/total');
		}

		$total_extension = $this->System_Extension_Total->get($code);

		//Page Head
		set_page_info('title', $total_extension->info('title'));

		//Page Title
		$data['page_title'] = $total_extension->info('title');

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Order Totals Extensions"), site_url('admin/extension/total'));
		breadcrumb($total_extension->info('title'), site_url('admin/extension/total', 'code=' . $code));

		//Entry Data
		$extension = $_POST;

		if (!IS_POST) {
			$extension = $total_extension->info();
		}

		$defaults = array(
			'title'      => '',
			'settings'   => array(),
			'sort_order' => 0,
			'status'     => 1,
		);

		$data += $extension + $defaults;

		//Get additional extension settings and profile data (this is the plugin part)
		if (method_exists($this->extension_controller, 'settings')) {
			$this->extension_controller->settings($data['settings']);
			$data['extend_settings'] = $this->extension_controller->output;
		}

		$data['data_statuses'] = array(
			0 => _l("Disabled"),
			1 => _l("Enabled"),
		);

		//Action Buttons
		$data['save']   = site_url('admin/extension/total', 'code=' . $code);
		$data['cancel'] = site_url('admin/extension/total');

		//Render
		output($this->render('extension/total', $data));
	}

	public function edit()
	{
		$code = !empty($_GET['code']) ? $_GET['code'] : '';

		//Verify File
		$file = DIR_SYSTEM . "extension/total/" . $code . '.php';

		if (!is_file($file)) {
			message('warning', _l("The extension file %s does not exist!", $file));
			redirect('admin/extension/total');
		}

		//Handle POST
		if (IS_POST) {
			if (file_put_contents($file, html_entity_decode($_POST['contents']))) {
				message('success', _l("Saved the extension file %s!", $file));
			} else {
				message('warning', _l("There was a problem while saving the file %s!", $file));
			}

			redirect('admin/extension/total');
		}

		//Load extension
		$extension = $this->System_Extension_Total->get($code)->info();

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Order Totals Extensions"), site_url('admin/extension/total'));
		breadcrumb($extension['title'], site_url('admin/extension/total/edit', 'code=' . $code));

		//Load Contents
		$data['contents'] = file_get_contents($file);

		//Template Data
		$data['page_title'] = $extension['title'];
		$data['edit_file']  = $file;

		//Action Buttons
		$data['save']   = site_url('admin/extension/total/edit', 'code=' . $code);
		$data['cancel'] = site_url('admin/extension/total');

		//Render
		output($this->render('extension/edit', $data));
	}

	private function loadExtensionController($code)
	{
		if (!$this->extension_controller && !empty($code)) {
			$action = new Action('extension/total/' . $code);

			if ($action->isValid()) {
				$this->extension_controller = $action->getController();
			}
		}
	}

	private function validate()
	{
		if (!user_can('w',  'extension/total')) {
			$this->error['warning'] = _l("You do not have permission to modify the Totals system extension!");
		}

		if (method_exists($this->extension_controller, 'validate')) {
			if (!$this->extension_controller->validate()) {
				return false;
			}
		}

		return empty($this->error);
	}

	public function install()
	{
		if ($this->System_Extension_Model->install('total', $_GET['code'])) {
			$this->loadExtensionController($_GET['code']);

			$settings = array();

			//Save Default Settings
			if (method_exists($this->extension_controller, 'settings')) {
				$this->extension_controller->settings($settings);
			}

			$this->System_Extension_Model->updateExtension('total', $_GET['code'], array('settings' => $settings));

			message('success', _l("Successfully installed the %s extension for Order Totals", $_GET['code']));
		} elseif ($this->System_Extension_Model->hasError()) {
			message('warning', $this->System_Extension_Model->getError());
		}

		redirect('admin/extension/total');
	}

	public function uninstall()
	{
		if ($this->System_Extension_Model->uninstall('total', $_GET['code'])) {
			message('notify', _l("Uninstalled the %s extension for Order Totals.", $_GET['code']));
		}


		redirect('admin/extension/total');
	}
}
