<?php

class App_Controller_Admin_Sale_Order extends Controller
{
	public function index()
	{
		//Page Head
		set_page_info('title', _l("Orders"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Orders"), site_url('admin/sale/order'));

		$data['listing'] = $this->listing();

		output($this->render('order/list'));
	}

	public function listing()
	{
		$sort    = (array)_request('sort', array('order_id' => 'DESC'));
		$filter  = (array)_request('filter');
		$options = array(
			'index'   => 'order_id',
			'page'    => _get('page', 1),
			'limit'   => _get('limit', option('admin_list_limit', 20)),
			'columns' => $this->Model_Sale_Order->getColumns((array)_request('columns')),
		);

		list($orders, $order_total) = $this->Model_Sale_Order->getRecords($sort, $filter, $options, true);

		foreach ($orders as $order_id => &$order) {
			$order['actions'] = array(
				'view' => array(
					'text' => _l("View"),
					'href' => site_url('admin/sale/order/info', 'order_id=' . $order_id)
				),
			);

			if ($this->order->isEditable($order)) {
				$action['edit'] = array(
					'text' => _l("Edit"),
					'href' => site_url('admin/sale/order/save', 'order_id=' . $order_id)
				);
			}

			$customer = $this->customer->getCustomer($order['customer_id']);

			if ($customer) {
				$order['customer'] = $customer['first_name'] . ' ' . $customer['last_name'];
			} elseif ($order['customer_id']) {
				$order['customer'] = "Unknown Customer ID ($order[customer_id])";
			} else {
				$order['customer'] = 'Guest';
			}

			$order['total']         = format('currency', $order['total'], $order['currency_code'], $order['currency_value']);
			$order['date_added']    = format('date', $order['date_added'], 'short');
			$order['date_modified'] = format('date', $order['date_modified'], 'short');
		}
		unset($order);

		$listing = array(
			'extra_cols'     => $this->Model_Order->getColumns(false),
			'records'        => $orders,
			'sort'           => $sort,
			'filter_value'   => $filter,
			'pagination'     => true,
			'total_listings' => $order_total,
			'listing_path'   => 'admin/order/listing',
			'save_path'      => 'admin/order/save',
		);

		$output = block('widget/listing', null, $listing + $options);

		//Response
		if ($this->is_ajax) {
			output($output);
		}

		return $output;
	}

	public function save()
	{
		if ($order_id = $this->Model_Order->save(_request('order_id'), $_POST)) {
			message('success', _l("The Order has been saved!"));
			message('data', array('order_id' => $order_id));
		} else {
			message('error', $this->Model_Order->fetchError());
		}

		if ($this->is_ajax) {
			output_message();
		} elseif ($this->message->has('error')) {
			post_redirect('admin/order/form');
		} else {
			redirect('admin/order');
		}
	}

	public function delete()
	{
		set_page_info('title', _l("Orders"));

		if (!empty($_GTE['order_id']) && $this->validateDelete()) {
			$this->Model_Sale_Order->deleteOrder($_GET['order_id']);

			if (!$this->message->has('error', 'warning')) {
				message('success', _l("Success: You have modified orders!"));

				redirect('admin/sale/order');
			}
		}

		$this->index();
	}

	public function form()
	{
		//Page Head
		set_page_info('title', _l("Orders"));

		//Insert or Update
		$order_id = !empty($_GET['order_id']) ? (int)$_GET['order_id'] : 0;

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Orders"), site_url('admin/sale/order'));
		breadcrumb($order_id ? _l("Edit") : _l("Add"), site_url('admin/sale/order/save', 'order_id=' . $order_id));

		//Load Information
		$order = $_POST;

		if ($order_id && !IS_POST) {
			$order = $this->order->get($order_id);

			if ($order) {
				$order['customer'] = $this->Model_Customer->getRecord($order['customer_id']);
				//TODO: Keep this? Need further implementation...
				$order['customer_addresses'] = $this->Model_Customer->getAddresses($order['customer_id']);
				$order['order_products']     = $this->Model_Sale_Order->getOrderProducts($order_id);
				$order['order_totals']       = $this->Model_Sale_Order->getOrderTotals($order_id);
			}
		}

		//Add Info / defaults to Template
		$defaults = array(
			'store_id'           => '',
			'customer'           => array(),
			'customer_id'        => '',
			'customer_group_id'  => '',
			'first_name'         => '',
			'last_name'          => '',
			'email'              => '',
			'phone'              => '',
			'fax'                => '',
			'order_status_id'    => '',
			'comment'            => '',
			'customer_addresses' => array(),

			//TODO: how best to handle this?
			'shipping_address'   => array(),
			'payment_address'    => array(),

			'shipping_code'      => '',
			'payment_code'       => '',
			'order_products'     => array(),
			'order_totals'       => array(),
		);

		$order += $defaults;

		//Template Data
		foreach ($order['order_products'] as &$order_product) {
			$order_product['options']   = $this->Model_Sale_Order->getOrderOptions($order_id, $order_product['order_product_id']);
			$order_product['downloads'] = $this->Model_Sale_Order->getOrderDownloads($order_id, $order_product['order_product_id']);
		}
		unset($order_product);

		$order['data_order_statuses'] = Order::$statuses;
		$order['data_stores']         = $this->Model_Setting_Store->getStores();
		$order['data_countries']      = $this->Model_Localisation_Country->getCountries();
		$order['data_voucher_themes'] = $this->Model_Sale_VoucherTheme->getVoucherThemes();

		//Urls
		$order['store_url'] = URL_SITE;

		//Action Buttons
		$order['save']   = site_url('admin/sale/order/save', 'order_id=' . $order_id);
		$order['cancel'] = site_url('admin/sale/order');

		//Render
		output($this->render('sale/order_form', $order));
	}

	private function validateForm()
	{
		if (!user_can('w', 'sale/order')) {
			$this->error['warning'] = _l("Warning: You do not have permission to modify orders!");
		}

		if (!$this->validation->text($_POST['first_name'], 1, 32)) {
			$this->error['first_name'] = _l("First Name must be between 1 and 32 characters!");
		}

		if (!$this->validation->text($_POST['last_name'], 1, 32)) {
			$this->error['last_name'] = _l("Last Name must be between 1 and 32 characters!");
		}

		if (!$this->validation->email($_POST['email'])) {
			$this->error['email'] = _l("E-Mail Address does not appear to be valid!");
		}

		if (!$this->validation->phone($_POST['phone'])) {
			$this->error['phone'] = _l("Telephone must be between 3 and 32 characters!");
		}

		//Validate Payment Information
		if (!$this->validation->text($_POST['payment_first_name'], 1, 32)) {
			$this->error['payment_first_name'] = _l("First Name must be between 1 and 32 characters!");
		}

		if (!$this->validation->text($_POST['payment_last_name'], 1, 32)) {
			$this->error['payment_last_name'] = _l("Last Name must be between 1 and 32 characters!");
		}

		if (!$this->validation->text($_POST['payment_address'], 3, 128)) {
			$this->error['payment_address'] = _l("Address 1 must be between 3 and 128 characters!");
		}

		if (!$this->validation->text($_POST['payment_city'], 3, 128)) {
			$this->error['payment_city'] = _l("City must be between 3 and 128 characters!");
		}

		$country_info = $this->Model_Localisation_Country->getCountry($_POST['payment_country_id']);

		if (!$country_info) {
			$this->error['payment_country'] = _l("Please select a country!");
		} elseif ($country_info['postcode_required'] && (!$this->validation->text($_POST['payment_postcode'], 2, 10))) {
			$this->error['payment_postcode'] = _l("Postcode must be between 2 and 10 characters for this country!");
		}

		if (empty($_POST['payment_zone_id'])) {
			$this->error['payment_zone'] = _l("Please select a region / state!");
		}

		// Check if any products require shipping
		$shipping = false;

		if (isset($_POST['order_product'])) {
			foreach ($_POST['order_product'] as $order_product) {
				$product_info = $this->Model_Product->getProduct($order_product['product_id']);

				if ($product_info && $product_info['shipping']) {
					$shipping = true;
				}
			}
		}

		if ($shipping) {
			if ((strlen($_POST['shipping_first_name']) < 1) || (strlen($_POST['shipping_first_name']) > 32)) {
				$this->error['shipping_first_name'] = _l("First Name must be between 1 and 32 characters!");
			}

			if ((strlen($_POST['shipping_last_name']) < 1) || (strlen($_POST['shipping_last_name']) > 32)) {
				$this->error['shipping_last_name'] = _l("Last Name must be between 1 and 32 characters!");
			}

			if ((strlen($_POST['shipping_address']) < 3) || (strlen($_POST['shipping_address']) > 128)) {
				$this->error['shipping_address'] = _l("Address 1 must be between 3 and 128 characters!");
			}

			if ((strlen($_POST['shipping_city']) < 3) || (strlen($_POST['shipping_city']) > 128)) {
				$this->error['shipping_city'] = _l("City must be between 3 and 128 characters!");
			}

			$country_info = $this->Model_Localisation_Country->getCountry($_POST['shipping_country_id']);

			if ($country_info && $country_info['postcode_required'] && (strlen($_POST['shipping_postcode']) < 2) || (strlen($_POST['shipping_postcode']) > 10)) {
				$this->error['shipping_postcode'] = _l("Postcode must be between 2 and 10 characters for this country!");
			}

			if ($_POST['shipping_country_id'] == '') {
				$this->error['shipping_country'] = _l("Please select a country!");
			}

			if ($_POST['shipping_zone_id'] == '') {
				$this->error['shipping_zone'] = _l("Please select a region / state!");
			}
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = _l("Warning: Please check the form carefully for errors!");
		}

		return empty($this->error);
	}

	private function validateDelete()
	{
		if (!user_can('w', 'sale/order')) {
			$this->error['warning'] = _l("Warning: You do not have permission to modify orders!");
		}

		return empty($this->error);
	}

	public function info()
	{
		$order_id = !empty($_GET['order_id']) ? (int)$_GET['order_id'] : 0;

		$order = $this->Model_Sale_Order->getOrder($order_id);

		//Order Not Found
		if (!$order) {
			message("warning", _l("The Order was not found in the system"));
			redirect('admin/sale/order');
		}

		//Page Head
		set_page_info('title', _l("Orders"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Orders"), site_url('admin/sale/order'));
		breadcrumb($order['invoice_id'], site_url('admin/sale/order/info', 'order_id=' . $order_id));

		$transaction = $this->transaction->get($order['transaction_id']);
		$shipping    = $this->shipping->get($order['shipping_id']);

		$order['payment_method']  = $this->System_Extension_Payment->get($transaction['payment_code'])->info();
		$order['payment_address'] = format('address', $this->order->getPaymentAddress($order_id));

		if ($shipping && $shipping['shipping_code']) {
			$order['shipping_method']  = $this->System_Extension_Shipping->get($shipping['shipping_code'])->info();
			$order['shipping_address'] = format('address', $this->order->getShippingAddress($order_id));
		}

		if ($order['customer_id']) {
			$order['url_customer'] = site_url('admin/sale/customer/update', 'customer_id=' . $order['customer_id']);
		}

		$order['comment'] = nl2br($order['comment']);
		$order['total']   = $this->currency->format($order['total'], $order['currency_code'], $order['currency_value']);

		$order['credit'] = max(0, -1 * $order['total']);
		//$order['credit_total'] = $this->Model_Sale_Customer->getTotalTransactionsByOrderId($order_id);
		$order['reward_total'] = $this->Model_Sale_Customer->getTotalCustomerRewardsByOrderId($order_id);

		$order['order_status'] = $this->order->getOrderStatus($order['order_status_id']);

		$order['date_added']    = $this->date->format($order['date_added'], 'datetime_long');
		$order['date_modified'] = $this->date->format($order['date_modified'], 'datetime_long');

		//Order Products
		$products = $this->Model_Sale_Order->getOrderProducts($order_id);

		foreach ($products as &$product) {
			$product += $this->Model_Product->getProduct($product['product_id']);

			$product['options'] = $this->Model_Sale_Order->getOrderProductOptions($order_id, $product['order_product_id']);

			$product['price_display'] = $this->currency->format($product['price'], $order['currency_code'], $order['currency_value']);
			$product['total_display'] = $this->currency->format($product['total'], $order['currency_code'], $order['currency_value']);
			$product['href']          = site_url('admin/product/product/update', 'product_id=' . $product['product_id']);
		}
		unset($product);

		$order['products'] = $products;

		$totals = $this->Model_Sale_Order->getOrderTotals($order_id);

		foreach ($totals as &$total) {
			$total['value_display'] = $this->currency->format($total['value'], $order['currency_code'], $order['currency_value']);
		}
		unset($total);

		$order['totals'] = $totals;

		$order['downloads'] = $this->Model_Sale_Order->getOrderDownloads($order_id);

		$order['data_order_statuses'] = Order::$statuses;

		// Fraud
		$order['fraud'] = $this->Model_Sale_Fraud->getFraud($order['order_id']);

		//Store
		$order['store'] = $this->Model_Setting_Store->getStore($order['store_id']);

		//History
		$order['history'] = $this->history();

		//Action Buttons
		$order['invoice'] = site_url('admin/sale/order/invoice', 'order_id=' . $order_id);
		$order['cancel']  = site_url('admin/sale/order');

		//Render
		output($this->render('sale/order_info', $order));
	}

	public function createInvoiceNo()
	{
		$order_id = !empty($_GET['order_id']) ? (int)$_GET['order_id'] : 0;

		if (!$order_id) {
			$json = array();

			if (!user_can('w', 'sale/order')) {
				$json['error'] = _l("Warning: You do not have permission to modify orders!");
			} else {
				$invoice_no = $this->Model_Sale_Order->generateInvoiceId($order_id);

				if ($invoice_no) {
					$json['invoice_no'] = $invoice_no;
				} else {
					$json['error'] = _l("Warning: Could not complete this action!");
				}
			}

			output(json_encode($json));
		}
	}

	public function addCredit()
	{
		$json = array();

		if (!user_can('w', 'sale/order')) {
			$json['error'] = _l("Warning: You do not have permission to modify orders!");
		} elseif (isset($_GET['order_id'])) {
			$order_info = $this->Model_Sale_Order->getOrder($_GET['order_id']);

			if ($order_info && $order_info['customer_id']) {
				$credit_total = $this->Model_Sale_Customer->getTotalTransactionsByOrderId($_GET['order_id']);

				if (!$credit_total) {
					$this->Model_Sale_Customer->addTransaction($order_info['customer_id'], _l("Order ID:") . ' #' . $_GET['order_id'], $order_info['total'], $_GET['order_id']);

					$json['success'] = _l("Success: Account credit added!");
				} else {
					$json['error'] = _l("Warning: Could not complete this action!");
				}
			}
		}

		output(json_encode($json));
	}

	public function removeCredit()
	{
		$json = array();

		if (!user_can('w', 'sale/order')) {
			$json['error'] = _l("Warning: You do not have permission to modify orders!");
		} elseif (isset($_GET['order_id'])) {
			$order_info = $this->Model_Sale_Order->getOrder($_GET['order_id']);

			if ($order_info && $order_info['customer_id']) {
				$this->Model_Sale_Customer->deleteTransaction($_GET['order_id']);

				$json['success'] = _l("Success: Account credit removed!");
			} else {
				$json['error'] = _l("Warning: Could not complete this action!");
			}
		}

		output(json_encode($json));
	}

	public function addReward()
	{
		$json = array();

		if (!user_can('w', 'sale/order')) {
			$json['error'] = _l("Warning: You do not have permission to modify orders!");
		} elseif (isset($_GET['order_id'])) {
			$order_info = $this->Model_Sale_Order->getOrder($_GET['order_id']);

			if ($order_info && $order_info['customer_id']) {
				$reward_total = $this->Model_Sale_Customer->getTotalCustomerRewardsByOrderId($_GET['order_id']);

				if (!$reward_total) {
					$this->Model_Sale_Customer->addReward($order_info['customer_id'], _l("Order ID:") . ' #' . $_GET['order_id'], $order_info['reward'], $_GET['order_id']);

					$json['success'] = _l("Success: Reward points added!");
				} else {
					$json['error'] = _l("Warning: Could not complete this action!");
				}
			} else {
				$json['error'] = _l("Warning: Could not complete this action!");
			}
		}

		output(json_encode($json));
	}

	public function removeReward()
	{
		$json = array();

		if (!user_can('w', 'sale/order')) {
			$json['error'] = _l("Warning: You do not have permission to modify orders!");
		} elseif (isset($_GET['order_id'])) {
			$order_info = $this->Model_Sale_Order->getOrder($_GET['order_id']);

			if ($order_info && $order_info['customer_id']) {
				$this->Model_Sale_Customer->deleteReward($_GET['order_id']);

				$json['success'] = _l("Success: Reward points removed!");
			} else {
				$json['error'] = _l("Warning: Could not complete this action!");
			}
		}

		output(json_encode($json));
	}

	public function history()
	{
		if (empty($_GET['order_id'])) {
			return;
		}

		$order_id = (int)$_GET['order_id'];

		if (IS_POST) {
			if (!user_can('w', 'sale/order')) {
				message('warning', _l("Warning: You do not have permission to modify orders!"));
			} else {
				$result = $this->order->updateOrder($order_id, $_POST['order_status_id'], $_POST['comment'], !empty($_POST['notify']) ? 1 : 0);

				if ($result) {
					message('success', _l("Success: You have modified orders!"));
				} else {
					message('error', $this->order->getError());
				}
			}
		}

		$filter = array(
			'order_ids' => array($order_id),
		);

		$histories = $this->Model_Sale_Order->getOrderHistories($filter);

		foreach ($histories as &$history) {
			$history['notify']     = $history['notify'] ? _l("Yes") : _l("No");
			$history['comment']    = nl2br($history['comment']);
			$history['date_added'] = $this->date->format($history['date_added'], 'datetime_long');
			$status                = $this->order->getOrderStatus($history['order_status_id']);
			$history['status']     = $status['title'];
		}
		unset($history);

		$data['histories'] = $histories;
	}

	public function download()
	{
		if (isset($_GET['order_option_id'])) {
			$order_option_id = $_GET['order_option_id'];
		} else {
			$order_option_id = 0;
		}

		$option_info = $this->Model_Sale_Order->getOrderOption($_GET['order_id'], $order_option_id);

		if ($option_info && $option_info['type'] == 'file') {
			$file = DIR_DOWNLOAD . $option_info['value'];
			$mask = basename(substr($option_info['value'], 0, strrpos($option_info['value'], '.')));

			if (!headers_sent()) {
				if (file_exists($file)) {
					$this->export->downloadFile($file, $mask);
					exit;
				} else {
					exit('Error: Could not find file ' . $file . '!');
				}
			} else {
				exit('Error: Headers already sent out!');
			}
		} else {
			set_page_info('title', _l("Orders"));

			breadcrumb(_l("Home"), site_url('admin'));
			breadcrumb(_l("Orders"), site_url('admin/error/not_found'));

			output($this->render('error/not_found', $data));
		}
	}

	public function upload()
	{
		$json = array();

		if (IS_POST) {
			if (!empty($_FILES['file']['name'])) {
				$filename = html_entity_decode($_FILES['file']['name'], ENT_QUOTES, 'UTF-8');

				if ((strlen($filename) < 3) || (strlen($filename) > 128)) {
					$json['error'] = _l("Filename must be between 3 and 128 characters!");
				}

				$allowed = array();

				$filetypes = explode(',', option('config_upload_allowed'));

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {
					$json['error'] = _l("Invalid file type!");
				}

				if (!$this->validation->fileUpload($_FILES['file'])) {
					$json['error'] = $this->validation->getError();
				}

			} else {
				$json['error'] = _l("Error uploading file to server!");
			}

			if (!isset($json['error'])) {
				if (is_uploaded_file($_FILES['file']['tmp_name']) && file_exists($_FILES['file']['tmp_name'])) {
					$file = basename($filename) . '.' . md5(rand());

					$json['file'] = $file;

					move_uploaded_file($_FILES['file']['tmp_name'], DIR_DOWNLOAD . $file);
				}

				$json['success'] = _l("Your file was successfully uploaded!");
			}
		}

		output(json_encode($json));
	}

	public function invoice()
	{
		$data['title'] = _l("Orders");

		$data['base'] = URL_SITE;

		$data['language'] = $this->language->info('code');

		$data['orders'] = array();

		$orders = array();

		if (isset($_GET['selected'])) {
			$orders = $_GET['selected'];
		} elseif (isset($_GET['order_id'])) {
			$orders[] = $_GET['order_id'];
		}

		foreach ($orders as $order_id) {
			$order_info = $this->Model_Sale_Order->getOrder($order_id);

			if ($order_info) {
				$store_info = $this->config->loadGroup('config', $order_info['store_id']);

				if ($store_info) {
					$store_address = $store_info['site_address'];
					$store_email   = $store_info['site_email'];
					$store_phone   = $store_info['site_phone'];
					$store_fax     = $store_info['config_fax'];
				} else {
					$store_address = option('site_address');
					$store_email   = option('site_email');
					$store_phone   = option('site_phone');
					$store_fax     = option('config_fax');
				}

				if ($order_info['invoice_no']) {
					$invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
				} else {
					$invoice_no = '';
				}

				if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				} else {
					$format = '{first_name} {last_name}' . "\n" . '{company}' . "\n" . '{address}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{first_name}',
					'{last_name}',
					'{company}',
					'{address}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'first_name' => $order_info['shipping_first_name'],
					'last_name'  => $order_info['shipping_last_name'],
					'company'    => $order_info['shipping_company'],
					'address'    => $order_info['shipping_address'],
					'address_2'  => $order_info['shipping_address_2'],
					'city'       => $order_info['shipping_city'],
					'postcode'   => $order_info['shipping_postcode'],
					'zone'       => $order_info['shipping_zone'],
					'zone_code'  => $order_info['shipping_zone_code'],
					'country'    => $order_info['shipping_country']
				);

				$shipping_address = str_replace(array(
					"\r\n",
					"\r",
					"\n"
				), '<br />', preg_replace(array(
					"/\\s\\s+/",
					"/\r\r+/",
					"/\n\n+/"
				), '<br />', trim(str_replace($find, $replace, $format))));

				if ($order_info['payment_address_format']) {
					$format = $order_info['payment_address_format'];
				} else {
					$format = '{first_name} {last_name}' . "\n" . '{company}' . "\n" . '{address}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{first_name}',
					'{last_name}',
					'{company}',
					'{address}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'first_name' => $order_info['payment_first_name'],
					'last_name'  => $order_info['payment_last_name'],
					'company'    => $order_info['payment_company'],
					'address'    => $order_info['payment_address'],
					'address_2'  => $order_info['payment_address_2'],
					'city'       => $order_info['payment_city'],
					'postcode'   => $order_info['payment_postcode'],
					'zone'       => $order_info['payment_zone'],
					'zone_code'  => $order_info['payment_zone_code'],
					'country'    => $order_info['payment_country']
				);

				$payment_address = str_replace(array(
					"\r\n",
					"\r",
					"\n"
				), '<br />', preg_replace(array(
					"/\\s\\s+/",
					"/\r\r+/",
					"/\n\n+/"
				), '<br />', trim(str_replace($find, $replace, $format))));

				$product_data = array();

				$products = $this->Model_Sale_Order->getOrderProducts($order_id);

				foreach ($products as $product) {
					$option_data = array();

					$options = $this->Model_Sale_Order->getOrderOptions($order_id, $product['order_product_id']);

					foreach ($options as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$value = substr($option['value'], 0, strrpos($option['value'], '.'));
						}

						$option_data[] = array(
							'name'  => $option['name'],
							'value' => $value
						);
					}

					$product_data[] = array(
						'name'     => $product['name'],
						'model'    => $product['model'],
						'option'   => $option_data,
						'quantity' => $product['quantity'],
						'price'    => $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value']),
						'total'    => $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value'])
					);
				}

				$total_data = $this->Model_Sale_Order->getOrderTotals($order_id);

				$data['orders'][] = array(
					'order_id'         => $order_id,
					'invoice_no'       => $invoice_no,
					'date_added'       => date('short', strtotime($order_info['date_added'])),
					'store_name'       => $order_info['store_name'],
					'store_url'        => rtrim($order_info['store_url'], '/'),
					'store_address'    => nl2br($store_address),
					'store_email'      => $store_email,
					'store_phone'      => $store_phone,
					'store_fax'        => $store_fax,
					'email'            => $order_info['email'],
					'phone'            => $order_info['phone'],
					'shipping_address' => $shipping_address,
					'payment_address'  => $payment_address,
					'payment_method'   => $order_info['payment_method'],
					'shipping_method'  => $order_info['shipping_method'],
					'product'          => $product_data,
					'total'            => $total_data,
					'comment'          => nl2br($order_info['comment'])
				);
			}
		}


		output($this->render('sale/order_invoice', $data));
	}
}
