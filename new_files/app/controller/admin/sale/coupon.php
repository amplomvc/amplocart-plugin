<?php

class App_Controller_Admin_Sale_Coupon extends Controller
{
	static $allow = array(
		'modify' => array(
			'form',
			'save',
			'save_field',
			'delete',
		),
	);

	public function index()
	{
		//Page Head
		set_page_info('title', _l("Coupon"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Coupon"), site_url('admin/sale/coupon'));

		//Actions
		$data['insert'] = site_url('admin/sale/coupon/form');

		//Response
		output($this->render('sale/coupon/list', $data));
	}

	public function listing()
	{
		$columns = $this->Model_Sale_Coupon->getColumns(_get('columns'));

		//Get Sorted / Filtered Data
		$sort   = $this->sort->getQueryDefaults('code', 'ASC');
		$filter = _get('filter', array());

		$coupon_total = $this->Model_Sale_Coupon->getTotalCoupons($filter);
		$coupons      = $this->Model_Sale_Coupon->getCoupons($sort + $filter);

		foreach ($coupons as &$coupon) {
			$coupon['actions'] = array(
				'edit'   => array(
					'text' => _l("Edit"),
					'href' => site_url('admin/sale/coupon/form', 'coupon_id=' . $coupon['coupon_id'])
				),
				'delete' => array(
					'text' => _l("Delete"),
					'href' => site_url('admin/sale/coupon/delete', 'coupon_id=' . $coupon['coupon_id'])
				)
			);
		}
		unset($coupon);

		$listing = array(
			'row_id'         => 'coupon_id',
			'extra_cols'     => $this->Model_Sale_Coupon->getColumns(false),
			'columns'        => $columns,
			'rows'           => $coupons,
			'filter_value'   => $filter,
			'pagination'     => true,
			'total_listings' => $coupon_total,
			'listing_path'   => 'admin/sale/coupon/listing',
			'save_path'      => 'admin/sale/coupon/save_field',
		);

		$output = block('widget/listing', null, $listing);

		//Response
		if ($this->is_ajax) {
			output($output);
		}

		return $output;
	}

	public function form()
	{
		//Page Head
		set_page_info('title', _l("Page"));

		//Insert or Update
		$coupon_id = _get('coupon_id');

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Coupons"), site_url('admin/sale/coupon'));
		breadcrumb($coupon_id ? _l("Edit") : _l("Add"), site_url('admin/sale/coupon/form', 'coupon_id=' . $coupon_id));

		//Load Information from POST or DB
		$coupon = $_POST;

		if ($coupon_id && !IS_POST) {
			$coupon = $this->Model_Sale_Coupon->getCoupon($coupon_id);

			$coupon['products']   = $this->Model_Sale_Coupon->getCouponProducts($coupon_id);
			$coupon['categories'] = $this->Model_Sale_Coupon->getCouponCategories($coupon_id);
			$coupon['customers']  = $this->Model_Sale_Coupon->getCouponCustomers($coupon_id);
		}

		//Set Values or Defaults
		$defaults = array(
			'name'             => '',
			'code'             => '',
			'auto_apply'       => 0,
			'type'             => 'P',
			'discount'         => '',
			'logged'           => 0,
			'shipping'         => 0,
			'shipping_geozone' => 0,
			'total'            => '',
			'date_start'       => '',
			'date_end'         => '',
			'uses_total'       => '',
			'uses_customer'    => '',
			'products'         => array(),
			'categories'       => array(),
			'customers'        => array(),
			'status'           => 1
		);

		$coupon += $defaults;

		//Template Data
		$coupon['data_geo_zones'] = $this->Model_Localisation_GeoZone->getGeoZones();

		$coupon['data_categories'] = $this->Model_Category->getCategoriesWithParents();

		$coupon['data_statuses'] = array(
			1 => _l("Active"),
			0 => _l("Inactive"),
		);

		$coupon['data_types'] = array(
			'P' => _l("Percent"),
			'F' => _l("Fixed Amount"),
		);

		$coupon['data_yes_no'] = array(
			1 => _l("Yes"),
			0 => _l("No"),
		);

		//Actions
		$coupon['save'] = site_url('admin/sale/coupon/save', 'coupon_id=' . $coupon_id);

		//Render
		output($this->render('sale/coupon/form', $coupon));
	}

	public function history()
	{
		//TODO: Impelment Coupon History!
		$coupon_id = _get('coupon_id');

		$page = _get('page');

		$data['histories'] = array();

		$results = $this->Model_Sale_Coupon->getCouponHistories($coupon_id, ($page - 1) * 10, 10);

		foreach ($results as $result) {
			$data['histories'][] = array(
				'order_id'   => $result['order_id'],
				'customer'   => $result['customer'],
				'amount'     => $result['amount'],
				'date_added' => $this->date->format($result['date_added'], 'short'),
			);
		}

		$history_total = $this->Model_Sale_Coupon->getTotalCouponHistories($coupon_id);

		$this->pagination->init();
		$this->pagination->total = $history_total;
		$data['pagination']      = $this->pagination->render();


		output($this->render('sale/coupon/history', $data));
	}

	public function save()
	{
		if ($this->Model_Sale_Coupon->save(_get('coupon_id'), $_POST)) {
			message('success', _l("The Coupon has been saved!"));
		} else {
			message('error', $this->Model_Sale_Coupon->getError());
		}

		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} elseif ($this->message->has('error')) {
			$this->form();
		} else {
			redirect('admin/sale/coupon');
		}
	}

	public function save_field()
	{
		$coupon_id = _request('coupon_id');

		if (!$coupon_id) {
			$coupon_id = _request('id');
		}

		if ($coupon_id) {
			if ($this->Model_Sale_Coupon->save($coupon_id, $_POST)) {
				message('success', _l("The Coupon was updated."));
			} else {
				message('error', $this->Model_Sale_Coupon->getError());
			}
		}

		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} else {
			redirect('admin/sale/coupon');
		}
	}

	public function delete()
	{
		if ($this->Model_Sale_Coupon->remove(_get('coupon_id'))) {
			message('notify', _l("Coupon was deleted!"));
		} else {
			message('error', $this->Model_Sale_Coupon->getError());
		}

		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} else {
			redirect('admin/sale/coupon');
		}
	}
}
