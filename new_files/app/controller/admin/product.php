<?php

/**
 * Name: Product
 */
class App_Controller_Admin_Product extends Controller
{
	public function index()
	{
		//Page Head
		set_page_info('title', _l("Products"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Products"), site_url('admin/product'));

		$classes = array(
				'' => array(
					'name'  => _l("Default"),
					'class' => '',
				)
			) + $this->Model_Product->getClasses();

		$categories = $this->Model_Category->getRecords(null, null, array('cache' => true));

		//Batch Actions
		$actions = array(
			'enable'             => array(
				'label' => _l("Enable")
			),
			'disable'            => array(
				'label' => _l("Disable"),
			),
			'copy'               => array(
				'label' => _l("Copy"),
			),
			'delete'             => array(
				'label' => _l("Delete"),
			),
			'product_class'      => array(
				'label'        => "Change Class",
				'type'         => 'select',
				'build_config' => array(
					'class',
					'name'
				),
				'build_data'   => $classes,
				'default'      => '',
			),

			'date_expires'       => array(
				'label'   => "Product Expiration Date",
				'type'    => 'datetime',
				'default' => $this->date->add('30 days'),
			),

			'shipping_policy_id' => array(
				'label'        => _l("Shipping Policy"),
				'type'         => 'select',
				'build_data'   => $this->cart->getShippingPolicies(),
				'build_config' => array(
					false,
					'title'
				),
				'default'      => option('config_default_shipping_policy'),
			),

			'return_policy_id'   => array(
				'label'        => _l("Return Policy"),
				'type'         => 'select',
				'build_data'   => $this->cart->getReturnPolicies(),
				'build_config' => array(
					false,
					'title'
				),
				'default'      => option('config_default_return_policy'),
			),

			'add_cat'            => array(
				'label'        => "Add Category",
				'type'         => 'select',
				'build_data'   => $categories,
				'build_config' => array(
					'category_id',
					'name'
				),
			),

			'remove_cat'         => array(
				'label'        => "Remove Category",
				'type'         => 'select',
				'build_data'   => $categories,
				'build_config' => array(
					'category_id',
					'name'
				),
			),
		);

		$data['batch_action'] = array(
			'actions' => $actions,
			'url'     => site_url('admin/product/batch-action'),
		);

		$data['product_classes'] = $classes;

		//Render
		output($this->render('product/list', $data));
	}

	public function listing()
	{
		$sort    = (array)_request('sort', array('name' => 'ASC'));
		$filter  = (array)_request('filter');
		$options = array(
			'index'   => 'product_id',
			'page'    => _get('page', 1),
			'limit'   => _get('limit', option('admin_list_limit', 20)),
			'columns' => $this->Model_Product->getColumns((array)_request('columns')),
		);

		list($products, $product_total) = $this->Model_Product->getRecords($sort, $filter, $options, true);

		foreach ($products as $product_id => &$product) {
			$product['actions'] = array(
				'edit' => array(
					'text' => _l("Edit"),
					'href' => site_url('admin/product/form', 'product_id=' . $product_id)
				),
				'copy' => array(
					'text' => _l("Copy"),
					'href' => site_url('admin/product/copy', 'product_id=' . $product_id)
				),
			);

			if (!$this->order->productInConfirmedOrder($product_id)) {
				$product['actions']['delete'] = array(
					'text' => _l("Delete"),
					'href' => site_url('admin/product/remove', 'product_id=' . $product_id),
				);
			}

			if (isset($product['image'])) {
				$product['image'] = image($product['image'], option('admin_list_image_width'), option('admin_list_image_height'));
			}

			if (isset($options['columns']['categories'])) {
				$product['categories'] = $this->Model_Product->getCategories($product_id);
			}

			if (isset($product['price'])) {
				$product['price'] = format('currency', $product['price']);

				$special = $this->Model_Product->getProductActiveSpecial($product_id);

				if ($special) {
					$product['price'] = "<span class =\"product_retail\">$product[price]</span><br /><span class=\"product_special\">" . format('currency', $special) . "</span>";
				}
			}

			if (isset($product['cost'])) {
				$product['cost'] = format('currency', $product['cost']);
			}

			//The # in front of the key signifies we want to output the raw string for the value when rendering the table
			if (isset($product['date_expires']) && $product['date_expires'] === DATETIME_ZERO) {
				$product['#date_expires'] = _l("No Expiration");
			}

			if (isset($product['subtract']) && !(int)$product['subtract']) {
				$product['quantity'] = _l("Unlimited");
			}
		}
		unset($product);

		$listing = array(
			'extra_cols'     => $this->Model_Product->getColumns(false),
			'records'        => $products,
			'sort'           => $sort,
			'filter_value'   => $filter,
			'pagination'     => true,
			'total_listings' => $product_total,
			'listing_path'   => 'admin/product/listing',
			'save_path'      => 'admin/product/save',
		);

		$output = block('widget/listing', null, $listing + $options);

		//Response
		if ($this->is_ajax) {
			output($output);
		}

		return $output;
	}

	public function form($product = array())
	{
		//Insert or Update
		$product_id = _get('product_id', 0);

		//Page Head
		set_page_info('title', _l("Product Form"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Products"), site_url('admin/product'));
		breadcrumb($product_id ? _l("Edit") : _l("Add"), site_url('admin/product/form', 'product_id=' . $product_id));

		//Load Information
		$product += $_POST;

		if (!IS_POST && $product_id) {
			$product += $this->Model_Product->getRecord($product_id);

			$product['product_attributes'] = isset($product['product_attributes']) ? $product['product_attributes'] : $this->Model_Product->getProductAttributes($product_id);
			$product['product_options']    = isset($product['product_options']) ? $product['product_options'] : $this->Model_Product->getProductOptions($product_id);
			$product['product_discounts']  = isset($product['product_discounts']) ? $product['product_discounts'] : $this->Model_Product->getProductDiscounts($product_id);
			$product['product_specials']   = isset($product['product_specials']) ? $product['product_specials'] : $this->Model_Product->getProductSpecials($product_id);
			$product['images']             = isset($product['images']) ? $product['images'] : $this->Model_Product->getImages($product_id);
			$product['product_categories'] = isset($product['product_categories']) ? $product['product_categories'] : $this->Model_Product->getCategories($product_id);
			$product['product_downloads']  = isset($product['product_downloads']) ? $product['product_downloads'] : $this->Model_Product->getProductDownloads($product_id);
			$product['product_templates']  = isset($product['product_templates']) ? $product['product_templates'] : $this->Model_Product->getProductTemplates($product_id);
			$product['product_related']    = isset($product['product_related']) ? $product['product_related'] : $this->Model_Product->getProductRelated($product_id);
			$product['product_tags']       = isset($product['product_tags']) ? $product['product_tags'] : $this->Model_Product->getProductTags($product_id);
		}

		//Set Values or Defaults
		$defaults = array(
			'product_id'         => $product_id,
			'product_class'      => '',
			'model'              => '',
			'sku'                => '',
			'upc'                => '',
			'location'           => '',
			'alias'              => '',
			'name'               => '',
			'description'        => '',
			'teaser'             => '',
			'information'        => '',
			'meta_keywords'      => '',
			'meta_description'   => '',
			'image'              => '',
			'manufacturer_id'    => 0,
			'shipping'           => 1,
			'price'              => '',
			'cost'               => '',
			'return_policy_id'   => option('config_default_return_policy'),
			'shipping_policy_id' => option('config_default_shipping_policy'),
			'tax_class_id'       => option('config_tax_default_id'),
			'date_available'     => $this->date->now(),
			'date_expires'       => '',
			'editable'           => 1,
			'quantity'           => 1,
			'minimum'            => 1,
			'subtract'           => 1,
			'sort_order'         => 1,
			'stock_status_id'    => option('config_stock_status_id'),
			'status'             => 1,
			'weight'             => '',
			'weight_unit'        => option('config_weight_unit'),
			'length'             => '',
			'width'              => '',
			'height'             => '',
			'length_unit'        => option('config_length_unit'),
			'product_stores'     => array(option('config_default_store_id')),
			'product_options'    => array(),
			'product_discounts'  => array(),
			'product_specials'   => array(),
			'product_attributes' => array(),
			'images'             => array(),
			'product_downloads'  => array(),
			'product_categories' => array(),
			'product_related'    => array(),
			'product_tags'       => array(),
			'points'             => '',
			'product_rewards'    => array(),
			'product_templates'  => array(),
		);

		$product += $defaults;

		//TODO: Make tags into a list of tag inputs (with js)
		if (is_string($product['product_tags'])) {
			$product['product_tags'] = explode(',', $product['product_tags']);
		}

		//Format Data
		foreach ($product['product_related'] as $key => &$related) {
			$related_product = $this->Model_Product->getProduct($related);

			if (!$related_product) {
				unset($product['product_related'][$key]);
			} else {
				$related = $related_product;
			}
		}
		unset($related);

		//Template Data
		$product['data_classes']           = array('' => _l("Default")) + $this->Model_Product->getClasses();
		$product['data_manufacturers']     = array('' => _l(" --- None --- ")) + $this->Model_Catalog_Manufacturer->getRecords(array('name' => 'ASC'));
		$product['data_tax_classes']       = array('' => _l(" --- None --- ")) + $this->Model_Localisation_TaxClass->getTaxClasses();
		$product['data_weight_units']      = $this->weight->getUnits();
		$product['data_length_units']      = $this->length->getUnits();
		$product['data_downloads']         = $this->Model_Catalog_Download->getRecords(null, null, array('cache' => true));
		$product['data_categories']        = $this->Model_Category->getCategoriesWithParents();
		$product['data_layouts']           = array('' => '') + $this->Model_Layout->getRecords(null, null, array('cache' => true));
		$product['data_templates']         = $this->theme->getTemplatesFrom('product', false, '');
		$product['data_shipping_policies'] = $this->cart->getShippingPolicies();
		$product['data_return_policies']   = $this->cart->getReturnPolicies();

		$product['data_statuses'] = array(
			0 => _l("Disabled"),
			1 => _l("Enabled"),
		);

		$product['data_yes_no'] = array(
			1 => _l("Yes"),
			0 => _l("No"),
		);

		$product['help_email'] = _l("mailto:%s?subject=New Product Option Request", option('site_email'));

		//Translations
		$product['translations'] = $this->Model_Product->getProductTranslations($product_id);

		//Product Attribute Template Defaults
		$product['product_attributes']['__ac_template__'] = array(
			'attribute_id' => '',
			'name'         => '',
			'image'        => '',
			'text'         => '',
			'sort_order'   => 0,
		);

		//Product Options Unused Option Values
		foreach ($product['product_options'] as &$product_option) {
			if (!empty($product_option['product_option_values'])) {
				$filter = array(
					'!option_value_ids' => array_column($product_option['product_option_values'], 'option_value_id'),
				);
			} else {
				$filter = array();
			}

			$filter['option_id'] = $product_option['option_id'];

			$option_values = $this->Model_Catalog_OptionValue->getRecords(null, $filter);

			$product_option['unused_option_values'] = $option_values;

		}
		unset($product_option);

		//Product Options Template Defaults
		$product['product_options']['__ac_template__'] = array(
			'product_option_id'     => 0,
			'option_id'             => '',
			'name'                  => '',
			'display_name'          => '',
			'type'                  => '',
			'group_type'            => '',
			'required'              => 1,
			'sort_order'            => 0,
			'unused_option_values'  => array(
				'__ac_template__' => array(
					'option_value_id' => '',
					'value'           => '',
				)
			),
			'product_option_values' => array(
				'__ac_template__' => array(
					'product_option_value_id' => 0,
					'option_value_id'         => '',
					'default'                 => 0,
					'value'                   => '',
					'display_value'           => '',
					'info'                    => '',
					'image'                   => '',
					'quantity'                => 1,
					'subtract'                => 0,
					'cost'                    => 0,
					'price'                   => 0,
					'points'                  => 0,
					'weight'                  => 0,
					'sort_order'              => 0,
					'restrictions'            => array(
						'__ac_template__' => array(
							'product_option_value_id'  => 0,
							'restrict_option_value_id' => 0,
							'quantity'                 => 0,
						)
					),

				)
			),
		);

		//Product Discount Template Defaults
		$product['product_discounts']['__ac_template__'] = array(
			'customer_group_id' => option('config_customer_group_id'),
			'quantity'          => 0,
			'priority'          => 0,
			'price'             => 0,
			'date_start'        => $this->date->now(),
			'date_end'          => $this->date->add('30 days'),
		);

		//Product Special Template Defaults
		$product['product_specials']['__ac_template__'] = array(
			'customer_group_id' => option('config_customer_group_id'),
			'priority'          => 0,
			'price'             => 0,
			'date_start'        => $this->date->now(),
			'date_end'          => $this->date->add('30 days'),
		);

		//Product Additional Images Template Defaults
		$product['images']['__ac_template__'] = array(
			'image'      => '',
			'thumb'      => '',
			'sort_order' => 0,
		);

		//Product Related Template Defaults
		$product['product_related']['__ac_template__'] = array(
			'product_id' => '',
			'name'       => '',
		);


		//Ajax Urls
		$product['url_generate_url']           = site_url('admin/product/generate_url');
		$product['url_generate_model']         = site_url('admin/product/generate_model');
		$product['url_autocomplete']           = site_url('admin/product/autocomplete');
		$product['url_attribute_autocomplete'] = site_url('admin/cart/attribute_group/autocomplete');
		$product['url_option_autocomplete']    = site_url('admin/cart/option/autocomplete');

		//Action Buttons
		$product['save']                = site_url('admin/product/update', 'product_id=' . $product_id);
		$product['cancel']              = site_url('admin/product/product');
		$product['add_shipping_policy'] = site_url('admin/setting/shipping_policy');
		$product['add_return_policy']   = site_url('admin/setting/return_policy');

		//The Template
		if (empty($product['template'])) {
			if ($product['product_class']) {
				$product['template'] = $this->theme->getFile('product/' . $product['product_class'] . '/form');
			}

			if (empty($product['template'])) {
				$product['template'] = 'product/form';
			}
		}

		//Render
		output($this->render($product['template'], $product));
	}

	public function save()
	{
		if ($product_id = $this->Model_Product->save(_request('product_id'), $_POST)) {
			message('success', _l("The product has been saved."));
			message('data', array('product_id' => $product_id));
		} else {
			message('error', $this->Model_Product->getError());
		}

		if ($this->is_ajax) {
			output_message();
		} elseif ($this->message->has('error')) {
			post_redirect('admin/product/form', _request('product_id'));
		} else {
			redirect('admin/product');
		}
	}

	public function remove()
	{
		if ($this->Model_Product->remove(_request('product_id'))) {
			message('success', _l("The product has been removed."));
		} else {
			message('error', $this->Model_Product->getError());
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('admin/product');
		}
	}

	public function copy()
	{
		if ($this->Model_Product->copy(_request('product_id'))) {
			message('success', _l("The product has been copied."));
		} else {
			message('error', $this->Model_Product->getError());
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('admin/product');
		}
	}

	public function batch_action()
	{
		$batch  = (array)_post('batch');
		$action = _request('action');
		$value  = _request('value');

		foreach ($batch as $product_id) {
			switch ($action) {
				case 'enable':
					$this->Model_Product->save($product_id, array('status' => 1));
					break;
				case 'disable':
					$this->Model_Product->save($product_id, array('status' => 0));
					break;

				case 'add_cat':
				case 'remove_cat':
					$categories = $this->Model_Product->getCategories($product_id);

					if ($action === 'add_cat') {
						$categories[] = $value;
					} else {
						$categories = array_diff($categories, array($value));
					}

					$this->Model_Product->save($product_id, array('product_categories' => $categories));
					break;

				case 'product_class_id':
				case 'date_expires':
				case 'shipping_policy_id':
				case 'return_policy_id':
					$this->Model_Product->save($product_id, array($action => $value));
					break;

				case 'copy':
					$this->Model_Product->copy($product_id);
					break;

				case 'delete':
					$this->Model_Product->remove($product_id);
					break;

				default:
					message('warning', _l("Unknown action %s", $action));
					break 2; //break the for loop
			}
		}

		if ($this->Model_Product->hasError()) {
			message('error', $this->Model_Product->getError());
		} else {
			message('success', _l("Products were updated successfully!"));
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('admin/product');
		}
	}

	public function select()
	{
		if (!isset($_POST['filter'])) {
			return;
		}

		$filter     = $_POST['filter'];
		$select     = isset($_POST['select']) ? $_POST['select'] : array();
		$field_keys = isset($_POST['fields']) ? $_POST['fields'] : array();

		$fields = array();
		foreach (explode(',', $field_keys) as $key) {
			$fields[$key] = 1;
		}

		$products = $this->Model_Product->getRecords(null, $filter);

		$html = '';

		foreach ($products as $product) {
			$data[$product['product_id']] = array_intersect_key($product, $fields);

			$selected = $product['product_id'] == $select ? 'selected="selected"' : '';

			$html .= "<option value=\"$product[product_id]\" " . $selected . ">$product[name]</option>";
		}

		$json = array(
			'option_data' => $data,
			'html'        => $html,
		);

		output(json_encode($json));
	}

	public function autocomplete()
	{
		//Sort
		$sort = $this->sort->getQueryDefaults('name', 'ASC', option('config_autocomplete_limit'));

		//Filter
		$filter = !empty($_GET['filter']) ? $_GET['filter'] : array();

		//Label and Value
		$label = !empty($_GET['label']) ? $_GET['label'] : 'name';
		$value = !empty($_GET['value']) ? $_GET['value'] : 'product_id';

		//Load Sorted / Filtered Data
		$products = $this->Model_Product->getRecords($sort, $filter);

		foreach ($products as &$product) {
			$product['label'] = $product[$label];
			$product['value'] = $product[$value];

			$product['name']  = html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8');
			$product['thumb'] = image($product['image'], 100, 100);

			$product_options = $this->Model_Product->getProductOptions($product['product_id']);

			foreach ($product_options as &$product_option) {
				foreach ($product_option['product_option_values'] as &$product_option_value) {
					$product_option_value['price'] = $this->currency->format($product_option_value['price']);
				}
			}
			unset($product_option);

		}
		unset($product);

		//JSON response
		output(json_encode($products));
	}

	public function generate_url()
	{
		$url = !empty($_POST['name']) ? $this->Model_UrlAlias->getUniqueAlias($_POST['name'], 'product/product', 'product_id=' . _request('product_id')) : '';

		output($url);
	}

	public function generate_model()
	{
		output($this->Model_Product->generateModel(_request('product_id', 0), _request('name')));
	}
}
