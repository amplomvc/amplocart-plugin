<?php

class App_Controller_Admin_Cart_Option extends Controller
{
	public function index()
	{
		//Page Head
		set_page_info('title', _l("Options"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Options"), site_url('admin/cart/option'));

		//Listing
		$data['listing'] = $this->listing();

		//Response
		output($this->render('cart/option/list', $data));
	}

	public function update()
	{
		if (IS_POST && $this->validateForm()) {
			//Insert
			if (empty($_GET['option_id'])) {
				$this->Model_Catalog_Option->addOption($_POST);
			} //Update
			else {
				$this->Model_Catalog_Option->editOption($_GET['option_id'], $_POST);
			}

			if (!$this->message->has('error', 'warning')) {
				message('success', _l("Success: You have modified options!"));

				redirect('admin/cart/option');
			}
		}

		$this->getForm();
	}

	public function delete()
	{
		set_page_info('title', _l("Options"));

		if (isset($_GET['option_id']) && $this->validateDelete()) {
			$this->Model_Catalog_Option->deleteOption($_GET['option_id']);

			if (!$this->message->has('error', 'warning')) {
				message('success', _l("Success: You have modified options!"));

				redirect('admin/cart/option');
			}
		}

		$this->getList();
	}

	public function batch_update()
	{
		if (!empty($_GET['selected']) && isset($_GET['action'])) {
			if ($_GET['action'] !== 'delete' || $this->validateDelete()) {
				foreach ($_GET['selected'] as $option_id) {
					switch ($_GET['action']) {
						case 'delete':
							$this->Model_Catalog_Option->deleteOption($option_id);
							break;
					}

					if ($this->error) {
						break;
					}
				}
			}

			if (!$this->error && !$this->message->has('error', 'warning')) {
				message('success', _l("Success: You have modified options!"));

				redirect('admin/cart/option', $this->url->getQueryExclude('action'));
			}
		}

		$this->getList();
	}

	public function listing()
	{
		$sort    = (array)_request('sort', array('name' => 'ASC'));
		$filter  = (array)_request('filter');
		$options = array(
			'index'   => 'option_id',
			'page'    => _get('page', 1),
			'limit'   => _get('limit', option('admin_list_limit', 20)),
			'columns' => $this->Model_Catalog_Option->getColumns((array)_request('columns')),
		);

		list($records, $total) = $this->Model_Catalog_Option->getRecords($sort, $filter, $options, true);

		foreach ($records as $option_id => &$option) {
			$option['actions'] = array(
				'edit'   => array(
					'text' => _l("Edit"),
					'href' => site_url('admin/cart/option/update', 'option_id=' . $option_id),
				),
				'delete' => array(
					'text' => _l("Delete"),
					'href' => site_url('admin/cart/option/delete', 'option_id=' . $option_id),
				),
			);
		}
		unset($option);

		$listing = array(
			'extra_cols'     => $this->Model_Catalog_Option->getColumns(false),
			'records'        => $records,
			'sort'           => $sort,
			'filter_value'   => $filter,
			'pagination'     => true,
			'total_listings' => $total,
			'listing_path'   => 'admin/user/listing',
			'save_path'      => 'admin/user/save',
		);

		$output = block('widget/listing', null, $listing + $options);

		//Response
		if ($this->is_ajax) {
			output($output);
		}

		return $output;
	}

	public function form()
	{
		//Page Head
		set_page_info('title', _l("Options"));

		//Insert or Update
		$option_id = isset($_GET['option_id']) ? (int)$_GET['option_id'] : 0;

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Options"), site_url('admin/cart/option'));

		if (!$option_id) {
			breadcrumb(_l("Add"), site_url('admin/cart/option/update'));
		} else {
			breadcrumb(_l("Edit"), site_url('admin/cart/option/update', 'option_id=' . $option_id));
		}

		//Action Buttons
		$data['save']   = site_url('admin/cart/option/update', 'option_id=' . $option_id);
		$data['cancel'] = site_url('admin/cart/option');

		//Load Information
		$option_info = array();

		if (IS_POST) {
			$option_info = $_POST;
		} elseif ($option_id) {
			$option_info = $this->Model_Catalog_Option->getOption($option_id);

			$option_values = $this->Model_Catalog_OptionValue->getRecords(null, array('option_id' => $option_id));

			$thumb_width  = option('admin_thumb_width');
			$thumb_height = option('admin_thumb_height');

			foreach ($option_values as &$option_value) {
				$option_value['thumb']        = image($option_value['image'], $thumb_width, $thumb_height);
				$option_value['translations'] = $this->Model_Catalog_Option->getOptionValueTranslations($option_value['option_value_id']);
			}
			unset($option_value);

			$option_info['option_values'] = $option_values;
		}

		//Load Values or Defaults
		$defaults = array(
			'name'          => '',
			'display_name'  => '',
			'type'          => '',
			'sort_order'    => '',
			'option_values' => array()
		);

		$data += $option_info + $defaults;

		//Template Data
		$data['data_option_types'] = array(
			'#optgroup1' => _l('Choose'),
			'select'     => _l('Select'),
			'radio'      => _l('Radio'),
			'checkbox'   => _l('Checkbox'),
			'image'      => _l('Image'),
			'#optgroup2' => _l('Input'),
			'text'       => _l('Text'),
			'textarea'   => _l('Textarea'),
			'#optgroup4' => _l('Date'),
			'date'       => _l('Date'),
			'datetime'   => _l('Date &amp; Time'),
			'time'       => _l('Time'),
		);

		//Product Options Template Defaults
		$data['option_values']['__ac_template__'] = array(
			'option_id'       => 0,
			'option_value_id' => '',
			'name'            => '',
			'value'           => '',
			'display_value'   => '',
			'info'            => '',
			'image'           => '',
			'thumb'           => '',
			'sort_order'      => 0,
		);

		//Render
		output($this->render('cart/option/form', $data));
	}

	private function validateForm()
	{
		if (!user_can('w', 'catalog/option')) {
			$this->error['warning'] = _l("Warning: You do not have permission to modify options!");
		}

		if (!$this->validation->text($_POST['name'], 3, 45)) {
			$this->error['name'] = _l("Option Name must be between 3 and 45 characters!");
		}

		if (!$this->validation->text($_POST['display_name'], 1, 128)) {
			$this->error['display_name'] = _l("Option Display Name must be between 1 and 128 characters!");
		}

		$multi_types = array('checkbox');

		$_POST['group_type'] = in_array($_POST['type'], $multi_types) ? 'multi' : 'single';

		if (!isset($_POST['option_value'])) {
			$this->error['warning'] = _l("Warning: Option Values required!");
		} else {
			foreach ($_POST['option_value'] as $option_value_id => $option_value) {
				if (!$this->validation->text($option_value['value'], 1, 128)) {
					$this->error["option_value[$option_value_id][value]"] = _l("Option Value must be between 1 and 128 characters!");
				}
			}
		}

		return empty($this->error);
	}

	private function validateDelete()
	{
		if (!user_can('w', 'catalog/option')) {
			$this->error['warning'] = _l("Warning: You do not have permission to modify options!");
		}

		return empty($this->error);
	}

	public function autocomplete()
	{
		//Sort / Filter
		$filter = (array)_request('filter');

		//Label and Value
		$label = _get('label', 'name');
		$value = _get('value', 'option_id');

		//Load Sorted / Filtered Data
		$options = $this->Model_Catalog_Option->getRecords(null, $filter);

		foreach ($options as &$option) {
			$option['label'] = isset($option[$label]) ? $option[$label] : $option['name'];
			$option['value'] = isset($option[$value]) ? $option[$value] : $option['option_id'];

			$option['name'] = html_entity_decode($option['name'], ENT_QUOTES, 'UTF-8');

			$option_values = $this->Model_Catalog_OptionValue->getRecords(null, array('option_id' => $option['option_id']));

			$image_width  = option('config_image_product_option_width');
			$image_height = option('config_image_product_option_height');

			foreach ($option_values as &$option_value) {
				$option_value['thumb'] = image($option_value['image'], $image_width, $image_height);
				$option_value['value'] = html_entity_decode($option_value['value'], ENT_QUOTES, 'UTF-8');
			}
			unset($option_value);

			$option['option_values'] = $option_values;

		}
		unset($option);

		$options[] = array(
			'label' => _l(" + Add Option"),
			'value' => false,
			'href'  => site_url('admin/cart/option'),
		);

		//JSON response
		output(json_encode($options));
	}
}
