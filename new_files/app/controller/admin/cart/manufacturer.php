<?php
class App_Controller_Admin_Catalog_Manufacturer extends Controller
{
	public function index()
	{
		$this->getList();
	}

	public function insert()
	{
		if (IS_POST && $this->validateForm()) {
			$this->Model_Catalog_Manufacturer->addManufacturer($_POST);

			if (!$this->message->has('error', 'warning')) {
				message('success', _l("Success: You have modified manufacturers!"));
			}

			redirect('admin/cart/manufacturer');
		}

		$this->getForm();
	}

	public function update()
	{
		if (IS_POST && $this->validateForm()) {
			$manufacturer_id = isset($_GET['manufacturer_id']) ? $_GET['manufacturer_id'] : 0;

			$this->Model_Catalog_Manufacturer->editManufacturer($_GET['manufacturer_id'], $_POST);

			if (!$this->message->has('error', 'warning')) {
				message('success', _l("Success: You have modified manufacturers!"));
				redirect('admin/cart/manufacturer');
			}
		}

		$this->getForm();
	}

	public function delete()
	{
		if (isset($_GET['manufacturer_id']) && $this->validateDelete()) {
			$this->Model_Catalog_Manufacturer->deleteManufacturer($_GET['manufacturer_id']);

			if (!$this->message->has('error', 'warning')) {
				message('success', _l("Success: You have modified manufacturers!"));
				redirect('admin/cart/manufacturer', $this->url->getQueryExclude('manufacturer_id'));
			}
		}

		$this->getList();
	}

	public function batch_update()
	{
		if (!empty($_GET['selected']) && isset($_GET['action'])) {
			if ($_GET['action'] !== 'delete' || $this->validateDelete()) {
				foreach ($_GET['selected'] as $manufacturer_id) {
					switch ($_GET['action']) {
						case 'enable':
							$this->Model_Catalog_Manufacturer->updateField($manufacturer_id, array('status' => 1));
							break;
						case 'disable':
							$this->Model_Catalog_Manufacturer->updateField($manufacturer_id, array('status' => 0));
							break;
						case 'delete':
							$this->Model_Catalog_Manufacturer->deleteManufacturer($manufacturer_id);
							break;
						case 'copy':
							$this->Model_Catalog_Manufacturer->copyManufacturer($manufacturer_id);
							break;
					}

					if ($this->error) {
						break;
					}
				}
			}

			if (!$this->error && !$this->message->has('error', 'warning')) {
				message('success', _l("Success: You have modified manufacturers!"));

				redirect('admin/cart/manufacturer', $this->url->getQueryExclude('action'));
			}
		}

		$this->getList();
	}

	private function getList()
	{
		//Page Head
		set_page_info('title', _l("Manufacturer"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Manufacturer"), site_url('admin/cart/manufacturer'));

		//The Table Columns
		$columns = array();

		$columns['thumb'] = array(
			'type'         => 'image',
			'display_name' => _l("Image"),
			'filter'       => false,
			'sortable'     => true,
			'sort_value'   => '__image_sort__image',
		);

		$columns['name'] = array(
			'type'         => 'text',
			'display_name' => _l("Manufacturer Name"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['vendor_id'] = array(
			'type'         => 'text',
			'display_name' => _l("Vendor ID"),
			'filter'       => true,
			'sortable'     => true,
		);


		$columns['date_active'] = array(
			'type'         => 'datetime',
			'display_name' => _l("Active On"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['date_expires'] = array(
			'type'         => 'datetime',
			'display_name' => _l("Expires On"),
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['stores'] = array(
			'type'         => 'multiselect',
			'display_name' => _l("Stores"),
			'filter'       => true,
			'build_config' => array(
				'store_id',
				'name'
			),
			'build_data'   => $this->Model_Setting_Store->getStores(),
			'sortable'     => false,
		);

		$columns['status'] = array(
			'type'         => 'select',
			'display_name' => _l("Status"),
			'filter'       => true,
			'build_data'   => array(
				0 => _l("Disabled"),
				1 => _l("Enabled"),
			),
			'sortable'     => true,
		);

		//Get Sorted / Filtered Data
		$sort   = $this->sort->getQueryDefaults('name', 'ASC');
		$filter = !empty($_GET['filter']) ? $_GET['filter'] : array();

		$manufacturer_total = $this->Model_Catalog_Manufacturer->getTotalManufacturers($filter);
		$manufacturers      = $this->Model_Catalog_Manufacturer->getManufacturers($sort + $filter);

		$url_query = $this->url->getQueryExclude('manufacturer_id');

		foreach ($manufacturers as &$manufacturer) {
			$manufacturer['actions'] = array(
				'edit'   => array(
					'text' => _l("Edit"),
					'href' => site_url('admin/cart/manufacturer/update', 'manufacturer_id=' . $manufacturer['manufacturer_id'])
				),
				'delete' => array(
					'text' => _l("Delete"),
					'href' => site_url('admin/cart/manufacturer/delete', 'manufacturer_id=' . $manufacturer['manufacturer_id'] . $url_query)
				)
			);

			if ($manufacturer['date_active'] === DATETIME_ZERO) {
				$manufacturer['date_active'] = _l("No Activation Date");
			} else {
				$manufacturer['date_active'] = $this->date->format($manufacturer['date_active'], 'datetime_format_long');
			}

			if ($manufacturer['date_expires'] === DATETIME_ZERO) {
				$manufacturer['date_expires'] = _l("No Expiration Date");
			} else {
				$manufacturer['date_expires'] = $this->date->format($manufacturer['date_active'], 'datetime_format_long');
			}

			$manufacturer['thumb'] = image($manufacturer['image'], option('admin_list_image_width'), option('admin_list_image_height'));

			$manufacturer['stores'] = $this->Model_Catalog_Manufacturer->getManufacturerStores($manufacturer['manufacturer_id']);
		}
		unset($manufacturer);

		//Build The Table
		$tt_data = array(
			'row_id' => 'manufacturer_id',
		);

		$this->table->init();
		$this->table->setTemplate('table/list_view');
		$this->table->setColumns($columns);
		$this->table->setRows($manufacturers);
		$this->table->setTemplateData($tt_data);
		$this->table->mapAttribute('filter_value', $filter);

		$data['list_view'] = $this->table->render();

		//Batch Actions
		$data['batch_actions'] = array(
			'enable'  => array(
				'label' => _l("Enable")
			),
			'disable' => array(
				'label' => _l("Disable"),
			),
			'copy'    => array(
				'label' => _l("Copy"),
			),
			'delete'  => array(
				'label' => _l("Delete"),
			),
		);

		$data['batch_update'] = 'catalog/manufacturer/batch_update';

		//Render Limit Menu
		$data['limits'] = $this->sort->renderLimits();

		//Action buttons
		$data['insert'] = site_url('admin/cart/manufacturer/insert');

		//Pagination
		$this->pagination->init();
		$this->pagination->total = $manufacturer_total;

		$data['pagination'] = $this->pagination->render();

		//Render
		output($this->render('catalog/manufacturer_list', $data));
	}

	private function getForm()
	{
		set_page_info('title', _l("Manufacturer"));

		$manufacturer_id = $data['manufacturer_id'] = isset($_GET['manufacturer_id']) ? (int)$_GET['manufacturer_id'] : null;

		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Manufacturer"), site_url('admin/cart/manufacturer'));

		if (!$manufacturer_id) {
			$data['action'] = site_url('admin/cart/manufacturer/insert');
		} else {
			$data['action'] = site_url('admin/cart/manufacturer/update', 'manufacturer_id=' . $manufacturer_id);
		}

		$data['cancel'] = site_url('admin/cart/manufacturer');

		if ($manufacturer_id && !IS_POST) {
			$manufacturer_info = $this->Model_Catalog_Manufacturer->getManufacturer($manufacturer_id);

			$manufacturer_info['stores'] = $this->Model_Catalog_Manufacturer->getManufacturerStores($manufacturer_id);
		}

		$defaults = array(
			'name'            => '',
			'alias'           => '',
			'image'           => '',
			'date_active'     => $this->date->now(),
			'date_expires'    => $this->date->add(null, '30 days'),
			'description'     => '',
			'teaser'          => '',
			'shipping_return' => '',
			'stores'          => array(1),
			'sort_order'      => 0,
			'status'          => 0,
		);


		foreach ($defaults as $key => $default) {
			if (isset($_POST[$key])) {
				$data[$key] = $_POST[$key];
			} elseif (isset($manufacturer_info[$key])) {
				$data[$key] = $manufacturer_info[$key];
			} else {
				$data[$key] = $default;
			}
		}

		$data['data_stores'] = $this->Model_Setting_Store->getStores();

		$data['data_statuses'] = array(
			0 => _l("Disabled"),
			1 => _l("Enabled"),
		);

		//Ajax Urls
		$data['url_generate_url'] = site_url('admin/cart/manufacturer/generate_url');
		$data['url_autocomplete'] = site_url('admin/cart/manufacturer/autocomplete');

		$translate_fields = array(
			'name',
			'description',
			'teaser',
			'shipping_return',
		);

		$data['translations'] = $this->translation->getTranslations('manufacturer', $manufacturer_id, $translate_fields);

		output($this->render('catalog/manufacturer_form', $data));
	}

	private function validateForm()
	{
		if (!user_can('w',  'catalog/manufacturer')) {
			$this->error['warning'] = _l("Warning: You do not have permission to modify manufacturers!");
		}

		if (!$this->validation->text($_POST['name'], 3, 128)) {
			$this->error['name'] = _l("Manufacturer Name must be between 3 and 64 characters!");
		}

		if (empty($_POST['alias'])) {
			$_POST['alias'] = slug($_POST['name']);
		}

		return empty($this->error);
	}

	private function validateDelete()
	{
		if (!user_can('w',  'catalog/manufacturer')) {
			$this->error['warning'] = _l("Warning: You do not have permission to modify manufacturers!");
		}

		$manufacturer_ids = array();

		if (isset($_GET['selected'])) {
			$manufacturer_ids = $_GET['selected'];
		}

		if (isset($_GET['manufacturer_id'])) {
			$manufacturer_ids[] = $_GET['manufacturer_id'];
		}

		foreach ($manufacturer_ids as $manufacturer_id) {
			$data = array(
				'manufacturer_id' => $manufacturer_id,
			);

			$product_count = $this->Model_Product->getTotalProducts($data);

			if ($product_count) {
				$this->error['manufacturer' . $manufacturer_id] = _l("Warning: This manufacturer cannot be deleted as it is currently assigned to %s products!", $product_count);
			}
		}

		return empty($this->error);
	}

	public function generate_url()
	{
		if (!empty($_POST['name'])) {
			$manufacturer_id = !empty($_POST['manufacturer_id']) ? (int)$_POST['manufacturer_id'] : 0;

			$url = $this->Model_UrlAlias->getUniqueAlias('catalog/manufacturer', 'manufacturer_id=' . $manufacturer_id, $_POST['name']);
		} else {
			$url = '';
		}

		output($url);
	}

	public function autocomplete()
	{
		//Sort
		$sort = $this->sort->getQueryDefaults('name', 'ASC', option('config_autocomplete_limit'));

		//Filter
		$filter = !empty($_GET['filter']) ? $_GET['filter'] : array();

		//Label and Value
		$label = !empty($_GET['label']) ? $_GET['label'] : 'name';
		$value = !empty($_GET['value']) ? $_GET['value'] : 'manufacturer_id';

		//Load Sorted / Filtered Data
		$manufacturers = $this->Model_Catalog_Manufacturer->getManufacturers($sort + $filter);

		foreach ($manufacturers as &$manufacturer) {
			$manufacturer['label'] = $manufacturer[$label];
			$manufacturer['value'] = $manufacturer[$value];
		}
		unset($manufacturer);

		//JSON response
		output(json_encode($manufacturers));
	}
}
