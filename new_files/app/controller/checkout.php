<?php

class App_Controller_Checkout extends Controller
{
	public function index()
	{
		//TODO: Need to implement a more dynamic cart system to incorporate other cart types (eg: subscriptions, user_custom_types, etc..)
		if (!$this->cart->canCheckout()) {
			message("warning", _l("You do not have any products in your cart. Please continue with your purchase via a different method provided from the cart."));
			redirect('cart');
		}

		if (!$this->cart->validate()) {
			redirect('cart');
		}

		$this->request->setRedirect(site_url('checkout'));

		//Page Head
		set_page_info('title', _l("Checkout"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Shopping Cart"), site_url('cart'));
		breadcrumb(_l("Checkout"), site_url('checkout'));

		//Statuses
		$data['is_guest']     = $this->session->get('guest_checkout');
		$data['has_shipping'] = $this->cart->hasShipping();

		//Shipping Address
		$data['shipping_address_id'] = 0;
		$data['shipping_addresses']  = $this->customer->getShippingAddresses();

		if ($this->cart->validateShippingAddress()) {
			$data['shipping_address_id'] = $this->cart->getShippingAddressId();
		} else {
			//Set the customer's default shipping address
			$address_id = $this->customer->getDefaultShippingAddressId();

			if ($this->cart->setShippingAddress($address_id)) {
				$data['shipping_address_id'] = $address_id;
			}
		}

		//Payment Address
		$data['payment_address_id'] = 0;
		$data['payment_addresses']  = $this->customer->getPaymentAddresses();

		if ($this->cart->validatePaymentAddress()) {
			$data['payment_address_id'] = $this->cart->getPaymentAddressId();
		} else {
			//Set the customer's default payment address
			$address_id = $this->customer->getDefaultPaymentAddressId();

			if ($this->cart->setPaymentAddress($address_id)) {
				$data['payment_address_id'] = $address_id;
			}
		}

		//Methods
		if ($this->cart->validateShippingMethod()) {
			$data['shipping_key'] = $this->cart->getShippingKey();
		} else {
			$data['shipping_key'] = '';
		}

		if ($this->cart->validatePaymentMethod()) {
			$data['payment_key'] = $this->cart->getPaymentKey();
		} else {
			$data['payment_key'] = '';
		}

		//Render
		output($this->render('checkout/checkout', $data));
	}

	public function guest()
	{
		$this->render('checkout/guest');
	}

	public function guest_checkout()
	{
		$this->session->set('guest_checkout', true);

		if (!$this->is_ajax) {
			redirect('checkout');
		}
	}

	public function cancel_guest_checkout()
	{
		$this->session->set('guest_checkout', false);

		if (!$this->is_ajax) {
			redirect('checkout');
		}
	}

	public function methods($data = array())
	{
		if (IS_POST) {
			if (!empty($_POST['payment_address_id'])) {
				$this->cart->setPaymentAddress($_POST['payment_address_id']);
			}

			if ($this->cart->hasShipping() && !empty($_POST['shipping_address_id'])) {
				$this->cart->setShippingAddress($_POST['shipping_address_id']);
			}
		}

		//Payment Methods
		$data['payment_methods'] = $this->cart->getPaymentMethods();
		$data['payment_code']    = $this->cart->getPaymentCode();
		$data['payment_key']     = $this->cart->getPaymentKey();

		//Shipping Methods
		$data['has_shipping'] = $this->cart->hasShipping();

		if ($data['has_shipping']) {
			$data['shipping_methods'] = $this->cart->getShippingMethods();
			$data['shipping_code']    = $this->cart->getShippingCode();
			$data['shipping_key']     = $this->cart->getShippingKey();
		}

		$output = $this->render('checkout/methods', $data, true);

		if ($this->is_ajax) {
			output($output);
		} else {
			return $output;
		}
	}

	public function add_order()
	{
		$payment_address_id  = _post('payment_address_id');
		$shipping_address_id = _post('shipping_address_id');
		$payment_code        = _post('payment_code');
		$payment_key         = _post('payment_key');

		//Handle Shipping
		if ($this->cart->hasShipping()) {
			$shipping_code = _post('shipping_code');
			$shipping_key  = _post('shipping_key');

			$this->cart->setShippingAddress($shipping_address_id);
			$this->cart->setShippingMethod($shipping_code, $shipping_key);
		}

		//Handle Payment
		if ($payment_code && $payment_key === 'new') {
			$payment_key = $this->System_Extension_Payment->get($payment_code)->addCard($_POST);

			if (!$payment_key) {
				message('error', $this->System_Extension_Payment->get($payment_code)->getError());
			}
		}

		if (!$payment_key) {
			message('error', _l("Your payment method could not be verified"));
		} else {
			$this->cart->setPaymentAddress($payment_address_id);
			$this->cart->setPaymentMethod($payment_code, $payment_key);
		}

		if ($this->cart->hasError()) {
			message('error', $this->cart->getError());
		}

		//Verify Order
		if (!$this->message->has('error')) {
			if (!$this->order->verify()) {
				message('error', $this->order->getError());
			}
		}

		if ($this->message->has('error')) {
			if (!$this->is_ajax) {
				redirect('checkout');
			}
		} else {
			message('success', _l("Please confirm your order."));
		}

		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} else {
			redirect('checkout/confirmation');
		}
	}

	public function confirmation()
	{
		//Shipping & Payment
		$data = array(
			'shipping_address_id' => $this->cart->getShippingAddressId(),
			'shipping_code'       => $this->cart->getShippingCode(),
			'shipping_key'        => $this->cart->getShippingKey(),

			'payment_address_id'  => $this->cart->getPaymentAddressId(),
			'payment_code'        => $this->cart->getPaymentCode(),
			'payment_key'         => $this->cart->getPaymentKey(),
		);

		$data['has_confirmation'] = is_callable($this->cart->getPaymentMethod(), 'confirmation');

		//Action
		$data['action'] = site_url('checkout/confirm');

		//Render
		output($this->render('checkout/confirmation', $data));
	}

	public function confirm()
	{
		$order_id = $this->order->add();

		if (!$order_id) {
			message('error', $this->order->getError());
		} else {

			if ($this->order->confirm($order_id)) {
				//Clear Cart
				$this->cart->clear();

				message('success', _l("Your order has been processed successfully!"));
			} else {
				message('error', $this->order->getError());
			}
		}

		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} else {
			if ($this->message->has('error')) {
				redirect('checkout');
			}

			redirect('checkout/success');
		}
	}

	public function success()
	{
		//Page Head
		set_page_info('title', _l("Your Order Has Been Processed!"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Cart"), site_url('cart'));
		breadcrumb(_l("Checkout"), site_url('checkout'));
		breadcrumb(_l("Success"), site_url('checkout/success'));

		//Template Data
		$data['page_title'] = _l("Success");

		//Action Buttons
		$data['continue'] = site_url();

		//Render
		output($this->render('checkout/success', $data));
	}
}
